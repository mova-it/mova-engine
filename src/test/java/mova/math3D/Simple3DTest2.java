package mova.math3D;

import mova.game.core.GameCore;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.Screen;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics3D.PolygonRenderer;
import mova.game.graphics3D.SolidPolygonRenderer;
import mova.game.graphics3D.math3D.*;
import mova.game.input.*;
import mova.lang.ThreadUtils;
import mova.trigo.Angle;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Simple3DTest2 extends GameCore {

    public static void main(String[] args) {
        new Simple3DTest2().run();
    }

    protected PolygonRenderer polygonRenderer;
    protected Screen screen;
    protected GameWindow gameWindow;
    protected Camera camera;

    protected List<Polygon3D> polygons;

    private boolean drawFrameRate = false;
    private boolean drawInstructions = true;

    // for calculating frame rate
    private int numFrames;
    private long startTime;
    private float frameRate;

    private final GameAction exit = new GameAction("exit");
    private final GameAction smallerView = new GameAction("smallerView", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
    private final GameAction largerView = new GameAction("largerView", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
    private final GameAction frameRateToggle = new GameAction("frameRateToggle", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
    private final GameAction goForward = new GameAction("goForward");
    private final GameAction goBackward = new GameAction("goBackward");
    private final GameAction goUp = new GameAction("goUp");
    private final GameAction goDown = new GameAction("goDown");
    private final GameAction goLeft = new GameAction("goLeft");
    private final GameAction goRight = new GameAction("goRight");
    private final GameAction turnLeft = new GameAction("turnLeft");
    private final GameAction turnRight = new GameAction("turnRight");
    private final GameAction tiltUp = new GameAction("tiltUp");
    private final GameAction tiltDown = new GameAction("tiltDown");
    private final GameAction tiltLeft = new GameAction("tiltLeft");
    private final GameAction tiltRight = new GameAction("tiltRight");

    public void init() {
        screen = new Screen();
        gameWindow = new GameWindow();

        screen.setFullScreenWindow(gameWindow);
        gameWindow.enableBufferStrategy();

        GameActionMapping gameActionMapping = new GameActionMapping();
        gameActionMapping.mapToKey(exit, KeyEvent.VK_ESCAPE);
        gameActionMapping.mapToKey(goForward, KeyEvent.VK_W);
        gameActionMapping.mapToKey(goForward, KeyEvent.VK_UP);
        gameActionMapping.mapToKey(goBackward, KeyEvent.VK_S);
        gameActionMapping.mapToKey(goBackward, KeyEvent.VK_DOWN);
        gameActionMapping.mapToKey(goLeft, KeyEvent.VK_A);
        gameActionMapping.mapToKey(goLeft, KeyEvent.VK_LEFT);
        gameActionMapping.mapToKey(goRight, KeyEvent.VK_D);
        gameActionMapping.mapToKey(goRight, KeyEvent.VK_RIGHT);
        gameActionMapping.mapToKey(goUp, KeyEvent.VK_PAGE_UP);
        gameActionMapping.mapToKey(goDown, KeyEvent.VK_PAGE_DOWN);
        gameActionMapping.mapToMouse(turnLeft, MouseUserAction.MOVE_LEFT);
        gameActionMapping.mapToMouse(turnRight, MouseUserAction.MOVE_RIGHT);
        gameActionMapping.mapToMouse(tiltUp, MouseUserAction.MOVE_UP);
        gameActionMapping.mapToMouse(tiltDown, MouseUserAction.MOVE_DOWN);

        gameActionMapping.mapToKey(tiltLeft, KeyEvent.VK_INSERT);
        gameActionMapping.mapToKey(tiltRight, KeyEvent.VK_DELETE);

        gameActionMapping.mapToKey(smallerView, KeyEvent.VK_SUBTRACT);
        gameActionMapping.mapToKey(smallerView, KeyEvent.VK_MINUS);
        gameActionMapping.mapToKey(largerView, KeyEvent.VK_ADD);
        gameActionMapping.mapToKey(largerView, KeyEvent.VK_PLUS);
        gameActionMapping.mapToKey(largerView, KeyEvent.VK_EQUALS);
        gameActionMapping.mapToKey(frameRateToggle, KeyEvent.VK_R);

        AdvancedGameInputsListener gameInputsListener = new AdvancedGameInputsListener(new GameInputsListener());
        gameInputsListener.setGameActionMapping(gameActionMapping);
        gameInputsListener.listenTo(gameWindow);

        gameWindow.hideCursor();
        gameInputsListener.setRelativeMouseMode(true);

        // create the polygon renderer
        createPolygonRenderer();

        // create polygons
        polygons = new ArrayList<>();
        createPolygons();
    }

    @Override
    protected void beforeExit() {
        if (screen != null) screen.dispose();
        ThreadUtils.lazilyExit();
    }

    @Override
    protected MovaGraphics getGraphics() {
        return gameWindow.getGraphics();
    }

    @Override
    protected void atLoopEnd() {
        if (gameWindow.hasBufferStrategy()) gameWindow.updateBufferStrategy();
    }

    // create a house (convex polyhedra)
    public void createPolygons() {
        SolidPolygon3D poly;

        // walls
        poly = new SolidPolygon3D(
            new Vector3D(-200, 0, -1000),
            new Vector3D(200, 0, -1000),
            new Vector3D(200, 250, -1000),
            new Vector3D(-200, 250, -1000));
        poly.setColor(Color.WHITE);
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(-200, 0, -1400),
            new Vector3D(-200, 250, -1400),
            new Vector3D(200, 250, -1400),
            new Vector3D(200, 0, -1400));
        poly.setColor(Color.WHITE);
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(-200, 0, -1400),
            new Vector3D(-200, 0, -1000),
            new Vector3D(-200, 250, -1000),
            new Vector3D(-200, 250, -1400));
        poly.setColor(Color.GRAY);
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(200, 0, -1000),
            new Vector3D(200, 0, -1400),
            new Vector3D(200, 250, -1400),
            new Vector3D(200, 250, -1000));
        poly.setColor(Color.GRAY);
        polygons.add(poly);

        // door and windows
        poly = new SolidPolygon3D(
            new Vector3D(0, 0, -1000),
            new Vector3D(75, 0, -1000),
            new Vector3D(75, 125, -1000),
            new Vector3D(0, 125, -1000));
        poly.setColor(new Color(0x660000));
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(-150, 150, -1000),
            new Vector3D(-100, 150, -1000),
            new Vector3D(-100, 200, -1000),
            new Vector3D(-150, 200, -1000));
        poly.setColor(new Color(0x660000));
        polygons.add(poly);

        // roof
        poly = new SolidPolygon3D(
            new Vector3D(-200, 250, -1000),
            new Vector3D(200, 250, -1000),
            new Vector3D(75, 400, -1200),
            new Vector3D(-75, 400, -1200));
        poly.setColor(new Color(0x660000));
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(-200, 250, -1400),
            new Vector3D(-200, 250, -1000),
            new Vector3D(-75, 400, -1200));
        poly.setColor(new Color(0x330000));
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(200, 250, -1400),
            new Vector3D(-200, 250, -1400),
            new Vector3D(-75, 400, -1200),
            new Vector3D(75, 400, -1200));
        poly.setColor(new Color(0x660000));
        polygons.add(poly);
        poly = new SolidPolygon3D(
            new Vector3D(200, 250, -1000),
            new Vector3D(200, 250, -1400),
            new Vector3D(75, 400, -1200));
        poly.setColor(new Color(0x330000));
        polygons.add(poly);
    }


    public void createPolygonRenderer() {
        camera = new Camera(new Transform3D(0,100,0), gameWindow);
        polygonRenderer = new SolidPolygonRenderer(camera);
    }


    /**
        Sets the view bounds, centering the view on the screen.
    */
    public void setViewBounds(int width, int height) {
        width = Math.min(width, gameWindow.getWidth());
        height = Math.min(height, gameWindow.getHeight());
        width = Math.max(64, width);
        height = Math.max(48, height);
        camera.getViewProjection().setBounds((gameWindow.getWidth() - width) /2,
            (gameWindow.getHeight() - height) /2, width, height);
    }


    public void update(long elapsedTime) {
        if (exit.isPressed()) {
            stop();
            return;
        }

        ViewProjection viewProjection = camera.getViewProjection();
        // check options
        if (largerView.isPressed()) {
            setViewBounds(viewProjection.getWidth() + 64,
                    viewProjection.getHeight() + 48);
        }
        else if (smallerView.isPressed()) {
            setViewBounds(viewProjection.getWidth() - 64,
                    viewProjection.getHeight() - 48);
        }
        if (frameRateToggle.isPressed()) {
            drawFrameRate = !drawFrameRate;
        }

        // cap elapsedTime
        elapsedTime = Math.min(elapsedTime, 100);

        float angleChange = 0.0002f*elapsedTime;
        float distanceChange = .5f*elapsedTime;

        Transform3D position = camera.getPosition();
        Vector3D cameraLoc = position.getLocation();
        Rotation3D rotation = position.getRotation();

        // apply movement
        Angle angleY = rotation.getAngleY();
        if (goForward.isPressed()) {
            cameraLoc.setX(cameraLoc.getX() - distanceChange * angleY.sin());
            cameraLoc.setZ(cameraLoc.getZ() - distanceChange * angleY.cos());
        }

        if (goBackward.isPressed()) {
            cameraLoc.setX(cameraLoc.getX() + distanceChange * angleY.sin());
            cameraLoc.setZ(cameraLoc.getZ() + distanceChange * angleY.cos());
        }

        if (goLeft.isPressed()) {
            cameraLoc.setX(cameraLoc.getX() - distanceChange * angleY.cos());
            cameraLoc.setZ(cameraLoc.getZ() + distanceChange * angleY.sin());
        }

        if (goRight.isPressed()) {
            cameraLoc.setX(cameraLoc.getX() + distanceChange * angleY.cos());
            cameraLoc.setZ(cameraLoc.getZ() - distanceChange * angleY.sin());
        }

        if (goUp.isPressed()) {
            cameraLoc.setY(cameraLoc.getY() + distanceChange);
        }

        if (goDown.isPressed()) {
            cameraLoc.setY(cameraLoc.getY() - distanceChange);
        }

        // look up/down (rotate around x)
        int tilt = tiltUp.getAmount() - tiltDown.getAmount();
        tilt = Math.min(tilt, 200);
        tilt = Math.max(tilt, -200);

        // limit how far you can look up/down
        float newAngleX = rotation.getAngleXRadians() + tilt * angleChange;
        newAngleX = Math.max(newAngleX, (float)-Math.PI/2);
        newAngleX = Math.min(newAngleX, (float)Math.PI/2);
        rotation.setAngleX(newAngleX);

        // turn (rotate around y)
        int turn = turnLeft.getAmount() - turnRight.getAmount();
        turn = Math.min(turn, 200);
        turn = Math.max(turn, -200);
        rotation.addAngleY(turn * angleChange);

        // tilet head left/right (rotate around z)
        if (tiltLeft.isPressed()) {
            rotation.addAngleZ(10*angleChange);
        }
        if (tiltRight.isPressed()) {
            rotation.addAngleZ(-10*angleChange);
        }
    }


    public void draw(MovaGraphics graphics, long elapsedTime) {

        // draw polygons
        polygonRenderer.startFrame(graphics);
        for (Polygon3D polygon : polygons) {
            polygonRenderer.draw(graphics, polygon);
        }
        polygonRenderer.endFrame(graphics);

        drawText(graphics);
    }


    public void drawText(Graphics g) {

        // draw text
        g.setColor(Color.WHITE);
        if (drawInstructions) {
            g.drawString("Use the mouse/arrow keys to move. " + "Press Esc to exit.", 5, Simple3DTest1.DEFAULT_FONT_SIZE);
        }
        // (you may have to turn off the BufferStrategy in
        // ScreenManager for more accurate tests)
        if (drawFrameRate) {
            calcFrameRate();
            g.drawString(frameRate + " frames/sec", 5, gameWindow.getHeight() - 5);
        }
    }


    public void calcFrameRate() {
        numFrames++;
        long currTime = System.currentTimeMillis();

        // calculate the frame rate every 500 milliseconds
        if (currTime > startTime + 500) {
            frameRate = (float)numFrames * 1000 / (currTime - startTime);
            startTime = currTime;
            numFrames = 0;
        }
    }

}