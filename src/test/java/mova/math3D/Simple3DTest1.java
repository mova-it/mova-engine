package mova.math3D;

import mova.game.graphics.MovaGraphics;
import mova.game.core.GameCore;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.Screen;
import mova.game.graphics3D.math3D.*;
import mova.game.input.GameAction;
import mova.game.input.GameActionMapping;
import mova.game.input.GameInputsListener;
import mova.lang.ThreadUtils;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.GeneralPath;

public class Simple3DTest1 extends GameCore {

    protected static final int DEFAULT_FONT_SIZE = 24;

    private final SolidPolygon3D treeLeaves = new SolidPolygon3D(
            new Vector3D(-50, -35, 0),
            new Vector3D(50, -35, 0),
            new Vector3D(0, 150, 0)
    );

    private final SolidPolygon3D treeTrunk = new SolidPolygon3D(
            new Vector3D(-5, -50, 0),
            new Vector3D(5, -50, 0),
            new Vector3D(5, -35, 0),
            new Vector3D(-5, -35, 0)
    );

    private final Transform3D treeTransform = new Transform3D(0, 0, -500);
    private final Polygon3D transformedPolygon = new Polygon3D();

    private final GameAction exit = new GameAction("exit");
    private final GameAction zoomIn = new GameAction("zoomIn");
    private final GameAction zoomOut = new GameAction("zoomOut");

    private Screen screen;
    private GameWindow gameWindow;
    private ViewProjection viewProjection;

    @Override
    protected void init() {
        screen = new Screen();
        gameWindow = new GameWindow();

        screen.setFullScreenWindow(gameWindow);
        gameWindow.enableBufferStrategy();


        GameActionMapping gameActionMapping = new GameActionMapping();
        gameActionMapping.mapToKey(exit, KeyEvent.VK_ESCAPE);
        gameActionMapping.mapToKey(zoomIn, KeyEvent.VK_UP);
        gameActionMapping.mapToKey(zoomOut, KeyEvent.VK_DOWN);

        GameInputsListener gameInputsListener = new GameInputsListener();
        gameInputsListener.setGameActionMapping(gameActionMapping);
        gameInputsListener.listenTo(gameWindow);

        // make the view window the entire screen
        viewProjection = new ViewProjection(0, 0, gameWindow.getWidth(), gameWindow.getHeight(), (float) Math.toRadians(75));

        // give the polygons color
        treeLeaves.setColor(new Color(0x008000));
        treeTrunk.setColor(new Color(0x714311));
    }

    @Override
    protected void beforeExit() {
        if (screen != null) screen.dispose();
        ThreadUtils.lazilyExit();
    }

    @Override
    protected MovaGraphics getGraphics() {
        return gameWindow.getGraphics();
    }

    @Override
    protected void update(long elapsedTime) {
        if (exit.isPressed()) {
            stop();
            return;
        }

        // cap elapsedTime
        elapsedTime = Math.min(elapsedTime, 100);

        // rotate around the y axis
        treeTransform.getRotation().addAngleY(0.002f * elapsedTime);

        // allow user to zoom in/out
        if (zoomIn.isPressed()) {
            treeTransform.getLocation().setZ(treeTransform.getLocation().getZ() + 0.5f * elapsedTime);
        }
        if (zoomOut.isPressed()) {
            treeTransform.getLocation().setZ(treeTransform.getLocation().getZ() - 0.5f * elapsedTime);
        }
    }

    @Override
    protected void draw(MovaGraphics movaGraphics, long elapsedTime) {
        // erase background
        movaGraphics.setColor(Color.black);
        movaGraphics.fillRect(0, 0, gameWindow.getWidth(), gameWindow.getHeight());

        // draw message
        movaGraphics.setColor(Color.white);
        movaGraphics.drawString("Press up/down to zoom. Press Esc to exit.", 5, DEFAULT_FONT_SIZE);

        // draw the tree polygons
        trandformAndDraw(movaGraphics, treeTrunk);
        trandformAndDraw(movaGraphics, treeLeaves);
    }


    /**
     * Projects and draws a polygon onto the view window.
     */
    private void trandformAndDraw(java.awt.Graphics2D g, SolidPolygon3D poly) {
        transformedPolygon.setTo(poly);

        // translate and rotate the polygon
        transformedPolygon.add(treeTransform);

        // project the polygon to the screen
        transformedPolygon.project(viewProjection);

        // convert the polygon to a Java2D GeneralPath and draw it
        GeneralPath path = new GeneralPath();
        Vector3D v = transformedPolygon.getVertex(0);
        path.moveTo(v.getX(), v.getY());
        for (int i = 1; i < transformedPolygon.getNumVertices(); i++) {
            v = transformedPolygon.getVertex(i);
            path.lineTo(v.getX(), v.getY());
        }
        g.setColor(poly.getColor());
        g.fill(path);
    }

    @Override
    protected void atLoopEnd() {
        if (gameWindow.hasBufferStrategy()) gameWindow.updateBufferStrategy();
    }

    public static void main(String[] args) {
        new Simple3DTest1().run();
    }
}
