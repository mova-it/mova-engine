package mova.graphics3D;

import mova.game.graphics3D.SimpleTexturedPolygonRenderer;
import mova.game.graphics3D.math3D.*;

public class TextureMapTest1 extends GameCore3D {

    public static void main(String[] args) {
        new TextureMapTest1().run();
    }

    public void createPolygons() {
        Polygon3D poly;

        // one wall for now
        poly = new Polygon3D(
            new Vector3D(-128, 256, -1000),
            new Vector3D(-128, 0, -1000),
            new Vector3D(128, 0, -1000),
            new Vector3D(128, 256, -1000));
        polygons.add(poly);
    }

    public void createPolygonRenderer() {
        camera = new Camera(new Transform3D(0,100,0), gameWindow);
        polygonRenderer = new SimpleTexturedPolygonRenderer(camera, "/images/test_pattern.png");
    }

}