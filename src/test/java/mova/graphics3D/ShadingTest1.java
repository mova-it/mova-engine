package mova.graphics3D;

import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ImageUtils;
import mova.game.graphics3D.ShadedTexturedPolygonRenderer;
import mova.game.graphics3D.math3D.Camera;
import mova.game.graphics3D.math3D.PointLight3D;
import mova.game.graphics3D.math3D.Transform3D;
import mova.game.graphics3D.texture.Texture;
import mova.game.input.GameAction;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

public class ShadingTest1 extends TextureMapTest2 {

    public static void main(String[] args) {
        new ShadingTest1().run();
    }

    private final GameAction brighterLight = new GameAction("brighter");
    private final GameAction dimmerLight = new GameAction("dimmer");

    private PointLight3D light;

    public void init() {
        super.init();

        gameActionMapping.mapToKey(brighterLight, KeyEvent.VK_PLUS);
        gameActionMapping.mapToKey(brighterLight, KeyEvent.VK_ADD);
        gameActionMapping.mapToKey(brighterLight, KeyEvent.VK_EQUALS);
        gameActionMapping.mapToKey(dimmerLight, KeyEvent.VK_SUBTRACT);
        gameActionMapping.mapToKey(dimmerLight, KeyEvent.VK_MINUS);
    }

    public Texture loadTexture(String imageName) {
        return createTexture(imageName, true);
    }


    public static Texture createTexture(String filename, boolean shaded) {
        try {
            BufferedImage image = ImageUtils.loadImage(Texture.class, filename);
            return Texture.createTexture(image, shaded);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void createPolygonRenderer() {
        camera = new Camera(new Transform3D(0, 100, 0), gameWindow);

        ShadedTexturedPolygonRenderer polygonRenderer = new ShadedTexturedPolygonRenderer(camera);
        light = new PointLight3D(-500, 500, 0, 1f);
        light.setDistanceFalloff(2000);
        polygonRenderer.setLightSource(light);
        polygonRenderer.setAmbientLightIntensity(.05f);

        this.polygonRenderer = polygonRenderer;

    }

    public void draw(MovaGraphics g, long elapsedTime) {
        super.draw(g, elapsedTime);
        g.setColor(Color.WHITE);
        g.drawString("Press +/- to change the light intensity.", 5, DEFAULT_FONT_SIZE * 2);
    }

    public void update(long elapsedTime) {
        super.update(elapsedTime);

        if (brighterLight.isPressed()) {
            light.setIntensity(Math.min(5, light.getIntensity() + .005f * elapsedTime));
        }
        if (dimmerLight.isPressed()) {
            light.setIntensity(Math.max(0, light.getIntensity() - .005f * elapsedTime));
        }
    }

}