package mova.graphics3D;

import mova.game.graphics.ImageUtils;
import mova.game.graphics3D.FastTexturedPolygonRenderer;
import mova.game.graphics3D.math3D.*;
import mova.game.graphics3D.texture.Texture;

import java.awt.image.BufferedImage;

public class TextureMapTest2 extends GameCore3D {

    public static void main(String[] args) {
        new TextureMapTest2().run();
    }

    // create a house (convex polyhedra)
    public void createPolygons() {

        // create Textures
        Texture wall = loadTexture("/images/wall1.png");
        Texture roof = loadTexture("/images/roof1.png");

        TexturedPolygon3D poly;

        // walls
        poly = new TexturedPolygon3D(wall,
                new Vector3D(-200, 250, -1000),
                new Vector3D(-200, 0, -1000),
                new Vector3D(200, 0, -1000),
                new Vector3D(200, 250, -1000));
        polygons.add(poly);

        poly = new TexturedPolygon3D(wall,
                new Vector3D(200, 250, -1400),
                new Vector3D(200, 0, -1400),
                new Vector3D(-200, 0, -1400),
                new Vector3D(-200, 250, -1400));
        polygons.add(poly);

        poly = new TexturedPolygon3D(wall,
                new Vector3D(-200, 250, -1400),
                new Vector3D(-200, 0, -1400),
                new Vector3D(-200, 0, -1000),
                new Vector3D(-200, 250, -1000));
        polygons.add(poly);

        poly = new TexturedPolygon3D(wall,
                new Vector3D(200, 250, -1000),
                new Vector3D(200, 0, -1000),
                new Vector3D(200, 0, -1400),
                new Vector3D(200, 250, -1400));
        polygons.add(poly);

        // roof
        poly = new TexturedPolygon3D(roof,
                new Vector3D(-200, 250, -1000),
                new Vector3D(200, 250, -1000),
                new Vector3D(75, 400, -1200),
                new Vector3D(-75, 400, -1200));
        polygons.add(poly);

        poly = new TexturedPolygon3D(roof,
                new Vector3D(-200, 250, -1400),
                new Vector3D(-200, 250, -1000),
                new Vector3D(-75, 400, -1200));
        polygons.add(poly);

        poly = new TexturedPolygon3D(roof,
                new Vector3D(200, 250, -1400),
                new Vector3D(-200, 250, -1400),
                new Vector3D(-75, 400, -1200),
                new Vector3D(75, 400, -1200));
        polygons.add(poly);

        poly = new TexturedPolygon3D(roof,
                new Vector3D(200, 250, -1000),
                new Vector3D(200, 250, -1400),
                new Vector3D(75, 400, -1200));
        polygons.add(poly);
    }

    public Texture loadTexture(String imageName) {
        return createTexture(imageName);
    }

    public static Texture createTexture(String filename) {
        return createTexture(filename, false);
    }


    public static Texture createTexture(String filename, boolean shaded) {
        try {
            BufferedImage image = ImageUtils.loadImage(Texture.class, filename);
            return Texture.createTexture(image, shaded);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void createPolygonRenderer() {
        camera = new Camera(new Transform3D(0, 100, 0), gameWindow);


        polygonRenderer = new FastTexturedPolygonRenderer(camera);
    }

}