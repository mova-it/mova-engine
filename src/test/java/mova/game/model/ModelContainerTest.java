package mova.game.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class ModelContainerTest {

    @Test
    public void create_empty() {
        Assert.assertThrows(IllegalArgumentException.class, () -> new ModelContainer(null));
        Assert.assertThrows(IllegalArgumentException.class, () -> new ModelContainer(Collections.EMPTY_LIST));
        Assert.assertThrows(IllegalArgumentException.class, () -> new ModelContainer(Collections.EMPTY_SET));
    }

    @Test
    public void create_one() {
        Object model = new Object();
        ModelContainer container = new ModelContainer(model);
        Assert.assertNull(container.getModel(Integer.class));
        Assert.assertFalse(container.hasModel(Integer.class));
        Assert.assertSame(model, container.getModel(Object.class));
        Assert.assertTrue(container.hasModel(Object.class));
    }

    @Test
    public void create_collection() {
        Object m1 = new Object();
        Integer i1 = 42;
        ModelContainer container = new ModelContainer(m1, i1);

        Assert.assertNull(container.getModel(String.class));
        Assert.assertSame(m1, container.getModel(Object.class));
        Assert.assertSame(i1, container.getModel(Integer.class));
        Assert.assertTrue(container.hasModel(Object.class));
        Assert.assertTrue(container.hasModel(Integer.class));
        Assert.assertFalse(container.hasModel(String.class));

        container = new ModelContainer(Arrays.asList(m1, i1));

        Assert.assertNull(container.getModel(String.class));
        Assert.assertSame(m1, container.getModel(Object.class));
        Assert.assertSame(i1, container.getModel(Integer.class));
        Assert.assertTrue(container.hasModel(Object.class));
        Assert.assertTrue(container.hasModel(Integer.class));
        Assert.assertFalse(container.hasModel(String.class));
    }
}
