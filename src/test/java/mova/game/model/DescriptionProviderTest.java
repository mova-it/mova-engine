package mova.game.model;

import mova.game.core.ecs.GameObject;
import mova.game.ecs.TestGameObject;
import mova.util.RandomUtils;
import mova.util.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DescriptionProviderTest {

    @Test
    public void createCollector_null() {
        DescriptionProvider provider = new DescriptionProvider();
        Assert.assertThrows(IllegalArgumentException.class, () -> provider.createCollector(null, null));
        Assert.assertThrows(IllegalArgumentException.class, () -> provider.createCollector(null, o -> StringUtils.EMPTY));
        String randString = RandomUtils.randString(20);
        Assert.assertThrows(IllegalArgumentException.class, () -> provider.createCollector(randString, null));
    }

    @Test
    public void createCollector_change() {
        DescriptionProvider provider = new DescriptionProvider();

        String key = RandomUtils.randString(20);
        Assert.assertEquals(StringUtils.EMPTY, provider.getDescription(key));

        String d1 = RandomUtils.randString(50);
        boolean result = provider.createCollector(key, gameObject -> d1);
        Assert.assertFalse(result);
        Assert.assertNotNull(provider.getDescription(key));
        Assert.assertEquals(d1, provider.getDescription(key));

        String d2 = RandomUtils.randString(51);
        result = provider.createCollector(key, gameObject -> d2);
        Assert.assertTrue(result);
        Assert.assertNotNull(provider.getDescription(key));
        Assert.assertEquals(d2, provider.getDescription(key));
    }

    @Test
    public void createCollector_GameObject() {
        String name = RandomUtils.randString(50);
        GameObject go = new TestGameObject(name);

        DescriptionProvider provider = new DescriptionProvider();
        go.addComponent(provider);

        String key = RandomUtils.randString(20);
        Assert.assertEquals(StringUtils.EMPTY, provider.getDescription(key));

        provider.createCollector(key, GameObject::getName);
        Assert.assertEquals(name, provider.getDescription(key));
    }
}
