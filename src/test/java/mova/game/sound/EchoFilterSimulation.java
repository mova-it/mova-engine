package mova.game.sound;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class EchoFilterSimulation {

    public static void main(String[] args) throws IOException {
        SimpleSoundPlayer soundPlayer = new SimpleSoundPlayer("/sounds/voice.wav");

        InputStream inputStream = new ByteArrayInputStream(soundPlayer.getSamples());

        EchoFilter echoFilter = new EchoFilter(11025, .6f);

        soundPlayer.play(inputStream);
        inputStream.reset();
        soundPlayer.play(new FilteredSoundStream(inputStream, echoFilter));
    }
}
