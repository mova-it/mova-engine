package mova.game.sound;

import mova.game.graphics.sprite.Sprite;

public class Filter3d implements SoundFilter {

    private static final int NUM_SHIFTING_SAMPLES = 500;

    // TODO plutôt que de prendre des Sprites, ce devrait être directement la distance
    // TODO la distance ne devrait pas être évaluer dans la lib de son
    private final Sprite source;
    private final Sprite listener;
    private final int maxDistance;
    private float lastVolume;

    public Filter3d(Sprite source, Sprite listener, int maxDistance) {
        this.source = source;
        this.listener = listener;
        this.maxDistance = maxDistance;
        this.lastVolume = 0f;
    }

    @Override
    public void filter(byte[] samples, int offset, int length) {

        if (source == null || listener == null) return;

        float distance = (float) source.getCoordinates().distance(listener.getCoordinates());
        float newVolume = (maxDistance - distance)/maxDistance;
        if (newVolume < 0) newVolume = 0;

        int shift = 0;
        for (int i = offset; i < offset + length; i+=2) {
            float volume = newVolume;
            if (shift < NUM_SHIFTING_SAMPLES) {
                volume = lastVolume + (newVolume - lastVolume)*shift/ NUM_SHIFTING_SAMPLES;
                shift++;
            }

            short oldSample = SoundFilter.getSample(samples, i);
            short newSample = (short)(oldSample*volume);
            SoundFilter.setSample(samples, i, newSample);
        }

        lastVolume = newVolume;

    }
}
