package mova.game.sound;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;

public class MidiSimulation implements MetaEventListener {

    private static final int DRUM_TRACK = 1;

    private MidiPlayer midiPlayer;

    public void run() {
        midiPlayer = new MidiPlayer();

        Sequence sequence = midiPlayer.getSequence("/sounds/music.midi");

        midiPlayer.play(sequence, true);
        System.out.println("Playing (without drums) ...");

        Sequencer sequencer = midiPlayer.getSequencer();
        sequencer.setTrackMute(DRUM_TRACK, true);
        sequencer.addMetaEventListener(this);
    }

    @Override
    public void meta(MetaMessage meta) {
        if (meta.getType() == MidiPlayer.END_OF_TRACK_MESSAGE) {
            Sequencer sequencer = midiPlayer.getSequencer();
            if (sequencer.getTrackMute(DRUM_TRACK)) {
                System.out.println("Turning on drums ...");
                sequencer.setTrackMute(DRUM_TRACK, false);
            } else {
                System.out.println("Exiting...");
                midiPlayer.close();
            }
        }
    }

    public static void main(String[] args) {
        new MidiSimulation().run();
    }
}
