package mova.game.sound;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class SimpleSoundPlayer {

    public static void main(String[] args) {
        SimpleSoundPlayer soundPlayer = new SimpleSoundPlayer("/sounds/voice.wav");

        InputStream inputStream = new ByteArrayInputStream(soundPlayer.getSamples());
        soundPlayer.play(inputStream);
    }

    private AudioFormat format;
    private byte[] samples;

    public SimpleSoundPlayer(String filename) {
        try {
            URL url = getClass().getResource(filename);
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);

            format = audioInputStream.getFormat();

            samples = getSamples(audioInputStream);
        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] getSamples() {
        return samples;
    }

    private byte[] getSamples(AudioInputStream audioInputStream) {
        int length = (int) audioInputStream.getFrameLength()*format.getFrameSize();
        byte[] samples = new byte[length];
        DataInputStream dataInputStream = new DataInputStream(audioInputStream);
        try {
            dataInputStream.readFully(samples);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return samples;
    }

    public void play(InputStream inputStream) {
        int bufferSize = format.getFrameSize()*Math.round(format.getSampleRate()/10);
        byte[] buffer = new byte[bufferSize];

        SourceDataLine line;
        try {
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
            line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(format, bufferSize);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
            return;
        }

        line.start();

        try {
            int numBytesRead;
            while ((numBytesRead = inputStream.read(buffer, 0, buffer.length)) != -1) {
                line.write(buffer, 0, numBytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        line.drain();
        line.close();
    }
}
