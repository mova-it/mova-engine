package mova.game.sound;

import mova.game.geom.Locator;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ImageUtils;
import mova.game.core.AbstractGameCore;
import mova.game.graphics.ScreenManager;
import mova.game.graphics.sprite.Animation;
import mova.game.graphics.sprite.Sprite;
import mova.game.input.GameAction;
import mova.game.input.GameInputsListenerOld;
import mova.game.sound.io.LoopingByteArrayInputStream;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;

public class Filter3DSimulation extends AbstractGameCore {

    private Sprite fly;
    private Sprite ear;
    private GameAction exit;

    private SimpleSoundPlayer bzzSound;
    private InputStream bzzSoundStream;

    public Filter3DSimulation() {
        super(new ScreenManager(), new GameInputsListenerOld());
    }

    @Override
    public void init() {
        super.init();

        exit = new GameAction("exit", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
        gameInputsListener.listenTo(screenManager.getFullScreenWindow());
        gameInputsListener.mapToKey(exit, KeyEvent.VK_ESCAPE);
        gameInputsListener.setCursor(GameInputsListenerOld.getInvisibleCursor());

        createSprites();

        bzzSound = new SimpleSoundPlayer("/sounds/fly-bzz.wav");
        Filter3d filter3d = new Filter3d(fly, ear, screenManager.getHeight());
        bzzSoundStream = new FilteredSoundStream(new LoopingByteArrayInputStream(bzzSound.getSamples()), filter3d);

        new Thread(() -> bzzSound.play(bzzSoundStream)).start();
    }

    private void createSprites() {
        Animation animation = new Animation();

        try {
            Image fly1 = ImageUtils.loadImage("/images/fly1.png");
            Image fly2 = ImageUtils.loadImage("/images/fly2.png");
            Image fly3 = ImageUtils.loadImage("/images/fly3.png");

            // TODO j'aime pas trop la manière dont les animations sont crées?
            animation.addFrame(fly1, 50);
            animation.addFrame(fly2, 50);
            animation.addFrame(fly3, 50);
            animation.addFrame(fly2, 50);
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images d'animation de la mouche", ioe);
        }

        fly = new Sprite(animation);

        // TODO encore moins quand y'a pas d'animation au final ????!!!!
        animation = new Animation();
        try {
            Image ear = ImageUtils.loadImage("/images/ear.png");
            animation.addFrame(ear, 0);
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images d'animation de l'oreille", ioe);
        }

        ear = new Sprite(animation);
        screenManager.centerLocation(ear);
    }

    @Override
    public void update(long elapsedTime) {
        if (exit.isPressed()) stop();
        else {
            ear.update(elapsedTime);
            fly.update(elapsedTime);
            fly.setCoordinates(new Locator(gameInputsListener.getMouseLocation()));
        }
    }

    @Override
    public void stop() {
        super.stop();

        try {
            bzzSoundStream.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public void draw(MovaGraphics movaGraphics, long elapsedTime) {
        movaGraphics.setColor(new Color(0x33cc33));
        movaGraphics.fillRect(0, 0, screenManager.getWidth(), screenManager.getHeight());

        movaGraphics.drawImage(ear.getImage(), ear.getCoordinates());
        movaGraphics.drawImage(fly.getImage(), fly.getCoordinates());
    }

    public static void main(String[] args) {
        new Filter3DSimulation().run();
    }
}
