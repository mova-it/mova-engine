package mova.game.view;

import mova.debug.DebugUtils;
import mova.game.core.GameCore;
import mova.game.geom.Locator;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.Screen;
import mova.game.graphics.component.GameWindow;
import mova.game.input.GameAction;
import mova.game.input.GameActionMapping;
import mova.game.input.GameInputsListener;
import mova.game.input.MouseUserAction;
import mova.lang.ThreadUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;

@RunWith(JUnit4.class)
public class ViewTest extends GameCore {

    private Screen screen;
    private GameWindow gameWindow;
    private GameInputsListener gameInputsListener;

    private GameAction quitAction;
    private GameAction whereamiAction;

    private View view;

    Rectangle world;
    Rectangle r = new Rectangle(200, 350, 262, 97);

    @Override
    protected void init() {
        gameWindow = new GameWindow();
        screen = new Screen();

        screen.setFullScreenWindow(gameWindow);
        gameWindow.enableBufferStrategy();


        gameInputsListener = new GameInputsListener();
        GameActionMapping mapping = new GameActionMapping();
        quitAction = mapping.mapToKey("Quit", KeyEvent.VK_ESCAPE);
        whereamiAction = mapping.mapToMouse("Where am I", MouseUserAction.LEFT_BUTTON);
        gameInputsListener.setGameActionMapping(mapping);
        gameInputsListener.listenTo(gameWindow);

        view = new View(50, 100, 1500, 1000);
        OptimizedCamera camera = view.addCamera(OptimizedCamera.class);
        camera.setScaleFactor(.5f);

        world = camera.getScreenProjection().createInversedProjection().transform(view.getBounds()).getBounds();
        world.setSize(world.width - 200, world.height - 100);
    }

    @Override
    protected MovaGraphics getGraphics() {
        return gameWindow.getGraphics();
    }

    private long startTime = 0;

    Projection screenProjection;
    Locator mouseScreen, mouseWorld, mouseControl;
    Locator camWorld, camScreen, camControl;
    Locator viewScreen, viewWorld, viewControl;
    Locator viewCenterScreen, viewCenterWorld, viewCenterControl;

    @Override
    protected void update(long elapsedTime) {
        startTime += elapsedTime;

        if ((startTime/2000)%2 == 0) {
            float scaleFactor = ((SimpleCamera) view.getActiveCamera()).getScaleFactor();
            ((SimpleCamera) view.getActiveCamera()).setScaleFactor(scaleFactor * (1 - .1f * elapsedTime / 1000f));
        } else {
            view.getActiveCamera().addX(elapsedTime*10/100f);
            view.getActiveCamera().addY(elapsedTime*10/100f);
        }

        if (whereamiAction.isPressed()) {
            screenProjection = view.getActiveCamera().getScreenProjection();

            mouseScreen = gameInputsListener.getMouseLocation();
            mouseWorld = screenProjection.inverseTransform(mouseScreen);
            mouseControl = screenProjection.transform(mouseWorld);

            camWorld = view.getActiveCamera().getLocation();
            camScreen = screenProjection.transform(camWorld);
            camControl = screenProjection.inverseTransform(camScreen);

            viewScreen = view.getCenter();
            viewWorld = screenProjection.inverseTransform(viewScreen);
            viewControl = screenProjection.transform(viewWorld);

            viewCenterScreen = view.getCenter();
            viewCenterWorld = screenProjection.inverseTransform(viewCenterScreen);
            viewCenterControl = screenProjection.transform(viewCenterWorld);
        }


        if (quitAction.isPressed()) stop();
    }

    @Override
    protected void draw(MovaGraphics movaGraphics, long elapsedTime) {

        movaGraphics.clip(view.getBounds());

        movaGraphics.fillRect(Color.BLUE, gameWindow.getWidth(), gameWindow.getHeight());

        Projection sp = view.getActiveCamera().getScreenProjection();

        Shape worldProjected = sp.transform(world);
        Shape shapeProjected = sp.transform(r);

        // Draw world
        movaGraphics.setColor(Color.BLACK);
        movaGraphics.draw(worldProjected);
        movaGraphics.setColor(Color.LIGHT_GRAY);
        movaGraphics.fill(worldProjected);

        // Draw shape
        movaGraphics.setColor(Color.BLACK);
        movaGraphics.draw(shapeProjected);
        movaGraphics.setColor(Color.DARK_GRAY);
        movaGraphics.fill(shapeProjected);

        // Draw view bounds and system
        movaGraphics.setColor(Color.BLACK);
        Rectangle viewBounds = view.getBounds();
        movaGraphics.drawLine((int) viewBounds.getCenterX(), (int) viewBounds.getY(), (int) viewBounds.getCenterX(), (int) (viewBounds.getY() + viewBounds.getHeight()));
        movaGraphics.drawLine((int) viewBounds.getX(), (int) viewBounds.getCenterY(), (int) (viewBounds.getX() + viewBounds.getWidth()), (int) viewBounds.getCenterY());
        movaGraphics.draw(viewBounds);

        movaGraphics.resetClip();

        // Draw infos
        if (screenProjection != null) {
            DebugUtils.drawDebugLine(movaGraphics, "worldProjection = " + screenProjection);
            DebugUtils.drawDebugLine(movaGraphics, "viewScreen = " + viewScreen + "; viewCenterScreen = " + viewCenterScreen + "; camScreen = " + camScreen);
            DebugUtils.drawDebugLine(movaGraphics, "viewWorld = " + viewWorld + "; viewCenterWorld = " + viewCenterWorld + "; camWorld = " + camWorld);
            DebugUtils.drawDebugLine(movaGraphics, "mouseScreen = " + mouseScreen);
            DebugUtils.drawDebugLine(movaGraphics, "mouseWorld = " + mouseWorld);
            DebugUtils.resetDebugSession();
        }
    }

    @Override
    protected void atLoopEnd() {
        gameWindow.updateBufferStrategy();
    }

    @Override
    protected void beforeExit() {
        try {
            screen.dispose();
        } finally {
            ThreadUtils.lazilyExit();
        }
    }

    @Test
    public void test_worldView() {
        View v = new View(200, 200, 1900 - 400, 1080 - 400);
        SimpleCamera c = v.addCamera(SimpleCamera.class);

        Rectangle2D worldView0 = v.getWorldView();

        c.moveTo(200, 200);

        Rectangle2D worldView = v.getWorldView();

        Assert.assertNotEquals(worldView0, worldView);
    }

    public static void main(String[] args) {
        ViewTest viewTest = new ViewTest();
        viewTest.run();
    }
}
