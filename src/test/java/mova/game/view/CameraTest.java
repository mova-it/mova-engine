package mova.game.view;

import mova.game.geom.Locator;
import mova.util.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;
import java.awt.geom.Rectangle2D;

@RunWith(JUnit4.class)
public class CameraTest {
    private static final float PIXEL_CALCULATION_ACCEPTED_DELTA = .5f;

    private View view;
    private final Locator origin = new Locator(0, 0);
    private Rectangle world;
    private Rectangle shape;

    @Before
    public void init() {
        int screenW = 1900;
        int screenH = 1080;
        int viewX = RandomUtils.randInt(screenW/2);
        int viewY = RandomUtils.randInt(screenH/2);
        int viewW = RandomUtils.randInt(screenW - viewX);
        int viewH = RandomUtils.randInt(screenH - viewY);
        view = new View(viewX, viewY, viewW, viewH);

        world = new Rectangle(0, 0, RandomUtils.randInt(20000) + 1, RandomUtils.randInt(20000) + 1);
        shape = new Rectangle(250, 300, 250, 300);
    }

    @Test
    public void test_viewProjection() {
        Projection v2sProjection = view.getViewToScreenProjection();

        Locator viewOriginOnScreen = v2sProjection.transform(origin);
        Assert.assertEquals(view.getCenter().getX(), viewOriginOnScreen.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY(), viewOriginOnScreen.getY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape worldOnScreen = v2sProjection.transform(world);
        Rectangle2D worldOnScreenBounds = worldOnScreen.getBounds2D();
        Assert.assertEquals(view.getCenter().getX(), worldOnScreenBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY(), worldOnScreenBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape shapeOnScreen = v2sProjection.transform(shape);
        Rectangle2D shapeOnScreenBounds = shapeOnScreen.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() + shape.getX(), shapeOnScreenBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() - shape.getY(), shapeOnScreenBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);
    }

    @Test
    public void test_camProjection_0_0_1() {
        Camera c = new SimpleCamera(view);

        Projection w2sProjection = c.getScreenProjection();

        Locator worldOriginOnScreen = w2sProjection.transform(origin);
        Assert.assertEquals(view.getCenter().getX(), worldOriginOnScreen.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY(), worldOriginOnScreen.getY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape worldOnScreen = w2sProjection.transform(world);
        Rectangle2D worldOnScreenBounds = worldOnScreen.getBounds2D();
        Assert.assertEquals(view.getCenter().getX(), worldOnScreenBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY(), worldOnScreenBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape shapeOnView = w2sProjection.transform(shape);
        Rectangle2D shapeOnViewBounds = shapeOnView.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() + shape.getX(), shapeOnViewBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() - shape.getY(), shapeOnViewBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getWidth(), shapeOnViewBounds.getWidth(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getHeight(), shapeOnViewBounds.getHeight(), PIXEL_CALCULATION_ACCEPTED_DELTA);
    }

    @Test
    public void test_camProjection_0_0_n() {
        SimpleCamera c = new SimpleCamera(view);

        float scaleFactor = RandomUtils.randFloat();
        c.setScaleFactor(scaleFactor);

        Projection w2sProjection = c.getScreenProjection();

        Locator worldOriginOnScreen = w2sProjection.transform(origin);
        Assert.assertEquals(view.getCenter().getX(), worldOriginOnScreen.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY(), worldOriginOnScreen.getY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape worldOnScreen = w2sProjection.transform(world);
        Rectangle2D worldOnScreenBounds = worldOnScreen.getBounds2D();
        Assert.assertEquals(view.getCenter().getX(), worldOnScreenBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY(), worldOnScreenBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape shapeOnView = w2sProjection.transform(shape);
        Rectangle2D shapeOnViewBounds = shapeOnView.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() + shape.getX()*scaleFactor, shapeOnViewBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() - shape.getY()*scaleFactor, shapeOnViewBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getWidth()*scaleFactor, shapeOnViewBounds.getWidth(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getHeight()*scaleFactor, shapeOnViewBounds.getHeight(), PIXEL_CALCULATION_ACCEPTED_DELTA);
    }

    @Test
    public void test_camProjection_x_y_1() {
        SimpleCamera c = new SimpleCamera(view);

        int camX = RandomUtils.randInt(world.width);
        int camY = RandomUtils.randInt(world.height);
        c.setX(camX);
        c.setY(camY);

        Projection w2sProjection = c.getScreenProjection();

        Locator worldOriginOnScreen = w2sProjection.transform(origin);
        Assert.assertEquals(view.getCenter().getX() - camX, worldOriginOnScreen.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() + camY, worldOriginOnScreen.getY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape worldOnScreen = w2sProjection.transform(world);
        Rectangle2D worldOnScreenBounds = worldOnScreen.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() - camX, worldOnScreenBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() + camY, worldOnScreenBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape shapeOnView = w2sProjection.transform(shape);
        Rectangle2D shapeOnViewBounds = shapeOnView.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() + shape.getX() - camX, shapeOnViewBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() - shape.getY() + camY, shapeOnViewBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getWidth(), shapeOnViewBounds.getWidth(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getHeight(), shapeOnViewBounds.getHeight(), PIXEL_CALCULATION_ACCEPTED_DELTA);
    }

    @Test
    public void test_camProjection_x_y_n() {
        SimpleCamera c = new SimpleCamera(view);

        int camX = RandomUtils.randInt(world.width);
        int camY = RandomUtils.randInt(world.height);
        float scaleFactor = RandomUtils.randFloat();
        c.setX(camX);
        c.setY(camY);
        c.setScaleFactor(scaleFactor);

        Projection w2sProjection = c.getScreenProjection();

        Locator worldOriginOnScreen = w2sProjection.transform(origin);
        Assert.assertEquals(view.getCenter().getX() - camX*scaleFactor, worldOriginOnScreen.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() + camY*scaleFactor, worldOriginOnScreen.getY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape worldOnScreen = w2sProjection.transform(world);
        Rectangle2D worldOnScreenBounds = worldOnScreen.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() - camX*scaleFactor, worldOnScreenBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() + camY*scaleFactor, worldOnScreenBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);

        Shape shapeOnView = w2sProjection.transform(shape);
        Rectangle2D shapeOnViewBounds = shapeOnView.getBounds2D();
        Assert.assertEquals(view.getCenter().getX() + (shape.getX() - camX)*scaleFactor, shapeOnViewBounds.getX(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(view.getCenter().getY() - (shape.getY() - camY)*scaleFactor, shapeOnViewBounds.getMaxY(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getWidth()*scaleFactor, shapeOnViewBounds.getWidth(), PIXEL_CALCULATION_ACCEPTED_DELTA);
        Assert.assertEquals(shape.getHeight()*scaleFactor, shapeOnViewBounds.getHeight(), PIXEL_CALCULATION_ACCEPTED_DELTA);
    }

}
