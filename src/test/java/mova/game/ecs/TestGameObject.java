package mova.game.ecs;

import mova.game.core.ecs.AbstractGameObject;
import mova.game.core.ecs.GameObject;

public class TestGameObject extends AbstractGameObject {

    public TestGameObject(String name) {
        super(name);
    }

    @Override
    public GameObject copy() {
        return null;
    }
}
