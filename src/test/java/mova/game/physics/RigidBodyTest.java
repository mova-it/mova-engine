package mova.game.physics;

import mova.game.core.ecs.GameObject;
import mova.game.ecs.TestGameObject;
import mova.game.graphics.Transform;
import mova.util.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;

@RunWith(JUnit4.class)
public class RigidBodyTest {

    @Test
    public void isColliding_null() {
        int x = RandomUtils.randInt(200);
        int y = RandomUtils.randInt(200);
        int w = RandomUtils.randInt(200) + 1;
        int h = RandomUtils.randInt(200) + 1;
        RigidBody rb = new RigidBody(new Rectangle(x, y, w, h));

        Assert.assertFalse(rb.isColliding(null));
        Assert.assertFalse(rb.isColliding((RigidBody) null, null));
        Assert.assertFalse(rb.isColliding(null, new Transform()));
        Assert.assertFalse(RigidBody.isColliding((Shape) null, null));
        Assert.assertFalse(RigidBody.isColliding(rb.getBodyShape(), null));
        Assert.assertFalse(RigidBody.isColliding(null, rb.getBodyShape()));
    }

    @Test
    public void isColliding_same_object() {
        GameObject go = new TestGameObject("go");

        int x = RandomUtils.randInt(200);
        int y = RandomUtils.randInt(200);
        int w = RandomUtils.randInt(200) + 1;
        int h = RandomUtils.randInt(200) + 1;
        go.addComponent(new RigidBody(new Rectangle(x, y, w, h)));

        go.addComponent(new Transform());

        Assert.assertFalse(go.getComponent(RigidBody.class).isColliding(go));
        Assert.assertFalse(go.getComponent(RigidBody.class).isColliding(go.getComponent(RigidBody.class), null));
        Assert.assertFalse(go.getComponent(RigidBody.class).isColliding(go.getComponent(RigidBody.class), go.getComponent(Transform.class)));
        Assert.assertFalse(RigidBody.isColliding(go.getComponent(RigidBody.class).getBodyShape(), go.getComponent(RigidBody.class).getBodyShape()));
    }

    @Test
    public void isColliding() {
        int x1 = RandomUtils.randInt(200);
        int y1 = RandomUtils.randInt(200);
        int w1 = RandomUtils.randInt(200) + 1;
        int h1 = RandomUtils.randInt(200) + 1;
        Rectangle r1 = new Rectangle(x1, y1, w1, h1);
        Rectangle r2 = new Rectangle(x1, y1, w1, h1);

        RigidBody rb1 = new RigidBody(r1);
        RigidBody rb2 = new RigidBody(r2);
        Assert.assertTrue(rb1.isColliding(rb2, null));

        r1.setLocation(r1.x + r1.width - 1, r1.y);
        Assert.assertTrue(rb1.isColliding(rb2, null));

        r1.setLocation(r1.x, r1.y + r1.height - 1);
        Assert.assertTrue(rb1.isColliding(rb2, null));

        r1.setLocation(r1.x + 1, r1.y);
        Assert.assertFalse(rb1.isColliding(rb2, null));
    }
}
