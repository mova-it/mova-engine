package mova.game.input;

import mova.util.StringUtils;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

// Note on garde la classe pour maintenir le fonctionnement de certaines Simulation
@Deprecated
public class GameInputsListenerOld implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

    private static final int NUM_KEYS_CODES = 600;

    private static final Cursor INVISIBLE_CURSOR;
    static {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        INVISIBLE_CURSOR = toolkit.createCustomCursor(toolkit.getImage(StringUtils.EMPTY), new Point(), "Invisible");
    }

    private final GameAction[] keysActions = new GameAction[NUM_KEYS_CODES];
    private final GameAction[] mouseActions = new GameAction[MouseUserAction.values().length];

    private Point mouseLocation;
    private Point centerLocation;
    private Component component;
    private Robot robot;
    private boolean isRecentering;

    public static String getKeyName(int keyCode) {
        return KeyEvent.getKeyText(keyCode);
    }

    public static String getMouseName(int mouseCode) {
        return MouseUserAction.values()[mouseCode].name;
    }

    public static int getMouseButtonCode(MouseEvent mouseEvent) {
        MouseUserAction mouseUserAction = getMouseButtonUserAction(mouseEvent);
        return mouseUserAction != null ? mouseUserAction.ordinal() : -1;
    }

    public static MouseUserAction getMouseButtonUserAction(MouseEvent mouseEvent) {
        switch (mouseEvent.getButton()) {
            case MouseEvent.BUTTON1: return MouseUserAction.LEFT_BUTTON;
            case MouseEvent.BUTTON2: return MouseUserAction.WHEEL_BUTTON;
            case MouseEvent.BUTTON3: return MouseUserAction.RIGHT_BUTTON;
            case 4: return MouseUserAction.REAR_BUTTON;
            case 5: return MouseUserAction.FRONT_BUTTON;
            default: return null;
        }
    }

    public static Cursor getInvisibleCursor() {
        return INVISIBLE_CURSOR;
    }

    public void listenTo(Component component) {
        mouseLocation = component.getMousePosition();
        LoggerFactory.getLogger(GameInputsListenerOld.class).warn("mouseLocation = " + mouseLocation);
        centerLocation = new Point(component.getWidth() / 2, component.getHeight() / 2);

        component.addKeyListener(this);
        component.addMouseListener(this);
        component.addMouseMotionListener(this);
        component.addMouseWheelListener(this);
        component.setFocusTraversalKeysEnabled(false);

        this.component = component;
    }

    public void setCursor(Cursor cursor) {
        component.setCursor(cursor);
    }

    public void mapToKey(GameAction gameAction, int keyCode) {
        keysActions[keyCode] = gameAction;
    }

    public void mapToMouse(GameAction gameAction, MouseUserAction mouseUserAction) {
        mouseActions[mouseUserAction.ordinal()] = gameAction;
    }

    public void clearGameActionMappings(GameAction gameAction) {
        for (int i = 0; i < keysActions.length; i++) {
            if (keysActions[i] == gameAction) keysActions[i] = null;
        }
        for (int i = 0; i < mouseActions.length; i++) {
            if (mouseActions[i] == gameAction) mouseActions[i] = null;
        }
        gameAction.reset();
    }

    public List<String> getGameActionMappings(GameAction gameAction) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < keysActions.length; i++) {
            if (keysActions[i] == gameAction) list.add(getKeyName(i));
        }
        for (int i = 0; i < mouseActions.length; i++) {
            if (mouseActions[i] == gameAction) list.add(getMouseName(i));
            if (mouseActions[i] == gameAction) list.add(MouseUserAction.values()[i].name);
        }

        return list;
    }

    public void resetAllGameActions() {
        for (GameAction keysAction : keysActions) {
            if (keysAction != null) keysAction.reset();
        }
        for (GameAction mouseAction : mouseActions) {
            if (mouseAction != null) mouseAction.reset();
        }
    }

    public Point getMouseLocation() {
        return mouseLocation.getLocation();
    }

    private GameAction _getGameAction(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        return keyCode < keysActions.length ? keysActions[keyCode] : null;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        GameAction gameAction = _getGameAction(e);
        if (gameAction != null) gameAction.press();
        e.consume();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        GameAction gameAction = _getGameAction(e);
        if (gameAction != null) gameAction.release();
        e.consume();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        GameAction gameAction = _getGameAction(e);
        if (gameAction != null) gameAction.press();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        GameAction gameAction = _getGameAction(e);
        if (gameAction != null) gameAction.release();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // DO NOTHING
    }

    private GameAction _getGameAction(MouseEvent mouseEvent) {
        int mouseCode = getMouseButtonCode(mouseEvent);
        return mouseCode != -1 ? mouseActions[mouseCode] : null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseMoved(e);
    }

    public void setRelativeMouseMode(boolean enable) {
        if (isRelativeMouseMode() != enable) {
            if (enable) {
                try {
                    robot = new Robot();
                    _recenterMouse();
                } catch (AWTException awte) {
                    throw new AWTError(awte.getLocalizedMessage());
                }
            } else {
                robot = null;
            }
        }
    }

    public boolean isRelativeMouseMode() {
        return robot != null;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point newLocation = e.getPoint();

        if (isRecentering) isRecentering = false;
        else {
            Point oldLocation = getMouseLocation();
            int dx = newLocation.x - oldLocation.x;
            int dy = newLocation.y - oldLocation.y;
            _mouseHelper(MouseUserAction.MOVE_LEFT, MouseUserAction.MOVE_RIGHT, dx);
            _mouseHelper(MouseUserAction.MOVE_UP, MouseUserAction.MOVE_DOWN, dy);

            if (isRelativeMouseMode()) _recenterMouse();
        }

        mouseLocation.setLocation(newLocation);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        _mouseHelper(MouseUserAction.WHEEL_UP, MouseUserAction.WHEEL_DOWN, e.getWheelRotation());
    }

    private synchronized void _recenterMouse() {
        if (isRelativeMouseMode() && component.isShowing()) {
            centerLocation.x = component.getWidth() / 2;
            centerLocation.y = component.getHeight() / 2;
            SwingUtilities.convertPointToScreen(centerLocation, component);
            isRecentering = true;
            robot.mouseMove(centerLocation.x, centerLocation.y);
        }
    }

    private void _mouseHelper(MouseUserAction codeNeg, MouseUserAction codePos, int amount) {
        GameAction gameAction;
        if (amount < 0) gameAction = mouseActions[codeNeg.ordinal()];
        else gameAction = mouseActions[codePos.ordinal()];

        if (gameAction != null) {
            gameAction.press(Math.abs(amount));
            gameAction.release();
        }
    }

    public enum MouseUserAction {
        MOVE_LEFT("Mouse Left"),
        MOVE_RIGHT("Mouse Right"),
        MOVE_UP("Mouse Up"),
        MOVE_DOWN("Mouse Down"),
        WHEEL_UP("Mouse Wheel Up"),
        WHEEL_DOWN("Mouse Wheel Down"),
        LEFT_BUTTON("Left Mouse Button"),
        WHEEL_BUTTON("Wheel Mouse Button"),
        RIGHT_BUTTON("Right Mouse Button"),
        REAR_BUTTON("Rear Mouse Button"),
        FRONT_BUTTON("Front Mouse Button");

        public final String name;

        MouseUserAction(String name) {
            this.name = name;
        }
    }
}
