package mova.game.input;

import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ImageUtils;
import mova.game.graphics.NullRepaintManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuSimulation extends InputManagerSimulation implements ActionListener {

    public static void main(String[] args) {
        new MenuSimulation().run();
    }

    protected GameAction configAction;

    private JButton playButton;
    private JButton configButton;
    private JButton quitButton;
    private JButton pauseButton;
    private JPanel buttonsPanel;

    @Override
    public void init() {
        super.init();

        NullRepaintManager.install();

        configAction = new GameAction("config");

        quitButton = createButton("quit", "Quit");
        playButton = createButton("play", "Continue");
        pauseButton = createButton("pause", "Pause");
        configButton = createButton("config", "Change settings");

        buttonsPanel = new JPanel();
        buttonsPanel.setOpaque(false);
        buttonsPanel.add(pauseButton);

        JFrame window = screenManager.getFullScreenWindow();
        Container container = window.getContentPane();
        if (container instanceof JComponent) ((JComponent) container).setOpaque(false);

        container.setLayout(new FlowLayout());
        container.add(buttonsPanel);
        container.add(configButton);
        container.add(quitButton);

        window.validate();
    }

    @Override
    public void draw(MovaGraphics movaGraphics, long elapsedTime) {
        super.draw(movaGraphics, elapsedTime);

        JFrame frame = screenManager.getFullScreenWindow();
        frame.getLayeredPane().paintComponents(movaGraphics);
    }

    @Override
    public void setPaused(boolean paused) {
        super.setPaused(paused);

        buttonsPanel.removeAll();
        if (isPaused()) buttonsPanel.add(playButton);
        else buttonsPanel.add(pauseButton);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();

        if (source == quitButton) exit.tap();
        else if (source == configButton) configAction.tap();
        else if (source == playButton || source == pauseButton) pause.tap();
    }

    public JButton createButton(String name, String tooltip) {
        ImageIcon iconRollover = ImageUtils.loadImageIcon("/images/menu/" + name + ".png");
        int width = iconRollover.getIconWidth();
        int height = iconRollover.getIconHeight();

        Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

        Image image = screenManager.createCompatibleImage(width, height, Transparency.TRANSLUCENT);
        MovaGraphics g = new MovaGraphics((java.awt.Graphics2D) image.getGraphics());
        Composite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f);
        g.setComposite(alpha);
        g.drawImage(iconRollover.getImage());
        g.dispose();
        ImageIcon iconDefault = new ImageIcon(image);
        image = screenManager.createCompatibleImage(width, height, Transparency.TRANSLUCENT);
        g = new MovaGraphics((java.awt.Graphics2D) image.getGraphics());
        g.drawImage(iconRollover.getImage(), 2, 2);
        g.dispose();
        ImageIcon iconPressed = new ImageIcon(image);

        JButton button = new JButton();
        button.addActionListener(this);
        button.setIgnoreRepaint(true);
        button.setFocusable(false);
        button.setToolTipText(tooltip);
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setCursor(cursor);
        button.setIcon(iconDefault);
        button.setRolloverIcon(iconRollover);
        button.setPressedIcon(iconPressed);

        return button;
    }
}
