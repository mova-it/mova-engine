package mova.game.input;

import mova.game.graphics.ImageUtils;
import mova.game.core.AbstractGameCore;
import mova.game.graphics.ScreenManager;
import mova.game.graphics.sprite.Animation;
import mova.game.graphics.sprite.Player;
import mova.game.graphics.MovaGraphics;

import java.awt.Image;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class InputManagerSimulation extends AbstractGameCore {

    protected GameAction jump;
    protected GameAction exit;
    protected GameAction moveLeft;
    protected GameAction moveRight;
    protected GameAction pause;
    private Player player;
    private Image backgroundImage;
    private boolean paused = false;

    public InputManagerSimulation() {
        super(new ScreenManager(), new GameInputsListenerOld());
    }

    @Override
    public void init() {
        super.init();

        Window window = screenManager.getFullScreenWindow();
        gameInputsListener.listenTo(window);

//        inputManager.setRelativeMouseMode(true);
//        inputManager.setCursor(InputManagerImpl.INVISIBLE_CURSOR);

        createGameActions();
        createSprite();
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        if (this.paused != paused) {
            this.paused = paused;
            gameInputsListener.resetAllGameActions();
        }
    }

    @Override
    public void update(long elapsedTime) {
        checkSystemInputs();

        if (!isPaused()) {
            checkGameInputs();

            player.update(elapsedTime);
        }
    }

    public void checkSystemInputs() {
        if (pause.isPressed()) setPaused(!isPaused());
        if (exit.isPressed()) stop();
    }

    public void checkGameInputs() {
        float velocityX = 0;
        if (moveLeft.isPressed()) velocityX -= Player.SPEED;
        if (moveRight.isPressed()) velocityX += Player.SPEED;
        player.setVelocityX(velocityX);

        if (jump.isPressed() && player.getState() != Player.State.JUMPING) player.jump();
    }

    @Override
    public void draw(MovaGraphics movaGraphics, long elapsedTime) {
        movaGraphics.drawImage(backgroundImage, screenManager.getDimension());

        movaGraphics.drawImage(player.getImage(), player.getCoordinates());
    }

    public void createGameActions() {
        jump = new GameAction("jump", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
        exit = new GameAction("exit", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
        moveLeft = new GameAction("moveLeft");
        moveRight = new GameAction("moveRight");
        pause = new GameAction("pause", GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);

        gameInputsListener.mapToKey(exit, KeyEvent.VK_ESCAPE);
        gameInputsListener.mapToKey(pause, KeyEvent.VK_P);

        gameInputsListener.mapToKey(jump, KeyEvent.VK_SPACE);
        gameInputsListener.mapToMouse(jump, GameInputsListenerOld.MouseUserAction.LEFT_BUTTON);

        gameInputsListener.mapToKey(moveLeft, KeyEvent.VK_LEFT);
        gameInputsListener.mapToKey(moveRight, KeyEvent.VK_RIGHT);

        gameInputsListener.mapToKey(moveLeft, KeyEvent.VK_Q);
        gameInputsListener.mapToKey(moveRight, KeyEvent.VK_D);

//        inputManager.mapToMouse(moveLeft, InputManager.MouseUserAction.MOVE_LEFT);
//        inputManager.mapToMouse(moveRight, InputManager.MouseUserAction.MOVE_RIGHT);
    }

    private void createSprite() {
        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger l'image de fond", ioe);
        }


        Animation animation = new Animation();
        try {
            Image player1 = ImageUtils.loadImage("/images/player1.png");
            Image player2 = ImageUtils.loadImage("/images/player2.png");
            Image player3 = ImageUtils.loadImage("/images/player3.png");
            animation.addFrame(player1, 250);
            animation.addFrame(player2, 150);
            animation.addFrame(player1, 150);
            animation.addFrame(player2, 150);
            animation.addFrame(player3, 200);
            animation.addFrame(player2, 150);
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images d'animation du joueur", ioe);
        }

        player = new Player(animation);
        player.setFloorY(screenManager.getHeight() - player.getHeight());
    }

    public static void main(String[] args) {
        new InputManagerSimulation().run();
    }
}
