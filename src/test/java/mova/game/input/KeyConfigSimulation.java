package mova.game.input;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

public class KeyConfigSimulation extends MenuSimulation {

    private static final String INSTRUCTIONS = "<html>" +
                    "Click an action's input box to change its keys.<br/>" +
                    "An action can have at most three keys associated with it.<br/>" +
                    "Press Backsapce to clear an action's key." +
                    "</html>";

    private JPanel dialog;
    private JButton okButton;
    private List<InputComponent> inputs;

    @Override
    public void init() {
        super.init();

        inputs = new ArrayList<>();

        JPanel configPanel = new JPanel(new GridLayout(5, 2, 2, 2));
        addActionConfig(configPanel, moveLeft);
        addActionConfig(configPanel, moveRight);
        addActionConfig(configPanel, jump);
        addActionConfig(configPanel, pause);
        addActionConfig(configPanel, exit);

        JPanel bottomPanel = new JPanel(new FlowLayout());
        okButton = new JButton("OK");
        okButton.setFocusable(false);
        okButton.addActionListener(this);
        bottomPanel.add(okButton);

        JPanel topPanel = new JPanel(new FlowLayout());
        topPanel.add(new JLabel(INSTRUCTIONS));

        Border border = BorderFactory.createLineBorder(Color.BLACK);

        dialog = new JPanel(new BorderLayout());
        dialog.add(topPanel, BorderLayout.NORTH);
        dialog.add(configPanel, BorderLayout.CENTER);
        dialog.add(bottomPanel, BorderLayout.SOUTH);
        dialog.setBorder(border);
        dialog.setVisible(false);
        dialog.setSize(dialog.getPreferredSize());

        screenManager.centerLocation(dialog);
        screenManager.getFullScreenWindow().getLayeredPane().add(dialog, JLayeredPane.MODAL_LAYER);
    }

    private void addActionConfig(JPanel configPanel, GameAction gameAction) {
        JLabel label = new JLabel(gameAction.getName(), JLabel.RIGHT);
        InputComponent input = new InputComponent(gameAction);
        configPanel.add(label);
        configPanel.add(input);
        inputs.add(input);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        super.actionPerformed(actionEvent);
        if (actionEvent.getSource() == okButton) configAction.tap();
    }

    @Override
    public void checkSystemInputs() {
        super.checkSystemInputs();
        if (configAction.isPressed()) {
            boolean show = !dialog.isVisible();
            dialog.setVisible(show);
            setPaused(show);
        }
    }

    private void resetInputs() {
        inputs.forEach(InputComponent::setText);
    }

    private class InputComponent extends JTextField {

        private final GameAction gameAction;

        public InputComponent(GameAction gameAction) {
            this.gameAction = gameAction;
            setText();
            enableEvents(KeyEvent.KEY_EVENT_MASK | MouseEvent.MOUSE_EVENT_MASK | MouseEvent.MOUSE_MOTION_EVENT_MASK | MouseEvent.MOUSE_WHEEL_EVENT_MASK);
        }

        private void setText() {
            List<String> list = gameInputsListener.getGameActionMappings(gameAction);
            String text = String.join(", ", list);

            synchronized (getTreeLock()) {
                setText(text);
            }
        }

        private void mapKeyGameAction(int code) {
            if (gameInputsListener.getGameActionMappings(gameAction).size() >= 3) gameInputsListener.clearGameActionMappings(gameAction);

            gameInputsListener.mapToKey(gameAction, code);

            resetInputs();
            screenManager.getFullScreenWindow().requestFocus();
        }

        private void mapMouseGameAction(GameInputsListenerOld.MouseUserAction mouseUserAction) {
            if (gameInputsListener.getGameActionMappings(gameAction).size() >= 3) gameInputsListener.clearGameActionMappings(gameAction);

            gameInputsListener.mapToMouse(gameAction, mouseUserAction);

            resetInputs();
            screenManager.getFullScreenWindow().requestFocus();
        }

        @Override
        protected void processKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_PRESSED) {
                if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE && !gameInputsListener.getGameActionMappings(gameAction).isEmpty()) {
                    gameInputsListener.clearGameActionMappings(gameAction);
                    setText("");
                    screenManager.getFullScreenWindow().requestFocus();
                } else {
                    mapKeyGameAction(e.getKeyCode());
                }
            }

            e.consume();
        }

        @Override
        protected void processMouseEvent(MouseEvent e) {
            if (e.getID() == MouseEvent.MOUSE_PRESSED) {
                if (hasFocus()) {
                    GameInputsListenerOld.MouseUserAction mouseUserAction = GameInputsListenerOld.getMouseButtonUserAction(e);
                    mapMouseGameAction(mouseUserAction);
                } else {
                    requestFocus();
                }
            }

            e.consume();
        }

        @Override
        protected void processMouseMotionEvent(MouseEvent e) {
            e.consume();
        }

        @Override
        protected void processMouseWheelEvent(MouseWheelEvent e) {
            if (hasFocus()) {
                GameInputsListenerOld.MouseUserAction mouseUserAction = e.getWheelRotation() < 0
                        ? GameInputsListenerOld.MouseUserAction.WHEEL_DOWN
                        : GameInputsListenerOld.MouseUserAction.WHEEL_UP;
                mapMouseGameAction(mouseUserAction);
            } else {
                requestFocus();
            }

            e.consume();
        }
    }

    public static void main(String[] args) {
        new KeyConfigSimulation().run();
    }
}
