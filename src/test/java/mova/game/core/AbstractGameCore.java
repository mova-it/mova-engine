package mova.game.core;

import mova.Settings;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ScreenManager;
import mova.game.input.GameInputsListenerOld;
import mova.io.IOUtils;
import mova.lang.ThreadUtils;
import mova.util.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.InputStream;

@Deprecated
public abstract class AbstractGameCore extends GameCore {

    protected static final int FONT_SIZE = Settings.DIALOG_FONT.getSize();

    private static final DisplayMode[] POSSIBLE_MODES;
    static {
        InputStream modesInputStream = AbstractGameCore.class.getResourceAsStream("modes.txt");
        if (modesInputStream == null) throw new Error("Impossible de trouver les modes d'affichage possible");

        POSSIBLE_MODES = IOUtils.lines(modesInputStream).map(s -> {
            int[] args = NumberUtils.convert(s.split(",\\s*"));
            return new DisplayMode(args[0], args[1], args[2], args[3]);
        }).toArray(DisplayMode[]::new);
    }

    protected final Logger logger = LoggerFactory.getLogger(AbstractGameCore.class);

    protected final ScreenManager screenManager;
    protected final GameInputsListenerOld gameInputsListener;

    public AbstractGameCore(ScreenManager screenManager, GameInputsListenerOld gameInputsListener) {
        this.screenManager = screenManager;
        this.gameInputsListener = gameInputsListener;
    }

    public void init() {
        DisplayMode displayMode = screenManager.findFirstCompatibleDisplayMode(POSSIBLE_MODES);
        screenManager.setFullScreen(displayMode);

        Window window = screenManager.getFullScreenWindow();
        window.setFont(Settings.DIALOG_FONT);
        window.setBackground(Color.BLUE);
        window.setForeground(Color.WHITE);

        gameInputsListener.listenTo(window);
    }

    @Override
    protected MovaGraphics getGraphics() {
        return screenManager.getGraphics();
    }

    public void update(long elapsedTime) {
        // On ne fait rien par défaut
    }

    public abstract void draw(MovaGraphics movaGraphics, long elapsedTime);

    @Override
    protected void atLoopEnd() {
        screenManager.update();
    }

    @Override
    protected void beforeExit() {
        screenManager.restoreScreen();
        ThreadUtils.lazilyExit();
    }
}
