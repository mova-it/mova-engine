package mova.game.core;

import mova.game.graphics.ImageUtils;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ScreenManager;
import mova.game.input.GameInputsListenerOld;

import javax.swing.*;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Window;
import java.awt.event.*;
import java.io.IOException;

public class MouselookSimulation extends AbstractGameCore implements KeyListener, MouseMotionListener {

    private final Point mouseLocation = new Point();
    private final Point centerLocation = new Point();
    private final Point imageLocation = new Point();

    private Image backgroundImage;
    private boolean relativeMouseModeEnabled = true;
    private boolean isRecentering = false;

    private Robot robot;

    public MouselookSimulation() {
        super(new ScreenManager(), new GameInputsListenerOld());
    }

    @Override
    public void init() {
        super.init();

        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger l'image de fond", ioe);
        }

        try {
            robot = new Robot();
            _recenterMouse();
            mouseLocation.setLocation(centerLocation);
        } catch (AWTException awte) {
            System.out.println("Couldn't create Robot!");
        }

        Window window = screenManager.getFullScreenWindow();

        window.addKeyListener(this);
        window.addMouseMotionListener(this);
    }

    @Override
    public synchronized void draw(MovaGraphics movaGraphics, long elapsedTime) {
        int width = screenManager.getWidth();
        int height = screenManager.getHeight();

        imageLocation.x %= width;
        imageLocation.y %= height;

        if (imageLocation.x < 0) imageLocation.x += width;
        if (imageLocation.y < 0) imageLocation.y += height;

        int x = imageLocation.x;
        int y = imageLocation.y;
        movaGraphics.drawImage(backgroundImage, x, y, width, height, null);
        movaGraphics.drawImage(backgroundImage, x - width, y, width, height, null);
        movaGraphics.drawImage(backgroundImage, x, y - height, width, height, null);
        movaGraphics.drawImage(backgroundImage, x - width, y - height, width, height, null);

        movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        movaGraphics.drawString("Press Space to change mouse modes.", 5, FONT_SIZE);
        movaGraphics.drawString("Press Escape to exist.", 5, FONT_SIZE*2);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public synchronized void mouseMoved(MouseEvent e) {
        if (isRecentering) isRecentering = false;
        else {
            imageLocation.x += e.getX() - mouseLocation.x;
            imageLocation.y += e.getY() - mouseLocation.y;

            if (relativeMouseModeEnabled) _recenterMouse();
        }

        mouseLocation.setLocation(e.getPoint());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) stop();
        else if (e.getKeyCode() == KeyEvent.VK_SPACE) relativeMouseModeEnabled = !relativeMouseModeEnabled;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // DO NOTHING
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // DO NOTHING
    }

    private synchronized void _recenterMouse() {
        Window window = screenManager.getFullScreenWindow();
        if (robot != null && window.isShowing()) {
            centerLocation.x = window.getWidth()/2;
            centerLocation.y = window.getHeight()/2;
            SwingUtilities.convertPointToScreen(centerLocation, window);
            isRecentering = true;
            robot.mouseMove(centerLocation.x, centerLocation.y);
        }
    }

    public static void main(String[] args) {
        new MouselookSimulation().run();
    }
}
