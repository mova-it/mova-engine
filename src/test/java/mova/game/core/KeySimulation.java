package mova.game.core;

import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ScreenManager;
import mova.game.input.GameInputsListenerOld;

import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

public class KeySimulation extends AbstractGameCore implements KeyListener {

    private final List<String> messages = new LinkedList<>();

    public KeySimulation() {
        super(new ScreenManager(), new GameInputsListenerOld());
    }

    @Override
    public void init() {
        super.init();

        Window window = screenManager.getFullScreenWindow();

        window.addKeyListener(this);

        _addMessage("KeySimulation. Press Escape to exit");
    }


    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();

        if (keyCode == KeyEvent.VK_ESCAPE) stop();
        else {
            _addMessage("Presssed: " + KeyEvent.getKeyText(keyCode));

            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();

        if (keyCode == KeyEvent.VK_ESCAPE) stop();
        else {
            _addMessage("Released: " + KeyEvent.getKeyText(keyCode));

            e.consume();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }

    @Override
    public synchronized void draw(MovaGraphics movaGraphics, long elapsedTime) {
        Window window = screenManager.getFullScreenWindow();

        movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        movaGraphics.setColor(window.getBackground());
        movaGraphics.fillRect(0, 0, screenManager.getWidth(), screenManager.getHeight());

        movaGraphics.setColor(window.getForeground());
        int y = FONT_SIZE;
        for (String message : messages) {
            movaGraphics.drawString(message, 5, y);
            y += FONT_SIZE;
        }
    }

    private synchronized void _addMessage(String message) {
        messages.add(message);

        if (messages.size() >= screenManager.getHeight() / FONT_SIZE) {
            messages.remove(0);
        }
    }

    public static void main(String[] args) {
        new KeySimulation().run();
    }
}
