package mova.game.core;

import mova.game.graphics.MovaGraphics;

public class NullGameCoreState extends AbstractGameCoreState {

    public static final NullGameCoreState INSTANCE = new NullGameCoreState();

    private NullGameCoreState() {}

    @Override
    void update(long elapsedTime) {

    }

    @Override
    void draw(MovaGraphics movaGraphics) {

    }

    @Override
    public void entry() {

    }
}
