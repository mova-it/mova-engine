package mova.game.core;

import mova.game.graphics.MovaGraphics;
import mova.game.graphics.NullRepaintManager;
import mova.game.graphics.ScreenManager;
import mova.game.graphics.component.IconSet;
import mova.game.input.GameInputsListenerOld;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

public class FSMScreenSimulation extends StatedAbstractGameCore {

    private final AbstractGameCoreState interfaceState;
    private final AbstractGameCoreState gameState;

    public FSMScreenSimulation() {
        super(new ScreenManager(), new GameInputsListenerOld());

        interfaceState = new InterfaceState(screenManager/*, inputManager*/);
        gameState = new GameState(screenManager, gameInputsListener);
    }

    @Override
    public void init() {
        super.init();

        interfaceState.init();
        gameState.init();

        setState(interfaceState);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) stop();
        else if (e.getKeyCode() == KeyEvent.VK_TAB) {
            if (currentState instanceof GameState) setState(interfaceState);
            else setState(gameState);
        } else {
            currentState.keyTyped(e);
        }
    }

    private static abstract class TransitionableGameCoreState extends AbstractGameCoreState {

        private static final long FADE_TIME = 1000;
        private boolean fade = true;
        private long fadeTime = FADE_TIME;

        protected final ScreenManager screenManager;

        public TransitionableGameCoreState(ScreenManager screenManager) {
            this.screenManager = screenManager;
        }

        @Override
        public void entry() {
            fade = true;
            fadeTime = FADE_TIME;
        }

        @Override
        public void update(long elapsedTime) {
            if (fadeTime < 0) fade = false;
            else if (fade) fadeTime -= elapsedTime;
        }

        @Override
        public void draw(MovaGraphics movaGraphics) {
            drawEffectiveScreen(movaGraphics);

            if (fade) {
                byte numBars = 8;
                int barHeight = screenManager.getHeight()/numBars;
                int blackHeight = (int) (fadeTime*barHeight/FADE_TIME);

                movaGraphics.setColor(Color.BLACK);
                for (int i = 0; i < numBars; i++) {
                    int y = i*barHeight + (barHeight - blackHeight)/2;
                    movaGraphics.fillRect(0, y, screenManager.getWidth(), blackHeight);
                }
            }
        }

        public abstract void drawEffectiveScreen(MovaGraphics movaGraphics);
    }

    private static class InterfaceState extends TransitionableGameCoreState implements ActionListener {

        private JButton quitButton;
        private JButton playButton;
        private JButton pauseButton;

        public InterfaceState(ScreenManager screenManager) {
            super(screenManager);
        }

        @Override
        public void init() {
            NullRepaintManager.install();

            createGraphicsContent();
        }

        @Override
        public void entry() {
            super.entry();
        }

        @Override
        public void createGraphicsContent() {
            quitButton = _createButton("quit", "Quit");
            playButton = _createButton("play", "Continue");
            pauseButton = _createButton("pause", "Pause");

            JFrame window = screenManager.getFullScreenWindow();
            Container container = window.getContentPane();
            if (container instanceof JComponent) ((JComponent) container).setOpaque(false);

            container.setLayout(new FlowLayout());
            container.add(playButton);
            container.add(pauseButton);
            container.add(quitButton);

            window.validate();
        }

        @Override
        public synchronized void drawEffectiveScreen(MovaGraphics movaGraphics) {
            JFrame window = screenManager.getFullScreenWindow();

            movaGraphics.setColor(window.getBackground());
            movaGraphics.fillRect(0, 0, screenManager.getWidth(), screenManager.getHeight());

            movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            movaGraphics.setColor(window.getForeground());
            movaGraphics.drawString("MouseTest. Press Escape to exit.", 5, FONT_SIZE);

            window.getLayeredPane().paintComponents(movaGraphics);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Object source = actionEvent.getSource();

            if (source == quitButton) {
                System.out.println("exit");
            }
            else if (source == playButton || source == pauseButton) {
                System.out.println("pause/play");
            }
        }

        private JButton _createButton(String name, String tooltip) {
            IconSet iconSet = screenManager.createIconSet("/images/menu/" + name + ".png");

            JButton button = new JButton();
            button.setName(name);

            button.setIgnoreRepaint(true);
            button.setFocusable(false);
            button.setBorder(null);
            button.setContentAreaFilled(false);

            Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
            button.setCursor(cursor);

            button.addActionListener(this);

            button.setToolTipText(tooltip);

            button.setIcon(iconSet.getIconDefault());
            button.setRolloverIcon(iconSet.getIconRollover());
            button.setPressedIcon(iconSet.getIconPressed());

            return button;
        }
    }

    private static class GameState extends TransitionableGameCoreState {

        private static final Color[] COLORS = { Color.WHITE, Color.BLACK, Color.YELLOW, Color.MAGENTA };
        private static final int TRAIL_SIZE = 300;

        private final LinkedList<Point> trailList = new LinkedList<>();
        private boolean trailEnabled = false;
        private int colorIndex = 1;

        private final GameInputsListenerOld gameInputsListener;

        public GameState(ScreenManager screenManager, GameInputsListenerOld gameInputsListener) {
            super(screenManager);
            this.gameInputsListener = gameInputsListener;
        }

        @Override
        public void createGraphicsContent() {
            JFrame window = screenManager.getFullScreenWindow();
            Container container = window.getContentPane();
            if (container instanceof JComponent) ((JComponent) container).setOpaque(false);

            JButton quitButton = _createButton("quit", "Quit");
            JButton playButton = _createButton("play", "Continue");
            JButton pauseButton = _createButton("pause", "Pause");

            JPanel controls = new JPanel();
            controls.setLayout(new FlowLayout());
            controls.add(playButton);
            controls.add(pauseButton);
            controls.add(quitButton);
            controls.setOpaque(true);

            container.setLayout(new BorderLayout());
            container.add(controls, BorderLayout.SOUTH);

            window.validate();
        }

        @Override
        public void entry() {
            super.entry();

            _reset();
        }

        @Override
        public synchronized void drawEffectiveScreen(MovaGraphics movaGraphics) {
            JFrame window = screenManager.getFullScreenWindow();

            movaGraphics.setColor(Color.GRAY);
            movaGraphics.fillRect(0, 0, screenManager.getWidth(), screenManager.getHeight());

            movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            movaGraphics.setColor(window.getForeground());
            movaGraphics.drawString("MouseTest. Press Escape to exit.", 5, FONT_SIZE);

            int count = trailList.size();
            if (count > 1 && !trailEnabled) count = 1;

            for (int i = 0; i < count; i++) {
                Point point = trailList.get(i);
                movaGraphics.drawString("Hello World!", point.x, point.y);
            }

            window.getLayeredPane().paintComponents(movaGraphics);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            trailEnabled = !trailEnabled;
            _reset();
        }

        @Override
        public synchronized void mouseMoved(MouseEvent e) {
            trailList.addFirst(e.getPoint());
            while (trailList.size() > TRAIL_SIZE) trailList.removeLast();
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            colorIndex = (colorIndex + e.getWheelRotation())%COLORS.length;
            if (colorIndex < 0) colorIndex += COLORS.length;

            Window window = screenManager.getFullScreenWindow();
            window.setForeground(COLORS[colorIndex]);
        }

        private void _reset() {
            trailList.clear();
            trailList.add(gameInputsListener.getMouseLocation());
        }

        private JButton _createButton(String name, String tooltip) {
            IconSet iconSet = screenManager.createIconSet("/images/menu/" + name + ".png");

            JButton button = new JButton();
            button.setName(name);

            button.setIgnoreRepaint(true);
            button.setFocusable(false);
            button.setBorder(null);
            button.setContentAreaFilled(false);

            Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
            button.setCursor(cursor);

            button.setToolTipText(tooltip);

            button.setIcon(iconSet.getIconDefault());
            button.setRolloverIcon(iconSet.getIconRollover());
            button.setPressedIcon(iconSet.getIconPressed());

            return button;
        }
    }

    public static void main(String[] args) {
        new FSMScreenSimulation().run();
    }

}
