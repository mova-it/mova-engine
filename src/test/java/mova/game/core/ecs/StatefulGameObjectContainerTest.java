package mova.game.core.ecs;

import javafx.util.Pair;
import mova.game.core.ecs.container.StatefulGameObjectContainer;
import mova.util.RandomUtils;
import mova.util.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class StatefulGameObjectContainerTest {

    private static class StatefulTestGameObject extends AbstractGameObject implements StatefulGameObject {

        private State state;

        public StatefulTestGameObject(String name) {
            super(name);

            state = State.IDLE;
        }

        @Override
        public GameObject copy() {
            return null;
        }

        @Override
        public State getState() {
            return state;
        }

        @Override
        public void setState(State state) {
            this.state = state;
        }
    }

    // TESTS: addGameObject

    @Test
    public void addGameObject_null() {
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        assertThrows(IllegalArgumentException.class, () -> gameObjectContainer.addGameObject(null));
    }

    @Test
    public void addGameObject() {
        try {
            String name = RandomUtils.randString(10);
            StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
            gameObjectContainer.addGameObject(new StatefulTestGameObject(name));
        } catch (Exception e) {
            fail();
        }
    }

    // TESTS: findGameObject

    @Test
    public void findGameObject_empty() {
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        assertNull(gameObjectContainer.findGameObject((String) null));
        assertNull(gameObjectContainer.findGameObject(StringUtils.EMPTY));
        assertNull(gameObjectContainer.findGameObject(RandomUtils.randString(10)));
    }

    @Test
    public void findGameObject_one() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject go = new StatefulTestGameObject(name);

        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(go);

        assertNull(gameObjectContainer.findGameObject((String) null));
        assertNull(gameObjectContainer.findGameObject(StringUtils.EMPTY));

        String otherName = RandomUtils.randString(10, name);
        assertNull(gameObjectContainer.findGameObject(otherName));

        assertEquals(go, gameObjectContainer.findGameObject(name));

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
    }

    @Test
    public void findGameObject_multiple() {
        String[] names = new String[100];
        for (int i = 0; i < names.length; i++) {
            names[i] = RandomUtils.randString(10, Arrays.copyOfRange(names, 0, i));
        }

        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();

        StatefulTestGameObject[] gameObjects = new StatefulTestGameObject[names.length];
        for (int i = 0; i < names.length; i++) {
            gameObjects[i] = new StatefulTestGameObject(names[i]);
            gameObjectContainer.addGameObject(gameObjects[i]);
        }

        assertNull(gameObjectContainer.findGameObject((String) null));
        assertNull(gameObjectContainer.findGameObject(StringUtils.EMPTY));

        String otherName = RandomUtils.randString(10, names);
        assertNull(gameObjectContainer.findGameObject(otherName));

        for (int i = 0; i < names.length; i++) {
            assertEquals(gameObjects[i], gameObjectContainer.findGameObject(names[i]));
        }

        gameObjectContainer.clear();

        for (String name : names) {
            assertNull(gameObjectContainer.findGameObject(name));
        }
    }

    // TESTS: notifyStateChange

    @Test
    public void notifyStateChange_null() {
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        assertThrows(IllegalArgumentException.class, () -> gameObjectContainer.notifyStateChange(null, null));
        StatefulTestGameObject gameObject = new StatefulTestGameObject(RandomUtils.randString(10));
        assertThrows(IllegalArgumentException.class, () -> gameObjectContainer.notifyStateChange(gameObject, null));
    }

    @Test
    public void notifyStateChange_not_added() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);

        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        for (State state : State.values()) {
            assertThrows(IllegalArgumentException.class, () -> gameObjectContainer.notifyStateChange(gameObject, state));
        }
    }

    @Test
    public void notifyStateChange_one_samestate() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.IDLE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_idletovisible() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.VISIBLE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.VISIBLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_idletoactive() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.ACTIVE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.ACTIVE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_idletodestroy() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.DESTROYED);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.DESTROYED, gameObject.getState());
        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_visibletoidle() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.VISIBLE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.VISIBLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.IDLE);
        gameObjectContainer.notifyStateChange(gameObject, State.VISIBLE);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_visibletoactive() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.VISIBLE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.VISIBLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.ACTIVE);
        gameObjectContainer.notifyStateChange(gameObject, State.VISIBLE);

        assertEquals(State.ACTIVE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_visibletodestroy() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.VISIBLE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.VISIBLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.DESTROYED);
        gameObjectContainer.notifyStateChange(gameObject, State.VISIBLE);

        assertEquals(State.DESTROYED, gameObject.getState());
        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_activetoidle() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.ACTIVE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.ACTIVE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.IDLE);
        gameObjectContainer.notifyStateChange(gameObject, State.ACTIVE);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_activetovisible() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.ACTIVE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.ACTIVE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.VISIBLE);
        gameObjectContainer.notifyStateChange(gameObject, State.ACTIVE);

        assertEquals(State.VISIBLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_activetodestroy() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.ACTIVE);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.ACTIVE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.DESTROYED);
        gameObjectContainer.notifyStateChange(gameObject, State.ACTIVE);

        assertEquals(State.DESTROYED, gameObject.getState());
        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_destroytootherstate() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        gameObject.setState(State.DESTROYED);
        gameObjectContainer.notifyStateChange(gameObject, State.IDLE);

        assertEquals(State.DESTROYED, gameObject.getState());
        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);


        gameObject.setState(State.ACTIVE);
        assertThrows(IllegalArgumentException.class, () -> gameObjectContainer.notifyStateChange(gameObject, State.DESTROYED));


        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_one_to_n_randomstates() {
        String name = RandomUtils.randString(10);
        StatefulTestGameObject gameObject = new StatefulTestGameObject(name);
        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        gameObjectContainer.addGameObject(gameObject);

        assertEquals(State.IDLE, gameObject.getState());
        assertNotNull(gameObjectContainer.findGameObject(name));
        assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

        List<State> history = new ArrayList<>();

        try {
            State state = gameObject.getState();
            for (int i = 0; i < 100; i++) {

                State newState;
                do newState = RandomUtils.randElement(State.class);
                while (newState == state || newState == State.DESTROYED);

                history.add(newState);

                gameObject.setState(newState);
                gameObjectContainer.notifyStateChange(gameObject, state);

                assertEquals(newState, gameObject.getState());
                assertNotNull(gameObjectContainer.findGameObject(name));
                assertNotNull(gameObjectContainer.getGameObjectsView()[0]);

                state = newState;
            }
        } catch (AssertionError ae) {
            System.out.println(history);
            throw ae;
        }

        gameObjectContainer.clear();

        assertNull(gameObjectContainer.findGameObject(name));
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }

    @Test
    public void notifyStateChange_1000_to_n_randomstates() {
        String[] names = new String[1000];
        for (int i = 0; i < names.length; i++) {
            names[i] = RandomUtils.randString(10, Arrays.copyOfRange(names, 0, i));
        }

        StatefulGameObjectContainer gameObjectContainer = new StatefulGameObjectContainer();
        StatefulTestGameObject[] gameObjects = new StatefulTestGameObject[names.length];
        for (int i = 0; i < names.length; i++) {
            gameObjects[i] = new StatefulTestGameObject(names[i]);
            gameObjectContainer.addGameObject(gameObjects[i]);
        }

        for (StatefulTestGameObject gameObject : gameObjects) {
            assertEquals(State.IDLE, gameObject.getState());
        }

        List<Pair<GameObject, State>> history = new ArrayList<>();

        try {
            while (gameObjectContainer.getGameObjectsView().length != 0) {
                StatefulTestGameObject gameObject;
                do gameObject = RandomUtils.randElement(gameObjects);
                while (gameObject.isDestroyed());

                State state = gameObject.getState();

                State newState;
                do newState = RandomUtils.randElement(State.class);
                while (newState == state);

                history.add(new Pair<>(gameObject, newState));

                gameObject.setState(newState);
                gameObjectContainer.notifyStateChange(gameObject, state);

                assertEquals(newState, gameObject.getState());

                if (newState != State.DESTROYED) {
                    assertNotNull(gameObjectContainer.findGameObject(gameObject.getName()));
                } else {
                    assertNull(gameObjectContainer.findGameObject(gameObject.getName()));
                }
            }
        } catch (AssertionError ae) {
            System.out.println(history);
            throw ae;
        }

        gameObjectContainer.clear();

        for (String name : names) {
            assertNull(gameObjectContainer.findGameObject(name));
        }
        assertEquals(0, gameObjectContainer.getGameObjectsView().length);
    }
}
