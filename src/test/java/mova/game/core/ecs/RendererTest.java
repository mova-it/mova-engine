package mova.game.core.ecs;

import mova.game.graphics.Renderer;
import mova.game.graphics.MovaGraphics;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;

@RunWith(JUnit4.class)
public class RendererTest {

    @Test
    public void render() {
        Renderer invisibleRenderable = new Renderer() {
            @Override
            public Renderer copy() {
                return null;
            }

            @Override
            public boolean isVisible() {
                return false;
            }

            @Override
            protected void render(MovaGraphics graphics2D) {
                Assert.fail("On ne peut pas avoir de rendu d'un objet invisible");
            }
        };

        invisibleRenderable.draw(null);

        Renderer visibleRenderable = new Renderer() {

            @Override
            public Renderer copy() {
                return null;
            }

            @Override
            public boolean isVisible() {
                return true;
            }

            @Override
            protected void render(MovaGraphics graphics2D) {
                graphics2D.draw(null);
            }
        };

        DrawnTester drawnTester = new DrawnTester();
        visibleRenderable.draw(drawnTester);
        if (!drawnTester.isDrawn()) Assert.fail("Un Renderable visible doit être dessiné");
    }

    private static class DrawnTester extends MovaGraphics {

        private boolean drawn = false;

        public DrawnTester() {
            super(null);
        }

        @Override
        public void draw(Shape s) {
            drawn = true;
        }

        public boolean isDrawn() {
            return drawn;
        }
    }
}
