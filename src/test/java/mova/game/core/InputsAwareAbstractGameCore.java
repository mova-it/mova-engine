package mova.game.core;

import mova.game.graphics.ScreenManager;
import mova.game.input.GameInputsListenerOld;

import java.awt.*;
import java.awt.event.*;

@Deprecated
public abstract class InputsAwareAbstractGameCore extends AbstractGameCore implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

    public InputsAwareAbstractGameCore(ScreenManager screenManager, GameInputsListenerOld gameInputsListener) {
        super(screenManager, gameInputsListener);
    }

    @Override
    public void init() {
        super.init();

        Window window = screenManager.getFullScreenWindow();
        window.addMouseListener(this);
        window.addMouseMotionListener(this);
        window.addMouseWheelListener(this);
        window.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO si c'est vraiment judicieux on garde. si c'est surchargé constamment on retirera
        //  Idem pour mouseExited et mouseDragged
        mouseMoved(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }
}
