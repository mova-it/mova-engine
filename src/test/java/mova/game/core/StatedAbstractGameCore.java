package mova.game.core;

import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ScreenManager;
import mova.game.input.GameInputsListenerOld;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

@Deprecated
public abstract class StatedAbstractGameCore extends InputsAwareAbstractGameCore {

    protected AbstractGameCoreState currentState = NullGameCoreState.INSTANCE;

    public StatedAbstractGameCore(ScreenManager screenManager, GameInputsListenerOld gameInputsListener) {
        super(screenManager, gameInputsListener);
    }

    public void setState(AbstractGameCoreState newState) {
        if (currentState != newState) {
            currentState = newState;
            currentState.entry();
        }
    }

    @Override
    public void update(long elapsedTime) {
        currentState.update(elapsedTime);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        currentState.keyTyped(e);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        currentState.keyTyped(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        currentState.keyReleased(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        currentState.mouseClicked(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        currentState.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        currentState.mouseReleased(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        currentState.mouseEntered(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        currentState.mouseExited(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        currentState.mouseDragged(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        currentState.mouseMoved(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        currentState.mouseWheelMoved(e);
    }

    @Override
    public void draw(MovaGraphics movaGraphics, long elapsedTime) {
        currentState.draw(movaGraphics);
    }
}
