package mova.game.core;

import mova.game.graphics.MovaGraphics;

import java.awt.event.*;

@Deprecated
abstract class AbstractGameCoreState implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

    public void init() {

    }

    abstract void update(long elapsedTime);


    abstract void draw(MovaGraphics movaGraphics);

    public void createGraphicsContent() {

    }

    public abstract void entry();

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO si c'est vraiment judicieux on garde. si c'est surchargé constamment on retirera
        //  Idem pour mouseExited et mouseDragged
        mouseMoved(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }
}
