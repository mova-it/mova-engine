package mova.game.core;

import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ScreenManager;
import mova.game.input.GameInputsListenerOld;

import java.awt.Color;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.*;
import java.util.LinkedList;

public class MouseSimulation extends AbstractGameCore implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

    private static final Color[] COLORS = { Color.WHITE, Color.BLACK, Color.YELLOW, Color.MAGENTA };
    private static final int TRAIL_SIZE = 10;

    private final LinkedList<Point> trailList = new LinkedList<>();
    private boolean trailEnabled = false;
    private int colorIndex = 1;

    public MouseSimulation() {
        super(new ScreenManager(), new GameInputsListenerOld());
    }

    @Override
    public void init() {
        super.init();

        Window window = screenManager.getFullScreenWindow();

        window.addMouseListener(this);
        window.addMouseMotionListener(this);
        window.addMouseWheelListener(this);
        window.addKeyListener(this);
    }

    @Override
    public synchronized void draw(MovaGraphics movaGraphics, long elapsedTime) {
        Window window = screenManager.getFullScreenWindow();

        movaGraphics.setColor(window.getBackground());
        movaGraphics.fillRect(0, 0, screenManager.getWidth(), screenManager.getHeight());

        movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        movaGraphics.setColor(window.getForeground());
        movaGraphics.drawString("MouseTest. Press Escape to exit.", 5, FONT_SIZE);

        int count = trailList.size();
        if (count > 1 && !trailEnabled) count = 1;

        for (int i = 0; i < count; i++) {
            Point point = trailList.get(i);
            movaGraphics.drawString("Hello World!", point.x, point.y);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) stop();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // DO NOTHING
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // DO NOTHING
    }

    @Override
    public void mousePressed(MouseEvent e) {
        trailEnabled = !trailEnabled;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public synchronized void mouseMoved(MouseEvent e) {
        trailList.addFirst(e.getPoint());
        while (trailList.size() > TRAIL_SIZE) trailList.removeLast();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        colorIndex = (colorIndex + e.getWheelRotation())%COLORS.length;
        if (colorIndex < 0) colorIndex += COLORS.length;

        Window window = screenManager.getFullScreenWindow();
        window.setForeground(COLORS[colorIndex]);
    }

    public static void main(String[] args) {
        new MouseSimulation().run();
    }
}
