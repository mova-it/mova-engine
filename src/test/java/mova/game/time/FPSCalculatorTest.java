package mova.game.time;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.concurrent.TimeUnit;
import java.util.stream.LongStream;

@RunWith(JUnit4.class)
public class FPSCalculatorTest {

    @Before
    public void clear() {
        FPSCalculator.clear();
    }

    @Test
    public void tac_test() {
        long[] dts = LongStream.generate(() -> 10).limit(51).toArray();
        for (int i = 0; i < dts.length - 1; i++) {
            Assert.assertEquals(0, FPSCalculator.tic(dts[i]), 0);
        }

        Assert.assertEquals(100, FPSCalculator.tic(dts[dts.length - 1]), 0);
    }

    @Test
    public void tac_test_micro() {
        long[] dts = LongStream.generate(() -> 10000).limit(51).toArray();
        for (int i = 0; i < dts.length - 1; i++) {
            Assert.assertEquals(0, FPSCalculator.tic(dts[i], TimeUnit.MICROSECONDS), 0);
        }

        Assert.assertEquals(100, FPSCalculator.tic(dts[dts.length - 1], TimeUnit.MICROSECONDS), 0);
    }
}
