package mova.game.patterns.doublebuffer;

abstract class Actor {

    private final String name;
    protected boolean slapped = false;

    public Actor(String name) {
        this.name = name;
    }

    abstract void update();

    void reset() {
        slapped = false;
    }

    void slap() {
        slapped = true;
        System.out.println(getName() + " has been slapped!");
    }

    boolean wasSlapped() {
        return slapped;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "name='" + name + '\'' +
                ", slapped=" + slapped +
                '}';
    }
}

class Stage {

    private static final int NUM_ACTORS = 3;
    Actor[] actors = new Actor[NUM_ACTORS];

    void add(Actor actor, int index) {
        actors[index] = actor;
        System.out.println(actor.getName() + " adding on the stage to place " + index);
    }

    void update() {
        for (Actor actor : actors) {
            actor.update();
            System.out.println("\t" + actor.getName() + " has been updated! => " + actor);
            actor.reset();
            System.out.println("\t" + actor.getName() + " has been reset! => " + actor);
        }

        System.out.println();
    }
}

class Comedian extends Actor {

    Actor facing;

    public Comedian(String name) {
        super(name);
    }

    void face(Actor facing) {
        this.facing = facing;
    }

    @Override
    void update() {
        if (wasSlapped()) {
            System.out.println(getName() + " was slapped, so he slap " + facing.getName());
            facing.slap();
        } else {
            System.out.println(getName() + " was not slapped, so he does nothing");
        }
    }
}

class BufferedComedian extends Comedian {

    protected boolean nextSlapped = false;

    public BufferedComedian(String name) {
        super(name);
    }

    void swap() {
        slapped = nextSlapped;
        nextSlapped = false;
    }

    @Override
    void slap() {
        nextSlapped = true;
        System.out.println(getName() + " has been slapped!");
    }

    @Override
    void update() {
        if (wasSlapped()) {
            System.out.println(getName() + " was slapped, so he slap " + facing.getName());
            facing.slap();
        } else {
            System.out.println(getName() + " was not slapped, so he does nothing");
        }
    }
}

class BufferedStage extends Stage {

    @Override
    void add(Actor actor, int index) {
        throw new UnsupportedOperationException();
    }

    void add(BufferedComedian actor, int index) {
        super.add(actor, index);
    }

    void update() {
        for (Actor actor : actors) {
            actor.update();
            System.out.println("\t" + actor.getName() + " has been updated! => " + actor);
        }
        for (Actor actor : actors) {
            ((BufferedComedian)actor).swap();
            System.out.println("\t" + actor.getName() + " has been swapped! => " + actor);
        }

        System.out.println();
    }
}


public class Stages {

    public static void main(String[] args) {
        Stage stage = new Stage();

        Comedian harry = new Comedian("Harry");
        Comedian baldy = new Comedian("Baldy");
        Comedian chump = new Comedian("Chump");

        harry.face(baldy);
        baldy.face(chump);
        chump.face(harry);

        stage.add(harry, 0);
        stage.add(baldy, 1);
        stage.add(chump, 2);

        System.out.println();

        harry.slap();
        stage.update();

//        System.out.println("\n\n");
//
//        stage.add(harry, 2);
//        stage.add(baldy, 1);
//        stage.add(chump, 0);

        System.out.println();

        harry.slap();
        stage.update();
        stage.update();
    }
}

// Note: L'idée ici c'est pas temps d'obtenir le même résultat que précédemment,
//  mais d'obtenir un résultat constant quelques soient les modifications apportées
//  à l'ordre des acteurs
 class BufferedStages {

    public static void main(String[] args) {
        BufferedStage stage = new BufferedStage();

        BufferedComedian harry = new BufferedComedian("Harry");
        BufferedComedian baldy = new BufferedComedian("Baldy");
        BufferedComedian chump = new BufferedComedian("Chump");

        harry.face(baldy);
        baldy.face(chump);
        chump.face(harry);

        stage.add(harry, 0);
        stage.add(baldy, 1);
        stage.add(chump, 2);

        System.out.println();

        harry.slap();
        stage.update();

//        System.out.println("\n\n");
//
//        stage.add(harry, 2);
//        stage.add(baldy, 1);
//        stage.add(chump, 0);

        System.out.println();

        harry.slap();
        stage.update();
        stage.update();
    }
}
