package mova.game.patterns.command;

import java.util.Scanner;

public class Solution1 {

    private GameCommand buttonX = new JumpCommand();
    private GameCommand buttonY = new FireGunCommand();

    public interface GameCommand {

        void execute();

    }

    static class JumpCommand implements GameCommand {
        @Override
        public void execute() {
            System.out.println("Jump");
        }
    }

    static class FireGunCommand implements GameCommand {
        @Override
        public void execute() {
            System.out.println("Gun fire");
        }
    }

    void handleInput(String button) {
        switch (button) {
            case "x": buttonX.execute();break;
            case "y": buttonY.execute();break;
            case "s": {
                GameCommand temp = buttonX;
                buttonX = buttonY;
                buttonY = temp;
                break;
            }
            // ...
            case "q": throw new QuitException();
            default:
                System.out.println("Aucune action assigné au bouton " + button);
        }
    }

    private static class QuitException extends RuntimeException {}

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();

            try {
                solution.handleInput(button);
            } catch (QuitException qe) {
                quit = true;
            }
        } while (!quit);
    }

}
