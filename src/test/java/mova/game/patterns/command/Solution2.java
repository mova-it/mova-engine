package mova.game.patterns.command;

import java.util.Scanner;

public class Solution2 {

    interface ContextAwareGameCommand<C> {
        void execute(C context);
    }

    private ContextAwareGameCommand<String> buttonX = new JumpCommand();
    private ContextAwareGameCommand<String> buttonY = new EatFruitCommand();

    static class JumpCommand implements ContextAwareGameCommand<String> {
        @Override
        public void execute(String context) {
            System.out.println("Make " + context + " jumping");
        }
    }

    static class EatFruitCommand implements ContextAwareGameCommand<String> {
        @Override
        public void execute(String context) {
            System.out.println("Make " + context + " eat fruit (and puke)");
        }
    }

    ContextAwareGameCommand<String> handleInput(String button) {
        switch (button) {
            case "x": return buttonX;
            case "y": return buttonY;
            case "s": {
                ContextAwareGameCommand<String> temp = buttonX;
                buttonX = buttonY;
                buttonY = temp;

                return null;
            }
            // ...
            case "q": throw new QuitException();
            default: return null;
        }
    }

    private static class QuitException extends RuntimeException {}

    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();

            try {
                ContextAwareGameCommand<String> gameCommand = solution.handleInput(button);
                if (gameCommand != null) {
                    System.out.println("Enter actor name: ");
                    String actor = scanner.nextLine();
                    gameCommand.execute(actor);
                }
            } catch (QuitException qe) {
                quit = true;
            }
        } while (!quit);
    }

}
