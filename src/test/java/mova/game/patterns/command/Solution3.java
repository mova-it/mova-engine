package mova.game.patterns.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface UndoableGameCommand extends Solution1.GameCommand {

    void execute();
    void undo();
}

class Player {
    public final String name;
    public int x = 50;

    public Player(String name) {
        this.name = name;
    }
}

class MovePlayerCommand implements UndoableGameCommand {

    private final Player player;
    private final int x;
    private int xBefore;

    public MovePlayerCommand(Player player, int x) {
        this.player = player;
        this.x = x;
    }

    @Override
    public void execute() {
        xBefore = player.x;
        player.x = x;

        System.out.println("Déplacement de " + player.name + " vers " + player.x);
    }

    @Override
    public void undo() {
        player.x = xBefore;

        System.out.println("Retour de " + player.name + " sur " + player.x);
    }
}

public class Solution3 {

    private final List<UndoableGameCommand> commands = new ArrayList<>();
    private int currentIndex = 0;

    private final Solution1.GameCommand undoCommand = new UndoCommand();
    private final Solution1.GameCommand redoCommand = new RedoCommand();

    Solution1.GameCommand handleInput(String button) {
        Player currentPlayer = getSelectedPlayer();

        switch (button) {
            case "z": {
                int newX = currentPlayer.x + 1;
                return new MovePlayerCommand(currentPlayer, newX);
            }
            case "s":  {
                int newX = currentPlayer.x - 1;
                return new MovePlayerCommand(currentPlayer, newX);
            }
            case "u": {
                return undoCommand;
            }
            case "r": {
                return redoCommand;
            }
            // ...
            case "q": throw new QuitException();
            default: return null;
        }
    }

    void addCommand(Solution1.GameCommand command) {
        if (!(command instanceof UndoableGameCommand)) return;

        commands.add(currentIndex++, (UndoableGameCommand) command);
        if (commands.size() > currentIndex) {
            commands.subList(currentIndex, commands.size()).clear();
        }
    }

    class UndoCommand implements Solution1.GameCommand {
        @Override
        public void execute() {
            if (currentIndex > 0) {
                System.out.println("Annulation de la commande à l'index " + (currentIndex - 1));

                UndoableGameCommand command = commands.get(--currentIndex);
                command.undo();
            } else {
                System.out.println("Aucune commande à annuler");
            }
        }
    }

    class RedoCommand implements Solution1.GameCommand {
        @Override
        public void execute() {
            if (currentIndex < commands.size()) {
                System.out.println("Relance de la commande à l'index " + currentIndex);

                UndoableGameCommand command = commands.get(currentIndex++);
                command.execute();
            } else {
                System.out.println("Aucune commande à relancer");
            }
        }
    }

    private static class QuitException extends RuntimeException {}

    private final Player player = new Player("Bro");

    private Player getSelectedPlayer() {
        return player;
    }

    public static void main(String[] args) {
        Solution3 solution = new Solution3();

        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();

            try {
                Solution1.GameCommand gameCommand = solution.handleInput(button);
                if (gameCommand != null) {
                    solution.addCommand(gameCommand);
                    gameCommand.execute();
                }
            } catch (QuitException qe) {
                quit = true;
            }

            System.out.println();
        } while (!quit);
    }
}
