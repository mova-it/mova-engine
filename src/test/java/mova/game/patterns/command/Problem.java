package mova.game.patterns.command;

/*
"Encapsulate a request as an object, thereby letting users
parameterize clients with different requests,queue, or log
requests, and support undoable operations."
*/

public class Problem {

    enum Button {
        X, Y, A, B
    }

    void handleInput() {
        if (isPresses(Button.X)) jump();
        else if (isPresses(Button.Y)) fireGun();
        else if (isPresses(Button.A)) swapWeappon();
        else if (isPresses(Button.B)) lurchIneffectively();
    }

    private boolean isPresses(Button b) {
        return false; // Return wether the button is pressed
    }

    private void jump() {
        // jump code
    }

    private void fireGun() {
        // gun fire code
    }

    private void swapWeappon() {
        // weappon swapping code
    }

    private void lurchIneffectively() {
        // lurch code
    }
}
