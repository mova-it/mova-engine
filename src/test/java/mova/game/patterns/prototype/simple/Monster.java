package mova.game.patterns.prototype.simple;

public abstract class Monster {

    @Override
    protected abstract Monster clone();
}
