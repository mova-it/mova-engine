package mova.game.patterns.prototype.simple;

public class Spawner {

    private final Monster prototype;

    public Spawner(Monster prototype) {
        this.prototype = prototype;
    }

    Monster spawn() {
        return prototype.clone();
    }
}
