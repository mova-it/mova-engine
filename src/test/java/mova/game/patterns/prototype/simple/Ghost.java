package mova.game.patterns.prototype.simple;

public class Ghost extends Monster {

    private int health;
    private int speed;

    public Ghost(int health, int speed) {
        this.health = health;
        this.speed = speed;
    }

    public void hit(int hurts) {
        health -= hurts;
    }

    @Override
    protected Monster clone() {
        return new Ghost(health, speed);
    }

    @Override
    public String toString() {
        return "Ghost{" +
                "health=" + health +
                ", speed=" + speed +
                '}';
    }
}
