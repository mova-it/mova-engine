package mova.game.patterns.prototype.generic;

import mova.game.patterns.prototype.simple.Ghost;

public class Main {

    public static void main(String[] args) {
        Spawner<Ghost> ghostSpawner = new Spawner<>(() -> new Ghost(100, 20));

        System.out.println("Create a ghost:");
        Ghost ghost1 = ghostSpawner.spawn();
        System.out.println("ghost1: " + ghost1);
        System.out.println("Hit the ghost:");
        ghost1.hit(15);
        System.out.println("ghost1: " + ghost1);

        System.out.println();

        System.out.println("Create another ghost:");
        Ghost ghost2 = ghostSpawner.spawn();
        System.out.println("ghost2: " + ghost2);
        System.out.println("ghost1: " + ghost1);
    }
}
