package mova.game.patterns.prototype.generic;

import mova.game.patterns.prototype.simple.Monster;

import java.util.function.Supplier;

public class Spawner<T extends Monster> {

    private final Supplier<T> monsterSuplier;

    public Spawner(Supplier<T> monsterSuplier) {
        this.monsterSuplier = monsterSuplier;
    }

    T spawn() {
        return monsterSuplier.get();
    }
}
