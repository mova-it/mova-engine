package mova.game.patterns.prototype.callback;

import mova.game.patterns.prototype.simple.Monster;

import java.util.function.Supplier;

public class Spawner {

    private final Supplier<Monster> monsterSuplier;

    public Spawner(Supplier<Monster> monsterSuplier) {
        this.monsterSuplier = monsterSuplier;
    }

    Monster spawn() {
        return monsterSuplier.get();
    }
}
