package mova.game.patterns.prototype.custom;

public interface Prototype<P> {
    String getName();
    P clone();
}

