package mova.game.patterns.prototype.custom;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import mova.util.CollectionUtils;
import mova.util.StringUtils;

import java.util.List;

/**
 * Créé par Mohicane le 09/03/2022.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
class Goblin implements Prototype<Goblin> {

    private String name;
    private int minHealth;
    private int maxHealth;
    private List<String> resists;
    private List<String> weaknesses;
    private List<String> spells;
    private List<String> attacks;

    @SuppressWarnings("unused")
    private Goblin() {
        // Utilisé par Jackson
    }

    private Goblin(Goblin goblin) {
        this.name = goblin.name;
        this.minHealth = goblin.minHealth;
        this.maxHealth = goblin.maxHealth;
        this.resists = goblin.resists;
        this.weaknesses = goblin.weaknesses;
        this.spells = goblin.spells;
        this.attacks = goblin.attacks;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Goblin{" +
                _includeIfPresent(name, "name='" + name + '\'') +
                ", minHealth=" + minHealth +
                ", maxHealth=" + maxHealth +
                _includeIfPresent(resists, ", resists=" + resists) +
                _includeIfPresent(weaknesses, ", weaknesses=" + weaknesses) +
                _includeIfPresent(spells, ", spells=" + spells) +
                _includeIfPresent(attacks, ", attacks=" + attacks) +
                '}';
    }

    private String _includeIfPresent(String test, String s) {
        return StringUtils.isNotBlank(test) ? s : StringUtils.EMPTY;
    }

    private String _includeIfPresent(List<String> test, String s) {
        return CollectionUtils.isNotEmpty(test) ? s : StringUtils.EMPTY;
    }

    @Override
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public Goblin clone() {
        return new Goblin(this);
    }
}
