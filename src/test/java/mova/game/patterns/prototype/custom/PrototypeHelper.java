package mova.game.patterns.prototype.custom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class PrototypeHelper {

    private static final String PROTOTYPE_FIELD_NAME = "prototype";
    private static final String PROTOTYPE_CLASS_FIELD_NAME = "class";

    ObjectMapper mapper = new ObjectMapper();

    // TODO est-il possible d'avoir en plus plusieurs classes?

    public Map<Class<? extends Prototype<?>>, List<? super Prototype<?>>> load(URL resource) throws IOException {
        JsonNode rootNode = mapper.readTree(resource);
        if (!rootNode.isArray()) throw new PrototypeFormatException(); // TODO gérer le cas ou il n'y a qu'un objet

        Map<Class<? extends Prototype<?>>, List<? super Prototype<?>>> prototypes = new HashMap<>();

        Iterator<JsonNode> nodes = rootNode.iterator();
        while (nodes.hasNext()) {
            JsonNode node = nodes.next();
            if (!node.isObject()) throw new PrototypeFormatException();

            if (_isBasePrototype(node)) {
                Class<? extends Prototype<?>> prototypeClass = _getClass(node);

                Prototype<?> prototype = mapper.treeToValue(node, prototypeClass);
                prototypes.putIfAbsent(prototypeClass, new ArrayList<>());
                prototypes.get(prototypeClass).add(prototype);

                nodes.remove();
            }
        }

        return prototypes;
    }

    @SuppressWarnings("unchecked")
    private Class<? extends Prototype<?>> _getClass(JsonNode node) {
        try {
            Class<?> clazz = Class.forName(node.get(PROTOTYPE_CLASS_FIELD_NAME).textValue());
            ((ObjectNode) node).remove(PROTOTYPE_CLASS_FIELD_NAME);
            return (Class<? extends Prototype<?>>) clazz.asSubclass(Prototype.class);
        } catch (ClassNotFoundException cnfe) {
            throw new PrototypeFormatException(cnfe);
        }
    }

    public <T extends Prototype<T>> List<T> load(URL resource, Class<T> clazz) throws IOException {
        JsonNode rootNode = mapper.readTree(resource);
        if (!rootNode.isArray()) throw new PrototypeFormatException(); // TODO gérer le cas ou il n'y a qu'un objet

        Map<String, T> prototypes = _consumeBasePrototypes(rootNode, clazz);

        while (rootNode.size() > 0) {
            int size = prototypes.size();

            _createNextPrototypeLayer(rootNode, clazz, prototypes);

            if (size == prototypes.size()) throw new PrototypeCycleException();
        }

        return new ArrayList<>(prototypes.values());
    }

    private <T extends Prototype<T>> Map<String, T> _consumeBasePrototypes(JsonNode rootNode, Class<T> clazz) throws JsonProcessingException {
        Map<String, T> prototypes = new HashMap<>();

        Iterator<JsonNode> nodes = rootNode.iterator();
        while (nodes.hasNext()) {
            JsonNode node = nodes.next();
            if (!node.isObject()) throw new PrototypeFormatException();

            if (_isBasePrototype(node)) {
                T prototype = mapper.treeToValue(node, clazz);
                prototypes.put(prototype.getName(), prototype);

                nodes.remove();
            }
        }

        return prototypes;
    }

    private <T extends Prototype<T>> void _createNextPrototypeLayer(JsonNode rootNode, Class<T> clazz, Map<String, T> prototypes) throws JsonProcessingException {
        Iterator<JsonNode> remainingNodes = rootNode.iterator();
        while (remainingNodes.hasNext()) {
            JsonNode remainingNode = remainingNodes.next();
            String prototypeName = remainingNode.get(PROTOTYPE_FIELD_NAME).textValue();

            if (prototypes.containsKey(prototypeName)) {
                ((ObjectNode) remainingNode).remove(PROTOTYPE_FIELD_NAME);

                T prototype = _createFromPrototype(prototypes.get(prototypeName), remainingNode, clazz);
                prototypes.put(prototype.getName(), prototype);

                remainingNodes.remove();
            }
        }
    }

    private boolean _isBasePrototype(JsonNode node) {
        return !node.has(PROTOTYPE_FIELD_NAME);
    }

    private <T extends Prototype<T>> T _createFromPrototype(T basePrototype, JsonNode updatedData, Class<T> clazz) throws JsonProcessingException {
        T clone = basePrototype.clone();

        return mapper.readerForUpdating(clone).treeToValue(updatedData, clazz);
    }

    private static class PrototypeException extends RuntimeException {

        public PrototypeException() {
        }

        public PrototypeException(Throwable throwable) {
            super(throwable);
        }

    }

    private static class PrototypeFormatException extends PrototypeException {

        public PrototypeFormatException() {
        }

        public PrototypeFormatException(Throwable throwable) {
            super(throwable);
        }
    }

    private static class PrototypeCycleException extends PrototypeException {

    }

    public static void main(String[] args) throws IOException {
        PrototypeHelper prototypeHelper = new PrototypeHelper();

        URL resource = PrototypeHelper.class.getResource("goblins.json");

        List<Goblin> goblins = prototypeHelper.load(resource, Goblin.class);
        goblins.forEach(System.out::println);

        System.out.println();

        resource = PrototypeHelper.class.getResource("orcs.json");

        List<Orc> orcs = prototypeHelper.load(resource, Orc.class);
        orcs.forEach(System.out::println);

        System.out.println();

        resource = PrototypeHelper.class.getResource("orcsAndGoblins.json");
        Map<Class<? extends Prototype<?>>, List<? super Prototype<?>>> orcsAndGoblinsMap = prototypeHelper.load(resource);
        for (Class<? extends Prototype<?>> key : orcsAndGoblinsMap.keySet()) {
            System.out.println(key + ": ");
            orcsAndGoblinsMap.get(key).forEach(System.out::println);
        }

    }

}

