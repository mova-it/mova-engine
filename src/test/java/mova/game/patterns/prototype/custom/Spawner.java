package mova.game.patterns.prototype.custom;

/**
 * Créé par Mohicane le 09/03/2022.
 */
class Spawner<M> {

    private final Monster<M> prototype;

    protected Spawner(Monster<M> prototype) {
        this.prototype = prototype;
    }

    public M spawn() {
        return prototype.clone();
    }
}
