package mova.game.patterns.prototype.custom;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import mova.util.CollectionUtils;
import mova.util.StringUtils;

import java.util.List;

/**
 * Créé par Mohicane le 09/03/2022.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
class Orc implements Prototype<Orc> {

    private String name;
    private int minHealth;
    private int maxHealth;
    private List<String> resists;
    private List<String> weaknesses;
    private List<String> spells;
    private List<String> attacks;

    @SuppressWarnings("unused")
    public Orc() {
        // Utilisé par Jackson
    }

    private Orc(Orc orc) {
        this.name = orc.name;
        this.minHealth = orc.minHealth;
        this.maxHealth = orc.maxHealth;
        this.resists = orc.resists;
        this.weaknesses = orc.weaknesses;
        this.spells = orc.spells;
        this.attacks = orc.attacks;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Orc{" +
                _includeIfPresent(name, "name='" + name + '\'') +
                ", minHealth=" + minHealth +
                ", maxHealth=" + maxHealth +
                _includeIfPresent(resists, ", resists=" + resists) +
                _includeIfPresent(weaknesses, ", weaknesses=" + weaknesses) +
                _includeIfPresent(spells, ", spells=" + spells) +
                _includeIfPresent(attacks, ", attacks=" + attacks) +
                '}';
    }

    private String _includeIfPresent(String test, String s) {
        return StringUtils.isNotBlank(test) ? s : StringUtils.EMPTY;
    }

    private String _includeIfPresent(List<String> test, String s) {
        return CollectionUtils.isNotEmpty(test) ? s : StringUtils.EMPTY;
    }

    @Override
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public Orc clone() {
        return new Orc(this);
    }
}
