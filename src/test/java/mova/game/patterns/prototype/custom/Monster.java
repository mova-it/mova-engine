package mova.game.patterns.prototype.custom;

//TODO a mon avis c'est a revoir le Monster<P>
abstract class Monster<P> implements Prototype<P> {

    public String getName() {
        return getClass().getSimpleName();
    }

    public abstract P clone();
}
