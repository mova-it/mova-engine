package mova.game.patterns.datalocality;

/* Note:
- Analogy:
    =>
        Comptable 1 minute pour une boite
        Boite stockée dans un entrepot => cariste 1 journée avec un transpalette

        Optimisation remarques:
        - souvent la boite suivante est juste à coté dans l'entrepot
        - utilisé un transpalette pour une boite c'est peut être too much
        - il y a des pièces disponible dans le bureau du comptable

        => On demande au cariste de ramener une palette entière

- Attends on parle de gestion de mémoire mais on a une JVM et un GC?! Alors quelle structure de données permet de gérer ca la continuité des données en mémoire.
- cache miss is terrible => indirection = cachemiss
- 2 manière de tenir compte de DataLocality arranger la mémoire ou maintenir trier la mémoire
 */


public class DataLocality {

    public static void main(String[] args) {
        int nbEntities = 10000;
        int nbCycles = 10000;

//        int nbEntities = 100000;
//        int nbCycles = 1000;

//        int nbEntities = 1000000;
//        int nbCycles = 100;

//        int nbEntities = 10000000;
//        int nbCycles = 10;

//        int nbEntities = 10000000;
//        int nbCycles = 100;

        double startTime, endTime, time;

        startTime = System.currentTimeMillis();

        GameEntity[] entities = new GameEntity[nbEntities];
        for (int i = 0; i < nbEntities; i++) {
            AIComponent aiComponent = new AIComponent(i);
            PhysicsComponent physicsComponent = new PhysicsComponent(i);
            RenderComponent renderComponent = new RenderComponent(i);
            entities[i] = new GameEntity(aiComponent, physicsComponent, renderComponent);
        }

        endTime = System.currentTimeMillis();
        time = endTime - startTime;
        System.out.println("Creation Time: time = " + time + "ms, thus " + time/entities.length + "ms per entity");

        startTime = System.currentTimeMillis();

        for (int cycle = 0; cycle < nbCycles; cycle++) {
            for (GameEntity entity : entities) {
                entity.aiComponent.update();
            }

            for (GameEntity entity : entities) {
                entity.physicsComponent.update();
            }

            for (GameEntity entity : entities) {
                entity.renderComponent.render();
            }
        }

        endTime = System.currentTimeMillis();
        time = endTime - startTime;
        System.out.println("[Enhanced for loops] Cycles Time: time = " + time + "ms, thus " + time/nbCycles + "ms per cycle");

        System.out.println();

        startTime = System.currentTimeMillis();

        entities = new GameEntity[nbEntities];
        AIComponent[] aiComponents = new AIComponent[nbEntities];
        PhysicsComponent[] physicsComponents = new PhysicsComponent[nbEntities];
        RenderComponent[] renderComponents = new RenderComponent[nbEntities];
        for (int i = 0; i < nbEntities; i++) {
            aiComponents[i] = new AIComponent(i);
            physicsComponents[i] = new PhysicsComponent(i);
            renderComponents[i] = new RenderComponent(i);
            entities[i] = new GameEntity(aiComponents[i], physicsComponents[i], renderComponents[i]);
        }

        endTime = System.currentTimeMillis();
        time = endTime - startTime;
        System.out.println("Creation Time: time = " + time + "ms, thus " + time/entities.length + "ms per entity");

        startTime = System.currentTimeMillis();

        for (int cycle = 0; cycle < nbCycles; cycle++) {
            for (AIComponent aiComponent : aiComponents) {
                aiComponent.update();
            }

            for (PhysicsComponent physicsComponent : physicsComponents) {
                physicsComponent.update();
            }

            for (RenderComponent renderComponent : renderComponents) {
                renderComponent.render();
            }
        }

        endTime = System.currentTimeMillis();
        time = endTime - startTime;
        System.out.println("[Enhanced DATA LOCALITY] Cycles Time: time = " + time + "ms, thus " + time/nbCycles + "ms per cycle");

    }
}

class GameEntity {

    AIComponent aiComponent;
    PhysicsComponent physicsComponent;
    RenderComponent renderComponent;

    public GameEntity(AIComponent aiComponent, PhysicsComponent physicsComponent, RenderComponent renderComponent) {
        this.aiComponent = aiComponent;
        this.physicsComponent = physicsComponent;
        this.renderComponent = renderComponent;
    }
}

class AIComponent {

    public int id;
    private int cumulate = 0;

    public AIComponent(int id) {
        this.id = id;
    }

    public void update() {
        cumulate += 1 + cumulate*2 - (2*cumulate);
    }
}

class PhysicsComponent {

    public int id;
    private int cumulate = 0;

    public PhysicsComponent(int id) {
        this.id = id;
    }

    public void update() {
        cumulate += 1 + cumulate*2 - (2*cumulate);
    }
}

class RenderComponent {

    public int id;
    private int cumulate = 0;

    public RenderComponent(int id) {
        this.id = id;
    }

    public void render() {
        cumulate += 1 + cumulate*2 - (2*cumulate);
    }
}
