package mova.game.patterns.datalocality;

public class SlowPeasSystem {

    private final Pea[] peas = new Pea[Pea.MAX_PEAS];
    private final int numPeas;

    public SlowPeasSystem(int numPeas) {
        this.numPeas = numPeas;

        int numActive = 0;
        for (int i = 0; i < numPeas; i++) {
            peas[i] = new Pea(i);
            if (peas[i].isActive()) numActive++;
        }

        System.out.println(numActive + " active peas / " + numPeas + " total peas");
    }

    public void update() {
        for (int i = 0; i < numPeas; i++) {
            if (peas[i].isActive()) peas[i].update();
        }
    }
}
