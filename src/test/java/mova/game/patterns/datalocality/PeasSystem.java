package mova.game.patterns.datalocality;

public class PeasSystem {

    private final Pea[] peas = new Pea[Pea.MAX_PEAS];
    private int numActive;

    public PeasSystem(int numPeas) {
        this.numActive = numPeas;
        for (int i = 0; i < numPeas; i++) peas[i] = new Pea(i);

        for (int i = numPeas - 1; i >= 0; i--) {
            if (!peas[i].isActive()) deactivatePea(i);
        }

        System.out.println(numActive + " active peas / " + numPeas + " total peas");
    }

    public void update() {
        for (int i = 0; i < numActive; i++) {
            if (peas[i].isActive()) peas[i].update();
        }
    }

    public void activatePea(int index) {
        if (index <= numActive) return;

        Pea temp = peas[numActive];
        peas[numActive] = peas[index];
        peas[index] = temp;

        numActive++;
    }

    public void deactivatePea(int index) {
        if (index > numActive) return;

        numActive--;

        Pea temp = peas[numActive];
        peas[numActive] = peas[index];
        peas[index] = temp;
    }
}
