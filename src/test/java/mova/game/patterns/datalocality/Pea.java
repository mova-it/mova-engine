package mova.game.patterns.datalocality;

public class Pea {

    static final int MAX_PEAS = 1000000;
    static final int NUM_PEAS = 1000000;
    static final int NB_CYCLES = 1000;

    public int id;
    private int cumulate = 0;
    private boolean active;

    public Pea(int id) {
        this.id = id;
        active = mustBeActive(this);
    }

    public void update() {
        cumulate++;
    }

    public boolean isActive() {
        return active;
    }

    public static boolean mustBeActive(Pea pea) {
        return pea.id%100 == 0;
    }

    public static void main(String[] args) {
        SlowPeasSystem slowPeasSystem = new SlowPeasSystem(Pea.NUM_PEAS);

        double startTime = System.currentTimeMillis();
        for (int i = 0; i < Pea.NB_CYCLES; i++) slowPeasSystem.update();
        double endTime = System.currentTimeMillis();
        System.out.println("Slow Time = " + (endTime - startTime) + " ms");
        System.out.println("Slow ops = " + 1000/(endTime - startTime) + " ops/s");

        System.out.println();

        PeasSystem peas = new PeasSystem(Pea.NUM_PEAS);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < Pea.NB_CYCLES; i++) peas.update();
        endTime = System.currentTimeMillis();
        System.out.println("Opti Time = " + (endTime - startTime) + " ms");
        System.out.println("Opti ops = " + 1000/(endTime - startTime) + " ops/s");
    }
}
