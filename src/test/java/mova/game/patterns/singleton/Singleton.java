package mova.game.patterns.singleton;

import mova.lang.ThreadUtils;

/*
Note:
    On pourrait faire de plein de manière différente mais une des premières contraintes c'est
    que l'on souhaite que notre instance soit créée le plus tard possible pour:
    - ne pas occupé de la mémoire pour rien
    - pouvoir utilisé des paramètres liés au Runtime au moment de sa création
    - potentiellement dépendre d'un ordre de création
    - fonctionner dans un cadre multithreads
*/
public class Singleton {

    private static Singleton INSTANCE;

    private Singleton() {
        System.out.println("Création du Singleton");
    }

    public String getReady() {
        return "READY!";
    }

    public static Singleton getSingleton() {
        if (INSTANCE == null) INSTANCE = new Singleton();

        return INSTANCE;
    }
}

class SimpleMain {

    public static void main(String[] args) {
        Singleton singleton = Singleton.getSingleton();
        System.out.println(singleton + ": " + singleton.getReady() + "\n");

        singleton = Singleton.getSingleton();
        System.out.println(singleton + ": " + singleton.getReady() + "\n");
    }

}




















class MainHelper {

    static void runTenThread(Runnable runnable) {
        Thread[] threads = new Thread[] {
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
                new Thread(runnable),
        };

        for (Thread thread : threads) thread.start();
    }
}


class ConcurentSingleton {

    public static void main(String[] args) {
        MainHelper.runTenThread( () -> {
            Singleton singleton = Singleton.getSingleton();
            System.out.println(singleton + ": " + singleton.getReady() + "\n");
        });
    }
}













class LongSingleton {

    private static LongSingleton INSTANCE;

    private LongSingleton() {
        System.out.println("Création du Singleton");
        ThreadUtils.takeANap(100);
    }

    public String getReady() {
        return "READY!";
    }

    public static LongSingleton getSingleton() {
        if (INSTANCE == null) INSTANCE = new LongSingleton();

        return INSTANCE;
    }
}

class ConcurentLongSingleton {

    public static void main(String[] args) {
        MainHelper.runTenThread( () -> {
            LongSingleton singleton = LongSingleton.getSingleton();
            System.out.println(singleton + ": " + singleton.getReady() + "\n");
        });
    }
}
















class SafeSingleton {

    private static final Object LOCK = new Object();

    private static volatile SafeSingleton INSTANCE;

    private SafeSingleton() {
        System.out.println("Création du Singleton");
        ThreadUtils.takeANap(100);
    }

    public String getReady() {
        return "READY!";
    }

    public static SafeSingleton getSingleton() {
        if (INSTANCE == null) {
            synchronized (LOCK) {
                if (INSTANCE == null) INSTANCE = new SafeSingleton();
            }
        }

        return INSTANCE;
    }
}

class ConcurentSafeSingleton {

    public static void main(String[] args) {
        MainHelper.runTenThread(() -> {
            SafeSingleton singleton = SafeSingleton.getSingleton();
            System.out.println(singleton + ": " + singleton.getReady() + "\n");
        });
    }
}


class FlaggedSingleton {

    private static final Object LOCK = new Object();

    private static boolean instanciated = false;

    public FlaggedSingleton() {
        synchronized (LOCK) {
            if (instanciated) throw new IllegalAccessError("Impossible de réinstantier FlaggedSingleton");
            instanciated = true;
        }
        System.out.println("Création du Singleton");
    }

    @Override
    protected void finalize() {
        instanciated = false;
    }

    public String getReady() {
        return "READY!";
    }
}

class ConcurentFlaggedSingleton {

    public static void main(String[] args) {
        MainHelper.runTenThread(() -> {
            FlaggedSingleton singleton = new FlaggedSingleton();
            System.out.println(singleton + ": " + singleton.getReady() + "\n");
        });
    }
}
