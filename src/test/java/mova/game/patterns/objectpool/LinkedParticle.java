package mova.game.patterns.objectpool;

public class LinkedParticle {

    private int framesLeft;
    private double x;
    private double y;
    private double xVel;
    private double yVel;

    protected LinkedParticle next;

    public LinkedParticle getNext() {
        return next;
    }

    public void setNext(LinkedParticle next) {
        this.next = next;
    }

    public void init(double x, double y, double xVel, double yVel, int lifeTime) {
        this.x = x;
        this.y = y;
        this.xVel = xVel;
        this.yVel = yVel;
        this.framesLeft = lifeTime;
    }

    public boolean animate() {
        if (!inUse()) return false;

        framesLeft--;
        x += xVel;
        y += yVel;

        return framesLeft == 0;
    }

    public boolean inUse() {
        return framesLeft > 0;
    }
}
