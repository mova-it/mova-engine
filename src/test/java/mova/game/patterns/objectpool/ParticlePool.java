package mova.game.patterns.objectpool;

public class ParticlePool {

    private static final int POOL_SIZE = 100;

    private final Particle[] particles = new Particle[POOL_SIZE];

    public ParticlePool() {
        for (int i = 0; i < POOL_SIZE; i++) {
            particles[i] = new Particle();
        }
    }

    public void create(double x, double y, double xVel, double yVel, int lifeTime) {
        for (Particle particle : particles) {
            if (!particle.inUse()) {
                particle.init(x, y, xVel, yVel, lifeTime);
                break;
            }
        }
    }

    public void animate() {
        for (Particle particle : particles) particle.animate();
    }
}
