package mova.game.patterns.objectpool;

public class LinkedPartciclePool {

    private static final int POOL_SIZE = 100;

    private final LinkedParticle[] particles = new LinkedParticle[POOL_SIZE];

    private LinkedParticle firstAvailable;

    public LinkedPartciclePool() {
        for (int i = 0; i < POOL_SIZE; i++) {
            particles[i] = new LinkedParticle();
        }

        firstAvailable = particles[0];

        for (int i = 0; i < POOL_SIZE - 1; i++) {
            particles[i].setNext(particles[i + 1]);
        }

        particles[POOL_SIZE - 1].setNext(null);
    }

    public void create(double x, double y, double xVel, double yVel, int lifeTime) {
        if (firstAvailable == null) return;

        LinkedParticle newParticle = firstAvailable;
        firstAvailable = newParticle.getNext();

        newParticle.init(x, y, xVel, yVel, lifeTime);
    }

    public void animate() {
        for (LinkedParticle particle : particles) {
            if (particle.animate()) {
                particle.setNext(firstAvailable);
                firstAvailable = particle;
            }
        }
    }
}
