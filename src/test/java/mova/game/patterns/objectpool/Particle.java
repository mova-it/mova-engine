package mova.game.patterns.objectpool;

public class Particle {

    private int framesLeft;
    private double x;
    private double y;
    private double xVel;
    private double yVel;

    public void init(double x, double y, double xVel, double yVel, int lifeTime) {
        this.x = x;
        this.y = y;
        this.xVel = xVel;
        this.yVel = yVel;
        this.framesLeft = lifeTime;
    }

    public void animate() {
        framesLeft--;
        x += xVel;
        y += yVel;
    }

    public boolean inUse() {
        return framesLeft > 0;
    }
}
