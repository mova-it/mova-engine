package mova.game.patterns;

// TODO utilisé ca pour les commandes dans le jeu. Mais laissé un pattern prêt dans le moteur.

import java.util.Scanner;

public interface GameCommand {

    void execute();

}

interface UndoableGameCommand extends GameCommand {

    void undo();
}

abstract class AbstractContextAwareCommand<C> implements GameCommand {

    protected C context;

    AbstractContextAwareCommand(C context) {
        setContext(context);
    }

    protected void setContext(C context) {
        this.context = context;
    }

    public void execute(C context) {
        //TODO ca peut poser un problem en cas d'accès concurent
        setContext(context);
        execute();
    }
}

class JumpCommand implements GameCommand {

    @Override
    public void execute() {
        System.out.println("Jump");
    }

}

class ActorJumpCommand extends AbstractContextAwareCommand<String> {

    ActorJumpCommand(String context) {
        super(context);
    }

    @Override
    public void execute() {
        System.out.printf("Make %s jump%n", context);
    }
}

class DynaActorJumpCommand extends AbstractContextAwareCommand<String> {

    DynaActorJumpCommand() {
        super(null);
    }

    @Override
    public void execute() {
        System.out.printf("Make %s jump%n", context);
    }
}

class MoveCommand extends AbstractContextAwareCommand<String> implements UndoableGameCommand {

    private final int x;
    private final int y;

    MoveCommand(String context, int x, int y) {
        super(context);

        this.x = x;
        this.y = y;
    }

    @Override
    public void execute() {
        System.out.printf("Make %s move to (%d, %d)%n", context, x, y);
    }

    @Override
    public void undo() {
        System.out.printf("Make %s move back from (%d, %d)%n", context, x, y);
    }
}

class DynaActorJumpCommandExample {

    static DynaActorJumpCommand dynaActorJumpCommand = new DynaActorJumpCommand();

    public static GameCommand handleInput(String button) {
        switch (button) {
            case "X": return new JumpCommand();
            case "Y": return new ActorJumpCommand("Fred");
            case "A": return dynaActorJumpCommand;
            case "M": return new MoveCommand("Hector", 10, 18);
            case "Q": return () -> {
                throw new QuitException();
            };
            default: return null;
        }
    }

    private static class QuitException extends RuntimeException {

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();  // Read user input

            try {
                GameCommand command = handleInput(button);
                if (command != null) command.execute();
                else System.out.println("Aucune commande ne correspond au bouton " + button);
            } catch (QuitException qe) {
                quit = true;
            }
        } while (!quit);

        System.out.println("\n\n\nSuite du programme.");

        DynaActorJumpCommand dynaActorJumpCommand = new DynaActorJumpCommand();
        dynaActorJumpCommand.setContext("Dyna Gustave");

        MoveCommand moveCommand = new MoveCommand("Hector", 10, 18);

        GameCommand[] commands = new GameCommand[]{
                new JumpCommand(),
                new ActorJumpCommand("Fred"),
                dynaActorJumpCommand,
                moveCommand
        };

        for (GameCommand command : commands) {
            command.execute();
        }

        dynaActorJumpCommand.setContext("Dyna Alfonse");
        dynaActorJumpCommand.execute();

        dynaActorJumpCommand.execute("Dyna Proctor");
        dynaActorJumpCommand.execute("Dyna Hans");

        moveCommand.undo();
    }
}
