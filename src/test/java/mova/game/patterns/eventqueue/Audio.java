package mova.game.patterns.eventqueue;

/*
- Système "correct", mais cela présume qu'on va jouer tous les sons d'une traite à chaque update
 */
public class Audio extends AbstractAudio {

    private static final int MAX_PENDING = 16;

    private static final PlayMessage[] playMessages = new PlayMessage[MAX_PENDING];
    private static int numPending = 0;

    public static void playSound(long soundId, int volume) {
        playMessages[numPending++] = new PlayMessage(soundId, volume);
    }

    public void update() {
        for (int i = 0; i < numPending; i++) {
            PlayMessage playMessage = playMessages[i];
            String resource = AbstractAudio.loadSound(playMessage.getSoundId());
            AbstractAudio.startSound(resource, playMessage.getVolume());
        }

        numPending = 0;
    }
}
