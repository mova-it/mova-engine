package mova.game.patterns.eventqueue;

import mova.lang.MathUtils;

import java.lang.reflect.Array;
import java.util.*;

// TODO attention au modification concurrente
public class RingBuffer<E> implements Queue<E> {

    private static final int DEFAULT_CAPACITY = 10;

    private final Object[] elements;
    private int head = 0;
    private int tail = 0;

    public RingBuffer() {
        this(DEFAULT_CAPACITY);
    }

    public RingBuffer(int initialCapacity) {
        // L'implémentation du Ringbuffer fait que tail ne peut jamais rejoindre head
        // Le tail élément sert donc de marqueur pour la fin du RingBuffer
        // Si l'on veut que la capacité du RingBuffer corresponde vraiment à initialCapacity
        // il faut donc ajouter 1 à la capacité déclarée.
        elements = new Object[initialCapacity + 1];
    }

    @Override
    public boolean isEmpty() {
        return head == tail;
    }

    @SuppressWarnings("unchecked")
    private E _elementData(int index) {
        return (E) elements[index];
    }

    private int _nextIndex(int index) {
        return MathUtils.euclidianModulo(index + 1, elements.length);
    }

    private int _previousIndex(int index) {
        return MathUtils.euclidianModulo(index - 1, elements.length);
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            private int index = head;
            private int lastRet = -1;

            @Override
            public boolean hasNext() {
                return index != tail;
            }

            @Override
            public E next() {
                if (!_isIndexInRange(index)) throw new NoSuchElementException();

                E element = _elementData(index);

                lastRet = index;
                index = _nextIndex(index);

                return element;
            }

            @Override
            public void remove() {
                if (lastRet < 0) throw new IllegalStateException();

                _remove(lastRet);
                index = lastRet;
                lastRet = -1;
            }

            private boolean _isIndexInRange(int index) {
                if (index < 0 || index > elements.length) return false;

                int range = tail - head;

                return range >= 0
                        ? index >= head && index < tail
                        : index >= head || index < tail;
            }
        };
    }

    @Override
    public int size() {
        return _distanceToTail(head);
    }

    private int _distanceToTail(int i) {
        int range = tail - i;

        return range >= 0 ? range : elements.length + range;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) throw new NullPointerException();

        for (E e : this) {
            if (o.equals(e)) return true;
        }

        return false;
    }

    @Override
    public Object[] toArray() {
        int size = size();

        Object[] elements = new Object[size];
        if (tail >= head) {
            System.arraycopy(this.elements, head, elements, 0, size);
        } else {
            int firstPartSize = this.elements.length - head;
            System.arraycopy(this.elements, head, elements, 0, firstPartSize);
            System.arraycopy(this.elements, 0, elements, firstPartSize, tail);
        }

        return elements;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        @SuppressWarnings("unchecked")
        T[] source = (T[]) this.elements;

        int size = size();
        @SuppressWarnings("unchecked")
        T[] elements = a.length >= size
                ? a
                : (T[]) Array.newInstance(a.getClass().getComponentType(), size);

        if (tail >= head) {
            System.arraycopy(source, head, elements, 0, size);
        } else {
            int firstPartSize = this.elements.length - head;
            System.arraycopy(source, head, elements, 0, firstPartSize);
            System.arraycopy(source, 0, elements, firstPartSize, tail);

        }

        return elements;
    }

    @Override
    public boolean offer(E element) {
        if (element == null) throw new NullPointerException();

        int nextIndex = _nextIndex(tail);
        if (nextIndex == head) return false;

        elements[tail] = element;

        tail = nextIndex;

        return true;
    }

    @Override
    public boolean add(E e) {
        if (offer(e)) return true;
        else throw new IllegalStateException("Tête à queue");
    }

    @Override
    public E poll() {
        if (isEmpty()) return null;

        E element = _elementData(head);
        elements[head] = null;
        head = _nextIndex(head);

        return element;
    }

    @Override
    public E remove() {
        E x = poll();
        if (x != null) return x;
        else throw new NoSuchElementException();
    }

    @Override
    public E element() {
        E x = peek();
        if (x != null) return x;
        else throw new NoSuchElementException();
    }

    @Override
    public E peek() {
        if (isEmpty()) return null;

        return _elementData(head);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object e : c) {
            if (!contains(e)) return false;
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        Object[] a = c.toArray();
        int numNew = a.length;

        int range = tail - head;
        int size = range >= 0 ? range : elements.length + range;

        if (size + numNew > elements.length - 1) throw new ArrayIndexOutOfBoundsException();

        int numTilEnd = range >= 0 ? elements.length - tail : -range;

        if (numTilEnd < numNew) {
            System.arraycopy(a, 0, elements, tail, numTilEnd);
            System.arraycopy(a, numTilEnd, elements, 0, numNew - numTilEnd);
        } else {
            System.arraycopy(a, 0, elements, tail, numNew);
        }

        tail = MathUtils.euclidianModulo(tail + numNew, elements.length);

        return numNew != 0;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null) throw new NullPointerException();

        return batchRemove(c, false);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if (c == null) throw new NullPointerException();

        return batchRemove(c, true);
    }

    private boolean batchRemove(Collection<?> c, boolean complement) {
        boolean modified = false;

        int w = head;
        for (int r = head; r != tail; r = _nextIndex(r)) {
            if (c.contains(elements[r]) == complement) {
                elements[w] = elements[r];
                w = _nextIndex(w);
            }
        }

        if (w != tail) {
            for (int i = w; i != tail; i = _nextIndex(i)) {
                elements[i] = null;
            }

            tail = w;
            modified = true;
        }

        return modified;
    }

    @Override
    public void clear() {
        for (int i = head; i != tail; i = _nextIndex(i)) {
            elements[i] = null;
        }

        tail = head;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) throw new NullPointerException();

        for (int i = head; i != tail; i = _nextIndex(i)) {
            if (elements[i].equals(o)) {
                _remove(i);

                return true;
            }
        }

        return false;
    }

    private void _remove(int i) {
        // On lâche la référence
        elements[i] = null;

        // Si on supprime le premier élément
        if (i == head) {
            head = _nextIndex(head);
            return;
        }

        // on récupère l'index précédent tail
        int previousTail = _previousIndex(tail);

        // Si on supprime le dernier élément
        if (i == previousTail) {
            tail = previousTail;
            return;
        }

        // si i ou head est avant tail
        if (i < tail || head < tail) {

            // On récupère l'index suivant
            int nextIndex = _nextIndex(i);

            // On rassemble les 2 parties du tableau coupées en i ; soit de 0 ou head à i et de i à tail
            // (On décale vers 0 ou head la seconde partie du tableau d'un élément)
            System.arraycopy(elements, nextIndex, elements, i, tail - nextIndex);

            // On décrémente tail, puis on lâche la référence sur tail
            // (qui a été copiée avec l'instruction précédente)
            tail = previousTail;
            elements[tail] = null;
        } else {
            // On rassemble les 2 parties du tableau coupé en i ; soit de head à i et de i à tail ou elements.length
            // (On décale vers la tail ou elements.length la première partie du tableau d'un élément)
            System.arraycopy(elements, head, elements, head + 1, i - head);

            // On lâche la référence sur head (qui a été copiée avec
            // l'instruction précédente), puis on incrémente head
            elements[head] = null;
            head = _nextIndex(head);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        for (int i = 0; i < elements.length; i++) {
            if (i == head) builder.append("HEAD => ");

            if (i == tail) {
                builder.append("TAIL, ");
            } else {
                builder.append(elements[i]).append(", ");
            }
        }

        builder.replace(builder.length() - 2, builder.length(), "]");

        return builder.toString();
    }

    public static void main(String[] args) {
        RingBuffer<Integer> ints = new RingBuffer<>(11);
        System.out.println(ints);

        ints.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        System.out.println(ints);
        System.out.println(Arrays.toString(ints.toArray(new Integer[0])));

        for (int i = 0; i < 3; i++) ints.remove();
        System.out.println(ints);

        ints.addAll(Arrays.asList(11, 12, 13));
        System.out.println(ints);
        System.out.println(Arrays.toString(ints.toArray(new Integer[0])));


        ints.remove(9);
        System.out.println(ints);

        System.out.println("\n");

        ints = new RingBuffer<>(16);

        for (int i = 0; i < 12; i++) {
            ints.add(i);
            ints.remove();
        }
        for (int i = 0; i < 9; i++) {
            ints.add(i);
        }
        for (int i = 0; i < 3; i++) {
            ints.remove();
        }
        System.out.println(ints);

        ints.retainAll(Arrays.asList(4, 6));
        System.out.println(ints);
    }

}
