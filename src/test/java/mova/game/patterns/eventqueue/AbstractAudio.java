package mova.game.patterns.eventqueue;

import mova.lang.ThreadUtils;

public abstract class AbstractAudio {

    public static final int SOUND_BLOOP = 1;

    protected static String loadSound(long soundId) {
        ThreadUtils.takeANap(20);
        return "Sound " + soundId;
    }

    protected static void startSound(String resource, int volume) {
        System.out.println("Play " + resource + " at " + volume + "db");
    }
}
