package mova.game.patterns.eventqueue;

public class PlayMessage {
    private long soundId;
    private int volume;

    public PlayMessage(long soundId, int volume) {
        this.soundId = soundId;
        this.volume = volume;
    }

    public long getSoundId() {
        return soundId;
    }

    public void setSoundId(long soundId) {
        this.soundId = soundId;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
