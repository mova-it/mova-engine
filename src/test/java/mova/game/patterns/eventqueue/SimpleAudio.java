package mova.game.patterns.eventqueue;

/*
- Analogy : On a un system de Combat qui tourne et on veut faire un turtorial (popup)
        => on ne veut pas inclure des tests dans le system de combat pour faire apparaitre les popups

- Imaginons un système audio
    - 1er problème : Bloquant (voir Menu)
    - 2ème problème : Pas d'aggregation (imaginer 10 monstres qui meurt en même temps)
    - 3ème problème : mauvais thread

- Le Pattern :
    Une Queue stoque une série de notifications ou de requête (en mode FIFO)
    Envoyer une notification ajoute simplement la notification (pas d'attente de retour de résultat)
    Les "récepteur" des notifications iront dans un second temps lire la Queue et les exécuter
    Les notifications peuvent être prises en charge directement ou bien router vers d'autres éléments.
    Cela découple l'émeteur et le récepteur de manière physique et dans le temps également

    Nb : C'est le fait que cela soit découplé dans le temps qui différencie ce pattern de Observer ou Command

- Se méfier :
    - Event Queue centrale signifie variable global (et donc se méfier des interdépendances cachées)
    - La notification doit refléter l'état dans lequel était le système sans oublie, car le système peut changer entre la notification et l'exécution
    - Les boucles de notification (A envoie un message, B le recoit et envoi un message, ce message est intéressant pour A qui renvoie un message, etc.)

 */

public class SimpleAudio extends AbstractAudio {

    public static void playSound(long soundId, int volume) {
        String resource = AbstractAudio.loadSound(soundId);
        AbstractAudio.startSound(resource, volume);
    }
}
