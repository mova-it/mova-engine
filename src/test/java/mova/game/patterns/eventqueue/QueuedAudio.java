package mova.game.patterns.eventqueue;

/*
- Système avec Queue (ringbuffer)
 */
public class QueuedAudio extends AbstractAudio {

    private static final RingBuffer<PlayMessage> playMessages = new RingBuffer<>(16);

    public static void playSound(long soundId, int volume) {
        // Aggregation
        for (PlayMessage playMessage : playMessages) {
            if (playMessage.getSoundId() == soundId) {
                playMessage.setVolume(Math.max(playMessage.getVolume(), volume));

                return;
            }
        }

        playMessages.offer(new PlayMessage(soundId, volume));
    }

    public void update() {
        PlayMessage playMessage;
        while ((playMessage = playMessages.poll()) != null) {
            String resource = AbstractAudio.loadSound(playMessage.getSoundId());
            AbstractAudio.startSound(resource, playMessage.getVolume());
        }
    }
}
