package mova.game.patterns.eventqueue;

import mova.lang.ThreadUtils;

public class Menu {

    public void onSelect(int index) {
        // (...)

        switch (index) {
            case 14: SimpleAudio.playSound(Audio.SOUND_BLOOP, 1);
                break;
            case 72: SimpleAudio.playSound(Audio.SOUND_BLOOP, 20);
                break;
            default: SimpleAudio.playSound(Audio.SOUND_BLOOP, 10);
        }

        // (...)
    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.onSelect(54);

        ThreadUtils.takeANap(1000);

        menu.onSelect(54);
        menu.onSelect(72);
        menu.onSelect(14);
    }
}

class Menu2 {

    public void onSelect(int index) {
        // (...)

        switch (index) {
            case 14: Audio.playSound(Audio.SOUND_BLOOP, 1);
                break;
            case 72: Audio.playSound(Audio.SOUND_BLOOP, 20);
                break;
            default: Audio.playSound(Audio.SOUND_BLOOP, 10);
        }

        // (...)
    }

    public static void main(String[] args) {
        Menu2 menu = new Menu2();
        menu.onSelect(54);

        Audio audio = new Audio();
        new Thread(audio::update).start();

        System.out.println("\nTake a nap\n");
        ThreadUtils.takeANap(1000);

        menu.onSelect(54);
        menu.onSelect(72);
        menu.onSelect(14);

        new Thread(audio::update).start();
    }
}

class Menu3 {

    public void onSelect(int index) {
        // (...)

        switch (index) {
            case 14: QueuedAudio.playSound(Audio.SOUND_BLOOP, 1);
                break;
            case 72: QueuedAudio.playSound(Audio.SOUND_BLOOP, 20);
                break;
            default: QueuedAudio.playSound(Audio.SOUND_BLOOP, 10);
        }

        // (...)
    }

    public static void main(String[] args) {
        Menu3 menu = new Menu3();
        menu.onSelect(54);

        QueuedAudio audio = new QueuedAudio();
        new Thread(audio::update).start();

        ThreadUtils.takeANap(1000);

        menu.onSelect(54);
        menu.onSelect(72);
        menu.onSelect(14);

        new Thread(audio::update).start();
    }
}
