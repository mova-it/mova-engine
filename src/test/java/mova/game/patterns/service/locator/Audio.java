package mova.game.patterns.service.locator;

public interface Audio {

    void playSound(long soundId);
    void stopSound(long soundId);
    void stopAllSounds();
}

class ConsoleAudio implements Audio {

    @Override
    public void playSound(long soundId) {
        System.out.println("Playing sound with id " + soundId);
    }

    @Override
    public void stopSound(long soundId) {
        System.out.println("Stopping sound with id " + soundId);
    }

    @Override
    public void stopAllSounds() {
        System.out.println("Stopping all sounds");
    }
}

class Locator {

    private static final Audio NULL_AUDIO = new NullAudio();

    private static Audio audio = NULL_AUDIO;

    public static void provideAudio(Audio audio) {
        if (audio == null) Locator.audio = NULL_AUDIO;
        else Locator.audio = audio;
    }

    public static Audio getAudio() {
        return audio;
    }

    private static class NullAudio implements Audio {

        @Override
        public void playSound(long soundId) {}

        @Override
        public void stopSound(long soundId) {}

        @Override
        public void stopAllSounds() {}
    }
}

class Main {

    private static class LoggedAudio implements Audio {

        private final Audio decorated;

        LoggedAudio(Audio audio) {
            decorated = audio;
        }

        @Override
        public void playSound(long soundId) {
            log("playSound(" + soundId + ")");
            decorated.playSound(soundId);
        }

        @Override
        public void stopSound(long soundId) {
            log("stopSound(" + soundId + ")");
            decorated.stopSound(soundId);
        }

        @Override
        public void stopAllSounds() {
            log("stopAllSounds()");
            decorated.stopAllSounds();
        }

        private void log(String message) {
            System.out.println("INFO: " + message);
        }
    }

    static void enabledAudioLogging() {
        Audio audio = new LoggedAudio(Locator.getAudio());
        Locator.provideAudio(audio);
    }

    public static void main(String[] args) {
        Locator.getAudio().playSound(32);
        Locator.getAudio().stopSound(32);
        Locator.getAudio().stopAllSounds();
        Locator.provideAudio(new ConsoleAudio());
        Locator.getAudio().playSound(32);
        Locator.getAudio().stopSound(32);
        enabledAudioLogging();
        Locator.getAudio().stopAllSounds();
        Locator.getAudio().playSound(32);
        Locator.provideAudio(null);
        enabledAudioLogging();
        Locator.getAudio().stopSound(32);
        Locator.getAudio().stopAllSounds();
    }
}
