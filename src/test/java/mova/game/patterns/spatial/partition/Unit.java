package mova.game.patterns.spatial.partition;

public class Unit {

    public final String name;
    private double x;
    private double y;
    private Grid grid;
    private Unit prev;
    private Unit next;

    public Unit(String name, double x, double y, Grid grid) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.grid = grid;

        grid.add(this);
    }

    public void move(double x, double y) {
        double oldX = this.x;
        double oldY = this.y;

        this.x = x;
        this.y = y;

        grid.move(this, oldX, oldY);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Unit getPrev() {
        return prev;
    }

    public void setPrev(Unit prev) {
        this.prev = prev;
    }

    public boolean hasPrev() {
        return prev != null;
    }

    public Unit getNext() {
        return next;
    }

    public void setNext(Unit next) {
        this.next = next;
    }

    public boolean hasNext() {
        return next != null;
    }
}
