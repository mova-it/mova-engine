package mova.game.patterns.spatial.partition;

import java.awt.geom.Point2D;

public class Grid {

    private static final int NUM_CELLS = 10;
    private static final int CELL_SIZE = 20;
    private static final double ATTACK_DISTANCE = 7;

    private final Unit[][] cells = new Unit[NUM_CELLS][NUM_CELLS];

    public Grid() {
        for (int x = 0; x < NUM_CELLS; x++) {
            for (int y = 0; y < NUM_CELLS; y++) {
                cells[x][y] = null;
            }
        }
    }

    public void add(Unit unit) {
        int cellX = (int) unit.getX()/CELL_SIZE;
        int cellY = (int) unit.getY()/CELL_SIZE;

        unit.setPrev(null);
        unit.setNext(cells[cellX][cellY]);
        cells[cellX][cellY] = unit;

        if (unit.hasNext()) unit.getNext().setPrev(unit);
    }

    public void move(Unit unit, double oldX, double oldY) {
        int oldCellX = (int) oldX/CELL_SIZE;
        int oldCellY = (int) oldY/CELL_SIZE;

        int cellX = (int) unit.getX()/CELL_SIZE;
        int cellY = (int) unit.getY()/CELL_SIZE;

        if (oldCellX == cellX && oldCellY == cellY) return;

        if (unit.hasPrev()) unit.getPrev().setNext(unit.getNext());
        if (unit.hasNext()) unit.getNext().setPrev(unit.getPrev());

        if (cells[oldCellX][oldCellY] == unit) {
            cells[oldCellX][oldCellY] = unit.getNext();
        }

        add(unit);
    }

    public void handleMelee() {
        System.out.println("Handle Melee:");
        for (int x = 0; x < NUM_CELLS; x++) {
            for (int y = 0; y < NUM_CELLS; y++) {
                handleCell(x, y);
            }
        }
    }

    public void handleCell(int x, int y) {
        Unit unit = cells[x][y];
        while (unit != null) {
            handleUnit(unit, unit.getNext());

            if (x > 0) handleUnit(unit, cells[x - 1][y]);
            if (y > 0) handleUnit(unit, cells[x][y - 1]);
            if (x > 0 && y > 0) handleUnit(unit, cells[x - 1][y - 1]);
            if (x > 0 && y < NUM_CELLS - 1) handleUnit(unit, cells[x - 1][y + 1]);

            unit = unit.getNext();
        }
    }

    public void handleUnit(Unit unit, Unit other) {
        while (other != null) {
            if (distance(unit, other) < ATTACK_DISTANCE) {
                handleAttack(unit, other);
            }

            other = other.getNext();
        }
    }

    public double distance(Unit unit, Unit other) {
        return Point2D.distance(unit.getX(), unit.getY(), other.getX(), other.getY());
    }

    public void handleAttack(Unit u1, Unit u2) {
        System.out.println(u1.name + " attaque " + u2.name);
    }

    public static void main(String[] args) {
        Grid grid = new Grid();
        Unit u1 = new Unit("Gaston", 19, 30, grid);
        Unit u2 = new Unit("Bertramb", 13, 27, grid);

        grid.handleMelee();

        u2.move(21, 30);

        grid.handleMelee();

        u2.move(10, 30);

        grid.handleMelee();
    }
}
