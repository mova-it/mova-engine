package mova.game.patterns.subclass.sandbox;

public abstract class Superpower {

    private float x;
    private float y;
    private float z;

    public abstract void activate();

    protected void move(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
        System.out.println("Faire bouger du vecteur (" + x + "; " + y + "; " + z + ")");
        System.out.println("Nouvelle coordonnées: (" + this.x + "; " + this.y + "; " + this.z + ")");
    }

    protected void playSound(String soundId) {
        System.out.println("Jouer le son \"" + soundId + "\"");
    }

    protected void spawnParticles(String particlesType, int count) {
        System.out.println("Afficher " + count + " particule" + (count > 1 ? "s" : "") + " de type " + particlesType);
    }

    protected float getHeroX() {
        return x;
    }

    protected float getHeroY() {
        return y;
    }

    protected float getHeroZ() {
        return z;
    }
}

class SkyLaunch extends Superpower {
    @Override
    public void activate() {
        if (getHeroZ() == 0) {
            move(0, 0, 20);
            playSound("SPROING");
            spawnParticles("poussières", 10);
        } else if (getHeroZ() <= 10f) {
            playSound("SWOOP");
            move(0, 0, getHeroZ() - 20);
        } else {
            playSound("DIVE");
            spawnParticles("étincelles", 1);
            move(0, 0, -getHeroZ()/2);
        }

        System.out.println();
    }

    public static void main(String[] args) {
        Superpower skylaunch = new SkyLaunch();
        skylaunch.activate();
        skylaunch.activate();
        skylaunch.activate();
    }
}


class SoundPlayer {

    protected void playSound(String soundId) {
        System.out.println("Jouer le son \"" + soundId + "\"");
    }

    protected void stopSound(String soundId) {
        System.out.println("Stopper le son \"" + soundId + "\"");
    }

    protected void setVolume(int volume) {
        System.out.println("Modifie le volume à " + volume + "dB");
    }
}

abstract class Superpower2 {

    private float x;
    private float y;
    private float z;

    private SoundPlayer soundPlayer;

    protected Superpower2(SoundPlayer soundPlayer) {
        this.soundPlayer = soundPlayer;
    }

    public abstract void activate();

    protected void move(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
        System.out.println("Faire bouger du vecteur (" + x + "; " + y + "; " + z + ")");
        System.out.println("Nouvelle coordonnées: (" + this.x + "; " + this.y + "; " + this.z + ")");
    }

    protected SoundPlayer getSoundPlayer() {
        return soundPlayer;
    }

    protected void spawnParticles(String particlesType, int count) {
        System.out.println("Afficher " + count + " particule" + (count > 1 ? "s" : "") + " de type " + particlesType);
    }

    protected float getHeroX() {
        return x;
    }

    protected float getHeroY() {
        return y;
    }

    protected float getHeroZ() {
        return z;
    }
}

class SkyLaunch2 extends Superpower2 {

    public SkyLaunch2(SoundPlayer soundPlayer) {
        super(soundPlayer);
    }

    @Override
    public void activate() {
        getSoundPlayer().setVolume(10);
        getSoundPlayer().playSound("MUSIQUE D'AMBIANCE");

        if (getHeroZ() == 0) {
            move(0, 0, 20);
            getSoundPlayer().setVolume(50);
            getSoundPlayer().playSound("SPROING");
            spawnParticles("poussières", 10);
        } else if (getHeroZ() <= 10f) {
            getSoundPlayer().setVolume(20);
            getSoundPlayer().playSound("SWOOP");
            move(0, 0, getHeroZ() - 20);
        } else {
            getSoundPlayer().setVolume(40);
            getSoundPlayer().playSound("DIVE");
            spawnParticles("étincelles", 1);
            move(0, 0, -getHeroZ()/2);
        }
        getSoundPlayer().stopSound("MUSIQUE D'AMBIANCE");

        System.out.println();
    }

    public static void main(String[] args) {
        Superpower2 skylaunch = new SkyLaunch2(new SoundPlayer());
        skylaunch.activate();
        skylaunch.activate();
        skylaunch.activate();
    }
}

abstract class Superpower3 {

    private float x;
    private float y;
    private float z;

    private SoundPlayer soundPlayer;

    private void init(SoundPlayer soundPlayer) {
        this.soundPlayer = soundPlayer;
    }

    public abstract void activate();

    protected void move(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
        System.out.println("Faire bouger du vecteur (" + x + "; " + y + "; " + z + ")");
        System.out.println("Nouvelle coordonnées: (" + this.x + "; " + this.y + "; " + this.z + ")");
    }

    protected SoundPlayer getSoundPlayer() {
        return soundPlayer;
    }

    protected void spawnParticles(String particlesType, int count) {
        System.out.println("Afficher " + count + " particule" + (count > 1 ? "s" : "") + " de type " + particlesType);
    }

    protected float getHeroX() {
        return x;
    }

    protected float getHeroY() {
        return y;
    }

    protected float getHeroZ() {
        return z;
    }

    public static Superpower3 createSkyLaunch(SoundPlayer soundPlayer) {
        Superpower3 skylaunch = new SkyLaunch3();
        skylaunch.init(soundPlayer);
        return skylaunch;
    }
}

class SkyLaunch3 extends Superpower3 {

    protected SkyLaunch3() {}

    @Override
    public void activate() {
        getSoundPlayer().setVolume(10);
        getSoundPlayer().playSound("MUSIQUE D'AMBIANCE");

        if (getHeroZ() == 0) {
            move(0, 0, 20);
            getSoundPlayer().setVolume(50);
            getSoundPlayer().playSound("SPROING");
            spawnParticles("poussières", 10);
        } else if (getHeroZ() <= 10f) {
            getSoundPlayer().setVolume(20);
            getSoundPlayer().playSound("SWOOP");
            move(0, 0, getHeroZ() - 20);
        } else {
            getSoundPlayer().setVolume(40);
            getSoundPlayer().playSound("DIVE");
            spawnParticles("étincelles", 1);
            move(0, 0, -getHeroZ()/2);
        }
        getSoundPlayer().stopSound("MUSIQUE D'AMBIANCE");

        System.out.println();
    }

    public static void main(String[] args) {
        Superpower3 skylaunch = Superpower3.createSkyLaunch(new SoundPlayer());
        skylaunch.activate();
        skylaunch.activate();
        skylaunch.activate();
    }
}

abstract class Superpower4 {

    private float x;
    private float y;
    private float z;

    private static SoundPlayer soundPlayer;

    public static void init(SoundPlayer soundPlayer) {
        Superpower4.soundPlayer = soundPlayer;
    }

    public abstract void activate();

    protected void move(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
        System.out.println("Faire bouger du vecteur (" + x + "; " + y + "; " + z + ")");
        System.out.println("Nouvelle coordonnées: (" + this.x + "; " + this.y + "; " + this.z + ")");
    }

    protected SoundPlayer getSoundPlayer() {
        return soundPlayer;
    }

    protected void spawnParticles(String particlesType, int count) {
        System.out.println("Afficher " + count + " particule" + (count > 1 ? "s" : "") + " de type " + particlesType);
    }

    protected float getHeroX() {
        return x;
    }

    protected float getHeroY() {
        return y;
    }

    protected float getHeroZ() {
        return z;
    }
}

class SkyLaunch4 extends Superpower4 {

    protected SkyLaunch4() {}

    @Override
    public void activate() {
        getSoundPlayer().setVolume(10);
        getSoundPlayer().playSound("MUSIQUE D'AMBIANCE");

        if (getHeroZ() == 0) {
            move(0, 0, 20);
            getSoundPlayer().setVolume(50);
            getSoundPlayer().playSound("SPROING");
            spawnParticles("poussières", 10);
        } else if (getHeroZ() <= 10f) {
            getSoundPlayer().setVolume(20);
            getSoundPlayer().playSound("SWOOP");
            move(0, 0, getHeroZ() - 20);
        } else {
            getSoundPlayer().setVolume(40);
            getSoundPlayer().playSound("DIVE");
            spawnParticles("étincelles", 1);
            move(0, 0, -getHeroZ()/2);
        }
        getSoundPlayer().stopSound("MUSIQUE D'AMBIANCE");

        System.out.println();
    }

    public static void main(String[] args) {
        Superpower4.init(new SoundPlayer());

        Superpower4 skylaunch = new SkyLaunch4();
        skylaunch.activate();
        skylaunch.activate();
        skylaunch.activate();
    }
}

