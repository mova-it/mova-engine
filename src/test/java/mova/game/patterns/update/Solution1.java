package mova.game.patterns.update;

abstract class Entity {

    protected final String name;

    public Entity(String name) {
        this.name = name;
    }

    public void update() {
        update(-1);
    }

    public abstract void update(long elapsedTime);
}

class Skeleton extends Entity {

    private boolean patrollingLeft = false;
    private int x = 0;

    public Skeleton() {
        super("Skeleton");
    }

    @Override
    public void update(long elapsedTime) {
        if (patrollingLeft) {
            if (--x == 0) patrollingLeft = false;
        } else {
            if (++x == 100) patrollingLeft = true;
        }

        System.out.println(name + " has moved to " + x);
    }
}

class Statue extends Entity {

    private final int delay;
    private int frames = 0;

    public Statue(String model, int delay) {
        super("Statue of " + model);

        this.delay = delay;
    }

    @Override
    public void update(long elapsedTime) {
        if (++frames == delay) {
            shootLightning();
            frames = 0;
        }
    }

    public void shootLightning() {
        System.out.println(name + " send lightning shoot");
    }
}

class TimeSkeleton extends Entity {

    private boolean patrollingLeft = false;
    private int x = 0;

    public TimeSkeleton() {
        super("Time Skeleton");
    }

    @Override
    public void update(long elapsedTime) {
        //TODO marche pas la formule de calcule avec elapsedTime
        if (patrollingLeft) {
            x -= elapsedTime;
            if (x <= 0) {
                patrollingLeft = false;
                x = -x;
            }
        } else {
            x += elapsedTime;
            if (x >= 100) {
                patrollingLeft = true;
                x = 100 - (x - 100);
            }
        }

        System.out.println(name + " has moved to " + x);
    }
}

public class Solution1 {

    private static final int DEMO_TIME = 5000;
    private static final int MAX_ENTITIES = 5;

    private int numEntities = 0;
    private final Entity[] entities = new Entity[MAX_ENTITIES];

    public Solution1() {
        addEntity(new Skeleton());
        addEntity(new Statue("Abraham", 90));
        addEntity(new Statue("Napoleon", 80));
        addEntity(new TimeSkeleton());
    }

    public void addEntity(Entity entity) {
        if (numEntities == MAX_ENTITIES) throw new UnsupportedOperationException("Nombre d'entités max = " + MAX_ENTITIES);

        entities[numEntities++] = entity;
    }

    public void gameloop() {
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            for (int i = 0; i < numEntities; i++) entities[i].update();
        }
    }

    public static void main(String[] args) {
        new Solution1().gameloop();
    }
}
