package mova.game.patterns.update;

class EntityWithoutUpdate {

    private final String name;
    private int x = 0;

    public EntityWithoutUpdate(String name) {
        this.name = name;
    }

    public void setX(int x) {
        this.x = x;
        System.out.println(name + " has moved to " + this.x);
    }

    public void shootLightning() {
        System.out.println(name + " send lightning shoot");
    }
}

public class Problem {

    private static final int DEMO_TIME = 5000;

    public void gameloop() {
        EntityWithoutUpdate skeleton = new EntityWithoutUpdate("Skeleton");

        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            for (int x = 0; x < 100; x++) skeleton.setX(x);

            for (int x = 100; x > 0; x--) skeleton.setX(x);
        }
    }

    public static void main(String[] args) {
        new Problem().gameloop();
    }
}

class Problem2 {

    private static final int DEMO_TIME = 5000;

    public void gameloop() {
        EntityWithoutUpdate skeleton = new EntityWithoutUpdate("Skeleton");
        boolean patrollingLeft = false;
        int x = 0;

        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            if (patrollingLeft) {
                if (--x == 0) patrollingLeft = false;
            } else {
                if (++x == 100) patrollingLeft = true;
            }

            skeleton.setX(x);
        }
    }

    public static void main(String[] args) {
        new Problem2().gameloop();
    }
}

class Problem3 {

    private static final int DEMO_TIME = 5000;

    public void gameloop() {
        EntityWithoutUpdate skeleton = new EntityWithoutUpdate("Skeleton");
        boolean patrollingLeft = false;
        int x = 0;

        EntityWithoutUpdate leftStatue = new EntityWithoutUpdate("Left Statue");
        EntityWithoutUpdate rightStatue = new EntityWithoutUpdate("Right Statue");
        int leftStatueFrames = 0;
        int rightStatueFrames = 0;

        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            if (patrollingLeft) {
                if (--x == 0) patrollingLeft = false;
            } else {
                if (++x == 100) patrollingLeft = true;
            }

            skeleton.setX(x);

            if (++leftStatueFrames == 90) {
                leftStatueFrames = 0;
                leftStatue.shootLightning();
            }

            if (++rightStatueFrames == 80) {
                rightStatueFrames = 0;
                rightStatue.shootLightning();
            }
        }
    }

    public static void main(String[] args) {
        new Problem3().gameloop();
    }
}
