package mova.game.patterns.state;

public class BuggedJumpinHeroine extends Heroine {

    // TODO trouver le problème
    public void handleInput(String button) {
        if (button.equals("j")) {
            jump();
        }
    }
}

class JumpingHeroine extends Heroine {

    private boolean jumping = false;

    // Note: Pour ne plus sauter "en l'air"
    public void handleInput(String button) {
        if (button.equals("j")) {
            if (!jumping) {
                jumping = true;
                jump();
            }
        }
    }
}

class BuggedDuckingHeroine extends Heroine {

    private boolean jumping = false;

    public void handleInput(String button) {
        switch (button) {
            case "j":
                if (!jumping) {
                    jumping = true;
                    jump();
                }
                break;
            case "d":
                if (!jumping) {
                    duck();
                }
                break;
            case "u":
                stand();
                break;
        }
    }
}

class DuckingHeroine extends Heroine {

    private boolean jumping = false;
    private boolean ducking = false;

    public void handleInput(String button) {
        switch (button) {
            case "j":
                if (!jumping && !ducking) {
                    jumping = true;
                    jump();
                }
                break;
            case "d":
                if (!jumping) {
                    ducking = true;
                    duck();
                }
                break;
            case "u":
                if (ducking) {
                    ducking = false;
                    stand();
                }
                break;
        }
    }
}

class DivingHeroine extends Heroine {

    private boolean jumping = false;
    private boolean ducking = false;

    public void handleInput(String button) {
        switch (button) {
            case "j":
                if (!jumping && !ducking) {
                    jumping = true;
                    jump();
                }
                break;
            case "d":
                if (!jumping) {
                    ducking = true;
                    duck();
                } else {
                    jumping = false;
                    dive(); // Note: We can jump while diving ... another flag!
                }
                break;
            case "u":
                if (ducking) {
                    ducking = false;
                    stand();
                }
                break;
        }
    }
}

// Note: https://app.genmymodel.com/api/projects/_H-bt8NBJEeuyUouQYKpTOg/diagrams/_H-bt8tBJEeuyUouQYKpTOg/svg


