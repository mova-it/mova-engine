package mova.game.patterns.state;

// Note: Mais qu'est-ce qui arrive si on veut faire une super attaque en concentrant la position "duck"

public class HeroinStateProblem extends Heroine {

    private final static int MAX_CHARGE = 10;

    enum State {
        STANDING,
        JUMPING,
        DUCKING,
        DIVING
    }

    private State state = State.STANDING;
    private int chargeTime;

    public void handleInput(String button) {
        switch (state) {
            case STANDING: {
                if (button.equals("d")) {
                    setState(State.DUCKING);
                    chargeTime = 0;
                    duck();
                }

                // Other buttons
                break;
            }
            // Other states
        }
    }

    public void update() {
        if (state == State.DUCKING) {
            chargeTime++;
            if (chargeTime > MAX_CHARGE) {
                superBomb();
            }
        }
    }

    public void setState(State state) {
        if (state != this.state) {
            this.state = state;
            System.out.println("Now state is " + state);
        }
    }
}
