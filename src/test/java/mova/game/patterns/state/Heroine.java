package mova.game.patterns.state;

public abstract class Heroine {

    abstract void handleInput(String button);

    public void update() {
        // Do nothing;
    }

    void jump() {
        System.out.println("Jump");
    }

    void duck() {
        System.out.println("Coin");
    }

    void dive() {
        System.out.println("Plonge");
    }

    void stand() {
        System.out.println("Debout");
    }

    void superBomb() {
        System.out.println("Explosion intense comme quand Bro mange un fruit!");
    }
}
