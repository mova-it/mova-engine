package mova.game.patterns.state;

import java.util.Scanner;

public class HeroinStatePattern extends Heroine {

    // NOTE: static or not static?
    final static HeroinState STANDING_STATE = new StandingState();
    final static HeroinState JUMPING_STATE = new JumpingState();
    final static HeroinState DUCKING_STATE = new DuckingState();
    final static HeroinState DIVING_STATE = new DivingState();

    private HeroinState state = STANDING_STATE;

    public void setState(HeroinState state) {
        if (state != this.state) {
            this.state = state;
            state.entry();
            System.out.println("Now state is " + state.getClass().getSimpleName());
        }
    }

    @Override
    void handleInput(String button) {
        state.handleInput(this, button);
    }

    @Override
    public void update() {
        state.update(this);
    }
}

interface HeroinState {

    default void entry() {}

    void handleInput(HeroinStatePattern heroine, String button);

    default void update(HeroinStatePattern heroine) {}
}

class StandingState implements HeroinState {

    @Override
    public void handleInput(HeroinStatePattern heroine, String button) {
        if (button.equals("j")) {
            heroine.setState(HeroinStatePattern.JUMPING_STATE);
            heroine.jump();
        } else if (button.equals("d")) {
            heroine.setState(HeroinStatePattern.DUCKING_STATE);
            heroine.duck();
        }
    }
}

class JumpingState implements HeroinState {
    @Override
    public void handleInput(HeroinStatePattern heroine, String button) {
        if (button.equals("d")) {
            heroine.setState(HeroinStatePattern.DIVING_STATE);
            heroine.dive();
        } else {
            heroine.setState(HeroinStatePattern.STANDING_STATE);
            heroine.stand();
        }
    }
}

class DuckingState implements HeroinState {

    private final static int MAX_CHARGE = 3;

    private int chargeTime;

    @Override
    public void entry() {
        chargeTime = 0;
    }

    @Override
    public void handleInput(HeroinStatePattern heroine, String button) {
        if (button.equals("u")) {
            heroine.setState(HeroinStatePattern.STANDING_STATE);
            heroine.stand();
        }
    }

    @Override
    public void update(HeroinStatePattern heroine) {
        if (chargeTime > 0 && chargeTime < MAX_CHARGE) System.out.println("Loading " + chargeTime + " charges.");

        chargeTime++;

        if (chargeTime > MAX_CHARGE) {
            heroine.setState(HeroinStatePattern.STANDING_STATE);
            heroine.superBomb();
            heroine.stand();
        }
    }
}

class DivingState implements HeroinState {
    @Override
    public void handleInput(HeroinStatePattern heroine, String button) {
        heroine.setState(HeroinStatePattern.STANDING_STATE);
        heroine.stand();
    }
}

class MainStatePattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Heroine heroine = new HeroinStatePattern();

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();
            if (button.equals("q")) {
                quit = true;
            } else {
                heroine.handleInput(button);
                heroine.update();
            }
        } while (!quit);
    }
}
