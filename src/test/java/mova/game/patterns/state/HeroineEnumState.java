package mova.game.patterns.state;

import java.util.Scanner;

public class HeroineEnumState extends Heroine {

    enum State {
        STANDING,
        JUMPING,
        DUCKING,
        DIVING
    }

    private State state = State.STANDING;

    public void setState(State state) {
        if (state != this.state) {
            this.state = state;
            System.out.println("Now state is " + state);
        }
    }

    public void handleInput(String button) {
        System.out.println(button + " button pressed.");

        switch (state) {
            case STANDING: {
                if (button.equals("j")) {
                    setState(State.JUMPING);
                    jump();
                } else if (button.equals("d")) {
                    setState(State.DUCKING);
                    duck();
                }
                break;
            }
            case JUMPING: {
                if (button.equals("d")) {
                    setState(State.DIVING);
                    dive();
                } else {
                    setState(State.STANDING);
                    stand();
                }
                break;
            }
            case DUCKING: {
                if (button.equals("u")) {
                    setState(State.STANDING);
                    stand();
                }
                break;
            }
            case DIVING: {
                setState(State.STANDING);
                stand();
                break;
            }
        }
    }
}

class MainEnumState {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Heroine heroine = new HeroineEnumState();

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();
            if (button.equals("q")) {
                quit = true;
            } else {
                heroine.handleInput(button);
            }
        } while (!quit);
    }
}
