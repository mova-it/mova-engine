package mova.game.patterns.state;

import java.util.Scanner;

public class AdvancedHeroinStatePattern extends Heroine {

    private AdvancedHeroinState state = new AdvancedStandingState();

    public void setState(AdvancedHeroinState state) {
        if (state != this.state) {
            this.state = state;
            state.entry(this);
            System.out.println("Now state is " + state.getClass().getSimpleName());
        }
    }

    @Override
    void handleInput(String button) {
        AdvancedHeroinState state = this.state.handleInput(this, button);
        setState(state);
    }

    @Override
    public void update() {
        state.update(this);
    }
}

interface AdvancedHeroinState {

    default void entry(AdvancedHeroinStatePattern heroine) {}

    AdvancedHeroinState handleInput(AdvancedHeroinStatePattern heroine, String button);

    default void update(AdvancedHeroinStatePattern heroine) {}
}

class AdvancedStandingState implements AdvancedHeroinState {

    @Override
    public void entry(AdvancedHeroinStatePattern heroine) {
        heroine.stand();
    }

    @Override
    public AdvancedHeroinState handleInput(AdvancedHeroinStatePattern heroine, String button) {
        if (button.equals("j")) {
            return new AdvancedJumpingState();
        } else if (button.equals("d")) {
            return new AdvancedDuckingState();
        }

        return this;
    }
}

class AdvancedJumpingState implements AdvancedHeroinState {

    @Override
    public void entry(AdvancedHeroinStatePattern heroine) {
        heroine.jump();
    }

    @Override
    public AdvancedHeroinState handleInput(AdvancedHeroinStatePattern heroine, String button) {
        if (button.equals("d")) {
            return new AdvancedDivingState();
        } else {
            return new AdvancedStandingState();
        }
    }
}

class AdvancedDuckingState implements AdvancedHeroinState {

    private final static int MAX_CHARGE = 3;

    private int chargeTime;

    @Override
    public void entry(AdvancedHeroinStatePattern heroine) {
        heroine.duck();
        chargeTime = 0;
    }

    @Override
    public AdvancedHeroinState handleInput(AdvancedHeroinStatePattern heroine, String button) {
        if (button.equals("u")) {
            return new AdvancedStandingState();
        }

        return this;
    }

    @Override
    public void update(AdvancedHeroinStatePattern heroine) {
        if (chargeTime > 0 && chargeTime < MAX_CHARGE) System.out.println("Loading " + chargeTime + " charges.");

        chargeTime++;

        if (chargeTime > MAX_CHARGE) {
            heroine.superBomb();
            heroine.setState(new AdvancedStandingState());
        }
    }
}

class AdvancedDivingState implements AdvancedHeroinState {

    @Override
    public void entry(AdvancedHeroinStatePattern heroine) {
        heroine.dive();
    }

    @Override
    public AdvancedHeroinState handleInput(AdvancedHeroinStatePattern heroine, String button) {
        return new AdvancedStandingState();
    }
}

class MainAdvancedStatePattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Heroine heroine = new AdvancedHeroinStatePattern();

        boolean quit = false;
        do {
            System.out.println("Enter button name: ");
            String button = scanner.nextLine();
            if (button.equals("q")) {
                quit = true;
            } else {
                heroine.handleInput(button);
                heroine.update();
            }
        } while (!quit);
    }
}