package mova.game.patterns;

import mova.game.geom.Locator;
import mova.util.StringUtils;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class QuadTree<E> {

    private static final int QUAD_CONSTANT = 4;
    private static final int DEFAULT_THRESHOLD = 3;
    public static final String LEFT_BRACKET = "[";
    public static final String RIGHT_BRACKET = "]";
    public static final char TABULATION = '\t';
    public static final String LINE_BREAK = "\n";
    public static final String ELEMENT_INTRODUCER = " => ";
    public static final String ELEMENT_SEPARATOR = ", ";
    public static final Collector<CharSequence, ?, String> JOINING_COLLECTOR = Collectors.joining(ELEMENT_SEPARATOR);
    public static final String QUADTREE_INTRODUCER = " : ";
    public static final int NORTH_WEST = 0;
    public static final int NORTH_EAST = 1;
    public static final int SOUTH_EAST = 2;
    public static final int SOUTH_WEST = 3;

    private final int threshold;
    private final Rectangle2D quadrantArea;
    private Object[] elements;
    private int nbElements;

    private QuadTree<E>[] quadTrees;
    private final Function<E, ? extends Point2D> locator;

    //TODO faire en sorte que le QuadTree soit composé de QuadNode plutot que de QuadTree... Et ainsi pouvoir envisager une extension de AbstractSet ou autre.

    public QuadTree(Rectangle2D quadrantArea, Function<E, ? extends Point2D> locator) {
        this(quadrantArea, locator, DEFAULT_THRESHOLD);
    }

    public QuadTree(Rectangle2D quadrantArea, Function<E, ? extends Point2D> locator, int threshold) {
        this.quadrantArea = quadrantArea;
        this.locator = locator;
        this.threshold = threshold;
        this.elements = new Object[threshold];
    }

    public boolean add(E element) {
        if (_matchLocationOf(element)) {
            if (_isLeaf() && nbElements < threshold) {
                elements[nbElements++] = element;
                return true;
            } else {
                if (quadTrees == null) {
                    _createQuadrants();
                    _moveToChilds();
                }

                return _addToChildQuadrant(element);
            }
        }

        return false;
    }

    public int size() {
        if (_isLeaf()) return nbElements;
        else return Arrays.stream(quadTrees).mapToInt(QuadTree::size).sum();
    }

    public boolean remove(E element) {
        Point2D location = locator.apply(element);

        return _remove(element, location);
    }

    private boolean _remove(E element, Point2D location) {
        if (!quadrantArea.contains(location)) return false;

        if (_isLeaf()) {
            int index = Arrays.asList(elements).indexOf(element);
            if (index > -1) {
                int numMoved = nbElements - index - 1;
                if (numMoved > 0) { // On ne fait pas de copy s'il s'agit du dernier element
                    System.arraycopy(elements, index + 1, elements, index, numMoved);
                }
                elements[nbElements--] = null;

                return true;
            }

            return false;
        } else {
            boolean removed = false;
            for (int i = 0; i < QUAD_CONSTANT && !removed; i++) {
                removed = quadTrees[i]._remove(element, location);
            }

            if (removed) {
                if (size() <= threshold) {
                    elements = new Object[threshold];

                    for (QuadTree<E> quadTree : quadTrees) {
                        for (int i = 0; i < quadTree.nbElements; i++) {
                            Object childElement = quadTree.elements[i];
                            elements[nbElements++] = childElement;
                        }
                    }

                    quadTrees = null;
                }
            }

            return removed;
        }
    }

    private boolean _matchLocationOf(E element) {
        return _matchLocationOf(element, quadrantArea);
    }

    private boolean _matchLocationOf(E element, Rectangle2D region) {
        Point2D location = locator.apply(element);
        return region.contains(location);
    }

    private boolean _addToChildQuadrant(E point) {
        for (int i = 0; i < QUAD_CONSTANT; i++) {
            if (quadTrees[i].add(point)) return true;
        }

        return false;
    }

    private Rectangle2D _getQuadrant(int quadrantIndex) {
        double quadrantWidth = quadrantArea.getWidth()/2;
        double quadrantHeight = quadrantArea.getHeight()/2;

        switch (quadrantIndex) {
            case NORTH_WEST:
                return _createNewArea(quadrantArea.getX(), quadrantArea.getY(), quadrantWidth, quadrantHeight);
            case NORTH_EAST:
                return _createNewArea(quadrantArea.getX() + quadrantWidth, quadrantArea.getY(), quadrantWidth, quadrantHeight);
            case SOUTH_EAST:
                return _createNewArea(quadrantArea.getX() + quadrantWidth, quadrantArea.getY() + quadrantHeight, quadrantWidth, quadrantHeight);
            case SOUTH_WEST:
                return _createNewArea(quadrantArea.getX(), quadrantArea.getY() + quadrantHeight, quadrantWidth, quadrantHeight);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private void _createQuadrants() {
        quadTrees = new QuadTree[QUAD_CONSTANT];

        Rectangle2D region;
        for (int i = 0; i < QUAD_CONSTANT; i++) {
            region = _getQuadrant(i);
            quadTrees[i] = new QuadTree<>(region, locator, threshold);
        }
    }

    private void _moveToChilds() {
        for (int i = 0; i < nbElements; i++) {
            E element = _elementData(i);
            _addToChildQuadrant(element);
        }
        elements = null;
        nbElements = 0;
    }

    private Rectangle2D _createNewArea(double x, double y, double width, double height) {
        Rectangle2D newArea = (Rectangle2D) quadrantArea.clone();
        newArea.setRect(x, y, width, height);

        return newArea;
    }

    private boolean _isLeaf() {
        return elements != null;
    }

    @SuppressWarnings("unchecked")
    private E _elementData(int index) {
        return (E) elements[index];
    }

    //TODO Set? List? Groso modo est-ce qu'on conserve un unique représentant d'une instance? ou plusieurs exemplaires? accepte-t-on null?
    //TODO quelle implication au niveau remove/add/contains ? On retire le premier exemplaire d'une instance par exemple ou tous?

//    public boolean contains(Object element) {
//        locator.apply(element);
//    }

    public List<E> search(Rectangle2D searchRegion, List<E> matches) {
        if (matches == null) matches = new ArrayList<>();

        if (!quadrantArea.intersects(searchRegion)) return matches;

        if (_isLeaf()) {
            for (int i = 0; i < nbElements; i++) {
                E element = _elementData(i);
                if (_matchLocationOf(element, searchRegion)) matches.add(element);
            }
        } else {
            for (int i = 0; i < QUAD_CONSTANT; i++) {
                quadTrees[i].search(searchRegion, matches);
            }
        }

        return matches;
    }

    @Override
    public String toString() {
        return toString(0);
    }

    public String toString(int level) {
        StringBuilder builder = new StringBuilder();

        builder.append(quadrantArea);

        if (_isLeaf()) {
            builder.append(QUADTREE_INTRODUCER);

            builder.append(LEFT_BRACKET);

            String elementsString = IntStream.range(0, nbElements)
                    .mapToObj(index -> {
                        E element = _elementData(index);
                        return locator.apply(element) + ELEMENT_INTRODUCER + element;
                    })
                    .collect(JOINING_COLLECTOR);
            builder.append(elementsString);

            builder.append(RIGHT_BRACKET);
        } else {
            int currentLevel = level + 1;
            String padding = StringUtils.fill(TABULATION, currentLevel);
            for (QuadTree<E> quadTree : quadTrees) {
                builder.append(LINE_BREAK).append(padding).append(quadTree.toString(currentLevel));
            }
        }

        return builder.toString();
    }

    private static class Unit {
        private float x;
        private float y;

        public Unit(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }
    }

    public static void main(String[] args) {
        Rectangle2D area = new Rectangle2D.Float(0, 0, 400, 400);
        QuadTree<Unit> units = new QuadTree<>(area, unit -> new Locator(unit.x, unit.y));

        float[][] points = new float[][] {
                { 21, 25 },
                { 55, 53 },
                { 70, 318 },
                { 98, 302 },
                { 49, 229 },
                { 135, 229 },
                { 224, 292 },
                { 206, 321 },
                { 197, 258 },
                { 245, 238 }
        };

        for (float[] floats : points) {
            Unit unit = new Unit(floats[0], floats[1]);
            units.add(unit);
        }

        System.out.println(units);

        System.out.println("\n");

        Rectangle2D searchArea = new Rectangle2D.Float(200, 200, 50, 50);
        List<Unit> result = units.search(searchArea, null);

        System.out.println(result);

        searchArea = new Rectangle2D.Float(0, 0, 100, 100);
        result = units.search(searchArea, null);

        System.out.println(result);

        searchArea = new Rectangle2D.Float(245, 238, 1, 1);
        result = units.search(searchArea, null);

        System.out.println(result);
    }
}
