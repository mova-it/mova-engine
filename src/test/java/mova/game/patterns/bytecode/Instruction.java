package mova.game.patterns.bytecode;

import java.util.Arrays;

enum Instruction {
    SET_HEALTH,
    GET_HEALTH,
    SET_WISDOM,
    GET_WISDOM,
    SET_AGILITY,
    GET_AGILITY,

    PLAY_SOUND,
    SPAWN_PARTICLES,

    LITERAL,
    ADD,
    DIVIDE;

    public final byte code;

    Instruction() {
        code = (byte) ordinal();
    }

    public byte[] translate(byte... args) {
        if (this == LITERAL) return new byte[] { this.code, args[0] };

        byte[] bytecode = new byte[args.length*2 + 1];

        int index;
        for (index = 0; index < args.length; index++) {
            byte[] literal = LITERAL.translate(args[index]);
            System.arraycopy(literal, 0, bytecode, index*2, 2);
        }

        bytecode[index*2] = this.code;

        return bytecode;
    }
}

class InstructionsBuilder {

    private byte[] bytecode = new byte[0];

    public InstructionsBuilder append(Instruction instruction, byte... args) {
        byte[] instructionByteCode = instruction.translate(args);
        int oldLength = bytecode.length;
        bytecode = Arrays.copyOf(bytecode, bytecode.length + instructionByteCode.length);
        System.arraycopy(instructionByteCode, 0, bytecode, oldLength, instructionByteCode.length);
        return this;
    }

    public byte[] build() {
        return bytecode;
    }

}
