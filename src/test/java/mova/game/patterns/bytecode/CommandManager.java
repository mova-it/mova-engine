package mova.game.patterns.bytecode;


public class CommandManager {

    private final Wizard[] wizards = new Wizard[] {
            new Wizard(),
            new Wizard()
    };

    byte getHealth(byte wizard) {
        return wizards[wizard].health;
    }

    void setHealth(byte wizard, byte amount) {
        wizards[wizard].health = amount;
        System.out.println("Set health of Wizard n°" + wizard + " to " + amount);
    }

    byte getWisdom(byte wizard) {
        return wizards[wizard].wisdom;
    }

    void setWisdom(byte wizard, byte amount) {
        wizards[wizard].wisdom = amount;
        System.out.println("Set wisdom of Wizard n°" + wizard + " to " + amount);
    }

    byte getAgility(byte wizard) {
        return wizards[wizard].agility;
    }

    void setAgility(byte wizard, byte amount) {
        wizards[wizard].agility = amount;
        System.out.println("Set agility of Wizard n°" + wizard + " to " + amount);
    }

    void playSound(byte soundId) {
        System.out.println("Play sound n°" + soundId);
    }

    void spanwParticles(byte particleType) {
        System.out.println("Spanw particles n°" + particleType);
    }

    static class Wizard {
        private byte health = 45;
        private byte agility = 7;
        private byte wisdom = 11;
    }
}
