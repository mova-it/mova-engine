package mova.game.patterns.bytecode;

import mova.lang.ByteUtils;

import java.util.Arrays;

import static mova.game.patterns.bytecode.Instruction.*;

public class VM {

    private static final int MAX_STACK_SIZE = 255;
    private final byte[] stack = new byte[MAX_STACK_SIZE];
    private int stackSize = 0;

    private final CommandManager commandManager = new CommandManager();

    public void interpret(byte[] bytecode) {
        interpret(bytecode, bytecode.length);
    }

    public void interpret(byte[] bytecode, int size) {
        for (int i = 0; i < size; i++) {
            Instruction instruction = Instruction.values()[bytecode[i]];
            switch (instruction) {
                case GET_HEALTH: {
                    byte wizard = pop();
                    byte health = commandManager.getHealth(wizard);
                    push(health);
                    break;
                }
                case SET_HEALTH: {
                    byte amount = pop();
                    byte wizard = pop();
                    commandManager.setHealth(wizard, amount);
                    break;
                }
                case GET_WISDOM: {
                    byte wizard = pop();
                    byte wisdom = commandManager.getWisdom(wizard);
                    push(wisdom);
                    break;
                }
                case SET_WISDOM: {
                    byte amount = pop();
                    byte wizard = pop();
                    commandManager.setWisdom(wizard, amount);
                    break;
                }
                case GET_AGILITY: {
                    byte wizard = pop();
                    byte agility = commandManager.getAgility(wizard);
                    push(agility);
                    break;
                }
                case SET_AGILITY: {
                    byte amount = pop();
                    byte wizard = pop();
                    commandManager.setAgility(wizard, amount);
                    break;
                }
                case PLAY_SOUND: {
                    byte soundId = pop();
                    commandManager.playSound(soundId);
                    break;
                }
                case SPAWN_PARTICLES: {
                    byte particlesType = pop();
                    commandManager.spanwParticles(particlesType);
                    break;
                }
                case LITERAL: {
                    byte value = bytecode[++i];
                    push(value);
                    break;
                }
                case ADD: {
                    byte b = pop();
                    byte a = pop();
                    push((byte) (a + b));
                    break;
                }
                case DIVIDE: {
                    byte b = pop();
                    byte a = pop();
                    push((byte) (a / b));
                    break;
                }
            }
        }
    }

    private void push(byte value) {
        if (stackSize >= MAX_STACK_SIZE) {
            throw new StackOverflowError("stackSize = " + stackSize + "; MAX_STACK_SIZE = " + MAX_STACK_SIZE);
        }

        stack[stackSize++] = value;
    }

    private byte pop() {
        if (stackSize <= 0) {
            throw new StackOverflowError("stackSize = " + stackSize);
        }

        return stack[--stackSize];
    }

    public static void main(String[] args) {
        VM vm = new VM();

        InstructionsBuilder builder = new InstructionsBuilder()
                .append(LITERAL, (byte) 0)
                    .append(GET_HEALTH, (byte) 0)
                            .append(GET_AGILITY, (byte) 0)
                            .append(GET_WISDOM, (byte) 0)
                            .append(ADD)
                        .append(DIVIDE, (byte) 2)
                    .append(ADD)
                .append(SET_HEALTH);

        byte[] bytecode = builder.build();
        System.out.println(Arrays.toString(bytecode) + " => " + ByteUtils.toHexString(bytecode));

        vm.interpret(bytecode);
    }
}
