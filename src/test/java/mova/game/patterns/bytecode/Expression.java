package mova.game.patterns.bytecode;

public interface Expression {

    double evaluate();
}

class NumberExpression implements Expression {

    private final double number;

    public NumberExpression(double number) {
        this.number = number;
    }

    @Override
    public double evaluate() {
        return number;
    }
}

class AdditionExpression implements Expression {

    private final Expression leftExpression;
    private final Expression rightExpression;

    public AdditionExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public double evaluate() {
        double leftResult = leftExpression.evaluate();
        double rightResult = rightExpression.evaluate();

        return leftResult + rightResult;
    }
}

class MultiplicationExpression implements Expression {

    private final Expression leftExpression;
    private final Expression rightExpression;

    public MultiplicationExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public double evaluate() {
        double leftResult = leftExpression.evaluate();
        double rightResult = rightExpression.evaluate();

        return leftResult*rightResult;
    }
}

class Main {
    public static void main(String[] args) {
        MultiplicationExpression calcul = new MultiplicationExpression(new AdditionExpression(new NumberExpression(1), new NumberExpression(2)), new AdditionExpression(new NumberExpression(3), new NumberExpression(4)));

        double result = calcul.evaluate();
        System.out.println("result = " + result);
    }
}
