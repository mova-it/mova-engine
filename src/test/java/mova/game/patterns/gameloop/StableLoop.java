package mova.game.patterns.gameloop;

import mova.lang.ThreadUtils;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

abstract class Loop {

    private final AtomicLong frames = new AtomicLong(0);
    private long interTime = System.currentTimeMillis();

    void processInput() {
        System.out.print("ProcessInput: ");
        takeATinyNap();
    }

    void update() {
        update(-1);
    }

    void update(long elapsedTime) {
        System.out.print(elapsedTime == -1 ? "Update: " : "Update for " + elapsedTime + "ms: ");
        takeATinyNap();
    }

    void render() {
        render(1);
    }

    void render(double percent) {
        System.out.printf("Render for %.2f%% of a frame: ", (percent*100));
        takeATinyNap();
    }

    void metrics() {
        long currTime = System.currentTimeMillis();

        frames.incrementAndGet();
        if (currTime > interTime + 500) {
            long rate = frames.get()*1000/(currTime - interTime);
            System.out.println("=============================================> rate = " + rate + " fps");
            frames.set(0);
            interTime = currTime;
        }
    }

    private void takeATinyNap() {
        takeANap(1 + ThreadLocalRandom.current().nextLong(2));
    }

    protected void takeANap(long napTime) {
        System.out.println("Take a nap of " + napTime + "ms");
        ThreadUtils.takeANap(napTime);
    }
}

public class StableLoop extends Loop {

    private static final long DEMO_TIME = 5000;
    private static final long MS_PER_FRAME = 16;

    public void gameLoop() {
        long frames = 0;
        long startTime = System.currentTimeMillis();

        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            System.out.println("Frame n°" + ++frames);
            long loopTime = System.currentTimeMillis();
            processInput();
            update();
            render();

            System.out.print("Stabilize: ");
            takeANap(Math.max(0, loopTime + MS_PER_FRAME - System.currentTimeMillis()));

            metrics();

            System.out.println();
        }
    }

    public static void main(String[] args) {
        new StableLoop().gameLoop();
    }
}

class FluidLoop extends Loop {

    private static final long DEMO_TIME = 5000;

    public void gameLoop() {
        long frames = 0;
        long startTime = System.currentTimeMillis();

        long lastTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            System.out.println("Frame n°" + ++frames);

            long currentTime = System.currentTimeMillis();
            long elapsedTime = currentTime - lastTime;
            lastTime = currentTime;

            processInput();
            update(elapsedTime);
            render();

            metrics();

            System.out.println();
        }
    }

    public static void main(String[] args) {
        new FluidLoop().gameLoop();
    }
}

class FullLoop extends Loop {

    private static final long DEMO_TIME = 5000;
    private static final long MS_PER_UPDATE = 5;

    public void gameLoop() {
        long frames = 0;
        long startTime = System.currentTimeMillis();

        long lastTime = System.currentTimeMillis();
        double lag = 0.0;
        while (System.currentTimeMillis() - startTime < DEMO_TIME) {
            System.out.println("Frame n°" + ++frames);

            long currentTime = System.currentTimeMillis();
            long elapsedTime = currentTime - lastTime;
            lastTime = currentTime;
            lag += elapsedTime;

            processInput();

            while (lag >= MS_PER_UPDATE) {
                update(MS_PER_UPDATE);
                lag -= MS_PER_UPDATE;
            }

            render(lag/MS_PER_UPDATE);

            metrics();

            System.out.println();
        }
    }

    public static void main(String[] args) {
        new FullLoop().gameLoop();
    }
}
