package mova.game.patterns.type.object;

public class Monster {

    private Breed breed;

    private int currentHealth;

    public Monster(Breed breed) {
        this.breed = breed;
    }

    public String getAttack() {
        return breed.getAttack();
    }

    public static void main(String[] args) {
        Breed goblinBreed = new Breed(20, "The goblin smash your head with à club!");
        Monster goblin = new Monster(goblinBreed);

        System.out.println(goblin.getAttack());
    }
}
