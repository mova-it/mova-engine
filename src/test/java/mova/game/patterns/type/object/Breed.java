package mova.game.patterns.type.object;

public class Breed {

    private int health;
    private String attack;

    public Breed(int health, String attack) {
        this.health = health;
        this.attack = attack;
    }

    public int getHealth() {
        return health;
    }

    public String getAttack() {
        return attack;
    }

    public Monster createMonster() {
        return new Monster(this);
    }

    public static void main(String[] args) {
        Breed dragonBreed = new Breed(200, "The dragon burns everything!");
        Monster dragon = dragonBreed.createMonster();
        System.out.println(dragon.getAttack());
    }
}
