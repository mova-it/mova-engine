package mova.game.patterns.type.object;

// Note: ne peut être modifié au runtime, mais meilleur performance (calcul uniquement à la construction)
public class InheritedBreed {

    private int health;
    private String attack;

    public InheritedBreed(InheritedBreed parent, int health, String attack) {
        this(health, attack);

        if (parent != null) {
            if (this.health == 0) this.health = parent.getHealth();
            if (this.attack == null) this.attack = parent.getAttack();
        }
    }

    public InheritedBreed(int health, String attack) {
        this.health = health;
        this.attack = attack;
    }

    public int getHealth() {
        return health;
    }

    public String getAttack() {
        return attack;
    }

    public class Monster {

        private int currentHealth;

        private Monster() {
            currentHealth = getHealth();
        }

        public String getAttack() {
            return InheritedBreed.this.getAttack();
        }
    }

    public Monster createMonster() {
        return new Monster();
    }

    public static void main(String[] args) {
        InheritedBreed dragonBreed = new InheritedBreed(200, "The dragon burns everything!");
        InheritedBreed dragonLychBreed = new InheritedBreed(dragonBreed, 0, "The dragon disintegrate everything!");

        Monster dragon = dragonBreed.createMonster();
        System.out.println(dragon.getAttack());
        System.out.println(dragon.currentHealth);

        Monster dragonLych = dragonLychBreed.createMonster();
        System.out.println(dragonLych.getAttack());
        System.out.println(dragonLych.currentHealth);
    }
}
