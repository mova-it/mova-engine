package mova.game.patterns.type.object;

// Note: peut êtr emodifier au runtime, mais avec une performance moindre (maintenir un pointeur en mémoire + if dans les getter)
public class PersistentInheritableBreed {

    private PersistentInheritableBreed parent;
    private int health;
    private String attack;

    public PersistentInheritableBreed(PersistentInheritableBreed parent, int health, String attack) {
        this.parent = parent;
        this.health = health;
        this.attack = attack;
    }

    public PersistentInheritableBreed(int health, String attack) {
        this.health = health;
        this.attack = attack;
    }

    public int getHealth() {
        if (health != 0 || parent == null) return health;

        return parent.getHealth();
    }

    public String getAttack() {
        if (attack != null || parent == null) return attack;

        return parent.getAttack();
    }

    public class Monster {

        private int currentHealth;

        private Monster() {
            currentHealth = getHealth();
        }

        public String getAttack() {
            return PersistentInheritableBreed.this.getAttack();
        }
    }

    public PersistentInheritableBreed.Monster createMonster() {
        return new PersistentInheritableBreed.Monster();
    }

    public static void main(String[] args) {
        PersistentInheritableBreed dragonBreed = new PersistentInheritableBreed(200, "The dragon burns everything!");
        PersistentInheritableBreed dragonLychBreed = new PersistentInheritableBreed(dragonBreed, 0, "The dragon disintegrate everything!");

        Monster dragon = dragonBreed.createMonster();
        System.out.println(dragon.getAttack());
        System.out.println(dragon.currentHealth);

        Monster dragonLych = dragonLychBreed.createMonster();
        System.out.println(dragonLych.getAttack());
        System.out.println(dragonLych.currentHealth);
    }
}
