package mova.game.patterns.datalocality2;

import mova.benchmark.Benchmark;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Main {

    private static final int ITER = 100;
    private static final int NB_ELEMENTS = 10000000;

    public static class A implements Serializable {
        public int i;

        public A(int i) {
            this.i = i;
        }

        @Override
        public String toString() {
            return Integer.toString(i);
        }
    }

    public static class B {

        private static int numBs;

        // Note:
        // L'objet prend environ NB_ELEMENTS * 32 / 8
        // Il s'agit de int mais on pourrait avoir un buffer de bytes qui stocke les informations de manière "serializée"
        // On pourrait des lors effectuer les opérations directement sur le buffer si on sait manipuler les bytes du buffer correspondant à la donnée
        // (cf. class C)
        public static int[] is = new int[NB_ELEMENTS];

        public int id;

        public B(int i) {
            id = numBs++;
            is[id] = i;
        }

        @Override
        public String toString() {
            return Integer.toString(is[id]);
        }
    }

    public static class C {

        private static int numCs;

        public static ByteBuffer buffer = ByteBuffer.allocate(NB_ELEMENTS *Integer.BYTES);

        public int id;

        public C(int i) {
            id = numCs++;
            buffer.putInt(id*Integer.BYTES, i);
        }

        @Override
        public String toString() {
            return Integer.toString(buffer.getInt(id*Integer.BYTES));
        }
    }

    public static class CDirect {

        private static int numCs;

        // TODO putain ca marche!!!!!
        public static ByteBuffer buffer = ByteBuffer.allocateDirect(NB_ELEMENTS *Integer.BYTES);

        public int id;

        public CDirect(int i) {
            id = numCs++;
            buffer.putInt(id*Integer.BYTES, i);
        }

        @Override
        public String toString() {
            return Integer.toString(buffer.getInt(id*Integer.BYTES));
        }
    }

    public static class CDirect2 {

        private static int numCs;

        public static ByteBuffer buffer = ByteBuffer.allocateDirect(NB_ELEMENTS *Integer.BYTES);

        public int id;

        public CDirect2(int i) {
            id = numCs++;
            buffer.putInt(id*Integer.BYTES, i);
        }

        @Override
        public String toString() {
            return Integer.toString(buffer.getInt(id*Integer.BYTES));
        }

        public static void inc(int index) {
            buffer.putInt(index, buffer.getInt(index) + 1);
        }
    }



    public static void main(String[] args) throws IOException, ClassNotFoundException {

        System.out.println("[AS]:");
        A[] as = IntStream.range(0, NB_ELEMENTS).mapToObj(A::new).toArray(A[]::new);
        Benchmark.of(() -> {
            for (A a : as) a.i += 1;
        }, ITER, TimeUnit.MILLISECONDS);
        System.out.println();




        System.out.println("[BS]:");
        IntStream.range(0, NB_ELEMENTS).forEach(B::new);
        Benchmark.of(() -> {
            for (int j = 0; j < B.numBs; j++) B.is[j] += 1;
        }, ITER, TimeUnit.MILLISECONDS);
        System.out.println();




        System.out.println("[CS]: array = " + C.buffer.hasArray());
        IntStream.range(0, NB_ELEMENTS).forEach(C::new);
        Benchmark.of(() -> {
            for (int j = 0; j < C.numCs; j++) {
                int index = j*Integer.BYTES;
                C.buffer.putInt(index, C.buffer.getInt(index) + 1);
            }
        }, ITER, TimeUnit.MILLISECONDS);
        System.out.println();




        System.out.println("[CDirectS]: array = " + CDirect.buffer.hasArray());
        IntStream.range(0, NB_ELEMENTS).forEach(CDirect::new);
        Benchmark.of(() -> {
            for (int j = 0; j < CDirect.numCs; j++) {
                int index = j*Integer.BYTES;
                CDirect.buffer.putInt(index, CDirect.buffer.getInt(index) + 1);
            }
        }, ITER, TimeUnit.MILLISECONDS);
        System.out.println();




        System.out.println("[CDirect2S]: array = " + CDirect2.buffer.hasArray());
        IntStream.range(0, NB_ELEMENTS).forEach(CDirect2::new);
        Benchmark.of(() -> {
            for (int j = 0; j < CDirect2.numCs; j++) {
                CDirect2.inc(j*Integer.BYTES);
            }
        }, ITER, TimeUnit.MILLISECONDS);
        System.out.println();
    }
}
