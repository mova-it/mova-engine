package mova.game.patterns.dirtyflag;

public class GraphNode {

    private static final int MAX_CHILDREN = 1000;

    protected final GraphNode[] children = new GraphNode[MAX_CHILDREN];
    protected int numChildren;

    protected final Mesh mesh;
    protected Transform local;

    public GraphNode(Mesh mesh) {
        this.mesh = mesh;
        local = Transform.origin();
    }

    void setTransform(Transform local) {
        this.local = local;
    }

    void renderMesh(Mesh mesh, Transform transform) {
        System.out.println("[GraphNode.render] Apply transform factor " + transform.getFactor() + " to mesh id = " + mesh.id);
    }

    void render(Transform parentWorld) {
        Transform world = local.combine(parentWorld);

        if (mesh != null) renderMesh(mesh, world);
        else System.out.println("[GraphNode.render] No mesh");

        for (int i = 0; i < numChildren; i++) {
            children[i].render(world);
        }
    }

    void addChild(GraphNode child) {
        children[numChildren++] = child;
    }
}
