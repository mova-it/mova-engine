package mova.game.patterns.dirtyflag;

import mova.lang.ThreadUtils;

public class Transform {

    private int factor;

    Transform() {
        factor = 1;
    }

    public static Transform origin() {
        return new Transform();
    }

    public Transform combine(Transform transform) {
        ThreadUtils.takeANap(1);
        System.out.println("[Transform.combine] Takes a lot of time: this.factor = " + factor + "; transform.factor = " + transform.getFactor());

        Transform t = new Transform();
        t.factor += transform.factor;
        return t;
    }

    public int getFactor() {
        return factor;
    }
}
