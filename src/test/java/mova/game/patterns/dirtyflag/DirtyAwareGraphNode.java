package mova.game.patterns.dirtyflag;

public class DirtyAwareGraphNode extends GraphNode {

    private Transform worldCache;
    private boolean dirty;

    public DirtyAwareGraphNode(Mesh mesh) {
        super(mesh);

        dirty = true;
    }

    void setTransform(Transform local) {
        this.local = local;
        dirty = true;
    }

    void render(Transform parentWorld, boolean dirty) {
        dirty |= this.dirty;
        if (dirty) {
            worldCache = local.combine(parentWorld);
            this.dirty = false;
        }

        if (mesh != null) renderMesh(mesh, worldCache);
        else System.out.println("[GraphNode.render] No mesh");

        for (int i = 0; i < numChildren; i++) {
            ((DirtyAwareGraphNode) children[i]).render(worldCache, dirty);
        }
    }

    @Override
    void render(Transform parentWorld) {
        render(parentWorld, dirty);
    }
}
