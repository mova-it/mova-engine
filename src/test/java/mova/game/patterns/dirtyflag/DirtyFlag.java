package mova.game.patterns.dirtyflag;

public class DirtyFlag {

    private static final int NB_CHILDS_PER_LEVEL = 2;

    public static void withoutDirtyFlag() {
        GraphNode node = null;

        GraphNode root = new GraphNode(null);

        int counter = 1;
        for (int i = 0; i < NB_CHILDS_PER_LEVEL; i++) {
            GraphNode child = new GraphNode(new Mesh(counter++));
            root.addChild(child);


            for (int j = 0; j < NB_CHILDS_PER_LEVEL; j++) {
                GraphNode greatChild = new GraphNode(new Mesh(counter++));
                child.addChild(greatChild);

                for (int k = 0; k < NB_CHILDS_PER_LEVEL; k++) {
                    GraphNode greatGreatChild = new GraphNode(new Mesh(counter++));
                    greatChild.addChild(greatGreatChild);
                }
            }

            node = child;
        }

        System.out.println("Initial sans flag:");
        root.render(Transform.origin());

        System.out.println("\n");

        System.out.println("Second sans flag:");
        root.render(Transform.origin());

        System.out.println("\n");

        System.out.println("Mis à jour sans flag:");
        node.setTransform(Transform.origin());
        root.render(Transform.origin());
    }

    public static void withDirtyFlag() {
        GraphNode node = null;

        GraphNode root = new DirtyAwareGraphNode(null);

        int counter = 1;
        for (int i = 0; i < NB_CHILDS_PER_LEVEL; i++) {
            GraphNode child = new DirtyAwareGraphNode(new Mesh(counter++));
            root.addChild(child);


            for (int j = 0; j < NB_CHILDS_PER_LEVEL; j++) {
                GraphNode greatChild = new DirtyAwareGraphNode(new Mesh(counter++));
                child.addChild(greatChild);

                for (int k = 0; k < NB_CHILDS_PER_LEVEL; k++) {
                    GraphNode greatGreatChild = new DirtyAwareGraphNode(new Mesh(counter++));
                    greatChild.addChild(greatGreatChild);
                }
            }

            node = child;
        }

        System.out.println("Initial avec flag:");
        root.render(Transform.origin());

        System.out.println("\n");

        System.out.println("Second avec flag:");
        root.render(Transform.origin());

        System.out.println("\n");

        System.out.println("Mis à jour avec flag:");
        node.setTransform(Transform.origin());
        root.render(Transform.origin());
    }

    public static void main(String[] args) {
//        withoutDirtyFlag();
        withDirtyFlag();
    }
}
