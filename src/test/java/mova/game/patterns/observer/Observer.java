package mova.game.patterns.observer;

import mova.game.patterns.observer.SubjectWithLinkedObservers.LinkableObserver;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/* Notes: importantes:
    - Il s'agit de découpler des choses qui n'ont rien à voir mais qui doivent travailler ensemble
    - Ce pattern est tellement utilisé qu'il possède sa propre implémentation dan l'API java
    - Le point critique est dans l'implémentation de la liste d'observer et sa gestion
 */

public interface Observer<E> {

    void onNotify(E entity, Object event);
}

class Achievements implements Observer<String> {

    @Override
    public void onNotify(String entity, Object event) {
        System.out.println(entity + " gagne le badge \"Buveur de bière\" level " + event);
    }
}

class Sounds implements Observer<String> {

    @Override
    public void onNotify(String entity, Object event) {
        System.out.println("Jouer le son " + event + " pour " + entity);
    }
}

class Etiquette implements Observer<String> {

    private final int number;

    public Etiquette(int number) {
        this.number = number;
    }

    @Override
    public void onNotify(String entity, Object event) {
        System.out.println(entity + " a été etiquetté par " + this + " avec le level " + event);
    }

    @Override
    public String toString() {
        return "Etiquette{" +
                "number=" + number +
                '}';
    }
}




abstract class Subject<E> {

    protected abstract Collection<Observer<E>> getObservers();

    protected boolean addObserver(Observer<E> observer) {
        return getObservers().add(observer);
    }

    protected boolean removeObserver(Observer<E> observer) {
        return getObservers().remove(observer);
    }

    protected void notify(E entity, Object event) {
        getObservers().forEach(observer -> observer.onNotify(entity, event));
    }
}

abstract class SubjectWithObserversList<E> extends Subject<E> {

    private final Vector<Observer<E>> observers = new Vector<>();

    @Override
    protected Collection<Observer<E>> getObservers() {
        return observers;
    }
}

abstract class SubjectWithObserversArray<E> extends Subject<E> {

    @SuppressWarnings("unchecked")
    private Observer<E>[] observers = new Observer[0];

    @Override
    protected Collection<Observer<E>> getObservers() {
        return Arrays.asList(observers);
    }

    @Override
    protected synchronized boolean addObserver(Observer<E> observer) {
        if (observer == null) throw new IllegalArgumentException("Un observateur ne peut être null");

        Observer<E>[] oldObservers = observers;
        observers = _createArray(oldObservers.length + 1);
        System.arraycopy(oldObservers, 0, observers, 0, oldObservers.length);

        observers[observers.length - 1] = observer;
        return true;
    }

    @Override
    protected synchronized boolean removeObserver(Observer<E> observer) {
        if (observer == null) throw new IllegalArgumentException("Les observateurs ne peuvent contenir d'observateur null");

        for (int i = 0; i < observers.length; i++) {
            if (observers[i].equals(observer)) {
                Observer<E>[] oldObservers = observers;
                observers = _createArray(oldObservers.length - 1);

                oldObservers[i] = null;

                if (observers.length > 0) {
                    System.arraycopy(oldObservers, 0, observers, 0, i + 1);
                    System.arraycopy(oldObservers, i + 1, observers, i, observers.length - i);
                }

                return true;
            }
        }

        return false;
    }

    @Override
    protected synchronized void notify(E entity, Object event) {
        super.notify(entity, event);
    }

    @SuppressWarnings("unchecked")
    private Observer<E>[] _createArray(int length) {
        return (Observer<E>[]) Array.newInstance(observers.getClass().getComponentType(), length);
    }
}

abstract class SubjectWithLinkedObservers<E> extends Subject<E> {

    private LinkableObserver<E> head = null;

    @Override
    protected Collection<Observer<E>> getObservers() {
        List<Observer<E>> observers = new ArrayList<>();

        LinkableObserver<E> currentObserver = head;
        while (currentObserver != null) {
            observers.add(currentObserver);

            currentObserver = currentObserver.next;
        }

        return observers;
    }

    @Override
    protected boolean addObserver(Observer<E> observer) {
        if (!(observer instanceof LinkableObserver)) throw new IllegalArgumentException("Les observateurs doitvent être du type LinkableObserver");

        return addObserver((LinkableObserver<E>) observer);
    }

    protected boolean addObserver(LinkableObserver<E> observer) {
        observer.next = head;
        head = observer;

        return true;
    }

    @Override
    protected boolean removeObserver(Observer<E> observer) {
        if (!(observer instanceof LinkableObserver)) throw new IllegalArgumentException("Les observateurs doitvent être du type LinkableObserver");

        return removeObserver((LinkableObserver<E>) observer);
    }

    protected boolean removeObserver(LinkableObserver<E> observer) {
        if (head == observer) {
            head = observer.next;
            observer.next = null;

            return true;
        }

        LinkableObserver<E> currentObserver = head;
        while (currentObserver != null) {
            if (currentObserver.next == observer) {
                currentObserver.next = observer.next;
                observer.next = null;

                return true;
            }

            currentObserver = currentObserver.next;
        }

        return false;
    }

    @Override
    protected void notify(E entity, Object event) {
        LinkableObserver<E> currentObserver = head;
        while (currentObserver != null) {
            currentObserver.onNotify(entity, event);
            currentObserver = currentObserver.next;
        }
    }

    protected static class LinkableObserver<E> implements Observer<E> {

        private final Observer<E> delegate;
        private LinkableObserver<E> next = null;

        public LinkableObserver(Observer<E> delegate) {
            this.delegate = delegate;
        }

        @Override
        public void onNotify(E entity, Object event) {
            delegate.onNotify(entity, event);
        }
    }
}

class Physics extends SubjectWithObserversList<String> {

    Random randomator = ThreadLocalRandom.current();

    void updateEntity(String entity) {
        notify(entity, randomator.nextInt(5));
    }
}

class PhysicsII extends SubjectWithObserversArray<String> {

    Random randomator = ThreadLocalRandom.current();

    void updateEntity(String entity) {
        notify(entity, randomator.nextInt(5));
    }

    void printObservers() {
        System.out.println("Observers = " + getObservers());
    }
}

class PhysicsIII extends SubjectWithLinkedObservers<String> {

    Random randomator = ThreadLocalRandom.current();

    void updateEntity(String entity) {
        notify(entity, randomator.nextInt(5));
    }

    void printObservers() {
        System.out.println("Observers = " + getObservers());
    }
}






class Main {
    public static void main(String[] args) {
        Physics physics = new Physics();

        Achievements achievements = new Achievements();
        physics.addObserver(achievements);

        Sounds sounds = new Sounds();
        physics.addObserver(sounds);

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();

        physics.removeObserver(sounds);

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");
    }
}

class MainII {
    public static void main(String[] args) {
        PhysicsII physics = new PhysicsII();

        Etiquette e1 = new Etiquette(1);
        physics.addObserver(e1);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        Etiquette e2 = new Etiquette(2);
        physics.addObserver(e2);
        physics.printObservers();

        Etiquette e3 = new Etiquette(3);
        physics.addObserver(e3);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();

        physics.removeObserver(e2);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();

        physics.removeObserver(e1);
        physics.printObservers();

        physics.removeObserver(new Etiquette(4));
        physics.printObservers();

        physics.removeObserver(e3);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();
    }
}

class MainIII {
    public static void main(String[] args) {
        PhysicsIII physics = new PhysicsIII();

        LinkableObserver<String> e1 = new LinkableObserver<>(new Etiquette(1));
        physics.addObserver(e1);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();

        LinkableObserver<String> e2 = new LinkableObserver<>(new Etiquette(2));
        physics.addObserver(e2);
        physics.printObservers();

        LinkableObserver<String> e3 = new LinkableObserver<>(new Etiquette(3));
        physics.addObserver(e3);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();

        physics.removeObserver(e2);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();

        physics.removeObserver(e1);
        physics.printObservers();

        physics.removeObserver(new LinkableObserver<>(new Etiquette(4)));
        physics.printObservers();

        physics.removeObserver(e3);
        physics.printObservers();

        physics.updateEntity("Athoy");
        physics.updateEntity("Keros ou qui qu'il soit");
        physics.updateEntity("Catherine");

        System.out.println();
    }
}
