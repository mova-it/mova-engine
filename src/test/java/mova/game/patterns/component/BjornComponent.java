package mova.game.patterns.component;

public class BjornComponent {

    int velocity;
    int x;
    int y;

    private final InputComponent inputComponent = new InputComponent();
    private final PhysicsComponent physicsComponent = new PhysicsComponent();
    private final GraphicsComponent graphicsComponent = new GraphicsComponent();

    public BjornComponent(int velocity, int x, int y) {
        this.velocity = velocity;
        this.x = x;
        this.y = y;
    }

    public void update(World world, Graphics graphics) {
        inputComponent.update(this);
        physicsComponent.update(this, world);
        graphicsComponent.update(this, graphics);
    }
}

class InputComponent {

    private static final int WALK_ACCELERATION = 1;

    public void update(BjornComponent bjorn) {

        switch (Controller.getJoystickDirection()) {
            case Controller.DIR_LEFT:
                bjorn.velocity -= WALK_ACCELERATION;
                break;
            case Controller.DIR_RIGHT:
                bjorn.velocity += WALK_ACCELERATION;
                break;
        }
    }
}

class PhysicsComponent {

    private Object volume;

    public void update(BjornComponent bjorn, World world) {
        bjorn.x += bjorn.velocity;
        world.resolveCollision(volume, bjorn.x, bjorn.y, bjorn.velocity);
    }

}

class GraphicsComponent {

    private Object spriteStand;
    private Object spriteWalkLeft;
    private Object spriteWalkRight;

    public void update(BjornComponent bjorn, Graphics graphics) {
        Object sprite = spriteStand;
        if (bjorn.velocity < 0) sprite = spriteWalkLeft;
        else if (bjorn.velocity > 0) sprite = spriteWalkRight;
        graphics.draw(sprite, bjorn.x, bjorn.y);
    }
}
