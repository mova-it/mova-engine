package mova.game.patterns.component;

public class Bjorn {

    private static final int WALK_ACCELERATION = 1;

    private int velocity;
    private int x;
    private int y;

    private Object volume;

    private Object spriteStand;
    private Object spriteWalkLeft;
    private Object spriteWalkRight;

    public Bjorn(int velocity, int x, int y) {
        this.velocity = velocity;
        this.x = x;
        this.y = y;
    }

    public void update(World world, Graphics graphics) {
        switch (Controller.getJoystickDirection()) {
            case Controller.DIR_LEFT:
                velocity -= WALK_ACCELERATION;
                break;
            case Controller.DIR_RIGHT:
                velocity += WALK_ACCELERATION;
                break;
        }

        x += velocity;
        world.resolveCollision(volume, x, y, velocity);

        Object sprite = spriteStand;
        if (velocity < 0) sprite = spriteWalkLeft;
        else if (velocity > 0) sprite = spriteWalkRight;
        graphics.draw(sprite, x, y);
    }
}

