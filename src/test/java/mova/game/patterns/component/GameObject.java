package mova.game.patterns.component;

public class GameObject {

    int velocity;
    int x;
    int y;

    private final InputComponentI inputComponent;
    private final PhysicsComponentI physicsComponent;
    private final GraphicsComponentI graphicsComponent;

    public GameObject(int velocity, int x, int y, InputComponentI inputComponent, PhysicsComponentI physicsComponent, GraphicsComponentI graphicsComponent) {
        this.velocity = velocity;
        this.x = x;
        this.y = y;

        this.inputComponent = inputComponent;
        this.physicsComponent = physicsComponent;
        this.graphicsComponent = graphicsComponent;
    }

    public void update(World world, Graphics graphics) {
        inputComponent.update(this);
        physicsComponent.update(this, world);
        graphicsComponent.update(this, graphics);
    }
}

interface InputComponentI {
    void update(GameObject gameObject);
}

interface PhysicsComponentI {
    void update(GameObject gameObject, World world);
}

interface GraphicsComponentI {
    void update(GameObject gameObject, Graphics graphics);
}

class PlayerInputComponent implements InputComponentI {

    private static final int WALK_ACCELERATION = 1;

    @Override
    public void update(GameObject gameObject) {

        switch (Controller.getJoystickDirection()) {
            case Controller.DIR_LEFT:
                gameObject.velocity -= WALK_ACCELERATION;
                break;
            case Controller.DIR_RIGHT:
                gameObject.velocity += WALK_ACCELERATION;
                break;
        }
    }
}

class DemoInputComponent implements InputComponentI {

    private static final int WALK_ACCELERATION = 1;

    @Override
    public void update(GameObject gameObject) {
        // Code pour le mode démo
    }
}

class BjornPhysicsComponent implements PhysicsComponentI {

    private Object volume;

    @Override
    public void update(GameObject gameObject, World world) {
        gameObject.x += gameObject.velocity;
        world.resolveCollision(volume, gameObject.x, gameObject.y, gameObject.velocity);
    }
}

class BjornGraphicsComponent implements GraphicsComponentI {

    private Object spriteStand;
    private Object spriteWalkLeft;
    private Object spriteWalkRight;

    @Override
    public void update(GameObject gameObject, Graphics graphics) {
        Object sprite = spriteStand;
        if (gameObject.velocity < 0) sprite = spriteWalkLeft;
        else if (gameObject.velocity > 0) sprite = spriteWalkRight;
        graphics.draw(sprite, gameObject.x, gameObject.y);
    }
}

class Main {
    public static void main(String[] args) {
        GameObject bjorn = new GameObject(0, 0, 0, new PlayerInputComponent(), new BjornPhysicsComponent(), new BjornGraphicsComponent());
        GameObject demoBjorn = new GameObject(0, 0, 0, new DemoInputComponent(), new BjornPhysicsComponent(), new BjornGraphicsComponent());
    }
}
