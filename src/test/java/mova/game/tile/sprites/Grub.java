package mova.game.tile.sprites;

import mova.game.graphics.sprite.Animation;
import mova.game.graphics.sprite.Creature;

public class Grub extends Creature {

    public Grub(Animation left, Animation right, Animation deadLeft, Animation deadRight) {
        super(left, right, deadLeft, deadRight);
    }

    public Grub(Grub grub) {
        super(grub);
    }

    public float getMaxSpeed() {
        return 0.05f;
    }
}
