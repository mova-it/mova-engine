package mova.game.tile.sprites;

import mova.game.graphics.sprite.Animation;
import mova.game.graphics.sprite.Creature;

public class Fly extends Creature {

    public Fly(Animation left, Animation right, Animation deadLeft, Animation deadRight) {
        super(left, right, deadLeft, deadRight);
    }

    // TODO évaluer l'utilisation d'une factory method ou d'un builder pattern pour retirer le risque d'oublier l'ajout d'un constructeur privé de copie (pour le clonage)
    public Fly(Fly fly) {
        super(fly);
    }

    public float getMaxSpeed() {
        return 0.2f;
    }

    public boolean isFlying() {
        return isAlive();
    }
}
