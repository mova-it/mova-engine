package mova.game.tile;

import mova.game.graphics.ImageUtils;
import mova.game.graphics.sprite.Animation;
import mova.game.graphics.sprite.Sprite;
import mova.game.graphics.tile.TileMap;
import mova.game.graphics.tile.TileMapRenderer;
import mova.game.tile.sprites.Fly;
import mova.game.tile.sprites.Grub;
import mova.game.tile.sprites.Player;
import mova.game.graphics.sprite.PowerUp;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.*;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.IntStream;


public class ResourceManager {

    private static final String COMMENT_LINE_CHARACTER_INTRODUCER = "#";
    private static final Predicate<String> COMMENT_LINE_FILTER = line -> !line.startsWith(COMMENT_LINE_CHARACTER_INTRODUCER);

    private final GraphicsConfiguration gc;

    private ArrayList<Image> tiles;
    private int currentMap;

    private Sprite playerSprite;
    private Sprite musicSprite;
    private Sprite coinSprite;
    private Sprite goalSprite;
    private Sprite grubSprite;
    private Sprite flySprite;

    public ResourceManager(GraphicsConfiguration gc) {
        this.gc = gc;
        loadTileImages();
        loadCreatureSprites();
        loadPowerUpSprites();
    }

    public Image getMirrorImage(Image image) {
        return getScaledImage(image, -1, 1);
    }

    public Image getFlippedImage(Image image) {
        return getScaledImage(image, 1, -1);
    }

    private Image getScaledImage(Image image, float x, float y) {
        AffineTransform transform = new AffineTransform();
        transform.scale(x, y);
        transform.translate(
                (x - 1)*image.getWidth(null)/2,
                (y - 1)*image.getHeight(null)/2);

        Image newImage = gc.createCompatibleImage(
                image.getWidth(null),
                image.getHeight(null),
                Transparency.BITMASK);

        Graphics2D g = (Graphics2D)newImage.getGraphics();
        g.drawImage(image, transform, null);
        g.dispose();

        return newImage;
    }

    public TileMap loadNextMap() {
        TileMap map = null;
        while (map == null) {
            currentMap++;
            try {
                map = loadMap("/maps/map" + currentMap + ".txt");
            } catch (IOException ex) {
                if (currentMap == 1) return null;
                currentMap = 0;
            }
        }

        return map;
    }

    public TileMap reloadMap() {
        try {
            return loadMap("/maps/map" + currentMap + ".txt");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private TileMap loadMap(String filename) throws IOException {
        InputStream inputStream = getClass().getResourceAsStream(filename);
        if (inputStream == null) throw new IOException("Map loading exception!"); // TODO ce serait mieux avec une exception perso: MapLoadingException

        //TODO je suis sûr qu'il y a moyen de simplifier encore cette partie
        //TODO il faudra faire la part des choses entre le jeu de plateforme et le moteur de jeu
        ArrayList<String> lines = new ArrayList<>();

        TileMap map;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            IntStream stream = reader.lines()
                    .filter(COMMENT_LINE_FILTER)
                    .mapToInt(line -> {
                        lines.add(line);
                        return line.length();
                    });

            map = new TileMap(stream.max().orElse(0), lines.size());
        }

        for (int y = 0; y < map.getHeight(); y++) {
            String line = lines.get(y);
            for (int x = 0; x < line.length(); x++) {
                char c = line.charAt(x);

                // TODO pour les tiles, uine map serait elle plus efficace?
                int tile = c - 'A';
                if (tile >= 0 && tile < tiles.size()) {
                    map.setTiles(x, y, tiles.get(tile));
                } else if (c == 'o') { //TODO une Map<Character, Sprite> pourrait avoir son utilité? ou un switch?
                    addSprite(map, coinSprite, x, y);
                } else if (c == '!') {
                    addSprite(map, musicSprite, x, y);
                } else if (c == '*') {
                    addSprite(map, goalSprite, x, y);
                } else if (c == '1') {
                    addSprite(map, grubSprite, x, y);
                } else if (c == '2') {
                    addSprite(map, flySprite, x, y);
                }
            }
        }

        Sprite player = copy(playerSprite);
        player.setCoordinates(TileMapRenderer.tilesToPixels(3), 0);
        map.setPlayer(player);

        return map;
    }

    private Sprite copy(Sprite sprite) {
        if (sprite instanceof Fly) {
            return new Fly((Fly) sprite);
        } else if (sprite instanceof Grub) {
            return new Grub((Grub) sprite);
        } else if (sprite instanceof Player) {
            return new Player((Player) sprite);
        } else if (sprite instanceof PowerUp.Goal) {
            return new PowerUp.Goal((PowerUp.Goal) sprite);
        } else if (sprite instanceof PowerUp.Star) {
            return new PowerUp.Star((PowerUp.Star) sprite);
        } else if (sprite instanceof PowerUp.Music) {
            return new PowerUp.Music((PowerUp.Music) sprite);
        } else {
            return new Sprite(sprite);
        }
    }

    private void addSprite(TileMap map, Sprite hostSprite, int tileX, int tileY) {
        if (hostSprite != null) {
            Sprite sprite = copy(hostSprite);

            sprite.setX(TileMapRenderer.tilesToPixels(tileX) + ((TileMapRenderer.tilesToPixels(1) - sprite.getWidth()) >> 1));
            sprite.setY(TileMapRenderer.tilesToPixels(tileY + 1) - sprite.getHeight());

            map.addSprite(sprite);
        }
    }


    public void loadTileImages() {
        // TODO Le chargement pourrait se faire dans une map si c'est plus efficace qu'une list à index (je ne crois pas)
        // TODO mieux gérer les erreurs (fichier non trouvé)
        tiles = new ArrayList<>();
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            String name = "/images/tile_" + ch + ".png";
            try {
                tiles.add(ImageUtils.loadImage(name));
            } catch (Exception iae) {
                System.out.println("Impossible de trouver l'image: " + name + ", " + iae.getClass() + ": " + iae.getMessage());
            }
        }
    }


    public void loadCreatureSprites() {
        Image[][] images = new Image[4][];

        try {
            images[0] = new Image[] {
                    ImageUtils.loadImage("/images/player1.png"),
                    ImageUtils.loadImage("/images/player2.png"),
                    ImageUtils.loadImage("/images/player3.png"),
                    ImageUtils.loadImage("/images/fly1.png"),
                    ImageUtils.loadImage("/images/fly2.png"),
                    ImageUtils.loadImage("/images/fly3.png"),
                    ImageUtils.loadImage("/images/grub1.png"),
                    ImageUtils.loadImage("/images/grub2.png")
            };
        } catch (IOException e) {
            throw new IllegalStateException("Impossible de charger les images", e);
        }

        images[1] = new Image[images[0].length];
        images[2] = new Image[images[0].length];
        images[3] = new Image[images[0].length];
        for (int i = 0; i < images[0].length; i++) {
            images[1][i] = getMirrorImage(images[0][i]);
            images[2][i] = getFlippedImage(images[0][i]);
            images[3][i] = getFlippedImage(images[1][i]);
        }

        Animation[] playerAnim = new Animation[4];
        Animation[] flyAnim = new Animation[4];
        Animation[] grubAnim = new Animation[4];
        for (int i = 0; i < 4; i++) {
            playerAnim[i] = createPlayerAnim(images[i][0], images[i][1], images[i][2]);
            flyAnim[i] = createFlyAnim(images[i][3], images[i][4], images[i][5]);
            grubAnim[i] = createGrubAnim(images[i][6], images[i][7]);
        }

        playerSprite = new Player(playerAnim[0], playerAnim[1], playerAnim[2], playerAnim[3]);
        flySprite = new Fly(flyAnim[0], flyAnim[1], flyAnim[2], flyAnim[3]);
        grubSprite = new Grub(grubAnim[0], grubAnim[1], grubAnim[2], grubAnim[3]);
    }


    private Animation createPlayerAnim(Image player1, Image player2, Image player3) {
        Animation anim = new Animation();
        anim.addFrame(player1, 250);
        anim.addFrame(player2, 150);
        anim.addFrame(player1, 150);
        anim.addFrame(player2, 150);
        anim.addFrame(player3, 200);
        anim.addFrame(player2, 150);
        return anim;
    }


    // TODO les animations pourraient être gérer par un fichier de configuration:
    // ex:
    // nom_du_sprite.animation.images=url_image1,url_image2,url_image3
    // nom_du_sprite.animation=image1:50,image2:50,image3:50,image2:50
    private Animation createFlyAnim(Image img1, Image img2, Image img3) {
        Animation anim = new Animation();
        anim.addFrame(img1, 50);
        anim.addFrame(img2, 50);
        anim.addFrame(img3, 50);
        anim.addFrame(img2, 50);
        return anim;
    }


    private Animation createGrubAnim(Image img1, Image img2) {
        Animation anim = new Animation();
        anim.addFrame(img1, 250);
        anim.addFrame(img2, 250);
        return anim;
    }

    private void loadPowerUpSprites() {
        Animation anim = new Animation();
        try {
            anim.addFrame(ImageUtils.loadImage("/images/heart1.png"), 150);
            anim.addFrame(ImageUtils.loadImage("/images/heart2.png"), 150);
            anim.addFrame(ImageUtils.loadImage("/images/heart3.png"), 150);
            anim.addFrame(ImageUtils.loadImage("/images/heart2.png"), 150);
        } catch (IOException e) {
            throw new IllegalStateException("Impossible de charger les images de coeur", e);
        }
        goalSprite = new PowerUp.Goal(anim);

        anim = new Animation();
        try {
            anim.addFrame(ImageUtils.loadImage("/images/star1.png"), 100);
            anim.addFrame(ImageUtils.loadImage("/images/star2.png"), 100);
            anim.addFrame(ImageUtils.loadImage("/images/star3.png"), 100);
            anim.addFrame(ImageUtils.loadImage("/images/star4.png"), 100);
        } catch (IOException e) {
            throw new IllegalStateException("Impossible de charger les images d'étoile", e);
        }
        coinSprite = new PowerUp.Star(anim);

        anim = new Animation();
        try {
            anim.addFrame(ImageUtils.loadImage("/images/music1.png"), 150);
            anim.addFrame(ImageUtils.loadImage("/images/music2.png"), 150);
            anim.addFrame(ImageUtils.loadImage("/images/music3.png"), 150);
            anim.addFrame(ImageUtils.loadImage("/images/music2.png"), 150);
        } catch (IOException e) {
            throw new IllegalStateException("Impossible de charger les images de note", e);
        }
        musicSprite = new PowerUp.Music(anim);
    }
}
