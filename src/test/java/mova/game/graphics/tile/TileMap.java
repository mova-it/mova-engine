package mova.game.graphics.tile;

import mova.game.graphics.sprite.Sprite;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class TileMap {

    private final Image[][] tiles;
    private final LinkedList<Sprite> sprites;
    private Sprite player;

    public TileMap(int width, int height) {
        tiles = new Image[width][height];
        sprites = new LinkedList<>();
    }

    public int getWidth() {
        return tiles.length;
    }

    public int getHeight() {
        return tiles[0].length;
    }

    public Image getTile(int x, int y) {
        if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) {
            return null;
        } else {
            return tiles[x][y];
        }
    }

    public void setTiles(int x, int y, Image tile) {
        tiles[x][y] = tile;
    }

    public Sprite getPlayer() {
        return player;
    }

    public void setPlayer(Sprite player) {
        this.player = player;
    }

    public void addSprite(Sprite sprite) {
        sprites.add(sprite);
    }

    public void removeSprite(Sprite sprite) {
        sprites.remove(sprite);
    }

    public List<Sprite> getSprites() {
        return sprites;
    }
}
