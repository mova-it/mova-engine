package mova.game.graphics.tile;

import mova.game.graphics.sprite.Sprite;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.sprite.Creature;

import java.awt.Color;
import java.awt.Image;

public class TileMapRenderer {

    private static final int TILE_SIZE = 64;
    // the size in bits of the tile
    // Math.pow(2, TILE_SIZE_BITS) == TILE_SIZE
    private static final int TILE_SIZE_BITS = 6;

    private Image background;

    public static int pixelsToTiles(float pixels) {
        return pixelsToTiles(Math.round(pixels));
    }

    public static int pixelsToTiles(int pixels) {
        // use shifting to get correct values for negative pixels
        return pixels >> TILE_SIZE_BITS;

        // or, for tile sizes that aren't a power of two,
        // use the floor function:
        //return (int)Math.floor((float)pixels / TILE_SIZE);
    }

    public static int tilesToPixels(int numTiles) {
        // no real reason to use shifting here.
        // it's slighty faster, but doesn't add up to much
        // on modern processors.
        return numTiles << TILE_SIZE_BITS;

        // use this if the tile size isn't a power of 2:
        //return numTiles * TILE_SIZE;
    }

    public void setBackground(Image background) {
        this.background = background;
    }

    public void draw(MovaGraphics g, TileMap map, int screenWidth, int screenHeight) {
        Sprite player = map.getPlayer();
        int mapWidth = tilesToPixels(map.getWidth());

        // get the scrolling position of the map based on player's position
        int offsetX = screenWidth / 2 - (int) Math.round(player.getX()) - TILE_SIZE;
        offsetX = Math.min(offsetX, 0);
        offsetX = Math.max(offsetX, screenWidth - mapWidth);

        // get the y offset to draw all sprites and tiles
        int offsetY = screenHeight - tilesToPixels(map.getHeight());

        // draw black background, if needed
        if (background == null || screenHeight > background.getHeight(null)) {
            g.fillRect(Color.BLACK, screenWidth, screenHeight);
        }

        // draw parallax background image
        if (background != null) {
            int x = offsetX*(screenWidth - background.getWidth(null))/(screenWidth - mapWidth);
            int y = screenHeight - background.getHeight(null);

            g.drawImage(background, x, y);
        }

        // draw the visible tiles
        int firstTileX = pixelsToTiles(-offsetX);
        int lastTileX = firstTileX + pixelsToTiles(screenWidth) + 1;
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = firstTileX; x <= lastTileX; x++) {
                Image image = map.getTile(x, y);
                if (image != null) {
                    g.drawImage(image, tilesToPixels(x) + offsetX, tilesToPixels(y) + offsetY);
                }
            }
        }

        // draw player
        g.drawImage(player.getImage(),
                (int) Math.round(player.getX()) + offsetX,
                (int) Math.round(player.getY()) + offsetY);

        // draw sprites
        for (Sprite sprite : map.getSprites()) {
            int x = (int) Math.round(sprite.getX()) + offsetX;
            int y = (int) Math.round(sprite.getX()) + offsetY;
            g.drawImage(sprite.getImage(), x, y);

            // wake up the creature when it's on screen
            if (sprite instanceof Creature && x >= 0 && x < screenWidth) {
                ((Creature)sprite).wakeUp();
            }
        }
    }

}
