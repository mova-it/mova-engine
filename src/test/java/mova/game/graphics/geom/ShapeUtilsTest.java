package mova.game.graphics.geom;

import mova.game.geom.Segment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.geom.GeneralPath;

@RunWith(JUnit4.class)
public class ShapeUtilsTest {

    @Test
    public void equals() {
        Segment line = new Segment(10, 10, 200, 200);
        GeneralPath generalPath = new GeneralPath();

        Assert.assertFalse(ShapeUtils.equals(line, generalPath));

        generalPath.reset();
        generalPath.append(line, false);

        Assert.assertTrue(ShapeUtils.equals(line, generalPath));

        generalPath.reset();
        generalPath.append(line, true);

        Assert.assertTrue(ShapeUtils.equals(line, generalPath));

        line.setLine(11, 11, 200, 200);

        Assert.assertFalse(ShapeUtils.equals(line, generalPath));

        generalPath.reset();
        generalPath.append(line, false);

        Assert.assertTrue(ShapeUtils.equals(line, generalPath));
    }
}
