package mova.game.graphics.geom;

import mova.game.geom.Locator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;

@RunWith(JUnit4.class)
public class GeometryUtilsTest {

    @Test
    public void closestPointTest() {
        Shape r = new Rectangle(400, 450, 200, 100);

        Locator closestPointFrom = GeometryUtils.getClosestPointFrom(r, new Locator(1, 1));
        Assert.assertEquals(new Locator(400, 450), closestPointFrom);
    }
}
