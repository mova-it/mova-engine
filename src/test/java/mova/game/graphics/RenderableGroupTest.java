package mova.game.graphics;

import mova.game.core.ecs.GameObject;
import mova.game.ecs.TestGameObject;
import mova.util.RandomUtils;
import mova.util.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;
import java.util.Set;

@RunWith(JUnit4.class)
public class RenderableGroupTest {

    private static class TestShapeRenderer extends Renderer {

        @Override
        public ShapeRenderer copy() {
            return null;
        }

        @Override
        protected void render(MovaGraphics movaGraphics) {

        }
    }

    @Test
    public void create_null() {
        RenderableGroup group = new RenderableGroup();
        Assert.assertThrows(IllegalArgumentException.class, () -> group.addShapeRenderer(null, null));
        Assert.assertThrows(IllegalArgumentException.class, () -> group.addShapeRenderer(StringUtils.EMPTY, null));
        String randomString = RandomUtils.randString(20);
        Assert.assertThrows(IllegalArgumentException.class, () -> group.addShapeRenderer(randomString, null));
        TestShapeRenderer renderer = new TestShapeRenderer();
        Assert.assertThrows(IllegalArgumentException.class, () -> group.addShapeRenderer(null, renderer));
        Assert.assertThrows(IllegalArgumentException.class, () -> group.addShapeRenderer(StringUtils.EMPTY, renderer));
    }

    @Test
    public void visibility_test() {
        Assert.assertTrue(new RenderableGroup().isVisible());
        Assert.assertTrue(new RenderableGroup(true).isVisible());
        Assert.assertFalse(new RenderableGroup(false).isVisible());
    }

    @Test
    public void owner_test() {
        RenderableGroup group = new RenderableGroup();
        Assert.assertNull(group.getGameObject());

        TestShapeRenderer child1 = new TestShapeRenderer();
        Assert.assertNull(child1.getGameObject());

        String child1Name = RandomUtils.randString(20);
        group.addShapeRenderer(child1Name, child1);

        TestShapeRenderer child2 = new TestShapeRenderer();
        Assert.assertNull(child2.getGameObject());

        GameObject go = new TestGameObject(RandomUtils.randString(20));
        go.addComponent(group);

        Assert.assertEquals(go, group.getGameObject());
        Assert.assertEquals(go, child1.getGameObject());

        group.addShapeRenderer(RandomUtils.randString(20, child1Name), child2);
        Assert.assertEquals(go, child2.getGameObject());

        group.removeShapeRenderer(child1Name);

        Assert.assertEquals(go, group.getGameObject());
        Assert.assertNull(child1.getGameObject());
        Assert.assertEquals(go, child2.getGameObject());

        go.removeComponent(RenderableGroup.class);

        Assert.assertNull(group.getGameObject());
        Assert.assertNull(child1.getGameObject());
        Assert.assertNull(child2.getGameObject());
    }

    @Test
    public void addGetAndRemoveShapeRenderer() {
        String randomName = RandomUtils.randString(20);

        RenderableGroup group = new RenderableGroup();
        TestShapeRenderer addedChildRenderer = group.addShapeRenderer(randomName, new TestShapeRenderer());
        Assert.assertNotNull(addedChildRenderer);

        Renderer childRenderer = group.getShapeRenderer(randomName);
        Assert.assertEquals(addedChildRenderer, childRenderer);

        TestShapeRenderer castedChildRenderer = group.getShapeRenderer(randomName, TestShapeRenderer.class);
        Assert.assertEquals(addedChildRenderer, castedChildRenderer);

        Set<String> names = group.getShapeRendererNames();
        Assert.assertEquals(Collections.singleton(randomName), names);

        Renderer notAChildRenderer = group.removeShapeRenderer(RandomUtils.randString(20, randomName));
        Assert.assertNull(notAChildRenderer);

        Renderer removedchildRenderer = group.removeShapeRenderer(randomName);
        Assert.assertEquals(addedChildRenderer, removedchildRenderer);

        Set<String> namesAfterRemoving = group.getShapeRendererNames();
        Assert.assertTrue(namesAfterRemoving.isEmpty());
    }

}
