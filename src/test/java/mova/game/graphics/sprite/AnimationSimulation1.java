package mova.game.graphics.sprite;

import mova.game.graphics.ImageUtils;
import mova.game.graphics.SimpleScreenManager;
import mova.lang.ThreadUtils;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class AnimationSimulation1 {

    private static final long DEMO_TIME = 5000;

    private final SimpleScreenManager screenManager = new SimpleScreenManager();
    private Image backgroundImage;
    private Animation animation;

    public void loadImages() {
        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger l'image de fond'", ioe);
        }

        animation = new Animation();
        try {
            Image player1 = ImageUtils.loadImage("/images/player1.png");
            Image player2 = ImageUtils.loadImage("/images/player2.png");
            Image player3 = ImageUtils.loadImage("/images/player3.png");
            animation.addFrame(player1, 250);
            animation.addFrame(player2, 150);
            animation.addFrame(player1, 150);
            animation.addFrame(player2, 150);
            animation.addFrame(player3, 200);
            animation.addFrame(player2, 150);
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images d'animation du joueur", ioe);
        }
    }

    public void run(DisplayMode displayMode) {
        try {
            screenManager.setFullScreen(displayMode, new JFrame());

            loadImages();

            animationLoop();
        } finally {
            screenManager.restoreScreen();
        }
    }

    public void animationLoop() {
        long startTime = System.currentTimeMillis();
        long currTime = startTime;

        while (currTime - startTime < DEMO_TIME) {
            long elapsedTime = System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            animation.update(elapsedTime);

            Graphics g = screenManager.getFullScreenWindow().getGraphics();
            draw(g);
            g.dispose();

            ThreadUtils.takeANap(20);
        }
    }

    public void draw(Graphics g) {
        g.drawImage(backgroundImage, 0, 0, screenManager.getWidth(), screenManager.getHeight(), null);

        g.drawImage(animation.getImage(), 0, 0, null);
    }

    public static void main(String[] args) {
        DisplayMode displayMode;

        if (args.length == 3) {
            displayMode = new DisplayMode(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), DisplayMode.REFRESH_RATE_UNKNOWN);
        } else {
            displayMode = new DisplayMode(1920, 1080, 32, 60);
        }

        AnimationSimulation1 test = new AnimationSimulation1();
        test.run(displayMode);
    }
}
