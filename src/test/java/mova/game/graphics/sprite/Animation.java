package mova.game.graphics.sprite;

import java.awt.*;
import java.util.ArrayList;

public class Animation implements Cloneable {

    private final ArrayList<Frame> frames = new ArrayList<>();
    private long totalDuration = 0;
    private int currentFrameIndex;
    private long animationTime;

    public Animation() {
        start();
    }

    public Animation(Animation animation) {
        frames.addAll(animation.frames);
        totalDuration = animation.totalDuration;
        currentFrameIndex = animation.currentFrameIndex;
        animationTime = animation.animationTime;
    }

    public synchronized void addFrame(Image image, long duration) {
        totalDuration += duration;
        frames.add(new Frame(image, totalDuration));
    }

    public synchronized void start() {
        animationTime = 0;
        currentFrameIndex = 0;
    }

    public synchronized void update(long elapsedTime) {
        if (frames.size() > 1) {
            animationTime += elapsedTime;

            if (animationTime >= totalDuration) {
                animationTime = animationTime %totalDuration;
                currentFrameIndex = 0;
            }

            while (animationTime > getFrame(currentFrameIndex).endTime) {
                currentFrameIndex++;
            }
        }
    }

    public synchronized Image getImage() {
        if (frames.isEmpty()) return null;

        return getFrame(currentFrameIndex).image;
    }

    private Frame getFrame(int i) {
        return frames.get(i);
    }

    private static class Frame {
        private final Image image;
        private final long endTime;

        public Frame(Image image, long endTime) {
            this.image = image;
            this.endTime = endTime;
        }
    }
}
