package mova.game.graphics.sprite;

public abstract class Creature extends Sprite {

    private static final int DIE_TIME = 1000;

    public enum State {
        NORMAL, DYING, DEAD
    }

    private final Animation left;
    private final Animation right;
    private final Animation deadLeft;
    private final Animation deadRight;
    private State state;
    private long stateTime;

    public Creature(Animation left, Animation right, Animation deadLeft, Animation deadRight) {
        super(right);

        this.left = left;
        this.right = right;
        this.deadLeft = deadLeft;
        this.deadRight = deadRight;
        state = State.NORMAL;
        stateTime = 0;
    }

    protected Creature(Creature creature) {
        this(new Animation(creature.left), new Animation(creature.right), new Animation(creature.deadLeft), new Animation(creature.deadRight));
    }

    public float getMaxSpeed() {
        return 0;
    }

    public void wakeUp() {
        if (getState() == State.NORMAL && getVelocityX() == 0) {
            setVelocityX(-getMaxSpeed());
        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        if (this.state != state) {
            this.state = state;
            stateTime = 0;
            if (state == State.DYING) setVelocity(0, 0);
        }
    }

    public boolean isAlive() {
        return state == State.NORMAL;
    }

    public boolean isFlying() {
        return false;
    }

    public void collideHorizontal() {
        setVelocityX((float) -getVelocityX());
    }

    public void collideVertical() {
        setVelocityY(0);
    }

    public void update(long elapsedTime) {
        Animation newAnimation = animation;
        if (getVelocityX() < 0) {
            newAnimation = left;
        } else if (getVelocityX() > 0) {
            newAnimation = right;
        }

        if (state == State.DYING && newAnimation == left) {
            newAnimation = deadLeft;
        } else if (state == State.DYING && newAnimation == right) {
            newAnimation = deadRight;
        }

        if (animation != newAnimation) {
            animation = newAnimation;
            animation.start();
        } else {
            animation.update(elapsedTime);
        }

        stateTime += elapsedTime;
        if (state == State.DYING && stateTime >= DIE_TIME) {
            setState(State.DEAD);
        }
    }
}
