package mova.game.graphics.sprite;

import mova.game.geom.Locator;
import mova.game.geom.Vector;
import mova.game.graphics.ImageUtils;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.ScreenManager;
import mova.lang.ThreadUtils;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;

public class SpriteSimulation2 {

    private static final DisplayMode[] POSSIBLE_MODES = {
            new DisplayMode(1920, 1080, 32, 75),
            new DisplayMode(1920, 1080, 32, 60),
            new DisplayMode(800, 600, 32, 0),
            new DisplayMode(800, 600, 24, 0),
            new DisplayMode(800, 600, 16, 0),
            new DisplayMode(640, 480, 32, 0),
            new DisplayMode(640, 480, 24, 0),
            new DisplayMode(640, 480, 16, 0)
    };

    private static final long DEMO_TIME = 10000;
    private static final long FADE_TIME = 1000;
    private static final int SPRITES_NUMBER = 5;

    private final ScreenManager screenManager = new ScreenManager();
    private final Sprite[] sprites = new Sprite[SPRITES_NUMBER];

    private Image backgroundImage;

    public void loadImages() {
        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger l'image de fond", ioe);
        }

        Animation animation = new Animation();
        try {
            Image player1 = ImageUtils.loadImage("/images/player1.png");
            Image player2 = ImageUtils.loadImage("/images/player2.png");
            Image player3 = ImageUtils.loadImage("/images/player3.png");

            animation.addFrame(player1, 250);
            animation.addFrame(player2, 150);
            animation.addFrame(player1, 150);
            animation.addFrame(player2, 150);
            animation.addFrame(player3, 200);
            animation.addFrame(player2, 150);
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images d'animation du joueur", ioe);
        }

        for (int i = 0; i < SPRITES_NUMBER; i++) {
            sprites[i] = new Sprite(new Animation(animation));
            sprites[i].setCoordinates((float) Math.random()*(screenManager.getWidth() - animation.getImage().getWidth(null)), (float) Math.random()*(screenManager.getHeight() - animation.getImage().getHeight(null)));
            sprites[i].setVelocity((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
        }
    }

    public void animationLoop() {
        long startTime = System.currentTimeMillis();
        long currTime = startTime;

        while (currTime - startTime < DEMO_TIME) {
            long elapsedTime = System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            update(elapsedTime);

            MovaGraphics g = screenManager.getGraphics();
            draw(g);
            drawFade(g, currTime - startTime);
            g.dispose();

            screenManager.update();

            ThreadUtils.takeANap(20);
        }
    }

    public void drawFade(MovaGraphics graphics, long currTime) {
        long time;
        if (currTime <= FADE_TIME) time = FADE_TIME - currTime;
        else if (currTime > DEMO_TIME - FADE_TIME) time = FADE_TIME - DEMO_TIME + currTime;
        else return;

        byte numBars = 8;
        int barHeight = screenManager.getHeight()/numBars;
        int blackHeight = (int) (time*barHeight/FADE_TIME);

        graphics.setColor(Color.BLACK);
        for (int i = 0; i < numBars; i++) {
            int y = i*barHeight + (barHeight - blackHeight)/2;
            graphics.fillRect(0, y, screenManager.getWidth(), blackHeight);
        }
    }

    public void update(long elapsedTime) {
        for (Sprite sprite : sprites) {
            Locator coordinates = sprite.getCoordinates();
            Vector velocity = sprite.getVelocity();

            if (coordinates.getX() < 0) {
                velocity.setX(Math.abs(velocity.getX()));
            } else if (coordinates.getX() + sprite.getWidth() >= screenManager.getWidth()) {
                velocity.setX(-Math.abs(velocity.getX()));
            }

            if (coordinates.getY() < 0) {
                velocity.setY(Math.abs(velocity.getY()));
            } else if (coordinates.getY() + sprite.getHeight() >= screenManager.getHeight()) {
                velocity.setY(-Math.abs(velocity.getY()));
            }

            sprite.update(elapsedTime);
        }
    }

    public void draw(MovaGraphics g) {
        g.drawImage(backgroundImage, 0, 0, screenManager.getWidth(), screenManager.getHeight(), null);

        AffineTransform affineTransform = new AffineTransform();
        for (Sprite sprite : sprites) {
            Locator coordinates = sprite.getCoordinates();
            affineTransform.setToTranslation(coordinates.getX(), coordinates.getY());
            if (sprite.getVelocityX() < 0) {
                affineTransform.scale(-1, 1);
                affineTransform.translate(-sprite.getWidth(), 0);
            }
            g.drawImage(sprite.getImage(), affineTransform, null);
        }
    }

    public void run() {
        try {
            DisplayMode displayMode = screenManager.findFirstCompatibleDisplayMode(POSSIBLE_MODES);
            screenManager.setFullScreen(displayMode);

            loadImages();

            animationLoop();
        } finally {
            screenManager.restoreScreen();
        }
    }

    public static void main(String[] args) {
        SpriteSimulation2 spriteSimulation2 = new SpriteSimulation2();
        spriteSimulation2.run();
    }

}
