package mova.game.graphics.sprite;

import mova.game.geom.Locator;
import mova.game.geom.Vector;

import java.awt.*;

public class Sprite {

    private final Locator coordinates;
    private final Vector velocity;

    protected Animation animation;

    public Sprite(Animation animation) {
        this(new Locator(), new Vector(), animation);
    }

    public Sprite(Locator coordinates, Vector velocity, Animation animation) {
        this.coordinates = coordinates;
        this.velocity = velocity;
        this.animation = animation;
    }

    public Sprite(Sprite sprite) {
        this(new Locator(sprite.getCoordinates()), new Vector(sprite.getVelocity()), new Animation(sprite.animation));
    }

    public void update(long elapsedTime) {
        Vector m = new Vector(velocity);
        m.mul(elapsedTime);
        coordinates.move(m);
        animation.update(elapsedTime);
    }

    public Locator getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double x, double y) {
        coordinates.setLocation(x, y);
    }

    public void setCoordinates(Locator coordinates) {
        this.coordinates.setLocation(coordinates);
    }

    public void setX(float x) {
        coordinates.setX(x);
    }

    public void setY(float y) {
        coordinates.setY(y);
    }

    public double getX() {
        return coordinates.getX();
    }

    public double getY() {
        return coordinates.getY();
    }

    public int getWidth() {
        return animation.getImage().getWidth(null);
    }

    public int getHeight() {
        return animation.getImage().getHeight(null);
    }

    public Vector getVelocity() {
        return velocity;
    }

    public double getVelocityX() {
        return velocity.getX();
    }

    public double getVelocityY() {
        return velocity.getY();
    }

    public void setVelocity(float dx, float dy) {
        velocity.setTo(dx, dy);
    }

    public void setVelocity(Vector velocity) {
        this.velocity.setTo(velocity);
    }

    public void setVelocityX(float dx) {
        velocity.setX(dx);
    }

    public void setVelocityY(float dy) {
        velocity.setY(dy);
    }

    public Image getImage() {
        return animation.getImage();
    }

}
