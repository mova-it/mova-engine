package mova.game.graphics.sprite;

public class Player extends Sprite {

    public enum State {
        NORMAL,
        JUMPING
    }

    public static final float SPEED = .3f;
    public static final float GRAVITY = .002f;

    private int floorY;
    private State state;

    public Player(Animation animation) {
        super(animation);

        setState(State.NORMAL);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setFloorY(int floorY) {
        this.floorY = floorY;
        setY(floorY);
    }

    public void jump() {
        setVelocityY(-1);
        setState(State.JUMPING);
    }

    @Override
    public void update(long elapsedTime) {
        if (getState() == State.JUMPING) setVelocityY((float) (getVelocityY() + GRAVITY*elapsedTime));

        super.update(elapsedTime);

        if (getState() == State.JUMPING && getY() >= floorY) {
            setVelocityY(0);
            setY(floorY);
            setState(State.NORMAL);
        }
    }
}
