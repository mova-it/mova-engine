package mova.game.graphics.sprite;

public abstract class PowerUp extends Sprite {

    public PowerUp(Animation anim) {
        super(anim);
    }

    protected PowerUp(PowerUp powerUp) {
        this(new Animation(powerUp.animation));
    }

    public static class Star extends PowerUp {
        public Star(Animation anim) {
            super(anim);
        }

        public Star(Star star) {
            super(star);
        }
    }

    public static class Music extends PowerUp {
        public Music(Animation anim) {
            super(anim);
        }

        public Music(Music music) {
            super(music);
        }
    }

    public static class Goal extends PowerUp {
        public Goal(Animation anim) {
            super(anim);
        }

        public Goal(Goal goal) {
            super(goal);
        }
    }

}
