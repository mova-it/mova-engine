package mova.game.graphics.sprite;

import mova.game.graphics.ImageUtils;
import mova.game.graphics.ScreenManager;
import mova.lang.ThreadUtils;

import java.awt.*;
import java.io.IOException;

public class AnimationSimulation2 {

    private static final DisplayMode[] POSSIBLE_MODES = {
            new DisplayMode(1920, 1080, 32, 75),
            new DisplayMode(1920, 1080, 32, 60),
            new DisplayMode(800, 600, 32, 0),
            new DisplayMode(800, 600, 24, 0),
            new DisplayMode(800, 600, 16, 0),
            new DisplayMode(640, 480, 32, 0),
            new DisplayMode(640, 480, 24, 0),
            new DisplayMode(640, 480, 16, 0)
    };

    private static final long DEMO_TIME = 10000;

    private final ScreenManager screenManager = new ScreenManager();
    private Image backgroundImage;
    private Animation animation;

    public void loadImages() {
        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger l'image de fond", ioe);
        }

        animation = new Animation();
        try {
            Image player1 = ImageUtils.loadImage("/images/player1.png");
            Image player2 = ImageUtils.loadImage("/images/player2.png");
            Image player3 = ImageUtils.loadImage("/images/player3.png");

            animation.addFrame(player1, 250);
            animation.addFrame(player2, 150);
            animation.addFrame(player1, 150);
            animation.addFrame(player2, 150);
            animation.addFrame(player3, 200);
            animation.addFrame(player2, 150);
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images d'animation du joueur'", ioe);
        }
    }

    public void run() {
        try {
            DisplayMode displayMode = screenManager.findFirstCompatibleDisplayMode(POSSIBLE_MODES);
            screenManager.setFullScreen(displayMode);

            loadImages();

            animationLoop();
        } finally {
            screenManager.restoreScreen();
        }
    }

    public void animationLoop() {
        long startTime = System.currentTimeMillis();
        long currTime = startTime;

        while (currTime - startTime < DEMO_TIME) {
            long elapsedTime = System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            animation.update(elapsedTime);

            Graphics g = screenManager.getGraphics();
            draw(g);
            g.dispose();
            screenManager.update();

            ThreadUtils.takeANap(20);
        }
    }

    public void draw(Graphics g) {
        g.drawImage(backgroundImage, 0, 0, screenManager.getWidth(), screenManager.getHeight(), null);

        g.drawImage(animation.getImage(), 0, 0, null);
    }

    public static void main(String[] args) {
        AnimationSimulation2 test = new AnimationSimulation2();
        test.run();
    }
}
