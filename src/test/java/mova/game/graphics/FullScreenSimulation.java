package mova.game.graphics;

import mova.Settings;
import mova.lang.ThreadUtils;

import javax.swing.*;
import java.awt.*;

public class FullScreenSimulation extends JFrame {

    private static final long DEMO_TIME = 5000;

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.drawString("Hello World", 20, 50);

        if (g instanceof MovaGraphics) {
            MovaGraphics movaGraphics = (MovaGraphics) g;
            movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }

        g.drawString("Hello World", 20, 100);
    }

    public void run(DisplayMode displayMode) {
        getContentPane().setBackground(Color.BLUE);
        getContentPane().setForeground(Color.WHITE);
        setFont(Settings.DIALOG_FONT);

        SimpleScreenManager screenManager = new SimpleScreenManager();
        try {
            screenManager.setFullScreen(displayMode, this);
            ThreadUtils.takeANap(DEMO_TIME);
        } finally {
            screenManager.restoreScreen();
        }
    }

    public static void main(String[] args) {
        DisplayMode displayMode;

        if (args.length == 3) {
            displayMode = new DisplayMode(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), DisplayMode.REFRESH_RATE_UNKNOWN);
        } else {
            displayMode = new DisplayMode(1920, 1080, 32, 60);
        }

        FullScreenSimulation test = new FullScreenSimulation();
        test.run(displayMode);
    }
}
