package mova.game.graphics;

import mova.Settings;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class ImageSpeedSimulation extends JFrame {

    private static final long TIME_PER_IMAGE = 1500;
    private static final int FONT_SIZE = Settings.DIALOG_FONT.getSize();

    private SimpleScreenManager screenManager;

    private Image backgroundImage;
    private Image opaqueImage;
    private Image transparentImage;
    private Image translucentImage;
    private Image antiAliasedImage;
    private boolean imagesLoaded;

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        if (g instanceof MovaGraphics) {
            MovaGraphics movaGraphics = (MovaGraphics) g;
            movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }

        if (imagesLoaded) {
            drawImage(g, opaqueImage, "Opaque");
            drawImage(g, transparentImage, "Transparent");
            drawImage(g, translucentImage, "Translucent");
            drawImage(g, antiAliasedImage, "Translucent (Anti-Aliased)");

            synchronized (this) {
                notify();
            }
        } else {
            g.drawString("Loading images ...", 5, FONT_SIZE);
        }
    }

    private void drawImage(Graphics g, Image image, String caption) {
        g.drawImage(backgroundImage, 0, 0, screenManager.getWidth(), screenManager.getHeight(), null);

        int width = screenManager.getWidth() - image.getWidth(null);
        int height = screenManager.getHeight() - image.getHeight(null);
        int numImages = 0;

        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < TIME_PER_IMAGE) {
            int x = Math.round((float) Math.random() * width);
            int y = Math.round((float) Math.random() * height);
            g.drawImage(image, x, y, null);
            numImages++;
        }
        long time = System.currentTimeMillis() - startTime;
        float speed = numImages*1000f/time;
        System.out.println(caption + ": " + speed + " images/seconde");
    }

    private void loadImages() {
        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
            opaqueImage = ImageUtils.loadImage("/images/opaque.png");
            transparentImage = ImageUtils.loadImage("/images/transparent.png");
            translucentImage = ImageUtils.loadImage("/images/translucent.png");
            antiAliasedImage = ImageUtils.loadImage("/images/antialiased.png");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images", ioe);
        }
        imagesLoaded = true;

        repaint();
    }

    public void run(DisplayMode displayMode) {
        getContentPane().setBackground(Color.BLUE);
        getContentPane().setForeground(Color.WHITE);
        setFont(Settings.DIALOG_FONT);
        imagesLoaded = false;

        screenManager = new SimpleScreenManager();
        try {
            screenManager.setFullScreen(displayMode, this);

            synchronized (this) {
                loadImages();

                try {
                    wait();
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        } finally {
            screenManager.restoreScreen();
        }
    }

    public static void main(String[] args) {
        DisplayMode displayMode;

        if (args.length == 3) {
            displayMode = new DisplayMode(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), DisplayMode.REFRESH_RATE_UNKNOWN);
        } else {
            displayMode = new DisplayMode(1920, 1080, 32, 60);
        }

        ImageSpeedSimulation test = new ImageSpeedSimulation();
        test.run(displayMode);
    }
}
