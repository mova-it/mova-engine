package mova.game.graphics;

import mova.game.core.AbstractScene;
import mova.game.core.Scene;
import mova.game.core.ecs.GameObject;
import mova.game.ecs.TestGameObject;
import mova.game.geom.Locator;
import mova.game.view.Camera;
import mova.game.view.Projection;
import mova.util.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;
import java.awt.geom.AffineTransform;

@RunWith(JUnit4.class)
public class ShapeRendererTest {

    private static class TestShapeRenderer extends ShapeRenderer {

        public TestShapeRenderer(Shape shape) {
            super(shape);
        }

        @Override
        public Renderer copy() {
            return null;
        }

        @Override
        protected void renderShape(MovaGraphics movaGraphics, Shape shape) {

        }
    }

    private static class TestScene extends AbstractScene {

        private final Camera camera;

        private TestScene(Camera camera) {
            this.camera = camera;
        }

        @Override
        public Camera getCamera() {
            return camera;
        }
    }

    @Test
    public void transformAndProject_alone() {
        int x = RandomUtils.randInt(200);
        int y = RandomUtils.randInt(200);
        int w = RandomUtils.randInt(200);
        int h = RandomUtils.randInt(200);

        Shape shape = new Rectangle(x, y, w, h);
        ShapeRenderer shapeRenderer = new TestShapeRenderer(shape);

        Assert.assertEquals(shape, shapeRenderer.transformAndProject());
    }

    @Test
    public void transformAndProject_transform() {
        int x = RandomUtils.randInt(200);
        int y = RandomUtils.randInt(200);
        int w = RandomUtils.randInt(200);
        int h = RandomUtils.randInt(200);

        Rectangle r = new Rectangle(x, y, w, h);
        ShapeRenderer shapeRenderer = new TestShapeRenderer(r);

        GameObject go = new TestGameObject(RandomUtils.randString(20));
        go.addComponent(shapeRenderer);

        Transform transform = new Transform();
        int dx = RandomUtils.randInt(200);
        int dy = RandomUtils.randInt(200);
        transform.setToTranslation(dx, dy);
        go.addComponent(transform);

        Shape s = shapeRenderer.transformAndProject();
        Assert.assertNotEquals(r, s);

        Rectangle rBounds = r.getBounds();
        Rectangle sBounds = s.getBounds();
        Assert.assertEquals(rBounds.x + dx, sBounds.x);
        Assert.assertEquals(rBounds.y + dy, sBounds.y);
        Assert.assertEquals(rBounds.width, sBounds.width);
        Assert.assertEquals(rBounds.height, sBounds.height);
    }

    @Test
    public void transformAndProject_projection() {
        int x = RandomUtils.randInt(200);
        int y = RandomUtils.randInt(200);
        int w = RandomUtils.randInt(200);
        int h = RandomUtils.randInt(200);

        Rectangle r = new Rectangle(x, y, w, h);
        ShapeRenderer shapeRenderer = new TestShapeRenderer(r);

        GameObject go = new TestGameObject(RandomUtils.randString(20));
        go.addComponent(shapeRenderer);

        int dx = RandomUtils.randInt(200);
        int dy = RandomUtils.randInt(200);
        Scene scene = new TestScene(new TestCamera(AffineTransform.getTranslateInstance(dx, dy)));
        go.setScene(scene);

        Shape s = shapeRenderer.transformAndProject();
        Assert.assertNotEquals(r, s);

        Rectangle rBounds = r.getBounds();
        Rectangle sBounds = s.getBounds();
        Assert.assertEquals(rBounds.x + dx, sBounds.x);
        Assert.assertEquals(rBounds.y + dy, sBounds.y);
        Assert.assertEquals(rBounds.width, sBounds.width);
        Assert.assertEquals(rBounds.height, sBounds.height);
    }

    @Test
    public void transformAndProject() {
        int x = RandomUtils.randInt(200);
        int y = RandomUtils.randInt(200);
        int w = RandomUtils.randInt(200);
        int h = RandomUtils.randInt(200);

        Rectangle r = new Rectangle(x, y, w, h);
        ShapeRenderer shapeRenderer = new TestShapeRenderer(r);

        GameObject go = new TestGameObject(RandomUtils.randString(20));
        go.addComponent(shapeRenderer);

        Transform transform = new Transform();
        int transformDx = RandomUtils.randInt(200);
        int transformDy = RandomUtils.randInt(200);
        transform.setToTranslation(transformDx, transformDy);
        go.addComponent(transform);

        int projectionDx = RandomUtils.randInt(200);
        int projectionDy = RandomUtils.randInt(200);
        Scene scene = new TestScene(new TestCamera(AffineTransform.getTranslateInstance(projectionDx, projectionDy)));
        go.setScene(scene);

        Shape s = shapeRenderer.transformAndProject();
        Assert.assertNotEquals(r, s);

        Rectangle rBounds = r.getBounds();
        Rectangle sBounds = s.getBounds();
        Assert.assertEquals(rBounds.x + transformDx + projectionDx, sBounds.x);
        Assert.assertEquals(rBounds.y + transformDy + projectionDy, sBounds.y);
        Assert.assertEquals(rBounds.width, sBounds.width);
        Assert.assertEquals(rBounds.height, sBounds.height);
    }

    private static class TestCamera implements Camera {

        private final Projection projection;

        private TestCamera(AffineTransform transform) {
            this.projection = new Projection(transform);
        }

        @Override
        public Projection getScreenProjection() {
            return projection;
        }

        @Override
        public Locator getLocation() {
            return null;
        }
    }
}
