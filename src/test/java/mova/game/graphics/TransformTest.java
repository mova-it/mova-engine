package mova.game.graphics;

import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.graphics.geom.ShapeUtils;
import mova.util.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;

@RunWith(JUnit4.class)
public class TransformTest {

    @Test
    public void test_copy() {
        Transform t = new Transform();
        Transform copy = t.copy();

        t.addTranslation(RandomUtils.randInt(200), RandomUtils.randInt(200));
        Assert.assertNotEquals(t, copy);
    }

    @Test
    public void test_identity() {
        test_transformation(0, 0);
    }

    @Test
    public void test_translation_simple_shape() {
        test_transformation(RandomUtils.randInt(200), RandomUtils.randInt(200));
    }

    @Test
    public void test_transformation_shape_preservation() {
        Transform t = new Transform();
        t.setToTranslation(RandomUtils.randInt(200), RandomUtils.randInt(200));

        Shape s = new Ellipse2D.Float(RandomUtils.randInt(200), RandomUtils.randInt(200), RandomUtils.randInt(200), RandomUtils.randInt(200));
        Shape sCopy = new GeneralPath(s);
        Assert.assertTrue(ShapeUtils.equals(s, sCopy));
        t.apply(s);
        Assert.assertTrue(ShapeUtils.equals(s, sCopy));
    }

    private void test_transformation(int tx, int ty) {
        Transform t = new Transform();
        if (tx != 0 || ty != 0) t.setToTranslation(tx, ty);

        String message = "Assertion error while testint transformation with tx = " + tx + " and ty = " + ty;

        Locator pf = new Locator(RandomUtils.randInt(200), RandomUtils.randInt(200));
        Locator pfCopy = new Locator((float) pf.getX(), (float) pf.getY());
        Assert.assertEquals(message, new Locator((float) (pf.getX() + tx), (float) (pf.getY() + ty)), t.apply(pf));
        Assert.assertEquals(message, new Locator((float) (pf.getX() - tx), (float) (pf.getY() - ty)), t.inverseApply(pf));
        Assert.assertEquals(message, pf, pfCopy);

        Locator pfDst = new Locator();
        Assert.assertNotEquals(message, pf, pfDst);
        t.apply(pf, pfDst);
        Assert.assertEquals(message, new Locator((float) (pf.getX() + tx), (float) (pf.getY() + ty)), pfDst);
        t.inverseApply(pf, pfDst);
        Assert.assertEquals(message, new Locator((float) (pf.getX() - tx), (float) (pf.getY() - ty)), pfDst);



        Segment lf = new Segment(RandomUtils.randInt(200), RandomUtils.randInt(200), RandomUtils.randInt(200), RandomUtils.randInt(200));
        Assert.assertTrue(message, ShapeUtils.equals(new Segment((float) (lf.getX1() + tx), (float) (lf.getY1() + ty), (float) (lf.getX2() + tx), (float) (lf.getY2() + ty)), t.apply(lf)));
    }
}
