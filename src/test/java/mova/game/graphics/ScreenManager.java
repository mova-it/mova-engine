package mova.game.graphics;

import mova.game.graphics.sprite.Sprite;
import mova.game.graphics.component.IconSet;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class ScreenManager extends SimpleScreenManager {

    public DisplayMode[] getCompatibleDisplayModes() {
        return graphicsDevice.getDisplayModes();
    }

    public DisplayMode findFirstCompatibleDisplayMode(DisplayMode[] displayModes) {
        DisplayMode[] goodModes = getCompatibleDisplayModes();
        for (DisplayMode displayMode : displayModes) {
            for (DisplayMode goodMode : goodModes) {
                if (DisplayModeUtils.match(displayMode, goodMode)) {
                    return displayMode;
                }
            }
        }

        return null;
    }

    public DisplayMode getCurrentDisplayMode() {
        return graphicsDevice.getDisplayMode();
    }

    public void setFullScreen(DisplayMode displayMode) {
        setFullScreen(displayMode, new JFrame());
    }

    public void setFullScreen(DisplayMode displayMode, JFrame window) {
        super.setFullScreen(displayMode, window);

        window.setIgnoreRepaint(true);
        window.createBufferStrategy(2);
    }

    public MovaGraphics getGraphics() {
        Window window = getFullScreenWindow();
        if (window != null) {
            BufferStrategy bufferStrategy = window.getBufferStrategy();
            return new MovaGraphics((java.awt.Graphics2D) bufferStrategy.getDrawGraphics());
        }

        return null;
    }

    public void update() {
        Window window = getFullScreenWindow();
        if (window != null) {
            BufferStrategy bufferStrategy = window.getBufferStrategy();
            if (!bufferStrategy.contentsLost()) bufferStrategy.show();

            Toolkit.getDefaultToolkit().sync();
        }
    }

    public BufferedImage createCompatibleImage(int w, int h, int transparency) {
        Window window = getFullScreenWindow();
        if (window != null) {
            GraphicsConfiguration graphicsConfiguration = window.getGraphicsConfiguration();
            return graphicsConfiguration.createCompatibleImage(w, h, transparency);
        }

        return null;
    }

    public IconSet createIconSet(String path) {
        // TODO le problème de cette méthode c'est que ca fait un truc trop générique pour quelque chose qui peut être complètement variable
        ImageIcon iconRollover = ImageUtils.loadImageIcon(path);
        int width = iconRollover.getIconWidth();
        int height = iconRollover.getIconHeight();

        Image image = createCompatibleImage(width, height, Transparency.TRANSLUCENT);
        MovaGraphics g = new MovaGraphics((java.awt.Graphics2D) image.getGraphics());
        Composite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f);
        g.setComposite(alpha);
        g.drawImage(iconRollover.getImage());
        g.dispose();
        ImageIcon iconDefault = new ImageIcon(image);

        image = createCompatibleImage(width, height, Transparency.TRANSLUCENT);
        g = new MovaGraphics((java.awt.Graphics2D) image.getGraphics());
        g.drawImage(iconRollover.getImage(), 2, 2);
        g.dispose();
        ImageIcon iconPressed = new ImageIcon(image);

        return new IconSet(iconDefault, iconRollover, iconPressed);
    }

    public void centerLocation(Sprite sprite) {
        sprite.setCoordinates((getWidth() - sprite.getWidth()) >> 1, (getHeight() - sprite.getHeight()) >> 1);
    }

    public void centerLocation(Component component) {
        component.setLocation((getWidth() - component.getWidth())/2, (getHeight() - component.getHeight())/2);
    }
}
