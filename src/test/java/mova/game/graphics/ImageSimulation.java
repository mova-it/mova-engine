package mova.game.graphics;

import mova.Settings;
import mova.lang.ThreadUtils;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class ImageSimulation extends JFrame {

    private static final long DEMOT_TIME = 10000;
    private static final int FONT_SIZE = Settings.DIALOG_FONT.getSize();

    private SimpleScreenManager screenManager;

    private Image backgroundImage;
    private Image opaqueImage;
    private Image transparentImage;
    private Image translucentImage;
    private Image antiAliasedImage;
    private boolean imagesLoaded;

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        if (g instanceof MovaGraphics) {
            MovaGraphics movaGraphics = (MovaGraphics) g;
            movaGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }

        if (imagesLoaded) {
            g.drawImage(backgroundImage, 0, 0, screenManager.getWidth(), screenManager.getHeight(), null);

            drawImage(g, opaqueImage, 0, 0, "Opaque");
            drawImage(g, transparentImage, 320, 0, "Transparent");
            drawImage(g, translucentImage, 0, 300, "Translucent");
            drawImage(g, antiAliasedImage, 320, 300, "Translucent (Anti-Aliased)");
        } else {
            g.drawString("Loading images ...", 5, FONT_SIZE);
        }
    }

    private void drawImage(Graphics g, Image image, int x, int y, String caption) {
        g.drawImage(image, x, y, null);
        g.drawString(caption, x + 5, y + FONT_SIZE + image.getHeight(null));
    }

    private void loadImages() {
        try {
            backgroundImage = ImageUtils.loadImage("/images/background.jpg");
            opaqueImage = ImageUtils.loadImage("/images/opaque.png");
            transparentImage = ImageUtils.loadImage("/images/transparent.png");
            translucentImage = ImageUtils.loadImage("/images/translucent.png");
            antiAliasedImage = ImageUtils.loadImage("/images/antialiased.png");
        } catch (IOException ioe) {
            throw new IllegalStateException("Impossible de charger les images", ioe);
        }
        imagesLoaded = true;

        repaint();
    }

    public void run(DisplayMode displayMode) {
        getContentPane().setBackground(Color.BLUE);
        getContentPane().setForeground(Color.WHITE);
        setFont(Settings.DIALOG_FONT);
        imagesLoaded = false;

        screenManager = new SimpleScreenManager();
        try {
            screenManager.setFullScreen(displayMode, this);

            loadImages();

            ThreadUtils.takeANap(DEMOT_TIME);
        } finally {
            screenManager.restoreScreen();
        }
    }

    public static void main(String[] args) {
        DisplayMode displayMode;

        if (args.length == 3) {
            displayMode = new DisplayMode(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), DisplayMode.REFRESH_RATE_UNKNOWN);
        } else {
            displayMode = new DisplayMode(1920, 1080, 32, 60);
        }

        ImageSimulation test = new ImageSimulation();
        test.run(displayMode);
    }
}
