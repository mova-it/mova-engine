package mova.game.graphics;

import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class SimpleScreenManager {

    private static final String DISPLAY_MODE_WARNING = "Impossible de configurer le plein écran pour cette resolution: %s\n%s";

    protected final Logger logger = Logger.getLogger(getClass().getName());

    protected final GraphicsDevice graphicsDevice;

    public SimpleScreenManager() {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
    }

    public void setFullScreen(DisplayMode displayMode, JFrame window) {
        window.setUndecorated(true);
        window.setResizable(false);

        graphicsDevice.setFullScreenWindow(window);
        if (displayMode != null && graphicsDevice.isDisplayChangeSupported()) {
            try {
                graphicsDevice.setDisplayMode(displayMode);
            } catch (IllegalArgumentException iae) {
                logger.warning(String.format(DISPLAY_MODE_WARNING, DisplayModeUtils.toString(displayMode), ExceptionUtils.getStackTrace(iae)));
            }
        }
    }

    public JFrame getFullScreenWindow() {
        return (JFrame) graphicsDevice.getFullScreenWindow();
    }

    public int getWidth() {
        Window window = getFullScreenWindow();
        return window != null ? window.getWidth() : 0;
    }

    public int getHeight() {
        Window window = getFullScreenWindow();
        return window != null ? window.getHeight() : 0;
    }

    public Dimension getDimension() {
        Window window = getFullScreenWindow();
        return window != null ? window.getSize() : null;
    }

    public void restoreScreen() {
        Window window = getFullScreenWindow();
        if (window != null) window.dispose();
        graphicsDevice.setFullScreenWindow(null);
    }
}
