package mova;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Collections;

public class Settings {

    private Settings() {}

    // TODO s'interroger sur la place de ces constantes
    public static final Font DIALOG_FONT = new Font("Dialog", Font.PLAIN, 24);
    public static final Font DIALOG_UNDERLINE_FONT = DIALOG_FONT.deriveFont(Collections.singletonMap(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON));

    public static final Color DEFAULT_TRANSPARENT_COLOR = new Color(0, 0, 0, .4f);
    public static final BasicStroke DEFAULT_STROKE = new BasicStroke(4);
}
