package mova.game.asset;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.stream.LongStream;

public abstract class ResourceCache<T> {

    protected final Map<String, T> resources = new HashMap<>();

    protected ResourceCache() {
        caches.put(this, true);
    }

    protected abstract String getResourceRoot();

    protected abstract T extract(URL url) throws IOException;

    /**
     * Méthode exéuté après avoir extrait la resource
     *
     * @param resourceName le nom de la resource
     * @param t la resource
     * @return la resource après traitement de la méthode
     */
    protected T postExtract(String resourceName, T t) {
        return t;
    }

    public T getResource(String resourceName) {
        return resources.computeIfAbsent(resourceName, name -> {
            URL resourceUrl = getClass().getResource(getResourceRoot() + name);

            try {
                T t = extract(resourceUrl);
                return postExtract(resourceName, t);
            } catch (Exception t) {
                throw new IllegalStateException("Impossible to load resource " + getResourceRoot() + resourceName, t);
            }
        });
    }

    private static final WeakHashMap<ResourceCache<?>, Boolean> caches = new WeakHashMap<>();

    long getUsedMemory() {
        return -1;
    }

    static void printUsedMemory() {
        LongStream.Builder builder = LongStream.builder();
        caches.keySet().forEach(resourceCache -> {
            long usedMemory = resourceCache.getUsedMemory();
            if (usedMemory > 0) {
                builder.accept(usedMemory);
                System.out.println(resourceCache + " used " + usedMemory + " bytes");
            }
        });
        long sum = builder.build().sum();
        if (sum > 0) System.out.println("Total memory used " + sum + " bytes");
    }

}
