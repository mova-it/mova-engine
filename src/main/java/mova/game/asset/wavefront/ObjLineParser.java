package mova.game.asset.wavefront;

import mova.game.graphics3D.math3D.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


class ObjLineParser implements LineParser {

    private final Logger logger = LoggerFactory.getLogger(ObjLineParser.class);

    public void parseLine(String line, ObjectLoaderContext context) {
        StringTokenizer tokenizer = new StringTokenizer(line);
        String command = tokenizer.nextToken();
        switch (command) {

            // create a new vertex
            case "v":
                context.addVector(tokenizer.nextToken(), tokenizer.nextToken(), tokenizer.nextToken());
                break;

            case "f":
                // create a new face (flat, convex polygon)
                List<Vector3D> currVertices = new ArrayList<>();
                while (tokenizer.hasMoreTokens()) {
                    String indexStr = tokenizer.nextToken();

                    currVertices.add(context.getVector(indexStr));
                }

                context.createPolygon(currVertices);
                break;

            // add new group
            case "g":
                context.createGroup();
                break;

            // load materials from file
            case "mtllib": {
                String name = tokenizer.nextToken();

                ObjectLoader.parseFile(name, context);
                break;
            }

            // define the current material
            case "usemtl": {
                String name = tokenizer.nextToken();

                context.setCurrentMaterial(name);
                if (context.getCurrentMaterial().name == null) logger.info("Empty material: {}", name);
                break;
            }

            // unknown command - ignore it
            default:
        }

    }
}
