package mova.game.asset.wavefront;

import mova.game.graphics3D.texture.PlainColorTexture;

import java.awt.*;
import java.util.StringTokenizer;

class MtlLineParser implements LineParser {

    public void parseLine(String line, ObjectLoaderContext context) {
        StringTokenizer tokenizer = new StringTokenizer(line);
        String command = tokenizer.nextToken();

        switch (command) {

            // create a new material if needed
            case "newmtl": {
                String name = tokenizer.nextToken();

                context.setCurrentMaterial(name, true);
                break;
            }

            // give the current material a texture
            case "map_Kd": {
                String name = tokenizer.nextToken();

                if (!context.matchCurrentMaterialTexturePath(name)) {
                    boolean shaded = tokenizer.hasMoreTokens() && Boolean.parseBoolean(tokenizer.nextToken());
                    context.setCurrentMaterialTexture(name, shaded);
                }
                break;
            }

            // give the current material a texture
            case "map_Sd": {
                String name = tokenizer.nextToken();

                int r = Integer.parseInt(tokenizer.nextToken());
                int g = Integer.parseInt(tokenizer.nextToken());
                int b = Integer.parseInt(tokenizer.nextToken());

                context.setCurrentMaterialTexture(name, new PlainColorTexture(new Color(r, g, b)));
                break;
            }

            // unknown command - ignore it
            default:

        }
    }
}
