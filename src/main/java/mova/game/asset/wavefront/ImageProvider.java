package mova.game.asset.wavefront;

import java.awt.image.BufferedImage;

public interface ImageProvider {

    BufferedImage getImage(String name);
}
