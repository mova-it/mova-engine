package mova.game.asset.wavefront;

import mova.game.graphics3D.PolygonGroup;
import mova.game.graphics3D.math3D.PointLight3D;
import mova.game.graphics3D.math3D.TexturedPolygon3D;
import mova.game.graphics3D.math3D.Vector3D;
import mova.game.graphics3D.texture.Texture;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class ObjectLoaderContext {

    private final Path parentPath;
    private final PolygonGroup object = new PolygonGroup();
    private final List<Vector3D> vertices = new ArrayList<>();
    private final HashMap<String, Material> materials = new HashMap<>();
    private final List<PointLight3D> lights;
    private final float ambientLightIntensity;
    private final ImageProvider imageProvider;

    private PolygonGroup currentGroup = object;
    private Material currentMaterial = null;

    ObjectLoaderContext(Path parentPath, ImageProvider imageProvider, List<PointLight3D> lights, float ambientLightIntensity) {
        this.parentPath = parentPath;
        this.imageProvider = imageProvider;
        this.lights = lights;
        this.ambientLightIntensity = ambientLightIntensity;
    }

    Path findFile(String filename) {
        return parentPath.resolve(filename);
    }

    Material getCurrentMaterial() {
        return currentMaterial;
    }

    void setCurrentMaterial(String name) {
        setCurrentMaterial(name, false);
    }

    void setCurrentMaterial(String name, boolean createIfAbsent) {
        currentMaterial = materials.compute(name, (key, material) -> createIfAbsent && material == null ? new Material() : material);
    }

    boolean matchCurrentMaterialTexturePath(String name) {
        return name.equals(currentMaterial.name);
    }

    void setCurrentMaterialTexture(String name, boolean shaded) {
        BufferedImage image = imageProvider.getImage(name);
        setCurrentMaterialTexture(name, Texture.createTexture(image, shaded));
    }

    void setCurrentMaterialTexture(String name, Texture texture) {
        currentMaterial.name = name;
        currentMaterial.texture = texture;
    }

    void addVector(String x, String y, String z) {
        addVector(Float.parseFloat(x), Float.parseFloat(y), Float.parseFloat(z));
    }

    void addVector(float x, float y, float z) {
        vertices.add(new Vector3D(x, y, z));
    }

    Vector3D getVector(String indexStr) {
        // ignore texture and normal coords
        int endIndex = indexStr.indexOf('/');
        if (endIndex != -1) indexStr = indexStr.substring(0, endIndex);

        int index = Integer.parseInt(indexStr);
        if (index < 0) index = vertices.size() + index + 1;

        return vertices.get(index - 1);
    }

    void createPolygon(List<Vector3D> verticesList) {
        // create textured polygon
        Vector3D[] verticesArray = verticesList.toArray(new Vector3D[0]);

//        TexturedPolygon3D poly = new TexturedPolygon3D();
//        poly.setTo(new Polygon3D(verticesArray));
//
        // set the texture
//        ShadedSurface.createShadedSurface(poly, currentMaterial.texture, lights, ambientLightIntensity);
        TexturedPolygon3D poly = new TexturedPolygon3D(currentMaterial.texture, verticesArray);

        currentGroup.addPolygon(poly);
    }

    void createGroup() {
        currentGroup = new PolygonGroup();
        object.addPolygonGroup(currentGroup);
    }

    PolygonGroup getObject() {
        return object;
    }

}
