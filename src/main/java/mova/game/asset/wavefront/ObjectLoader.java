package mova.game.asset.wavefront;

import mova.game.core.FatalError;
import mova.game.graphics3D.PolygonGroup;
import mova.game.graphics3D.math3D.PointLight3D;
import mova.util.StringUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class ObjectLoader {

    // TODO dans l'idéal le ImageProvider ne devrait pas être instantiable d'ailleurs que dans ce package
    // TODO pouvoir gérer plusieurs types de texture? Ce serait utile?
    // TODO comment ca se passe s'il y a plusieurs materials plusieurs chargement avec un object loader? Est-ce que les matériaux sont rechargés?

    private static final Map<String, LineParser> parsers;

    static {
        parsers = new HashMap<>();
        parsers.put("obj", new ObjLineParser());
        parsers.put("mtl", new MtlLineParser());
    }

    private final ImageProvider imageProvider;

    public ObjectLoader(ImageProvider imageProvider) {
        this.imageProvider = imageProvider;
    }


    public PolygonGroup loadObject(String filename) throws IOException {
        return loadObject(filename, new ArrayList<>(), 1);
    }


    public PolygonGroup loadObject(URL url) {
        return loadObject(url, new ArrayList<>(), 1);
    }

    public PolygonGroup loadObject(String filename, List<PointLight3D> lights, float ambientLightIntensity) throws IOException {
        URL url = ObjectLoader.class.getResource(filename);
        if (url == null) throw new FileNotFoundException(filename);

        return loadObject(url, lights, ambientLightIntensity);
    }

    public PolygonGroup loadObject(URL url, List<PointLight3D> lights, float ambientLightIntensity)  {
        URI uri;
        try {
            uri = url.toURI();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }

        Path path = Paths.get(uri);
        String filename = path.getFileName().toString();
        Path parentPath = path.getParent();

        return parseFile(filename, new ObjectLoaderContext(parentPath, imageProvider, lights, ambientLightIntensity));
    }


    static PolygonGroup parseFile(String filename, ObjectLoaderContext context) {
        // get the file relative to the source path
        Path file = context.findFile(filename);

        LineParser parser = getParser(filename);

        try (Stream<String> lines = Files.lines(file)) {
            lines.forEachOrdered(line -> {
                line = line.trim();

                // ignore blank lines and comments
                if (line.length() > 0 && !line.startsWith("#")) {
                    // interpret the line
                    parser.parseLine(line, context);
                }
            });
        } catch (IOException e) {
            throw new FatalError("Impossible de parser le fichier: " + filename, e);
        }

        return context.getObject();
    }

    private static LineParser getParser(String filename) {
        int extIndex = filename.lastIndexOf('.');

        String ext = StringUtils.EMPTY;
        if (extIndex != -1) {
            ext = filename.substring(extIndex + 1).toLowerCase();
        }

        return parsers.getOrDefault(ext, parsers.get("obj"));
    }
}
