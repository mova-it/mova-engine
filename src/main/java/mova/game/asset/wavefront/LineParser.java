package mova.game.asset.wavefront;

interface LineParser {
    void parseLine(String line, ObjectLoaderContext context);
}
