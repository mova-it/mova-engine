package mova.game.graphics3D;

import mova.debug.BenchDataObserver;
import mova.game.core.ecs.GameObject;
import mova.game.graphics3D.math3D.*;
import mova.game.graphics3D.texture.*;

import java.awt.*;
import java.util.HashMap;

public class ZBufferedRenderer extends ShadedSurfacePolygonRenderer {

    protected static final int MIN_DISTANCE = 12;

    protected TexturedPolygon3D temp;
    protected ZBuffer zBuffer;
    // used for calculating depth
    protected float w;

    public ZBufferedRenderer(Camera camera) {
        this(camera, true);
    }

    public ZBufferedRenderer(Camera camera, boolean eraseView) {
        super(camera, eraseView);
        temp = new TexturedPolygon3D();
    }

    @Override
    protected void init() {
        scratchPolygon = new TexturedPolygon3D();

        scanConverter = new ScanConverter(viewProjection);

        // create renders for each texture (HotSpot optimization)
        scanRenderers = new HashMap<>();
        scanRenderers.put(PlainColorTexture.class, new PlainColorTextureZRenderer());
        scanRenderers.put(PowerOf2Texture.class, new PowerOf2TextureZRenderer());
        scanRenderers.put(ShadedTexture.class, new ShadedTextureZRenderer());
        scanRenderers.put(ShadedSurface.class, new ShadedSurfaceZRenderer());
    }


    @Override
    public void startFrame(Graphics2D g) {
        super.startFrame(g);

        // initialize depth buffer
        if (zBuffer == null
                || zBuffer.getWidth() != viewProjection.getWidth()
                || zBuffer.getHeight() != viewProjection.getHeight()) {
            zBuffer = new ZBuffer(viewProjection.getWidth(), viewProjection.getHeight());
        } else if (clearViewEveryFrame) {
            zBuffer.clear();
        }
    }

    public boolean draw(Graphics2D g, GameObject object) {
        PolygonGroup polygonGroup = object.getComponent(PolygonGroup.class);
        return polygonGroup != null && draw(g, polygonGroup);
    }

    public boolean draw(Graphics2D g, PolygonGroup group) {
        boolean visible = false;
        group.resetIterator();
        while (group.hasNext()) {
            group.nextPolygonTransformed(temp);
            visible |= draw(g, temp);
        }
        return visible;
    }


    @Override
    protected void drawCurrentPolygon(Graphics2D g, Polygon3D currentPolygon) {
        if (!(currentPolygon instanceof TexturedPolygon3D)) {
            // not a textured polygon - return
            return;
        }

        buildSurface(currentPolygon);

        TexturedPolygon3D poly = (TexturedPolygon3D) scratchPolygon;
        Texture texture = poly.getTexture();
        ScanRenderer scanRenderer = scanRenderers.get(texture.getClass());
        scanRenderer.setTexture(texture);
        Rectangle3D textureBounds = poly.getTextureBounds();

        a.setToCrossProduct(textureBounds.getDirectionV(), textureBounds.getOrigin());
        b.setToCrossProduct(textureBounds.getOrigin(), textureBounds.getDirectionU());
        c.setToCrossProduct(textureBounds.getDirectionU(), textureBounds.getDirectionV());

        // w is used to compute depth at each pixel
        w = SCALE * MIN_DISTANCE * Short.MAX_VALUE / (viewProjection.getDistance() * c.dotProduct(textureBounds.getOrigin()));

        int y = scanConverter.getTopBoundary();
        viewPos.setY(viewProjection.convertFromScreenYToViewY(y));
        viewPos.setZ(-viewProjection.getDistance());

        while (y <= scanConverter.getBottomBoundary()) {
            ScanConverter.Scan scan = scanConverter.getScan(y);

            if (scan.isValid()) {
                viewPos.setX(viewProjection.convertFromScreenXToViewX(scan.left));
                int offset = (y - viewProjection.getTopOffset()) * viewProjection.getWidth() + (scan.left - viewProjection.getLeftOffset());

                scanRenderer.render(offset, scan.left, scan.right);
            }
            y++;
            viewPos.setY(viewPos.getY() - 1);
        }
    }

    public class PlainColorTextureZRenderer extends ScanRenderer {

        public void render(int offset, int left, int right) {
            PlainColorTexture texture = (PlainColorTexture) currentTexture;

            float z = c.dotProduct(viewPos);
            int depth = (int) (w * z);
            int dDepth = (int) (w * c.getX());

            for (int x = left; x <= right; x++) {
                if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                    doubleBufferData[offset] = texture.getColor(0, 0);
                }
                offset++;
                depth += dDepth;
            }
        }
    }


    // the following three ScanRenderers are the same, but refer
    // to textures explicitly as either a PowerOf2Texture, a
    // ShadedTexture, or a ShadedSurface.
    // This allows HotSpot to do some inlining of the textures'
    // getColor() method, which significantly increases
    // performance.

    public class PowerOf2TextureZRenderer extends ScanRenderer {

        public void render(int offset, int left, int right) {
            PowerOf2Texture texture = (PowerOf2Texture) currentTexture;
            float u = SCALE * a.dotProduct(viewPos);
            float v = SCALE * b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = INTERP_SIZE * SCALE * a.getX();
            float dv = INTERP_SIZE * SCALE * b.getX();
            float dz = INTERP_SIZE * c.getX();
            int nextTx = (int) (u / z);
            int nextTy = (int) (v / z);
            int depth = (int) (w * z);
            int dDepth = (int) (w * c.getX());
            int x = left;
            while (x <= right) {
                int tx = nextTx;
                int ty = nextTy;
                int maxLength = right - x + 1;
                if (maxLength > INTERP_SIZE) {
                    u += du;
                    v += dv;
                    z += dz;
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) >> INTERP_SIZE_BITS;
                    int dty = (nextTy - ty) >> INTERP_SIZE_BITS;
                    int endOffset = offset + INTERP_SIZE;
                    while (offset < endOffset) {
                        if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                            doubleBufferData[offset] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        }
                        offset++;
                        tx += dtx;
                        ty += dty;
                        depth += dDepth;
                    }
                    x += INTERP_SIZE;
                } else {
                    // variable interpolation size
                    int interpSize = maxLength;
                    u += interpSize * SCALE * a.getX();
                    v += interpSize * SCALE * b.getX();
                    z += interpSize * c.getX();
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) / interpSize;
                    int dty = (nextTy - ty) / interpSize;
                    int endOffset = offset + interpSize;
                    while (offset < endOffset) {
                        if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                            doubleBufferData[offset] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        }
                        offset++;
                        tx += dtx;
                        ty += dty;
                        depth += dDepth;
                    }
                    x += interpSize;

                }

            }
        }
    }

    public class ShadedTextureZRenderer extends ScanRenderer {

        public void render(int offset, int left, int right) {
            String m = BenchDataObserver.tic("ShadedTextureZRenderer.render");
            ShadedTexture texture = (ShadedTexture) currentTexture;

            float u = SCALE * a.dotProduct(viewPos);
            float v = SCALE * b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = INTERP_SIZE * SCALE * a.getX();
            float dv = INTERP_SIZE * SCALE * b.getX();
            float dz = INTERP_SIZE * c.getX();
            int nextTx = (int) (u / z);
            int nextTy = (int) (v / z);
            int depth = (int) (w * z);
            int dDepth = (int) (w * c.getX());
            int x = left;
            while (x <= right) {
                int tx = nextTx;
                int ty = nextTy;
                int maxLength = right - x + 1;
                if (maxLength > INTERP_SIZE) {
                    u += du;
                    v += dv;
                    z += dz;
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) >> INTERP_SIZE_BITS;
                    int dty = (nextTy - ty) >> INTERP_SIZE_BITS;
                    int endOffset = offset + INTERP_SIZE;
                    while (offset < endOffset) {
                        if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                            doubleBufferData[offset] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        }
                        offset++;
                        tx += dtx;
                        ty += dty;
                        depth += dDepth;
                    }
                    x += INTERP_SIZE;
                } else {
                    // variable interpolation size
                    int interpSize = maxLength;
                    u += interpSize * SCALE * a.getX();
                    v += interpSize * SCALE * b.getX();
                    z += interpSize * c.getX();
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) / interpSize;
                    int dty = (nextTy - ty) / interpSize;
                    int endOffset = offset + interpSize;
                    while (offset < endOffset) {
                        if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                            doubleBufferData[offset] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        }
                        offset++;
                        tx += dtx;
                        ty += dty;
                        depth += dDepth;
                    }
                    x += interpSize;
                }

            }

            BenchDataObserver.tac(m);
        }
    }

    public class ShadedSurfaceZRenderer extends ScanRenderer {

        public int checkBounds(int vScaled, int bounds) {
            int v = vScaled >> SCALE_BITS;
            if (v < 0) {
                vScaled = 0;
            } else if (v >= bounds) {
                vScaled = (bounds - 1) << SCALE_BITS;
            }
            return vScaled;
        }

        public void render(int offset, int left, int right) {
            ShadedSurface texture = (ShadedSurface) currentTexture;
            float u = SCALE * a.dotProduct(viewPos);
            float v = SCALE * b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = INTERP_SIZE * SCALE * a.getX();
            float dv = INTERP_SIZE * SCALE * b.getX();
            float dz = INTERP_SIZE * c.getX();
            int nextTx = (int) (u / z);
            int nextTy = (int) (v / z);
            int depth = (int) (w * z);
            int dDepth = (int) (w * c.getX());
            int x = left;
            while (x <= right) {
                int tx = nextTx;
                int ty = nextTy;
                int maxLength = right - x + 1;
                if (maxLength > INTERP_SIZE) {
                    u += du;
                    v += dv;
                    z += dz;
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) >> INTERP_SIZE_BITS;
                    int dty = (nextTy - ty) >> INTERP_SIZE_BITS;
                    int endOffset = offset + INTERP_SIZE;
                    while (offset < endOffset) {
                        if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                            doubleBufferData[offset] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        }
                        offset++;
                        tx += dtx;
                        ty += dty;
                        depth += dDepth;
                    }
                    x += INTERP_SIZE;
                } else {
                    // variable interpolation size
                    int interpSize = maxLength;
                    u += interpSize * SCALE * a.getX();
                    v += interpSize * SCALE * b.getX();
                    z += interpSize * c.getX();
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);

                    // make sure tx, ty, nextTx, and nextTy are
                    // all within bounds
                    tx = checkBounds(tx, texture.getWidth());
                    ty = checkBounds(ty, texture.getHeight());
                    nextTx = checkBounds(nextTx, texture.getWidth());
                    nextTy = checkBounds(nextTy, texture.getHeight());

                    int dtx = (nextTx - tx) / interpSize;
                    int dty = (nextTy - ty) / interpSize;
                    int endOffset = offset + interpSize;
                    while (offset < endOffset) {
                        if (zBuffer.checkDepth(offset, (short) (depth >> SCALE_BITS))) {
                            doubleBufferData[offset] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        }
                        offset++;
                        tx += dtx;
                        ty += dty;
                        depth += dDepth;
                    }
                    x += interpSize;

                }

            }
        }
    }

}
