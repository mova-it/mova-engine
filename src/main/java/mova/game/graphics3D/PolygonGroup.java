package mova.game.graphics3D;

import mova.game.core.Copyable;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.graphics3D.math3D.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PolygonGroup extends AbstractGameComponent implements Copyable<PolygonGroup> {
    private final List<Object> polygons;
    private final MovingTransform3D transform;
    private int iteratorIndex;

    public PolygonGroup() {
        polygons = new ArrayList<>();
        transform = new MovingTransform3D();
        iteratorIndex = 0;
    }

    private PolygonGroup(PolygonGroup polygonGroup) {
        polygons = new ArrayList<>();
        for (Object o : polygonGroup.polygons) {
            if (o instanceof SolidPolygon3D) {
                polygons.add(new SolidPolygon3D((Polygon3D) o));
            } else if (o instanceof TexturedPolygon3D) {
                polygons.add(new TexturedPolygon3D((Polygon3D) o));
            } else if (o instanceof Polygon3D) {
                polygons.add(new Polygon3D((Polygon3D) o));
            } else {
                polygons.add(new PolygonGroup((PolygonGroup) o));
            }
        }

        transform = new MovingTransform3D(polygonGroup.transform);
        iteratorIndex = 0;
    }

    public void addPolygon(Polygon3D o) {
        polygons.add(o);
    }

    public void addPolygonGroup(PolygonGroup o) {
        polygons.add(o);
    }

    public Collection<Object> getPolygons() {
        return polygons;
    }

    public void resetIterator() {
        iteratorIndex = 0;
        for (Object obj : polygons) {
            if (obj instanceof PolygonGroup) {
                ((PolygonGroup) obj).resetIterator();
            }
        }
    }


    public boolean hasNext() {
        return (iteratorIndex < polygons.size());
    }


    public Polygon3D nextPolygon() {
        Object obj = polygons.get(iteratorIndex);

        if (obj instanceof PolygonGroup) {
            PolygonGroup group = (PolygonGroup)obj;
            Polygon3D poly = group.nextPolygon();
            if (!group.hasNext()) {
                iteratorIndex++;
            }
            return poly;
        }
        else {
            iteratorIndex++;
            return (Polygon3D)obj;
        }
    }

    public void nextPolygonTransformed(Polygon3D cache) {
        Object obj = polygons.get(iteratorIndex);

        if (obj instanceof PolygonGroup) {
            PolygonGroup group = (PolygonGroup)obj;
            group.nextPolygonTransformed(cache);
            if (!group.hasNext()) {
                iteratorIndex++;
            }
        }
        else {
            iteratorIndex++;
            cache.setTo((Polygon3D)obj);
        }

        cache.add(transform);
    }


    @Override
    public void update(long elapsedTime) {
        transform.update(elapsedTime);
        for (Object obj : polygons) {
            if (obj instanceof PolygonGroup) {
                PolygonGroup group = (PolygonGroup) obj;
                group.update(elapsedTime);
            }
        }
    }

    public void add(Vector3D u) {
        transform.getLocation().add(u);
    }

    public void subtract(Vector3D u) {
        transform.getLocation().subtract(u);
    }

    public void add(Transform3D xform) {
        addRotation(xform.getRotation());
        add(xform.getLocation());
    }

    public void subtract(Transform3D xform) {
        subtract(xform.getLocation());
        subtractRotation(xform.getRotation());
    }

    public void addRotation(Rotation3D rotation) {
        transform.getRotation().addRotation(rotation);
    }

    public void subtractRotation(Rotation3D rotation) {
        transform.getRotation().subtractRotation(rotation);
    }

    @Override
    public PolygonGroup copy() {
        return new PolygonGroup(this);
    }
}
