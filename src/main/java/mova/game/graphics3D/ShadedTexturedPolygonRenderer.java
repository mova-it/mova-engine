package mova.game.graphics3D;

import java.awt.*;

import mova.game.graphics3D.math3D.*;
import mova.game.graphics3D.texture.ShadedTexture;
import mova.game.graphics3D.texture.Texture;

public class ShadedTexturedPolygonRenderer extends FastTexturedPolygonRenderer {

    private PointLight3D lightSource;
    private float ambientLightIntensity = 0.5f;
    private final Vector3D directionToLight = new Vector3D();

    public ShadedTexturedPolygonRenderer(Camera camera) {
        this(camera, true);
    }

    public ShadedTexturedPolygonRenderer(Camera camera, boolean clearViewEveryFrame) {
        super(camera, clearViewEveryFrame);
    }


    public PointLight3D getLightSource() {
        return lightSource;
    }


    public void setLightSource(PointLight3D lightSource) {
        this.lightSource = lightSource;
    }


    public float getAmbientLightIntensity() {
        return ambientLightIntensity;
    }


    public void setAmbientLightIntensity(float i) {
        ambientLightIntensity = i;
    }


    @Override
    protected void drawCurrentPolygon(Graphics2D g, Polygon3D currentPolygon) {
        // set the shade level of the polygon before drawing it
        if (currentPolygon instanceof TexturedPolygon3D) {
            TexturedPolygon3D poly = ((TexturedPolygon3D) currentPolygon);
            Texture texture = poly.getTexture();
            if (texture instanceof ShadedTexture) calcShadeLevel(currentPolygon);
        }
        super.drawCurrentPolygon(g, currentPolygon);
    }


    /**
     * Calculates the shade level of the current polygon
     */
    private void calcShadeLevel(Polygon3D currentPolygon) {
        TexturedPolygon3D poly = (TexturedPolygon3D) currentPolygon;
        float intensity = 0;
        if (lightSource != null) {


            // average all the vertices in the polygon
            directionToLight.setTo(0, 0, 0);
            for (int i = 0; i < poly.getNumVertices(); i++) {
                directionToLight.add(poly.getVertex(i));
            }
            directionToLight.divide(poly.getNumVertices());

            // make the vector from the average vertex
            // to the light
            directionToLight.subtract(lightSource);
            directionToLight.multiply(-1);

            // get the distance to the light for falloff
            float distance = directionToLight.length();

            // compute the diffuse reflect
            directionToLight.normalize();
            Vector3D normal = poly.getNormal();
            intensity = lightSource.getIntensity(distance) * directionToLight.dotProduct(normal);
            intensity = Math.min(intensity, 1);
            intensity = Math.max(intensity, 0);
        }

        intensity += ambientLightIntensity;
        intensity = Math.min(intensity, 1);
        intensity = Math.max(intensity, 0);
        int level = Math.round(intensity * ShadedTexture.MAX_LEVEL);
        ((ShadedTexture) poly.getTexture()).setDefaultShadeLevel(level);
    }

}
