package mova.game.graphics3D;

import mova.game.graphics3D.math3D.Camera;
import mova.game.graphics3D.math3D.Polygon3D;
import mova.game.graphics3D.math3D.SolidPolygon3D;

import java.awt.*;

public class SolidPolygonRenderer extends PolygonRenderer {

    public SolidPolygonRenderer(Camera camera) {
        this(camera, true);
    }

    public SolidPolygonRenderer(Camera camera, boolean clearViewEveryFrame) {
        super(camera, clearViewEveryFrame);
    }

    protected void drawCurrentPolygon(Graphics2D graphics, Polygon3D currentPolygon) {

        // set the color
        if (currentPolygon instanceof SolidPolygon3D) {
            graphics.setColor(((SolidPolygon3D) currentPolygon).getColor());
        } else {
            graphics.setColor(Color.GREEN);
        }

        // draw the scans
        int y = scanConverter.getTopBoundary();
        while (y <= scanConverter.getBottomBoundary()) {
            ScanConverter.Scan scan = scanConverter.getScan(y);
            if (scan.isValid()) graphics.drawLine(scan.left, y, scan.right, y);
            y++;
        }
    }

}
