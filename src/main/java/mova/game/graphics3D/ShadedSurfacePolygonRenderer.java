package mova.game.graphics3D;

import mova.game.graphics3D.math3D.Camera;
import mova.game.graphics3D.math3D.Polygon3D;
import mova.game.graphics3D.math3D.TexturedPolygon3D;
import mova.game.graphics3D.texture.ShadedSurface;
import mova.game.graphics3D.texture.Texture;

import java.awt.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ShadedSurfacePolygonRenderer extends FastTexturedPolygonRenderer {

    private final List<ShadedSurface> builtSurfaces = new LinkedList<>();

    public ShadedSurfacePolygonRenderer(Camera camera) {
        this(camera, true);
    }

    public ShadedSurfacePolygonRenderer(Camera camera, boolean eraseView) {
        super(camera, eraseView);
    }

    @Override
    public void endFrame(Graphics2D g) {
        super.endFrame(g);

        // clear all built surfaces that weren't used this frame.
        Iterator<ShadedSurface> i = builtSurfaces.iterator();
        while (i.hasNext()) {
            ShadedSurface surface = i.next();
            if (surface.isDirty()) {
                surface.clearSurface();
                i.remove();
            } else {
                surface.setDirty(true);
            }
        }
    }

    @Override
    protected void drawCurrentPolygon(Graphics2D g, Polygon3D currentPolygon) {
        buildSurface(currentPolygon);
        super.drawCurrentPolygon(g, currentPolygon);
    }


    /**
     * Builds the surface of the polygon if it has a
     * ShadedSurface that is cleared.
     */
    protected void buildSurface(Polygon3D currentPolygon) {
        // build surface, if needed
        if (currentPolygon instanceof TexturedPolygon3D) {
            Texture texture = ((TexturedPolygon3D) currentPolygon).getTexture();
            if (texture instanceof ShadedSurface) {
                ShadedSurface surface = (ShadedSurface) texture;
                if (surface.isCleared()) {
                    surface.buildSurface();
                    builtSurfaces.add(surface);
                }
                surface.setDirty(false);
            }
        }
    }

}
