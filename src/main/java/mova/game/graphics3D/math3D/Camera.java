package mova.game.graphics3D.math3D;

import mova.game.graphics.component.GameWindow;

public class Camera {

    private final Transform3D position;
    private final ViewProjection viewProjection;

    public Camera(GameWindow gameWindow) {
        this(new Transform3D(), gameWindow);
    }
    public Camera(float x, float y, float z, GameWindow gameWindow) {
        this(x, y, z, gameWindow.getWidth(), gameWindow.getHeight());
    }

    public Camera(float x, float y, float z, GameWindow gameWindow, float angle) {
        this(x, y, z, gameWindow.getWidth(), gameWindow.getHeight(), angle);
    }

    public Camera(float x, float y, float z, int width, int height) {
        this(new Transform3D(x, y, z), new ViewProjection(0, 0, width, height, (float) Math.toRadians(75)));
    }

    public Camera(float x, float y, float z, int width, int height, float angle) {
        this(new Transform3D(x, y, z), new ViewProjection(0, 0, width, height, angle));
    }

    public Camera(Transform3D position, GameWindow gameWindow) {
        this(position, gameWindow, (float) Math.toRadians(75));
    }

    public Camera(Transform3D position, GameWindow gameWindow, float angle) {
        this(position, new ViewProjection(0, 0, gameWindow.getWidth(), gameWindow.getHeight(), angle));
    }

    /**
     * @deprecated la ViewProjection ne devrait pas être construite en dehors de la camera
     */
    @Deprecated
    public Camera(Transform3D position, ViewProjection viewProjection) {
        this.position = position;
        this.viewProjection = viewProjection;
    }

    public ViewProjection getViewProjection() {
        return viewProjection;
    }

    public Transform3D getPosition() {
        return position;
    }

    public float getWorldX() {
        return position.location.getX();
    }

    public void setWorldX(float x) {
        position.location.setX(x);
    }

    public void addWorldX(float x) {
        setWorldX(position.location.getX() + x);
    }

    public void subWorldX(float x) {
        setWorldX(position.location.getX() - x);
    }

    public float getWorldY() {
        return position.location.getY();
    }

    public void setWorldY(float y) {
        position.location.setY(y);
    }

    public void addWorldY(float y) {
        setWorldY(position.location.getY() + y);
    }

    public void subWorldY(float y) {
        setWorldY(position.location.getY() - y);
    }

    public float getWorldZ() {
        return position.location.getZ();
    }

    public void setWorldZ(float z) {
        position.location.setZ(z);
    }

    public void addWorldZ(float z) {
        setWorldZ(position.location.getZ() + z);
    }

    public void subWorldZ(float z) {
        setWorldZ(position.location.getZ() - z);
    }

}
