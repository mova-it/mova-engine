package mova.game.graphics3D.math3D;

public class Rectangle3D {

    private final Vector3D origin;
    private final Vector3D directionU;
    private final Vector3D directionV;
    private final Vector3D normal = new Vector3D();
    private float width;
    private float height;

    public Rectangle3D() {
        origin = new Vector3D();
        directionU = new Vector3D(1,0,0);
        directionV = new Vector3D(0,1,0);
        width = 0;
        height = 0;
        calcNormal();
    }


    public Rectangle3D(Vector3D origin, Vector3D directionU, Vector3D directionV, float width, float height) {
        this.origin = new Vector3D(origin);
        this.directionU = new Vector3D(directionU);
        this.directionU.normalize();
        this.directionV = new Vector3D(directionV);
        this.directionV.normalize();
        this.width = width;
        this.height = height;
        calcNormal();
    }


    public void setTo(Rectangle3D rect) {
        origin.setTo(rect.origin);
        directionU.setTo(rect.directionU);
        directionV.setTo(rect.directionV);
        width = rect.width;
        height = rect.height;
        calcNormal();
    }


    public Vector3D getOrigin() {
        return origin;
    }


    public Vector3D getDirectionU() {
        return directionU;
    }


    public Vector3D getDirectionV() {
        return directionV;
    }


    public float getWidth() {
        return width;
    }


    public void setWidth(float width) {
        this.width = width;
    }


    public float getHeight() {
        return height;
    }


    public void setHeight(float height) {
        this.height = height;
    }


    protected void calcNormal() {
        normal.setToCrossProduct(directionU, directionV);
        normal.normalize();
    }


    public Vector3D getNormal() {
        return normal;
    }


    public void setNormal(Vector3D n) {
        normal.setTo(n);
    }


    public void add(Vector3D u) {
        origin.add(u);
        // don't translate direction vectors or size
    }

    public void subtract(Vector3D u) {
        origin.subtract(u);
        // don't translate direction vectors or size
    }

    public void add(Transform3D xform) {
        addRotation(xform.getRotation());
        add(xform.getLocation());
    }

    public void subtract(Transform3D xform) {
        subtract(xform.getLocation());
        subtractRotation(xform.getRotation());
    }

    public void addRotation(Rotation3D rotation) {
        origin.addRotation(rotation);
        directionU.addRotation(rotation);
        directionV.addRotation(rotation);
    }

    public void subtractRotation(Rotation3D rotation) {
        origin.subtractRotation(rotation);
        directionU.subtractRotation(rotation);
        directionV.subtractRotation(rotation);
    }

}
