package mova.game.graphics3D.math3D;

import java.awt.*;

public class SolidPolygon3D extends Polygon3D {

    private Color color = Color.GREEN;

    public SolidPolygon3D() {
        super();
    }

    public SolidPolygon3D(Vector3D v1, Vector3D v2, Vector3D v3) {
        super(v1, v2, v3);
    }

    public SolidPolygon3D(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) {
        super(v1, v2, v3, v4);
    }

    public SolidPolygon3D(Vector3D[] vertices) {
        super(vertices);
    }

    public SolidPolygon3D(Polygon3D polygon3D) {
        setTo(polygon3D);
    }


    @Override
    public void setTo(Polygon3D polygon) {
        super.setTo(polygon);
        if (polygon instanceof SolidPolygon3D) {
            color = ((SolidPolygon3D)polygon).color;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
