package mova.game.graphics3D.math3D;

public class Transform3D {

    protected final Vector3D location;
    private final Rotation3D rotation;

    public Transform3D() {
        this(0, 0, 0);
    }

    public Transform3D(float x, float y, float z) {
        location = new Vector3D(x, y, z);
        rotation = new Rotation3D();
    }

    public Transform3D(Transform3D transform) {
        this(transform.location.getX(), transform.location.getY(), transform.location.getZ());
    }

    public Rotation3D getRotation() {
        return rotation;
    }

    public void setTo(Transform3D transform) {
        location.setTo(transform.location);
        rotation.setTo(transform.rotation);
    }

    public Vector3D getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Transform3D{" +
                "location=" + location +
                ", rotation=" + rotation +
                '}';
    }
}
