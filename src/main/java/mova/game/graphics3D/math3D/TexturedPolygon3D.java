package mova.game.graphics3D.math3D;

import mova.game.graphics3D.texture.Texture;

public class TexturedPolygon3D extends Polygon3D {

    protected Rectangle3D textureBounds;
    protected Texture texture;

    public TexturedPolygon3D() {
        textureBounds = new Rectangle3D();
    }

    public TexturedPolygon3D(Texture texture, Vector3D... vertices) {
        super(vertices);

        Vector3D origin = getVertex(0);

        Vector3D dv = new Vector3D(getVertex(1));
        dv.subtract(origin);

        Vector3D du = new Vector3D();
        du.setToCrossProduct(getNormal(), dv);

        this.texture = texture;
        this.textureBounds = new Rectangle3D(origin, du, dv, texture.getWidth(), texture.getHeight());
    }

    public TexturedPolygon3D(Polygon3D polygon3D) {
        setTo(polygon3D);
    }

    @Override
    public void setTo(Polygon3D poly) {
        super.setTo(poly);
        if (poly instanceof TexturedPolygon3D) {
            TexturedPolygon3D tPoly = (TexturedPolygon3D) poly;
            if (textureBounds == null) textureBounds = new Rectangle3D();
            textureBounds.setTo(tPoly.textureBounds);
            texture = tPoly.texture;
        }
    }


    public Texture getTexture() {
        return texture;
    }


    public Rectangle3D getTextureBounds() {
        return textureBounds;
    }


    public void setTexture(Texture texture) {
        this.texture = texture;
        textureBounds.setWidth(texture.getWidth());
        textureBounds.setHeight(texture.getHeight());
    }


    public void setTexture(Texture texture, Rectangle3D bounds) {
        setTexture(texture);
        textureBounds.setTo(bounds);
    }

    @Override
    public void add(Vector3D u) {
        super.add(u);
        textureBounds.add(u);
    }

    @Override
    public void subtract(Vector3D u) {
        super.subtract(u);
        textureBounds.subtract(u);
    }

    @Override
    public void addRotation(Rotation3D rotation) {
        super.addRotation(rotation);
        textureBounds.addRotation(rotation);
    }

    @Override
    public void subtractRotation(Rotation3D rotation) {
        super.subtractRotation(rotation);
        textureBounds.subtractRotation(rotation);
    }


    public Rectangle3D calcBoundingRectangle() {
        Vector3D u = new Vector3D(textureBounds.getDirectionU());
        Vector3D v = new Vector3D(textureBounds.getDirectionV());
        Vector3D d = new Vector3D();
        u.normalize();
        v.normalize();

        float uMin = 0;
        float uMax = 0;
        float vMin = 0;
        float vMax = 0;
        for (int i = 0; i < getNumVertices(); i++) {
            d.setTo(getVertex(i));
            d.subtract(getVertex(0));
            float uLength = d.dotProduct(u);
            float vLength = d.dotProduct(v);
            uMin = Math.min(uLength, uMin);
            uMax = Math.max(uLength, uMax);
            vMin = Math.min(vLength, vMin);
            vMax = Math.max(vLength, vMax);
        }

        Rectangle3D boundingRect = new Rectangle3D();
        Vector3D origin = boundingRect.getOrigin();
        origin.setTo(getVertex(0));
        d.setTo(u);
        d.multiply(uMin);
        origin.add(d);
        d.setTo(v);
        d.multiply(vMin);
        origin.add(d);
        boundingRect.getDirectionU().setTo(u);
        boundingRect.getDirectionV().setTo(v);
        boundingRect.setWidth(uMax - uMin);
        boundingRect.setHeight(vMax - vMin);

        // explictly set the normal since the texture directions
        // could create a normal negative to the polygon normal
        boundingRect.setNormal(getNormal());

        return boundingRect;
    }
}
