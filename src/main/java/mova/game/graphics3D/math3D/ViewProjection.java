package mova.game.graphics3D.math3D;

import java.awt.*;

/**
 * Effectue deux opérations principales:
 * - Change le système de coordonnées de l'écran prenant comme origine le coin supérieur gauche de l'écran et un axe des Y
 * croissant vers le bas, vers un système de coordonnées centré dans un cadre défini avec un axe des Y croissant vers le haut
 * - Permet de projeter des points dans l'espace vers le cadre défini en respectant un angle de focal
 */
public class ViewProjection {

    private final Rectangle bounds = new Rectangle();
    private float angle;
    private float distance;

    public ViewProjection(int left, int top, int width, int height, float angle) {
        this.angle = angle;
        setBounds(left, top, width, height);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(int left, int top, int width, int height) {
        bounds.setBounds(left, top, width, height);
        recalcDistance();
    }

    public void setAngle(float angle) {
        this.angle = angle;
        recalcDistance();
    }

    private void recalcDistance() {
        distance = bounds.width/2f/(float)Math.tan(angle/2f);
    }

    public float getAngle() {
        return angle;
    }

    public int getWidth() {
        return bounds.width;
    }

    public int getHeight() {
        return bounds.height;
    }

    public int getLeftOffset() {
        return bounds.x;
    }

    public int getTopOffset() {
        return bounds.y;
    }

    public float getDistance() {
        return distance;
    }

    public float convertFromViewXToScreenX(float x) {
        return x + (float) bounds.getCenterX();
    }

    public float convertFromViewYToScreenY(float y) {
        return -y + (float) bounds.getCenterY();
    }

    public float convertFromScreenXToViewX(float x) {
        return x - (float) bounds.getCenterX();
    }

    public float convertFromScreenYToViewY(float y) {
        return -y + (float) bounds.getCenterY();
    }

    private static final Point scratchPoint = new Point();

    public Point convertFromViewToScreen(Point p) {
        float x = convertFromViewXToScreenX(p.x);
        float y = convertFromViewYToScreenY(p.y);

        scratchPoint.setLocation(x, y);
        return scratchPoint;
    }

    public Point convertFromScreenToView(Point p) {
        float x = convertFromScreenXToViewX(p.x);
        float y = convertFromScreenYToViewY(p.y);

        scratchPoint.setLocation(x, y);
        return scratchPoint;
    }

    public void project(Vector3D v) {
        v.setX(distance* v.getX()/-v.getZ());
        v.setY(distance* v.getY()/-v.getZ());

        v.setX(convertFromViewXToScreenX(v.getX()));
        v.setY(convertFromViewYToScreenY(v.getY()));
    }

    @Override
    public String toString() {
        return "ViewProjection{" +
                "bounds=" + bounds +
                ", angle=" + angle +
                ", distance=" + distance +
                '}';
    }
}
