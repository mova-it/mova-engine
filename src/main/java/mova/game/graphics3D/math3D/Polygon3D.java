package mova.game.graphics3D.math3D;

import java.util.Arrays;

public class Polygon3D {

    // temporary vectors used for calculation
    // Fonctionne seulement si on est sûr que chaque polygon est annalyser séparément
    // Sinon il y aura concurence.
    private static final Vector3D temp1 = new Vector3D();
    private static final Vector3D temp2 = new Vector3D();

    private final Vector3D normal = new Vector3D();

    private Vector3D[] vertices;
    private int numVertices;

    public Polygon3D() {
        numVertices = 0;
        vertices = new Vector3D[0];
    }

    public Polygon3D(Vector3D... vertices) {
        this.vertices = vertices;
        numVertices = vertices.length;
        calcNormal();
    }

    public Polygon3D(Polygon3D polygon3D) {
        setTo(polygon3D);
    }

    public void setTo(Polygon3D polygon) {
        numVertices = polygon.numVertices;
        normal.setTo(polygon.normal);

        ensureCapacity(numVertices);
        for (int i = 0; i < numVertices; i++) {
            vertices[i].setTo(polygon.vertices[i]);
        }
    }

    public void ensureCapacity(int length) {
        if (vertices.length < length) {
            Vector3D[] newVertices = new Vector3D[length];
            System.arraycopy(vertices, 0, newVertices, 0, vertices.length);
            for (int i = vertices.length; i < newVertices.length; i++) {
                newVertices[i] = new Vector3D();
            }
            vertices = newVertices;
        }
    }

    public int getNumVertices() {
        return numVertices;
    }

    public Vector3D getVertex(int i) {
        return vertices[i];
    }

    public Vector3D getVertexNextTo(int i) {
        return vertices[(i + 1) % numVertices];
    }

    public void project(ViewProjection viewProjection) {
        for (Vector3D vertex : vertices) {
            viewProjection.project(vertex);
        }
    }

    public void add(Vector3D u) {
        for (int i = 0; i < numVertices; i++) {
            vertices[i].add(u);
        }
    }

    public void subtract(Vector3D u) {
        for (int i = 0; i < numVertices; i++) {
            vertices[i].subtract(u);
        }
    }

    public void add(Transform3D xform) {
        addRotation(xform.getRotation());
        add(xform.getLocation());
    }

    public void subtract(Transform3D xform) {
        subtract(xform.getLocation());
        subtractRotation(xform.getRotation());
    }

    public void addRotation(Rotation3D rotation) {
        for (int i = 0; i < numVertices; i++) {
            vertices[i].addRotation(rotation);
        }
        normal.addRotation(rotation);
    }

    public void subtractRotation(Rotation3D rotation) {
        for (int i = 0; i < numVertices; i++) {
            vertices[i].subtractRotation(rotation);
        }
        normal.subtractRotation(rotation);
    }

    public void calcNormal() {
        temp1.setTo(vertices[2]);
        temp1.subtract(vertices[1]);

        temp2.setTo(vertices[0]);
        temp2.subtract(vertices[1]);

        normal.setToCrossProduct(temp1, temp2);
        normal.normalize();

    }

    public Vector3D getNormal() {
        return normal;
    }

    public void setNormal(Vector3D n) {
        normal.setTo(n);
    }

    public boolean isFacing(Vector3D u) {
        temp1.setTo(u);
        temp1.subtract(vertices[0]); // TODO ne faudrait-il pas plutot définir le vecteur test comme étant le vecteur caméra - un point de la vue plutot?
        return normal.dotProduct(temp1) >= 0;
    }


    public boolean clip(float clipZ) {
        // TODO j'arrive pas encore à déterminer pourquoi x 3
        ensureCapacity(numVertices * 3);

        boolean isCompletelyHidden = true;

        // insert vertices so all edges are either completly
        // in front or behind the clip plane
        for (int i = numVertices - 1; i >= 0; i--) {
            int next = (i + 1) % numVertices;
            Vector3D v1 = vertices[i];
            Vector3D v2 = vertices[next];

            if (v1.getZ() < clipZ) isCompletelyHidden = false;

            // ensure v1.z < v2.z
            if (v1.getZ() > v2.getZ()) {
                Vector3D temp = v1;
                v1 = v2;
                v2 = temp;
            }

            if (v1.getZ() < clipZ && v2.getZ() > clipZ) {
                float scale = (clipZ - v1.getZ()) / (v2.getZ() - v1.getZ());
                float intX = v1.getX() + scale * (v2.getX() - v1.getX());
                float intY = v1.getY() + scale * (v2.getY() - v1.getY());
                insertVertex(next, intX, intY, clipZ);
                // TODO normalement on incrémentait l'index pour skipper le vertex insérer. A la place on parcours les vertices à rebours (A TESTER)
            }
        }

        if (isCompletelyHidden) return false;

        // delete all vertices that have z > clipZ
        for (int i = numVertices - 1; i >= 0; i--) {
            if (vertices[i].getZ() > clipZ) {
                deleteVertex(i);
            }
        }

        return numVertices >= 3;
    }


    protected void insertVertex(int index, float x, float y, float z) {
        Vector3D newVertex = vertices[vertices.length - 1];
        newVertex.setX(x);
        newVertex.setY(y);
        newVertex.setZ(z);


        if (vertices.length - 1 - index >= 0) {
            System.arraycopy(vertices, index, vertices, index + 1, vertices.length - 1 - index);
        }
        vertices[index] = newVertex;
        numVertices++;
    }


    protected void deleteVertex(int index) {
        Vector3D deleted = vertices[index];

        if (vertices.length - 1 - index >= 0) {
            System.arraycopy(vertices, index + 1, vertices, index, vertices.length - 1 - index);
        }

        vertices[vertices.length - 1] = deleted;
        numVertices--;
    }


    public void insertVertex(int index, Vector3D vertex) {
        Vector3D[] newV = new Vector3D[numVertices + 1];
        System.arraycopy(vertices, 0, newV, 0, index);
        newV[index] = vertex;
        System.arraycopy(vertices, index, newV, index + 1, numVertices - index);
        vertices = newV;
        numVertices++;
    }

    @Override
    public String toString() {
        return "Polygon3D{" +
                "normal=" + normal +
                ", vertices=" + Arrays.toString(vertices) +
                '}';
    }
}
