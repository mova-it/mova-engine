package mova.game.graphics3D.math3D;

import mova.trigo.Angle;

public class Rotation3D {

    private final Angle angleX = new Angle();
    private final Angle angleY = new Angle();
    private final Angle angleZ = new Angle();

    public Rotation3D() {
        // Initialisation directement dans les paramètres
    }

    public void setTo(Rotation3D rotation) {
        angleX.set(rotation.angleX);
        angleY.set(rotation.angleY);
        angleZ.set(rotation.angleZ);
    }

    public Angle getAngleX() {
        return angleX;
    }

    public Angle getAngleY() {
        return angleY;
    }

    public Angle getAngleZ() {
        return angleZ;
    }

    public float getAngleXRadians() {
        return angleX.toRadians();
    }

    public float getAngleYRadians() {
        return angleY.toRadians();
    }

    public float getAngleZRadians() {
        return angleZ.toRadians();
    }

    public void setAngleX(float radians) {
        angleX.set(radians);
    }

    public void setAngleX(float cos, float sin) {
        angleX.set(cos, sin);
    }

    public void setAngleY(float radians) {
        angleY.set(radians);
    }

    public void setAngleY(float cos, float sin) {
        angleY.set(cos, sin);
    }

    public void setAngleZ(float radians) {
        angleZ.set(radians);
    }

    public void setAngleZ(float cos, float sin) {
        angleZ.set(cos, sin);
    }

    public void setAngles(float radX, float radY, float radZ) {
        setAngleX(radX);
        setAngleY(radY);
        setAngleZ(radZ);
    }

    public void addAngleX(float radians) {
        if (radians != 0) angleX.add(radians);
    }

    public void addAngleX(Angle angle) {
        addAngleX(angle.cos(), angle.sin());
    }

    public void addAngleX(float cos, float sin) {
        angleX.add(cos, sin);
    }

    public void addAngleY(float radians) {
        if (radians != 0) angleY.add(radians);
    }

    public void addAngleY(Angle angle) {
        addAngleY(angle.cos(), angle.sin());
    }

    public void addAngleY(float cos, float sin) {
        angleY.add(cos, sin);
    }

    public void addAngleZ(float radians) {
        if (radians != 0) angleZ.add(radians);
    }

    public void addAngleZ(Angle angle) {
        addAngleZ(angle.cos(), angle.sin());
    }

    public void addAngleZ(float cos, float sin) {
        angleZ.add(cos, sin);
    }

    public void addRotation(float radX, float radY, float radZ) {
        addAngleX(radX);
        addAngleY(radY);
        addAngleZ(radZ);
    }

    public void addRotation(Rotation3D rotation) {
        addAngleX(rotation.angleX);
        addAngleY(rotation.angleY);
        addAngleZ(rotation.angleZ);
    }

    public void subAngleX(float radians) {
        if (radians != 0) angleX.sub(radians);
    }

    public void subAngleX(Angle angle) {
        subAngleX(angle.cos(), angle.sin());
    }

    public void subAngleX(float cos, float sin) {
        angleX.sub(cos, sin);
    }

    public void subAngleY(float radians) {
        if (radians != 0) angleY.sub(radians);
    }

    public void subAngleY(Angle angle) {
        subAngleY(angle.cos(), angle.sin());
    }

    public void subAngleY(float cos, float sin) {
        angleY.sub(cos, sin);
    }

    public void subAngleZ(float radians) {
        if (radians != 0) angleZ.sub(radians);
    }

    public void subAngleZ(Angle angle) {
        subAngleZ(angle.cos(), angle.sin());
    }

    public void subAngleZ(float cos, float sin) {
        angleZ.sub(cos, sin);
    }

    public void subtractRotation(float radX, float radY, float radZ) {
        subAngleX(radX);
        subAngleY(radY);
        subAngleZ(radZ);
    }

    public void subtractRotation(Rotation3D rotation) {
        subAngleX(rotation.angleX);
        subAngleY(rotation.angleY);
        subAngleZ(rotation.angleZ);
    }

    @Override
    public String toString() {
        return "Rotation3D{" +
                "angleX=" + angleX.toRadians() +
                ", angleY=" + angleY.toRadians() +
                ", angleZ=" + angleZ.toRadians() +
                '}';
    }

    public String toDegreesString() {
        return "Rotation3D{" +
                "angleX=" + angleX.toDegrees() +
                ", angleY=" + angleY.toDegrees() +
                ", angleZ=" + angleZ.toDegrees() +
                '}';
    }

}