package mova.game.graphics3D.math3D;

import java.util.Objects;

public class PointLight3D extends Vector3D {

    public static final float NO_DISTANCE_FALLOFF = -1;

    private float intensity;
    private float distanceFalloff;


    public PointLight3D() {
        this(0, 0, 0, 1, NO_DISTANCE_FALLOFF);
    }

    public PointLight3D(PointLight3D p) {
        setTo(p);
    }

    public PointLight3D(float x, float y, float z, float intensity) {
        this(x, y, z, intensity, NO_DISTANCE_FALLOFF);
    }

    public PointLight3D(float x, float y, float z, float intensity, float distanceFalloff) {
        setTo(x, y, z);
        setIntensity(intensity);
        setDistanceFalloff(distanceFalloff);
    }


    public void setTo(PointLight3D p) {
        setTo(p.getX(), p.getY(), p.getZ());
        setIntensity(p.getIntensity());
        setDistanceFalloff(p.getDistanceFalloff());
    }


    public float getIntensity(float distance) {
        if (distanceFalloff == NO_DISTANCE_FALLOFF) {
            return intensity;
        } else if (distance >= distanceFalloff) {
            return 0;
        } else {
            return intensity * (distanceFalloff - distance) / (distanceFalloff + distance);
        }
    }


    public float getIntensity() {
        return intensity;
    }


    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }


    public float getDistanceFalloff() {
        return distanceFalloff;
    }


    public void setDistanceFalloff(float distanceFalloff) {
        this.distanceFalloff = distanceFalloff;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PointLight3D light = (PointLight3D) o;

        return equals(light.getX(), light.getY(), light.getZ()) && intensity == light.intensity && distanceFalloff == light.distanceFalloff;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + Objects.hash(intensity, distanceFalloff);
    }
}
