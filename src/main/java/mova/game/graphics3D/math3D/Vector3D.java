package mova.game.graphics3D.math3D;

import mova.trigo.Angle;

public class Vector3D {

    private float x;
    private float y;
    private float z;

    public Vector3D() {
        this(0, 0, 0);
    }

    public Vector3D(Vector3D v) {
        this(v.x, v.y, v.z);
    }

    public Vector3D(float x, float y, float z) {
        setTo(x, y, z);
    }

    public void setTo(Vector3D v) {
        setTo(v.x, v.y, v.z);
    }

    public void setTo(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void add(Vector3D v) {
        add(v.x, v.y, v.z);
    }

    public void add(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    public void subtract(Vector3D v) {
        add(-v.x, -v.y, -v.z);
    }

    public void subtract(float x, float y, float z) {
        add(-x, -y, -z);
    }

    public void multiply(float s) {
        x *= s;
        y *= s;
        z *= s;
    }

    public void divide(float s) {
        x /= s;
        y /= s;
        z /= s;
    }

    public float length() {
        return (float) Math.sqrt(x*x + y*y + z*z);
    }

    public void normalize() {
        divide(length());
    }

    public void rotateX(Angle angle) {
        rotateX(angle.cos(), angle.sin());
    }

    public void subRotateX(Angle angle) {
        rotateX(angle.cos(), -angle.sin());
    }

    public void rotateX(float cosAngle, float sinAngle) {
        float newY = y*cosAngle - z*sinAngle;
        float newZ = y*sinAngle + z*cosAngle;

        y = newY;
        z = newZ;
    }

    public void rotateY(Angle angle) {
        rotateY(angle.cos(), angle.sin());
    }

    public void subRotateY(Angle angle) {
        rotateY(angle.cos(), -angle.sin());
    }

    public void rotateY(float cosAngle, float sinAngle) {
        float newX = z*sinAngle + x*cosAngle;
        float newZ = z*cosAngle - x*sinAngle;

        x = newX;
        z = newZ;
    }

    public void rotateZ(Angle angle) {
        rotateZ(angle.cos(), angle.sin());
    }

    public void subRotateZ(Angle angle) {
        rotateZ(angle.cos(), -angle.sin());
    }

    public void rotateZ(float cosAngle, float sinAngle) {
        float newX = x*cosAngle - y*sinAngle;
        float newY = x*sinAngle + y*cosAngle;

        x = newX;
        y = newY;
    }

    // TODO les objets transformables pourraient bénéficier d'une interface ; cela pourrait être l'occasion de factoriser certains comportements dans des default method
    public void add(Transform3D transform) {
        // L'ordre est important : il doit être l'opposé de subtract
        addRotation(transform.getRotation());
        add(transform.getLocation());
    }

    public void subtract(Transform3D transform) {
        // L'ordre est important : il doit être l'opposé de add
        subtract(transform.getLocation());
        subtractRotation(transform.getRotation());
    }

    public void addRotation(Rotation3D rotation) {
        rotateX(rotation.getAngleX());
        rotateZ(rotation.getAngleZ());
        rotateY(rotation.getAngleY());
    }

    public void subtractRotation(Rotation3D rotation) {
        subRotateY(rotation.getAngleY());
        subRotateZ(rotation.getAngleZ());
        subRotateX(rotation.getAngleX());
    }

    /**
     * Permet d'analyser l'angle entre 2 vecteurs. Notamment:
     * <p>
     * - si U.V < 0 alors teta > 90°
     * - si U.V = 0 alors teta = 90°
     * - si U.V > 0 alors teta < 90°
     */
    public float dotProduct(Vector3D v) {
        return x*v.x + y*v.y + z*v.z;
    }

    /**
     * Transform le vecteur courant en un vecteur, orthogonal aux 2 autres.
     *
     * @param u un vecteur
     * @param v un autre vecteur
     */
    public void setToCrossProduct(Vector3D u, Vector3D v) {
        // assign to local vars first in case u or v is 'this'
        float xTmp = u.y*v.z - u.z*v.y;
        float yTmp = u.z*v.x - u.x*v.z;
        float zTmp = u.x*v.y - u.y*v.x;

        this.x = xTmp;
        this.y = yTmp;
        this.z = zTmp;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3D v = (Vector3D) o;

        return equals(v.x, v.y, v.z);
    }

    public boolean equals(float x, float y, float z) {
        if (Float.compare(this.x, x) != 0) return false;
        if (Float.compare(this.y, y) != 0) return false;
        return Float.compare(this.z, z) == 0;
    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (z != +0.0f ? Float.floatToIntBits(z) : 0);
        return result;
    }
}
