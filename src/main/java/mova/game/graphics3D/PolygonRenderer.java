package mova.game.graphics3D;

import mova.game.graphics3D.math3D.Camera;
import mova.game.graphics3D.math3D.Polygon3D;
import mova.game.graphics3D.math3D.Transform3D;
import mova.game.graphics3D.math3D.ViewProjection;

import java.awt.*;

public abstract class PolygonRenderer {

    protected ScanConverter scanConverter;

    protected Transform3D cameraPosition;
    protected ViewProjection viewProjection;
    protected boolean clearViewEveryFrame;
    protected Polygon3D scratchPolygon;

    protected PolygonRenderer(Camera camera) {
        this(camera, true);
    }

    protected PolygonRenderer(Camera camera, boolean clearViewEveryFrame) {
        this.viewProjection = camera.getViewProjection();
        this.cameraPosition = camera.getPosition();
        this.clearViewEveryFrame = clearViewEveryFrame;
        init();
    }

    protected void init() {
        scratchPolygon = new Polygon3D();
        scanConverter = new ScanConverter(viewProjection);
    }


    public void startFrame(Graphics2D graphics) {
        if (clearViewEveryFrame) {
            graphics.setColor(Color.BLACK);
            graphics.fillRect(viewProjection.getLeftOffset(),
                    viewProjection.getTopOffset(),
                    viewProjection.getWidth(), viewProjection.getHeight());
        }
    }


    public void endFrame(Graphics2D g) {
        // do nothing, for now.
    }

    public boolean draw(Graphics2D graphics, Polygon3D polygon) {
        if (polygon.isFacing(cameraPosition.getLocation())) {
            scratchPolygon.setTo(polygon);
            scratchPolygon.subtract(cameraPosition);
            boolean visible = scratchPolygon.clip(-1);
            if (visible) {
                scratchPolygon.project(viewProjection);
                visible = scanConverter.convert(scratchPolygon);
                if (visible) {
                    drawCurrentPolygon(graphics, polygon);
                    return true;
                }
            }
        }
        return false;
    }


    protected abstract void drawCurrentPolygon(Graphics2D g, Polygon3D currentPolygon);

}
