package mova.game.graphics3D;

import mova.game.graphics3D.math3D.*;
import mova.game.graphics3D.texture.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferUShort;
import java.util.Arrays;
import java.util.HashMap;

public class FastTexturedPolygonRenderer extends PolygonRenderer {

    public static final int SCALE_BITS = 12;
    public static final int SCALE = 1 << SCALE_BITS;

    public static final int INTERP_SIZE_BITS = 4;
    public static final int INTERP_SIZE = 1 << INTERP_SIZE_BITS;

    protected Vector3D a = new Vector3D();
    protected Vector3D b = new Vector3D();
    protected Vector3D c = new Vector3D();
    protected Vector3D viewPos = new Vector3D();
    protected BufferedImage doubleBuffer;
    protected short[] doubleBufferData;
    protected HashMap<Class<? extends Texture>, ScanRenderer> scanRenderers;

    public FastTexturedPolygonRenderer(Camera camera) {
        this(camera, true);
    }

    public FastTexturedPolygonRenderer(Camera camera, boolean clearViewEveryFrame) {
        super(camera, clearViewEveryFrame);
    }

    @Override
    protected void init() {
        scratchPolygon = new TexturedPolygon3D();
        scanConverter = new ScanConverter(viewProjection);

        // create renders for each texture (HotSpot optimization)
        scanRenderers = new HashMap<>();
        scanRenderers.put(PlainColorTexture.class, new PlainColorTextureRenderer());
//        scanRenderers.put(PowerOf2Texture.class, new Method0());
//        scanRenderers.put(PowerOf2Texture.class, new Method1());
//        scanRenderers.put(PowerOf2Texture.class, new Method2());
//        scanRenderers.put(PowerOf2Texture.class, new Method3());
        scanRenderers.put(PowerOf2Texture.class, new PowerOf2TextureRenderer());
        scanRenderers.put(ShadedTexture.class, new ShadedTextureRenderer());
        scanRenderers.put(ShadedSurface.class, new ShadedSurfaceRenderer());
    }


    @Override
    public void startFrame(Graphics2D g) {
        // initialize buffer
        if (doubleBuffer == null || doubleBuffer.getWidth() != viewProjection.getWidth() || doubleBuffer.getHeight() != viewProjection.getHeight()) {
            doubleBuffer = new BufferedImage(viewProjection.getWidth(), viewProjection.getHeight(), BufferedImage.TYPE_USHORT_565_RGB);

            // Note: Not possible since we use 16bits images
//            doubleBuffer = g.getDeviceConfiguration().createCompatibleImage(viewProjection.getWidth(), viewProjection.getHeight());

            DataBuffer dest = doubleBuffer.getRaster().getDataBuffer();
            doubleBufferData = ((DataBufferUShort) dest).getData();
        }

        // clear view
        if (clearViewEveryFrame) {
            // TODO comment définir la couleur de fond correctement?
            Arrays.fill(doubleBufferData, (short) 0); // Autre couleur possible: 0x6666ff
        }
    }

    @Override
    public void endFrame(Graphics2D g) {
        // draw the double buffer onto the screen
        g.drawImage(doubleBuffer, viewProjection.getLeftOffset(), viewProjection.getTopOffset(), null);
    }

    protected void drawCurrentPolygon(Graphics2D g, Polygon3D currentPolygon) {
        if (!(currentPolygon instanceof TexturedPolygon3D)) {
            // not a textured polygon - return
            return;
        }

        TexturedPolygon3D poly = (TexturedPolygon3D) scratchPolygon;
        Texture texture = poly.getTexture();
        ScanRenderer scanRenderer = scanRenderers.get(texture.getClass());
        scanRenderer.setTexture(texture);
        Rectangle3D textureBounds = poly.getTextureBounds();

        a.setToCrossProduct(textureBounds.getDirectionV(), textureBounds.getOrigin());
        b.setToCrossProduct(textureBounds.getOrigin(), textureBounds.getDirectionU());
        c.setToCrossProduct(textureBounds.getDirectionU(), textureBounds.getDirectionV());

        int y = scanConverter.getTopBoundary();

        viewPos.setY(viewProjection.convertFromScreenYToViewY(y));
        viewPos.setZ(-viewProjection.getDistance());

        while (y <= scanConverter.getBottomBoundary()) {
            ScanConverter.Scan scan = scanConverter.getScan(y);

            if (scan.isValid()) {
                viewPos.setX(viewProjection.convertFromScreenXToViewX(scan.left));
                int offset = (y - viewProjection.getTopOffset()) * viewProjection.getWidth() + (scan.left - viewProjection.getLeftOffset());

                scanRenderer.render(offset, scan.left, scan.right);
            }
            y++;
            viewPos.setY(viewPos.getY() - 1);
        }
    }

    public abstract static class ScanRenderer {

        protected Texture currentTexture;

        public void setTexture(Texture texture) {
            this.currentTexture = texture;
        }

        public abstract void render(int offset, int left, int right);

    }

    public class PlainColorTextureRenderer extends ScanRenderer {

        public void render(int offset, int left, int right) {
            PlainColorTexture texture = (PlainColorTexture) currentTexture;
            for (int x = left; x <= right; x++) {
                doubleBufferData[offset++] = texture.getColor(0, 0);
            }
        }
    }

    //================================================
    // FASTEST METHOD: no texture (for comparison)
    //================================================
    public class Method0 extends ScanRenderer {

        public void render(int offset, int left, int right) {
            for (int x = left; x <= right; x++) {
                doubleBufferData[offset++] = (short) 0x0007;
            }
        }
    }


    //================================================
    // METHOD 1: access pixel buffers directly
    // and use textures sizes that are a power of 2
    //================================================
    public class Method1 extends ScanRenderer {

        public void render(int offset, int left, int right) {
            for (int x = left; x <= right; x++) {
                int tx = (int) (a.dotProduct(viewPos) / c.dotProduct(viewPos));
                int ty = (int) (b.dotProduct(viewPos) / c.dotProduct(viewPos));
                doubleBufferData[offset++] = currentTexture.getColor(tx, ty);
                viewPos.setX(viewPos.getX() + 1);
            }
        }
    }


    //================================================
    // METHOD 2: avoid redundant calculations
    //================================================
    public class Method2 extends ScanRenderer {

        public void render(int offset, int left, int right) {
            float u = a.dotProduct(viewPos);
            float v = b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = a.getX();
            float dv = b.getX();
            float dz = c.getX();
            for (int x = left; x <= right; x++) {
                doubleBufferData[offset++] = currentTexture.getColor((int) (u / z), (int) (v / z));
                u += du;
                v += dv;
                z += dz;
            }
        }
    }


    //================================================
    // METHOD 3: use ints instead of floats
    //================================================
    public class Method3 extends ScanRenderer {

        public void render(int offset, int left, int right) {
            int u = (int) (SCALE * a.dotProduct(viewPos));
            int v = (int) (SCALE * b.dotProduct(viewPos));
            int z = (int) (SCALE * c.dotProduct(viewPos));
            int du = (int) (SCALE * a.getX());
            int dv = (int) (SCALE * b.getX());
            int dz = (int) (SCALE * c.getX());
            for (int x = left; x <= right; x++) {
                doubleBufferData[offset++] = currentTexture.getColor(u / z, v / z);
                u += du;
                v += dv;
                z += dz;
            }
        }
    }


    //================================================
    // METHOD 4: reduce the number of divides
    // (interpolate every 16 pixels)
    // Also, apply a VM optimization by referring to
    // the texture's class rather than it's parent class.
    //================================================

    // the following three ScanRenderers are the same, but refer
    // to textures explicitly as either a PowerOf2Texture, a
    // ShadedTexture, or a ShadedSurface.
    // This allows HotSpot to do some inlining of the textures'
    // getColor() method, which significantly increases
    // performance.

    public class PowerOf2TextureRenderer extends ScanRenderer {

        public void render(int offset, int left, int right) {
            PowerOf2Texture texture = (PowerOf2Texture) currentTexture;
            float u = SCALE * a.dotProduct(viewPos);
            float v = SCALE * b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = INTERP_SIZE * SCALE * a.getX();
            float dv = INTERP_SIZE * SCALE * b.getX();
            float dz = INTERP_SIZE * c.getX();
            int nextTx = (int) (u / z);
            int nextTy = (int) (v / z);
            int x = left;
            while (x <= right) {
                int tx = nextTx;
                int ty = nextTy;
                int maxLength = right - x + 1;
                if (maxLength > INTERP_SIZE) {
                    u += du;
                    v += dv;
                    z += dz;
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) >> INTERP_SIZE_BITS;
                    int dty = (nextTy - ty) >> INTERP_SIZE_BITS;
                    int endOffset = offset + INTERP_SIZE;
                    while (offset < endOffset) {
                        doubleBufferData[offset++] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        tx += dtx;
                        ty += dty;
                    }
                    x += INTERP_SIZE;
                } else {
                    // variable interpolation size
                    int interpSize = maxLength;
                    u += interpSize * SCALE * a.getX();
                    v += interpSize * SCALE * b.getX();
                    z += interpSize * c.getX();
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) / interpSize;
                    int dty = (nextTy - ty) / interpSize;
                    int endOffset = offset + interpSize;
                    while (offset < endOffset) {
                        doubleBufferData[offset++] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        tx += dtx;
                        ty += dty;
                    }
                    x += interpSize;
                }

            }
        }
    }


    public class ShadedTextureRenderer extends ScanRenderer {

        public void render(int offset, int left, int right) {
            ShadedTexture texture = (ShadedTexture) currentTexture;
            float u = SCALE * a.dotProduct(viewPos);
            float v = SCALE * b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = INTERP_SIZE * SCALE * a.getX();
            float dv = INTERP_SIZE * SCALE * b.getX();
            float dz = INTERP_SIZE * c.getX();
            int nextTx = (int) (u / z);
            int nextTy = (int) (v / z);
            int x = left;
            while (x <= right) {
                int tx = nextTx;
                int ty = nextTy;
                int maxLength = right - x + 1;
                if (maxLength > INTERP_SIZE) {
                    u += du;
                    v += dv;
                    z += dz;
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) >> INTERP_SIZE_BITS;
                    int dty = (nextTy - ty) >> INTERP_SIZE_BITS;
                    int endOffset = offset + INTERP_SIZE;
                    while (offset < endOffset) {
                        doubleBufferData[offset++] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        tx += dtx;
                        ty += dty;
                    }
                    x += INTERP_SIZE;
                } else {
                    // variable interpolation size
                    int interpSize = maxLength;
                    u += interpSize * SCALE * a.getX();
                    v += interpSize * SCALE * b.getX();
                    z += interpSize * c.getX();
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) / interpSize;
                    int dty = (nextTy - ty) / interpSize;
                    int endOffset = offset + interpSize;
                    while (offset < endOffset) {
                        doubleBufferData[offset++] = texture.getColor(tx >> SCALE_BITS, ty >> SCALE_BITS);
                        tx += dtx;
                        ty += dty;
                    }
                    x += interpSize;
                }

            }
        }
    }

    public class ShadedSurfaceRenderer extends ScanRenderer {

        public int checkBounds(int vScaled, int bounds) {
            int v = vScaled >> SCALE_BITS;
            if (v < 0) {
                vScaled = 0;
            } else if (v >= bounds) {
                vScaled = (bounds - 1) << SCALE_BITS;
            }
            return vScaled;
        }

        public void render(int offset, int left, int right) {
            ShadedSurface texture =
                    (ShadedSurface) currentTexture;
            float u = SCALE * a.dotProduct(viewPos);
            float v = SCALE * b.dotProduct(viewPos);
            float z = c.dotProduct(viewPos);
            float du = INTERP_SIZE * SCALE * a.getX();
            float dv = INTERP_SIZE * SCALE * b.getX();
            float dz = INTERP_SIZE * c.getX();
            int nextTx = (int) (u / z);
            int nextTy = (int) (v / z);
            int x = left;
            while (x <= right) {
                int tx = nextTx;
                int ty = nextTy;
                int maxLength = right - x + 1;
                if (maxLength > INTERP_SIZE) {
                    u += du;
                    v += dv;
                    z += dz;
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);
                    int dtx = (nextTx - tx) >> INTERP_SIZE_BITS;
                    int dty = (nextTy - ty) >> INTERP_SIZE_BITS;
                    int endOffset = offset + INTERP_SIZE;
                    while (offset < endOffset) {
                        doubleBufferData[offset++] =
                                texture.getColor(
                                        tx >> SCALE_BITS, ty >> SCALE_BITS);
                        tx += dtx;
                        ty += dty;
                    }
                    x += INTERP_SIZE;
                } else {
                    // variable interpolation size
                    int interpSize = maxLength;
                    u += interpSize * SCALE * a.getX();
                    v += interpSize * SCALE * b.getX();
                    z += interpSize * c.getX();
                    nextTx = (int) (u / z);
                    nextTy = (int) (v / z);

                    // make sure tx, ty, nextTx, and nextTy are
                    // all within bounds
                    tx = checkBounds(tx, texture.getWidth());
                    ty = checkBounds(ty, texture.getHeight());
                    nextTx = checkBounds(nextTx, texture.getWidth());
                    nextTy = checkBounds(nextTy, texture.getHeight());

                    int dtx = (nextTx - tx) / interpSize;
                    int dty = (nextTy - ty) / interpSize;
                    int endOffset = offset + interpSize;
                    while (offset < endOffset) {
                        doubleBufferData[offset++] =
                                texture.getColor(
                                        tx >> SCALE_BITS, ty >> SCALE_BITS);
                        tx += dtx;
                        ty += dty;
                    }
                    x += interpSize;
                }
            }

        }
    }


}
