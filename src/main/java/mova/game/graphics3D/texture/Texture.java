package mova.game.graphics3D.texture;

import mova.game.graphics.ImageUtils;
import mova.lang.MathUtils;

import java.awt.image.*;

public abstract class Texture {

    protected int width;
    protected int height;

    protected Texture(int width, int height) {
        this.width = width;
        this.height = height;
    }


    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }


    public abstract short getColor(int x, int y);


    public static Texture createTexture(BufferedImage image) {
        return createTexture(image, false);
    }


    public static Texture createTexture(BufferedImage image, boolean shaded) {
        int width = image.getWidth();
        int height = image.getHeight();

        if (MathUtils.isNotPowerOfTwo(width) || MathUtils.isNotPowerOfTwo(height)) {
            throw new IllegalArgumentException("Size of texture must be a power of two.");
        }

        if (shaded) {
            // convert image to an indexed image
            image = ImageUtils.convertTo(image, BufferedImage.TYPE_BYTE_INDEXED);
            DataBuffer dest = image.getRaster().getDataBuffer();
            return new ShadedTexture(((DataBufferByte) dest).getData(), MathUtils.countbits(width - 1), MathUtils.countbits(height - 1), (IndexColorModel) image.getColorModel());
        } else {
            // convert image to an 16-bit image
            image = ImageUtils.convertTo(image, BufferedImage.TYPE_USHORT_565_RGB);

            DataBuffer dest = image.getRaster().getDataBuffer();
            return new PowerOf2Texture(((DataBufferUShort) dest).getData(), MathUtils.countbits(width - 1), MathUtils.countbits(height - 1));
        }
    }


}
