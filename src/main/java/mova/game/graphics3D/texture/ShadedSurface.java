package mova.game.graphics3D.texture;

import mova.game.graphics3D.math3D.PointLight3D;
import mova.game.graphics3D.math3D.Rectangle3D;
import mova.game.graphics3D.math3D.TexturedPolygon3D;
import mova.game.graphics3D.math3D.Vector3D;

import java.lang.ref.SoftReference;
import java.util.List;

public final class ShadedSurface extends Texture {

    public static final int SURFACE_BORDER_SIZE = 1;

    public static final int SHADE_RES_BITS = 4;
    public static final int SHADE_RES = 1 << SHADE_RES_BITS;
    public static final int SHADE_RES_MASK = SHADE_RES - 1;
    public static final int SHADE_RES_SQ = SHADE_RES * SHADE_RES;
    public static final int SHADE_RES_SQ_BITS = SHADE_RES_BITS * 2;

    private short[] buffer;
    private SoftReference<short[]> bufferReference;
    private boolean dirty;
    private ShadedTexture sourceTexture;
    private final Rectangle3D sourceTextureBounds;
    private Rectangle3D surfaceBounds;
    private byte[] shadeMap;
    private int shadeMapWidth;

    // for incrementally calculating shade values
    private int shadeValue;
    private int shadeValueInc;


    public ShadedSurface(int width, int height) {
        this(null, width, height);
    }


    public ShadedSurface(short[] buffer, int width, int height) {
        super(width, height);
        this.buffer = buffer;
        bufferReference = new SoftReference<>(buffer);
        sourceTextureBounds = new Rectangle3D();
        dirty = true;
    }


    public static void createShadedSurface(TexturedPolygon3D poly, ShadedTexture texture, List<PointLight3D> lights, float ambientLightIntensity) {
        // create the texture bounds
        Vector3D origin = poly.getVertex(0);
        Vector3D dv = new Vector3D(poly.getVertex(1));
        dv.subtract(origin);
        Vector3D du = new Vector3D();
        du.setToCrossProduct(poly.getNormal(), dv);
        Rectangle3D bounds = new Rectangle3D(origin, du, dv, texture.getWidth(), texture.getHeight());

        createShadedSurface(poly, texture, bounds, lights, ambientLightIntensity);
    }


    public static void createShadedSurface(TexturedPolygon3D poly, ShadedTexture texture, Rectangle3D textureBounds, List<PointLight3D> lights, float ambientLightIntensity) {

        // create the surface bounds
        poly.setTexture(texture, textureBounds);
        Rectangle3D surfaceBounds = poly.calcBoundingRectangle();

        // give the surfaceBounds a border to correct for
        // slight errors when texture mapping
        Vector3D du = new Vector3D(surfaceBounds.getDirectionU());
        Vector3D dv = new Vector3D(surfaceBounds.getDirectionV());
        du.multiply(SURFACE_BORDER_SIZE);
        dv.multiply(SURFACE_BORDER_SIZE);
        surfaceBounds.getOrigin().subtract(du);
        surfaceBounds.getOrigin().subtract(dv);
        int width = (int) Math.ceil(surfaceBounds.getWidth() + SURFACE_BORDER_SIZE * 2);
        int height = (int) Math.ceil(surfaceBounds.getHeight() + SURFACE_BORDER_SIZE * 2);
        surfaceBounds.setWidth(width);
        surfaceBounds.setHeight(height);

        // create the shaded surface texture
        ShadedSurface surface = new ShadedSurface(width, height);
        surface.setTexture(texture, textureBounds);
        surface.setSurfaceBounds(surfaceBounds);

        // create the surface's shade map
        surface.buildShadeMap(lights, ambientLightIntensity);

        // set the polygon's surface
        poly.setTexture(surface, surfaceBounds);
    }


    public short getColor(int x, int y) {
        return buffer[x + y * width];
    }


    public short getColorChecked(int x, int y) {
        if (x < 0) x = 0;
        else if (x >= width) x = width - 1;

        if (y < 0) y = 0;
        else if (y >= height) y = height - 1;

        return getColor(x, y);
    }


    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }


    public boolean isDirty() {
        return dirty;
    }


    private void newSurface(int width, int height) {
        buffer = new short[width * height];
        bufferReference = new SoftReference<>(buffer);
    }


    public void clearSurface() {
        buffer = null;
    }


    public boolean isCleared() {
        return buffer == null;
    }


    public boolean retrieveSurface() {
        if (buffer == null) buffer = bufferReference.get();
        return buffer != null;
    }


    public void setTexture(ShadedTexture texture) {
        sourceTexture = texture;
        sourceTextureBounds.setWidth(texture.getWidth());
        sourceTextureBounds.setHeight(texture.getHeight());
    }


    public void setTexture(ShadedTexture texture, Rectangle3D bounds) {
        setTexture(texture);
        sourceTextureBounds.setTo(bounds);
    }

    public void setSurfaceBounds(Rectangle3D surfaceBounds) {
        this.surfaceBounds = surfaceBounds;
    }

    public Rectangle3D getSurfaceBounds() {
        return surfaceBounds;
    }

    public void buildSurface() {
        if (retrieveSurface()) return;

        int width = (int) surfaceBounds.getWidth();
        int height = (int) surfaceBounds.getHeight();

        // create a new surface (buffer)
        newSurface(width, height);

        // builds the surface.
        // assume surface bounds and texture bounds are aligned
        // (possibly with different origins)
        Vector3D origin = sourceTextureBounds.getOrigin();
        Vector3D directionU = sourceTextureBounds.getDirectionU();
        Vector3D directionV = sourceTextureBounds.getDirectionV();

        Vector3D d = new Vector3D(surfaceBounds.getOrigin());
        d.subtract(origin);
        int startU = (int) (d.dotProduct(directionU) - SURFACE_BORDER_SIZE);
        int startV = (int) (d.dotProduct(directionV) - SURFACE_BORDER_SIZE);
        int offset = 0;
        int shadeMapOffsetU = SHADE_RES - SURFACE_BORDER_SIZE - startU;
        int shadeMapOffsetV = SHADE_RES - SURFACE_BORDER_SIZE - startV;

        for (int v = startV; v < startV + height; v++) {
            sourceTexture.setCurrRow(v);
            int u = startU;
            int amount = SURFACE_BORDER_SIZE;
            while (u < startU + width) {
                getInterpolatedShade(u + shadeMapOffsetU, v + shadeMapOffsetV);

                // keep drawing until we need to recalculate
                // the interpolated shade. (every SHADE_RES pixels)
                int endU = Math.min(startU + width, u + amount);
                while (u < endU) {
                    buffer[offset++] = sourceTexture.getColorCurrRow(u, shadeValue >> SHADE_RES_SQ_BITS);
                    shadeValue += shadeValueInc;
                    u++;
                }
                amount = SHADE_RES;
            }
        }
    }


    public int getInterpolatedShade(int u, int v) {

        int fracU = u & SHADE_RES_MASK;
        int fracV = v & SHADE_RES_MASK;

        int offset = (u >> SHADE_RES_BITS) + ((v >> SHADE_RES_BITS) * shadeMapWidth);

        int shade00 = (SHADE_RES - fracV) * shadeMap[offset];
        int shade01 = fracV * shadeMap[offset + shadeMapWidth];
        int shade10 = (SHADE_RES - fracV) * shadeMap[offset + 1];
        int shade11 = fracV * shadeMap[offset + shadeMapWidth + 1];

        shadeValue = SHADE_RES_SQ / 2 + (SHADE_RES - fracU) * shade00 + (SHADE_RES - fracU) * shade01 + fracU * shade10 + fracU * shade11;

        // the value to increment as u increments
        shadeValueInc = -shade00 - shade01 + shade10 + shade11;

        return shadeValue >> SHADE_RES_SQ_BITS;
    }


    public int getShade(int u, int v) {
        return shadeMap[u + v * shadeMapWidth];
    }

    public void buildShadeMap(List<PointLight3D> pointLights, float ambientLightIntensity) {

        Vector3D surfaceNormal = surfaceBounds.getNormal();

        int polyWidth = (int) surfaceBounds.getWidth() - SURFACE_BORDER_SIZE * 2;
        int polyHeight = (int) surfaceBounds.getHeight() - SURFACE_BORDER_SIZE * 2;
        // assume SURFACE_BORDER_SIZE is <= SHADE_RES
        shadeMapWidth = polyWidth / SHADE_RES + 4;
        int shadeMapHeight = polyHeight / SHADE_RES + 4;
        shadeMap = new byte[shadeMapWidth * shadeMapHeight];

        // calculate the shade map origin
        Vector3D origin = new Vector3D(surfaceBounds.getOrigin());
        Vector3D du = new Vector3D(surfaceBounds.getDirectionU());
        Vector3D dv = new Vector3D(surfaceBounds.getDirectionV());
        du.multiply(SHADE_RES - (float) SURFACE_BORDER_SIZE);
        dv.multiply(SHADE_RES - (float) SURFACE_BORDER_SIZE);
        origin.subtract(du);
        origin.subtract(dv);

        // calculate the shade for each sample point.
        Vector3D point = new Vector3D();
        du.setTo(surfaceBounds.getDirectionU());
        dv.setTo(surfaceBounds.getDirectionV());
        du.multiply(SHADE_RES);
        dv.multiply(SHADE_RES);
        for (int v = 0; v < shadeMapHeight; v++) {
            point.setTo(origin);
            for (int u = 0; u < shadeMapWidth; u++) {
                shadeMap[u + v * shadeMapWidth] = calcShade(surfaceNormal, point, pointLights, ambientLightIntensity);
                point.add(du);
            }
            origin.add(dv);
        }
    }


    private byte calcShade(Vector3D normal, Vector3D point, List<PointLight3D> pointLights, float ambientLightIntensity) {
        float intensity = 0;
        Vector3D directionToLight = new Vector3D();

        for (PointLight3D light : pointLights) {
            directionToLight.setTo(light);
            directionToLight.subtract(point);

            float distance = directionToLight.length();
            directionToLight.normalize();
            float lightIntensity = light.getIntensity(distance) * directionToLight.dotProduct(normal);
            lightIntensity = Math.min(lightIntensity, 1);
            lightIntensity = Math.max(lightIntensity, 0);
            intensity += lightIntensity;
        }

        intensity = Math.min(intensity, 1);
        intensity = Math.max(intensity, 0);

        intensity += ambientLightIntensity;

        intensity = Math.min(intensity, 1);
        intensity = Math.max(intensity, 0);
        int level = Math.round(intensity * ShadedTexture.MAX_LEVEL);
        return (byte) level;
    }
}
