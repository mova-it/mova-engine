package mova.game.graphics3D.texture;

public final class PowerOf2Texture extends Texture {

    private final short[] buffer;
    private final int widthBits;
    private final int widthMask;
    private final int heightMask;


    public PowerOf2Texture(short[] buffer, int widthBits, int heightBits) {
        super(1 << widthBits, 1 << heightBits);
        this.buffer = buffer;
        this.widthBits = widthBits;
        this.widthMask = getWidth() - 1;
        this.heightMask = getHeight() - 1;
    }

    public short getColor(int x, int y) {
        return buffer[(x & widthMask) + ((y & heightMask) << widthBits)];
    }

}
