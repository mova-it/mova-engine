package mova.game.graphics3D.texture;


import java.awt.*;

public class PlainColorTexture extends Texture {

    private final short color;

    public PlainColorTexture(Color color) {
        this((short) (((color.getRed() >> 3) << 11) | ((color.getGreen() >> 2) << 5) | (color.getBlue() >> 3)));
    }

    public PlainColorTexture(short color) {
        super(0, 0);

        this.color = color;
    }

    @Override
    public short getColor(int x, int y) {
        return color;
    }

    public short getColor() {
        return color;
    }
}
