package mova.game.graphics3D;

import mova.lang.MathUtils;
import mova.game.graphics3D.math3D.Polygon3D;
import mova.game.graphics3D.math3D.Vector3D;
import mova.game.graphics3D.math3D.ViewProjection;

public class ScanConverter {

    private static final int SCALE_BITS = 16;
    private static final int SCALE = 1 << SCALE_BITS;
    private static final int SCALE_MASK = SCALE - 1;

    protected ViewProjection view;
    protected Scan[] scans;
    protected int top;
    protected int bottom;

    public ScanConverter(ViewProjection view) {
        this.view = view;
    }

    public int getTopBoundary() {
        return top;
    }

    public int getBottomBoundary() {
        return bottom;
    }

    public Scan getScan(int y) {
        return scans[y];
    }

    protected void ensureCapacity() {
        int height = view.getTopOffset() + view.getHeight();
        if (scans == null || scans.length != height) {
            scans = new Scan[height];
            for (int i = 0; i < height; i++) {
                scans[i] = new Scan();
            }
            // set top and bottom so clearCurrentScan clears all
            top = 0;
            bottom = height - 1;
        }

    }

    private void clearCurrentScan() {
        for (int i = top; i <= bottom; i++) {
            scans[i].clear();
        }
        top = Integer.MAX_VALUE;
        bottom = Integer.MIN_VALUE;
    }

    public boolean convert(Polygon3D polygon) {

        ensureCapacity();
        clearCurrentScan();

        int minX = view.getLeftOffset();
        int maxX = view.getLeftOffset() + view.getWidth() - 1;
        int minY = view.getTopOffset();
        int maxY = view.getTopOffset() + view.getHeight() - 1;

        for (int i = 0; i < polygon.getNumVertices(); i++) {
            Vector3D v1 = polygon.getVertex(i);
            Vector3D v2 = polygon.getVertexNextTo(i);

            // On s'assure que v1.y < v2.y, si ce n'est pas le cas on switch les vecteurs
            if (v1.getY() > v2.getY()) {
                Vector3D temp = v1;
                v1 = v2;
                v2 = temp;
            }

            // Calcul de dy
            float dy = v2.getY() - v1.getY();

            // On ignore les lignes horizontales
            if (dy != 0) setBoundaries(minX, maxX, minY, maxY, v1, v2, dy);
        }

        return checkVisibility();
    }

    private void setBoundaries(int minX, int maxX, int minY, int maxY, Vector3D v1, Vector3D v2, float dy) {
        // Au vu des opérations précédentes, v1 est forcément plus haut que v2 (le repère est inversé sur l'écran)
        // Donc startY est la limite haute (de l'écran) de la différence entre les 2 vecteurs et endY la limite basse
        int startY = Math.max(MathUtils.ceil(v1.getY()), minY);
        int endY = Math.min(MathUtils.ceil(v2.getY()) - 1, maxY);

        // On ajuste les limites hautes et basses globales du scan
        top = Math.min(top, startY);
        bottom = Math.max(bottom, endY);

        // Calcul de dx
        float dx = v2.getX() - v1.getX();

        // cas special: la ligne est verticale
        if (dx == 0) {
            int x = MathUtils.ceil(v1.getX());
            // On borne x dans la vue
            x = Math.min(maxX + 1, Math.max(x, minX));
            for (int y = startY; y <= endY; y++) {
                scans[y].setBoundary(x);
            }
        } else {
            // scan-convert this edge (line equation)
            float gradient = dx / dy;

            // trim start of line
            startY = trimLineStart(minX, maxX, v1, startY, endY, gradient);

            if (startY > endY) return;

            // trim back of line
            endY = trimLineEnd(minX, maxX, v1, startY, endY, gradient);

            if (startY > endY) return;

            // line equation using integers
            // On calcule le premier x en fixed point float (+ SCALE_MASK est l'équivalent de Math.ceil)
            int xScaled = (int) (SCALE * (v1.getX() + (startY - v1.getY()) * gradient)) + SCALE_MASK;
            // On convertit le gradient en fixed point float
            int dxScaled = (int) (gradient * SCALE);

            // Pour chaque y
            for (int y = startY; y <= endY; y++) {
                // On récupère la vraie valeur de x
                scans[y].setBoundary(xScaled >> SCALE_BITS);
                // On déduit le prochain x à partir du gradient (si y = f(x), y + 1 = f(x + gradient))
                xScaled += dxScaled;
            }
        }
    }

    private int trimLineEnd(int minX, int maxX, Vector3D v1, int startY, int endY, float gradient) {
        float endX = v1.getX() + (endY - v1.getY()) * gradient;
        if (endX < minX) {
            int yInt = MathUtils.ceil(v1.getY() + (minX - v1.getX()) / gradient);
            yInt = Math.max(yInt, startY);
            while (endY >= yInt) {
                scans[endY].setBoundary(minX);
                endY--;
            }
        } else if (endX > maxX) {
            int yInt = MathUtils.ceil(v1.getY() + (maxX - v1.getX()) / gradient);
            yInt = Math.max(yInt, startY);
            while (endY >= yInt) {
                scans[endY].setBoundary(maxX + 1);
                endY--;
            }
        }
        return endY;
    }

    private int trimLineStart(int minX, int maxX, Vector3D v1, int startY, int endY, float gradient) {
        float startX = v1.getX() + (startY - v1.getY()) * gradient;
        if (startX < minX) {
            int yInt = (int) (v1.getY() + (minX - v1.getX()) / gradient);
            yInt = Math.min(yInt, endY);
            while (startY <= yInt) {
                scans[startY].setBoundary(minX);
                startY++;
            }
        } else if (startX > maxX) {
            int yInt = (int) (v1.getY() + (maxX - v1.getX()) / gradient);
            yInt = Math.min(yInt, endY);
            while (startY <= yInt) {
                scans[startY].setBoundary(maxX + 1);
                startY++;
            }
        }
        return startY;
    }

    private boolean checkVisibility() {
        // check if visible (any valid scans)
        for (int i = top; i <= bottom; i++) {
            if (scans[i].isValid()) {
                return true;
            }
        }
        return false;
    }

    public static class Scan {

        int left;
        int right;

        public void setBoundary(int x) {
            if (x < left) left = x;

            if (x - 1 > right) right = x - 1;
        }

        public void clear() {
            left = Integer.MAX_VALUE;
            right = Integer.MIN_VALUE;
        }

        public boolean isValid() {
            return left <= right;
        }

        public void setTo(int left, int right) {
            this.left = left;
            this.right = right;
        }

        public boolean equals(int left, int right) {
            return this.left == left && this.right == right;
        }
    }

}
