package mova.game.graphics3D;

import mova.game.graphics.ImageUtils;
import mova.game.graphics3D.math3D.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class SimpleTexturedPolygonRenderer extends PolygonRenderer {
    protected Vector3D a = new Vector3D();
    protected Vector3D b = new Vector3D();
    protected Vector3D c = new Vector3D();
    protected Vector3D viewPos = new Vector3D();
    protected Rectangle3D textureBounds = new Rectangle3D();
    protected BufferedImage texture;

    public SimpleTexturedPolygonRenderer(Camera camera, String textureFile) {
        super(camera);
        // TODO s'assurer d'utiliser le imageCache à la place
        try {
            texture = ImageUtils.loadImage(textureFile);
        } catch (IOException e) {
            throw new IllegalStateException("Impossible de charger la texture", e);
        }
    }


    protected void drawCurrentPolygon(Graphics2D g, Polygon3D currentPolygon) {

        // Calculate texture bounds.
        // Ideally texture bounds are pre-calculated and stored
        // with the polygon. Coordinates are computed here for
        // demonstration purposes.
        Vector3D textureOrigin = textureBounds.getOrigin();
        Vector3D textureDirectionU = textureBounds.getDirectionU();
        Vector3D textureDirectionV = textureBounds.getDirectionV();

        textureOrigin.setTo(currentPolygon.getVertex(0));

        textureDirectionU.setTo(currentPolygon.getVertex(3));
        textureDirectionU.subtract(textureOrigin);
        textureDirectionU.normalize();

        textureDirectionV.setTo(currentPolygon.getVertex(1));
        textureDirectionV.subtract(textureOrigin);
        textureDirectionV.normalize();

        // transform the texture bounds
        textureBounds.subtract(cameraPosition);

        // start texture-mapping calculations
        a.setToCrossProduct(textureBounds.getDirectionV(), textureBounds.getOrigin());
        b.setToCrossProduct(textureBounds.getOrigin(), textureBounds.getDirectionU());
        c.setToCrossProduct(textureBounds.getDirectionU(), textureBounds.getDirectionV());

        int y = scanConverter.getTopBoundary();
        viewPos.setZ(-viewProjection.getDistance());

        while (y <= scanConverter.getBottomBoundary()) {
            ScanConverter.Scan scan = scanConverter.getScan(y);

            if (scan.isValid()) {
                viewPos.setY(viewProjection.convertFromScreenYToViewY(y));
                for (int x = scan.left; x <= scan.right; x++) {
                    viewPos.setX(viewProjection.convertFromScreenXToViewX(x));

                    // compute the texture location
                    int tx = (int) (a.dotProduct(viewPos) / c.dotProduct(viewPos));
                    int ty = (int) (b.dotProduct(viewPos) / c.dotProduct(viewPos));

                    // get the color to draw
                    try {
                        int color = texture.getRGB(tx, ty);

                        g.setColor(new Color(color));
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        g.setColor(Color.red);
                    }

                    // draw the pixel
                    g.drawLine(x, y, x, y);
                }
            }
            y++;
        }
    }

}
