package mova.game.core;

// TODO en attendant de savoir quoi faire dans certain cas plutot que de propager une exception, on crash le jeu
//  On pourra toujours mettre un comportement par défaut à la place pour éviter le crash
public class FatalError extends Error {

    public FatalError(String message) {
        super(message);
    }

    public FatalError(String message, Throwable cause) {
        super(message, cause);
    }
}
