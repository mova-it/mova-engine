package mova.game.core;

import mova.game.core.ecs.GameObject;
import mova.game.core.ecs.container.GameObjectContainer;
import mova.game.graphics.MovaGraphics;
import mova.game.view.Camera;
import mova.game.view.View;

public interface Scene {

    GameObjectContainer getGameObjects();

    default void start() {
        getGameObjects().forEach(GameObject::start);
    }

    default void update(long dt) {
        getGameObjects().forEach(gameObject -> {
            if (gameObject.isActive()) gameObject.update(dt);
        });
    }

    default void draw(MovaGraphics movaGraphics) {
        getGameObjects().forEach(GameObjectContainer.Z_INDEX_COMPARATOR, gameObject -> {
            if (gameObject.isVisible()) gameObject.draw(movaGraphics);
        });
    }

    default void release() {
        getGameObjects().clear();
    }

    default View getView() {
        return null;
    }

    default Camera getCamera() {
        return null;
    }
}
