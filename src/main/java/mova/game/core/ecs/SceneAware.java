package mova.game.core.ecs;

import mova.game.core.Scene;

public interface SceneAware {

    boolean hasScene();

    Scene getScene();

    void setScene(Scene currentScene);
}
