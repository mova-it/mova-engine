package mova.game.core.ecs;

import mova.game.core.Copyable;
import mova.game.graphics.MovaGraphics;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public interface GameObject extends SceneAware, Copyable<GameObject> {

    String getName();

    List<GameComponent> getComponents();

    default boolean isVisible() {
        return true;
    }

    default boolean isActive() {
        return true;
    }

    default void start() {
        for (GameComponent component : getComponents()) {
            component.start();
        }
    }

    default void update(long dt) {
        for (GameComponent component : getComponents()) {
            component.update(dt);
        }
    }

    default void draw(MovaGraphics movaGraphics) {
        for (GameComponent component : getComponents()) {
            component.draw(movaGraphics);
        }
    }

    // TODO est ce que ce serait possible de faire une méthode getComponent qui retourne un Component avec generic comme ModelComponent<ShipModel>
    default <T extends GameComponent> T getComponent(Class<T> componentClass) {
        for (GameComponent component : getComponents()) {
            if (componentClass.isAssignableFrom(component.getClass())) {
                return componentClass.cast(component);
            }
        }

        return null;
    }

    default <T extends GameComponent> void applyIfPresent(Class<T> componentClass, Consumer<T> consumer) {
        Optional.ofNullable(getComponent(componentClass)).ifPresent(consumer);
    }

    default <T extends GameComponent> boolean hasComponent(Class<T> componentClass) {
        return getComponent(componentClass) != null;
    }

    default <T extends GameComponent> void removeComponent(Class<T> componentClass) {
        for (int i = getComponents().size() - 1; i >= 0; i--) {
            GameComponent component = getComponents().get(i);
            if (componentClass.isAssignableFrom(component.getClass())) {
                getComponents().remove(i);

                component.setGameObject(null);
                return;
            }
        }
    }

    default <T extends GameComponent> T addComponent(T component) {
        getComponents().add(component);

        component.setGameObject(this);

        return component;
    }
}
