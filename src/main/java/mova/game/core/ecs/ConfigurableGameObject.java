package mova.game.core.ecs;

import mova.util.Properties;

public interface ConfigurableGameObject extends GameObject {

    Properties getProperties();

    default boolean is(String key) {
        return getProperties().is(key);
    }

    default void set(String key, boolean value) {
        getProperties().putBoolean(key, value);
    }
}
