package mova.game.core.ecs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractGameComponent implements GameComponent {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    private GameObject parent;

    @Override
    public GameObject getGameObject() {
        return parent;
    }

    @Override
    public void setGameObject(GameObject gameObject) {
        this.parent = gameObject;
    }
}
