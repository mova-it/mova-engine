package mova.game.core.ecs;

import mova.game.graphics.MovaGraphics;

public interface GameComponent {

    GameObject getGameObject();

    void setGameObject(GameObject gameObject);

    default void start() {

    }

    default void update(long dt) {

    }

    default void draw(MovaGraphics movaGraphics) {

    }

}
