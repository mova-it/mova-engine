package mova.game.core.ecs;

public enum State {
    IDLE,
    VISIBLE,
    ACTIVE,
    DESTROYED
}
