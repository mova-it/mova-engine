package mova.game.core.ecs;

import mova.game.core.Copyable;

public class SimpleGameObject extends AbstractGameObject {

    public SimpleGameObject(String name) {
        super(name);
    }

    private SimpleGameObject(SimpleGameObject gameObject) {
        super(gameObject.getName());

        for (GameComponent component : gameObject.getComponents()) {
            if (component instanceof Copyable) {
                addComponent((GameComponent) ((Copyable<?>) component).copy());
            }
        }
    }

    @Override
    public GameObject copy() {
        return new SimpleGameObject(this);
    }
}
