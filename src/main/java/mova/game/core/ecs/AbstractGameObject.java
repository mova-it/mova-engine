package mova.game.core.ecs;

import mova.game.core.Scene;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGameObject implements GameObject {

    private final String name;

    private final List<GameComponent> components = new ArrayList<>();

    private Scene currentScene;

    protected AbstractGameObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<GameComponent> getComponents() {
        return components;
    }

    @Override
    public boolean hasScene() {
        return currentScene != null;
    }

    @Override
    public Scene getScene() {
        return currentScene;
    }

    @Override
    public void setScene(Scene currentScene) {
        this.currentScene = currentScene;
    }
}
