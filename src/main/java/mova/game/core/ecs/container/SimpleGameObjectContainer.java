package mova.game.core.ecs.container;

import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;
import mova.game.graphics.Transform;
import mova.game.physics.RigidBody;
import mova.util.function.FunctionalInterfaceUtils;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class SimpleGameObjectContainer implements GameObjectContainer {

    private static final int MAX_GAME_OBJECTS = 1000;

    protected final GameObject[] gameObjects = new GameObject[MAX_GAME_OBJECTS];
    protected int nbGameObjects = 0;

    @Override
    public void addGameObject(GameObject gameObject) {
        if (gameObject == null) throw new IllegalArgumentException("Can't add null game object.");
        if (nbGameObjects >= MAX_GAME_OBJECTS) throw new ArrayIndexOutOfBoundsException();

        gameObjects[nbGameObjects++] = gameObject;
    }

    @Override
    public GameObject findGameObject(String name) {
        for (int i = 0; i < nbGameObjects; i++) {
            GameObject gameObject = gameObjects[i];
            if (gameObject.getName().equals(name)) return gameObject;
        }

        return null;
    }

    @Override
    public GameObject findGameObject(Locator p) {
        return findGameObject(p, FunctionalInterfaceUtils.noopConsumer());
    }

    public GameObject findGameObject(Locator location, Consumer<GameObject> gameObjectConsumer) {
        for (int i = 0; i < nbGameObjects; i++) {
            GameObject gameObject = gameObjects[i];
            if (!gameObject.isVisible()) continue;

            RigidBody body = gameObject.getComponent(RigidBody.class);
            if (body != null) {
                Shape shape = body.getBodyShape();

                Transform transform = gameObject.getComponent(Transform.class);
                if (transform != null) {
                    shape = transform.apply(shape);
                }

                if (shape.contains(location)) {
                    gameObjectConsumer.accept(gameObject);
                    return gameObject;
                }
            }
        }

        return null;
    }

    @Override
    public GameObject[] findGameObjects(Predicate<GameObject> predicate) {
        return Arrays.stream(gameObjects).limit(nbGameObjects).filter(predicate).toArray(GameObject[]::new);
    }

    @Override
    public GameObject[] getGameObjectsView() {
        return Arrays.copyOfRange(gameObjects, 0, nbGameObjects);
    }

    @Override
    public void forEach(Consumer<GameObject> consumer) {
//        for (int i = 0; i < nbGameObjects; i++) {
//            consumer.accept(gameObjects[i]);
//        }
        forEach(null,  null, consumer);
    }

    @Override
    public void forEach(Predicate<GameObject> predicate, Consumer<GameObject> consumer) {
        forEach(predicate, null, consumer);
//        Arrays.stream(gameObjects, 0, nbGameObjects).sorted(Comparator.comparingInt(gameObject -> -gameObject.getComponent(Transform.class).getZIndex())).filter(predicate).forEach(consumer);
    }

    @Override
    public void forEach(Comparator<GameObject> comparator, Consumer<GameObject> consumer) {
        forEach(null, comparator, consumer);
    }


    @Override
    public void forEach(Predicate<GameObject> predicate, Comparator<GameObject> comparator, Consumer<GameObject> consumer) {
        Stream<GameObject> gameObjectStream = Arrays.stream(gameObjects, 0, nbGameObjects);

        if (predicate != null) gameObjectStream = gameObjectStream.filter(predicate);
        if (comparator != null) gameObjectStream = gameObjectStream.sorted(comparator);

        gameObjectStream.forEach(consumer);
    }

    @Override
    public void clear() {
        Arrays.fill(gameObjects, null);
        nbGameObjects = 0;
    }
}
