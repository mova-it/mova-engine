package mova.game.core.ecs.container;

import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Predicate;

public final class NoopGameObjectContainer implements GameObjectContainer {

    private static final NoopGameObjectContainer INSTANCE = new NoopGameObjectContainer();
    private static final GameObject[] NO_GAME_OBJECTS = new GameObject[0];

    private NoopGameObjectContainer() {}

    @Override
    public void addGameObject(GameObject gameObject) {
        // Do nothring
    }

    @Override
    public GameObject findGameObject(String name) {
        return null;
    }

    @Override
    public GameObject findGameObject(Locator location, Consumer<GameObject> shipConsumer) {
        return null;
    }

    @Override
    public GameObject[] findGameObjects(Predicate<GameObject> predicate) {
        return NO_GAME_OBJECTS;
    }

    @Override
    public GameObject[] getGameObjectsView() {
        return NO_GAME_OBJECTS;
    }

    @Override
    public void forEach(Predicate<GameObject> predicate, Comparator<GameObject> comparator, Consumer<GameObject> consumer) {
        // Do nothing
    }

    @Override
    public void clear() {
        // Do nothing
    }

    public static GameObjectContainer getInstance() {
        return INSTANCE;
    }
}
