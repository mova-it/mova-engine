package mova.game.core.ecs.container;

import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;
import mova.game.graphics.Transform;
import mova.util.function.FunctionalInterfaceUtils;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface GameObjectContainer {

    Comparator<GameObject> NOOP_COMPARATOR = Comparator.comparingInt(value -> 0);
    Predicate<GameObject> NOOP_FILTER = gameObject -> true;
    Comparator<GameObject> Z_INDEX_COMPARATOR = Comparator.comparingInt(g -> {
        Transform component = g.getComponent(Transform.class);
        return component != null ? component.getZIndex() : 0;
    });

    void addGameObject(GameObject gameObject);

    GameObject findGameObject(String name);

    default GameObject findGameObject(Locator location) {
        return findGameObject(location, FunctionalInterfaceUtils.noopConsumer());
    }

    GameObject findGameObject(Locator location, Consumer<GameObject> shipConsumer);

    GameObject[] findGameObjects(Predicate<GameObject> predicate);

    GameObject[] getGameObjectsView();

    default void forEach(Consumer<GameObject> consumer) {
        forEach(NOOP_FILTER, NOOP_COMPARATOR, consumer);
    }

    default void forEach(Predicate<GameObject> predicate, Consumer<GameObject> consumer) {
        forEach(predicate, NOOP_COMPARATOR, consumer);
    }

    default void forEach(Comparator<GameObject> comparator, Consumer<GameObject> consumer) {
        forEach(NOOP_FILTER, comparator, consumer);
    }

    void forEach(Predicate<GameObject> predicate, Comparator<GameObject> comparator, Consumer<GameObject> consumer);

    void clear();

}
