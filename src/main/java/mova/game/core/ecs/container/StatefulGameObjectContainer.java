package mova.game.core.ecs.container;

import mova.game.core.ecs.GameObject;
import mova.game.core.ecs.State;
import mova.game.core.ecs.StatefulGameObject;
import mova.lang.ExceptionUtils;
import mova.util.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class StatefulGameObjectContainer extends SimpleGameObjectContainer implements GameObjectContainer {

    private int nbVisibleGameObjects = 0;

    @Override
    public void addGameObject(GameObject gameObject) {
        if (!(gameObject instanceof StatefulGameObject)) throw new IllegalArgumentException(getClass() + " only support " + StatefulGameObject.class);

        super.addGameObject(gameObject);

        notifyStateChange((StatefulGameObject) gameObject, State.IDLE);
    }

    @Override
    public void clear() {
        super.clear();

        nbVisibleGameObjects = 0;
    }

    public synchronized void notifyStateChange(StatefulGameObject gameObject, State oldState) {
        if (gameObject == null) throw new IllegalArgumentException("Can't notify for null game object.");
        if (oldState == null) throw new IllegalArgumentException("Can't notify with null old state.");

        int i = ArrayUtils.indexOf(gameObjects, gameObject);
        if (i < 0) throw new IllegalArgumentException("Impossible to notify for a none registered game object.");
        if (i >= nbGameObjects) throw new IllegalStateException();

        if (gameObject.getState() != oldState) {

            // Si l'ancienne etat était active ou visible, on procède forcémment au shift car on va vers idle ou destroyed
            if (oldState == State.ACTIVE || oldState == State.VISIBLE) {
                shiftAndIdle(gameObject, i);
                i = nbVisibleGameObjects;
            }

            if (gameObject.isDestroyed()) {
                ArrayUtils.swap(gameObjects, i, nbGameObjects - 1);
                gameObjects[nbGameObjects - 1] = null;
                nbGameObjects--;
            }
            // Sinon c'est qu'on teste si on passe de idle vers active ou visible
            else if (gameObject.isActive() || gameObject.isVisible()){
                // TODO ne pas oublier de le rajouter au bon endroit quand on aura le tri
                ArrayUtils.swap(gameObjects, nbVisibleGameObjects, i);
                nbVisibleGameObjects++;
            }
        }
    }

    private void shiftAndIdle(StatefulGameObject gameObject, int index) {
        int lastVisibleIndex = nbVisibleGameObjects - 1;
        if (index < lastVisibleIndex) {
            System.arraycopy(gameObjects, index + 1, gameObjects, index, lastVisibleIndex - index);
            gameObjects[lastVisibleIndex] = gameObject;
        }

        nbVisibleGameObjects--;
    }











    void notifyStateChangeWithDebug(StatefulGameObject gameObject, State oldState) {
        int i = ArrayUtils.indexOf(gameObjects, gameObject);

        Exception potentialThrowable = null;
        try {
            notifyStateChange(gameObject, oldState);

            boolean checked = Arrays.stream(gameObjects, 0, nbGameObjects).noneMatch(Objects::isNull);
            if (!checked) throw new IllegalStateException("Modification produce null gameobject");
        }
        catch (Exception throwable) {
            potentialThrowable = throwable;
            throw throwable;
        }
        finally {
            printState("notifyStateChange", gameObject + ": go.name = " + gameObject.getName() + "; go.state = " + gameObject.getState() + " go.index = " + i + "; go.state.old = " + oldState, potentialThrowable);
        }
    }

    void addGameObjectWithDebug(StatefulGameObject gameObject) {
        addGameObject(gameObject);

        printState("addGameObject", gameObject + ": go.name = " + gameObject.getName() + ": go.state = " + gameObject.getState(), null);
    }

    private void printState(String from, String infos, Throwable throwable) {
        Logger logger = LoggerFactory.getLogger(StatefulGameObjectContainer.class);
        if (logger.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("info = ").append(infos).append("\n");
            builder.append("nbGameObjects = ").append(nbGameObjects).append("; nbVisibleGameObjects = ").append(nbVisibleGameObjects).append("\n");
            if (nbGameObjects > 0) {
                builder.append(printGameObjects("\n")).append("\n");
            }
            if (throwable != null) {
                builder.append(ExceptionUtils.getStackTrace(throwable)).append("\n");
            }

            logger.info("{}:\n{}", from, builder);
        }
    }

    private String printGameObjects(String delimiter) {
        return Arrays.stream(gameObjects, 0, nbGameObjects).map(go -> go != null ? go + " : " + go.getName() + " = " + ((StatefulGameObject)go).getState() : "null").collect(Collectors.joining(delimiter));
    }
}
