package mova.game.core.ecs;

public interface StatefulGameObject extends GameObject {

    State getState();

    void setState(State state);

    default boolean isIdle() {
        return getState() == State.IDLE;
    }

    @Override
    default boolean isVisible() {
        return getState() == State.VISIBLE || getState() == State.ACTIVE;
    }

    @Override
    default boolean isActive() {
        return getState() == State.ACTIVE;
    }

    default boolean isDestroyed() {
        return getState() == State.DESTROYED;
    }

    default void disable() {
        if (!isDestroyed()) setState(State.IDLE);
    }

    default void show() {
        if (isIdle()) setState(State.VISIBLE);
    }

    default void deactivate() {
        if (isActive()) setState(State.VISIBLE);
    }

    default void activate() {
        if (!isDestroyed()) setState(State.ACTIVE);
    }

    default void destroy() {
        if (!isDestroyed()) setState(State.DESTROYED);
    }
}
