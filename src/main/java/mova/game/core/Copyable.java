package mova.game.core;

// TODO y'a peut être un souci à ce niveau là. Comment on gère correctement les composant qui doivent être copiés, de ceux qui doivent être simplement ajoutés et de ceux qui ne doivent pas être ajoutés du tout?
public interface Copyable<T> {

    T copy();
}
