package mova.game.core;

import mova.game.graphics.MovaGraphics;
import mova.game.time.Timer;

import java.util.concurrent.TimeUnit;

class BasicGameLoop implements GameLoopStrategy {

    public void gameLoop(GameCore gameCore) {
        gameCore.start();

        Timer.start();

        while (gameCore.isRunning()) {
            long milliElapsedtime = Timer.getElapsedTime(TimeUnit.MILLISECONDS);

            gameCore.update(milliElapsedtime);

            MovaGraphics g = gameCore.getGraphics();
            gameCore.draw(g, milliElapsedtime);
            g.dispose();

            gameCore.atLoopEnd();
        }
    }
}
