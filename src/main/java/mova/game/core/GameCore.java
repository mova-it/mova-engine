package mova.game.core;

import mova.game.graphics.MovaGraphics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class GameCore {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private GameLoopStrategy gameLoopStrategy;

    private boolean running;

    protected GameCore() {
        this(GameLoopType.BASIC);
    }

    protected GameCore(GameLoopType type) {
        setGameLoopStrategy(type);
    }

    private void setGameLoopStrategy(GameLoopType type) {
        gameLoopStrategy = type;
    }

    public void run() {
        try {
            init();

            gameLoop();
        }
        catch (Exception | FatalError e) {
            onUncaughtError(e);
        }
        finally {
            beforeExit();
        }
    }

    void start() {
        running = true;
    }

    boolean isRunning() {
        return running;
    }

    protected abstract void init();

    protected void gameLoop() {
        gameLoopStrategy.gameLoop(this);
    }

    protected void beforeExit() {}

    protected abstract MovaGraphics getGraphics();

    protected abstract void update(long elapsedTime);

    protected abstract void draw(MovaGraphics movaGraphics, long elapsedTime);

    protected void atLoopEnd() {}

    protected void onUncaughtError(Throwable t) {
        LoggerFactory.getLogger(getClass()).error("Une erreur inattendue s'est produite", t);
    }

    public void stop() {
        running = false;
    }

}
