package mova.game.core;

public enum GameLoopType implements GameLoopStrategy {
    BASIC(new BasicGameLoop()), ADVANCED(new AdvancedGameLoop());

    private final GameLoopStrategy strategy;

    GameLoopType(GameLoopStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public void gameLoop(GameCore gameCore) {
        strategy.gameLoop(gameCore);
    }
}
