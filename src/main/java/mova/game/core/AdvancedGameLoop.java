package mova.game.core;

import mova.game.graphics.MovaGraphics;
import mova.game.time.Timer;

import java.util.concurrent.TimeUnit;

class AdvancedGameLoop implements GameLoopStrategy {

    // TODO rendre se paramêtre configurable peut être même de manière générique
    private static final long MS_PER_UPDATE = 15;

    public void gameLoop(GameCore gameCore) {
        gameCore.start();

        Timer.start();
        float lag = 0;

        while (gameCore.isRunning()) {
            long milliElapsedtime = Timer.getElapsedTime(TimeUnit.MILLISECONDS);
            lag += milliElapsedtime;

            while (lag >= MS_PER_UPDATE) {
                gameCore.update(MS_PER_UPDATE);
                lag -= MS_PER_UPDATE;
            }

            MovaGraphics g = gameCore.getGraphics();
            gameCore.draw(g, milliElapsedtime);
            g.dispose();

            gameCore.atLoopEnd();
        }
    }
}
