package mova.game.core;

import mova.game.core.ecs.GameObject;
import mova.game.core.ecs.container.GameObjectContainer;
import mova.game.core.ecs.container.NoopGameObjectContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public abstract class AbstractScene implements Scene {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private final GameObjectContainer gameObjectContainer;

    private boolean running = false;

    protected AbstractScene() {
        this(NoopGameObjectContainer.getInstance());
    }

    protected AbstractScene(GameObjectContainer gameObjectContainer) {
        if (gameObjectContainer == null) throw new IllegalArgumentException("GameObjectContainer can't be null. Use NoopGameObjectContainer instead");
        this.gameObjectContainer = gameObjectContainer;
    }

    @Override
    public GameObjectContainer getGameObjects() {
        return gameObjectContainer;
    }

    public void addGameObject(GameObject gameObject) {
        getGameObjects().addGameObject(gameObject);

        gameObject.setScene(this);

        if (running) gameObject.start();
    }

    public void addGameObjects(List<? extends GameObject> gameObjects) {
        gameObjects.forEach(this::addGameObject);
    }

    @Override
    public void start() {
        Scene.super.start();

        running = true;
    }

    @Override
    public void release() {
        Scene.super.release();

        running = false;
    }
}
