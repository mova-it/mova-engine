package mova.game.core;

interface GameLoopStrategy {

    void gameLoop(GameCore gameCore);
}
