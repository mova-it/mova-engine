package mova.game.core;

import mova.game.core.ecs.container.GameObjectContainer;
import mova.game.core.ecs.container.NoopGameObjectContainer;
import mova.game.graphics.MovaGraphics;

import java.util.function.Function;

public final class SceneOrchestrator {

    private final Function<Class<? extends Scene>,? extends Scene> sceneProvider;

    private Scene currentScene = new NullScene();

    public SceneOrchestrator(Function<Class<? extends Scene>,? extends Scene> sceneProvider) {
        this.sceneProvider = sceneProvider;
    }

    public Scene getCurrentScene() {
        return currentScene;
    }

    public void setScene(Class<? extends Scene> sceneClass) {
        if (!sceneClass.isAssignableFrom(currentScene.getClass())) {
            // TODO trouver un moyen pour faire de manière asynchrone afin de pouvoir attendre la fin d'une animation de transition
            // TODO y'a tout de même un soucis: On doit release AVANT de changer le Scene (problème du partage du GameWindow, pas thread safe, etc.)
            //  Ou bien créer une Scène intermédaire (constante)
            currentScene.release();

            currentScene = createScene(sceneClass);
            currentScene.start(); // TODO on démarre à la main mais au final on pourrait laisser le choix @PostConstruct agir?
        }
    }

    private Scene createScene(Class<? extends Scene> sceneClass) {
        try {
            return sceneProvider.apply(sceneClass);
        } catch (Exception e) {
            throw new IllegalStateException(sceneClass + " is not a valid scene", e);
        }
    }

    private static final class NullScene implements Scene {
        @Override
        public GameObjectContainer getGameObjects() {
            return NoopGameObjectContainer.getInstance();
        }

        @Override
        public void start() {
            // DO NOTHING
        }

        @Override
        public void release() {
            // DO NOTHING
        }

        @Override
        public void update(long dt) {
            // DO NOTHING
        }

        @Override
        public void draw(MovaGraphics movaGraphics) {
            // DO NOTHING
        }

    }
}
