package mova.game.physics;

import mova.game.core.Copyable;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.core.ecs.GameObject;
import mova.game.graphics.Transform;

import java.awt.*;
import java.awt.geom.Area;

public class RigidBody extends AbstractGameComponent implements Copyable<RigidBody> {

    private final Shape bodyShape;

    public RigidBody(Shape bodyShape) {
        this.bodyShape = bodyShape;
    }

    public Shape getBodyShape() {
        return bodyShape;
    }

    public boolean isColliding(GameObject other) {
        if (other == null) return false;
        if (other == getGameObject()) return false;

        return isColliding(other.getComponent(RigidBody.class), other.getComponent(Transform.class));
    }

    public boolean isColliding(RigidBody otherBody, Transform otherTransformation) {
        if (otherBody == null) return false;
        if (otherBody == this) return false;

        Shape otherShape = otherBody.bodyShape;
        if (otherTransformation != null) otherShape = otherTransformation.apply(otherShape);

        Shape currentShape = bodyShape;
        Transform currentTransformation = getGameObject() != null ? getGameObject().getComponent(Transform.class) : null;
        if (currentTransformation != null) currentShape = currentTransformation.apply(currentShape);

        return isColliding(currentShape, otherShape);
    }

    public static boolean isColliding(Shape s1, Shape s2) {
        if (s1 == null || s2 == null) return false;
        if (s1 == s2) return false;

        if (s1.getBounds2D().intersects(s2.getBounds2D())) {
            Area checkedArea = new Area(s1);
            Area area = new Area(s2);
            checkedArea.intersect(area);

            return !checkedArea.isEmpty();
        }

        return false;
    }

    @Override
    public RigidBody copy() {
        return new RigidBody(bodyShape);
    }
}
