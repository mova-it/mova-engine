package mova.game.view;

import mova.game.core.FatalError;
import mova.game.geom.Locator;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Constructor;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Une vue est une zone de l'écran sur laquelle est projeté le monde courant à travers une camera.
 * Une seule caméra à la fois peut projeter le monde courant sur la vue.
 * <p/>
 * La distance de la vue à la caméra ne servirai que dans le cas de la 3D pour gérer la "focal" de la caméra
 * Pour l'instant on laisse de côté
 * <p/>
 * Il y a certainement moyen de décomposer la projection du monde vers la vue puis de la vue vers l'écran
 */
public class View {

    // La position de la vue dans l'écran
    private final Rectangle bounds;
    private final Locator center;

    private final Deque<Camera> cameras = new LinkedList<>();

    private final Projection v2sProjection = new Projection();

    public View(int x, int y, int width, int height) {
        bounds = new Rectangle(x, y, width, height);
        center = new Locator(bounds.getCenterX(), bounds.getCenterY());

        computeTransformation();
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public int getWidth() {
        return bounds.width;
    }

    public int getHeight() {
        return bounds.height;
    }

    public Locator getCenter() {
        return center;
    }

    void addCamera(Camera camera) {
        cameras.add(camera);
    }

    public <T extends AbstractCamera> T addCamera(Class<T> cameraClass) {
        try {
            Constructor<T> cameraConstructor = cameraClass.getDeclaredConstructor(View.class);
            T camera = cameraConstructor.newInstance(this);
            cameras.add(camera);

            return camera;
        } catch (ReflectiveOperationException e) {
            throw new FatalError("Impossible de créer la caméra " + cameraClass, e);
        }
    }

    public Camera getActiveCamera() {
        return cameras.getFirst();
    }

    private void computeTransformation() {
        AffineTransform transform = v2sProjection.getTransform();
        transform.setToTranslation(center.getX(), center.getY());
        transform.scale(1, -1);
    }

    Projection getViewToScreenProjection() {
        return v2sProjection;
    }


    private final Projection currentProjection = new Projection();
    private Rectangle2D worldViewBounds;
    public Rectangle2D getWorldView() {
        Projection cameraProjection = getActiveCamera().getScreenProjection();
        if (!cameraProjection.equals(currentProjection)) {
            currentProjection.setTo(cameraProjection);

            Shape shape = currentProjection.createInversedProjection().transform(getBounds());
            worldViewBounds = shape.getBounds2D();
        }

        return worldViewBounds;
    }

    @Override
    public String toString() {
        return String.format("View{bounds=%s}", bounds);
    }
}
