package mova.game.view;

import mova.game.geom.Locator;
import mova.game.geom.Vector;

public class OptimizedCamera extends SimpleCamera {

    protected Projection upToDateProjection;

    OptimizedCamera(View view) {
        super(view);
    }

    @Override
    public void setScaleFactor(float scaleFactor) {
        super.setScaleFactor(scaleFactor);

        markAsDirty();
    }

    @Override
    public void setX(float x) {
        super.setX(x);

        markAsDirty();
    }

    @Override
    public void addX(float x) {
        super.addX(x);

        markAsDirty();
    }

    @Override
    public void subX(float x) {
        super.subX(x);

        markAsDirty();
    }

    @Override
    public void setY(float y) {
        super.setY(y);

        markAsDirty();
    }

    @Override
    public void addY(float y) {
        super.addY(y);

        markAsDirty();
    }

    @Override
    public void subY(float y) {
        super.subY(y);

        markAsDirty();
    }

    @Override
    public void moveTo(float x, float y) {
        super.moveTo(x, y);

        markAsDirty();
    }

    @Override
    public void moveTo(Locator p) {
        super.moveTo(p);

        markAsDirty();
    }

    @Override
    public void moveOf(Vector v) {
        super.moveOf(v);

        markAsDirty();
    }

    @Override
    public void moveOf(float x, float y) {
        super.moveOf(x, y);

        markAsDirty();
    }

    @Override
    public void moveBack(float x, float y) {
        super.moveBack(x, y);

        markAsDirty();
    }

    @Override
    public void moveBack(Vector v) {
        super.moveBack(v);

        markAsDirty();
    }

    private void markAsDirty() {
        upToDateProjection = null;
    }

    private boolean isDirty() {
        return upToDateProjection == null;
    }

    @Override
    public Projection getScreenProjection() {
        if (isDirty()) {
            upToDateProjection = super.getScreenProjection();
        }

        return upToDateProjection;
    }
}
