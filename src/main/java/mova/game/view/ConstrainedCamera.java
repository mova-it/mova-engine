package mova.game.view;

import java.awt.*;

public class ConstrainedCamera extends OptimizedCamera {

    private Rectangle world = new Rectangle(0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
    private float minScaleFactor = .001f;
    private float maxScaleFactor = 1;

    ConstrainedCamera(View view) {
        super(view);
    }

    @Override
    public void setX(float x) {
        x = getXWithinBounds(x);
        super.setX(x);
    }

    @Override
    public void setY(float y) {
        y = getYWithinBounds(y);
        super.setY(y);
    }

    @Override
    public void setScaleFactor(float scaleFactor) {
        scaleFactor = Math.min(scaleFactor, maxScaleFactor);
        scaleFactor = Math.max(scaleFactor, minScaleFactor);

        super.setScaleFactor(scaleFactor);

        // On ajuste en fonction des contraintes la position de la caméra horizontalement et verticalement
        adjustCameraHorizontalPosition();
        adjustCameraVerticalPosition();
    }

    public void stickToWorld(Rectangle world) {
        if (world == null) throw new IllegalArgumentException("World cannot be null");

        this.world = world;

        adjustCameraHorizontalPosition();
        adjustCameraVerticalPosition();
    }

    public void adaptMinScaleFactorToBoundsConstraints() {
        if (world != null) {
            float minScaleFactorForWidth = (float) (view.getWidth()/world.getWidth());
            float minScaleFactorForHeight = (float) (view.getHeight()/world.getHeight());
            this.minScaleFactor = Math.max(minScaleFactorForWidth, minScaleFactorForHeight);
        }
    }

    public void setMaxScaleFactor(float maxScaleFactor) {
        if (maxScaleFactor <= 0) throw new IllegalArgumentException("Max scale factor can't be a negative value");

        this.maxScaleFactor = maxScaleFactor;
    }

    public void setScaleFactorLevel(float level) {
        if (level <= 0 || level > 1) throw new IllegalArgumentException("ScaleFactor level must be included between ]0, 1]");

        setScaleFactor(minScaleFactor + level*(maxScaleFactor - minScaleFactor));
    }

    private void adjustCameraHorizontalPosition() {
        float xWithinBounds = getXWithinBounds(getX());
        super.setX(xWithinBounds);
    }

    private void adjustCameraVerticalPosition() {
        float yWithinBounds = getYWithinBounds(getY());
        super.setY(yWithinBounds);
    }

    private float getXWithinBounds(float newX) {
        float halfViewWidthToWorld = getScreenProjection().unscaleDistance(view.getWidth()/2f);
        float max = Math.max(halfViewWidthToWorld + world.x, newX);
        return Math.min(world.width - world.x - halfViewWidthToWorld, max);
    }

    private float getYWithinBounds(float newY) {
        float halfViewHeightToWorld = getScreenProjection().unscaleDistance(view.getHeight()/2f);
        newY = Math.max(halfViewHeightToWorld + world.y, newY);
        return Math.min(world.height - world.y - halfViewHeightToWorld, newY);
    }

    @Override
    public String toString() {
        return String.format("ConstrainedCamera{scaleFactor=%s, boundsConstraints=%s, minScaleFactor=%s, maxScaleFactor=%s, view=%s}", getScaleFactor(), world, minScaleFactor, maxScaleFactor, view);
    }

}
