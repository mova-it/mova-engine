package mova.game.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractCamera implements Camera {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected View view;

    AbstractCamera(View view) {
        this.view = view;

        view.addCamera(this);
    }
}
