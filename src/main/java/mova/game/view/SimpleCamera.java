package mova.game.view;

import mova.game.geom.Locator;
import mova.game.geom.Vector;

import java.awt.geom.AffineTransform;

/**
 * Une caméra est un point de vue sur le monde. On peut projeter un élément du monde
 * sur une vue grâce à elle et vice versa.
 */
public class SimpleCamera extends AbstractCamera {

    protected static final float DEFAULT_SCALE_FACTOR = 1f;

    // La position de la caméra dans le monde
    private final Locator location = new Locator();

    private final Projection w2sProjection = new Projection();
    private final Projection w2vProjection = new Projection();

    /**
     * Un scaleFactor < 1 correspond à un zoom arrière et un scaleFactor > 1 un zoomAvant
     * Le scaleFactor ne peut être égale à 0.
     */
    private float scaleFactor = DEFAULT_SCALE_FACTOR;

    SimpleCamera(View view) {
        super(view);
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        if (scaleFactor <= 0) {
            logger.warn("On ne peut pas avoir de facteur de mis à l'échelle inférieur ou égal à 0: {}", scaleFactor);
            return;
        }

        this.scaleFactor = scaleFactor;
    }

    public void increaseScaleFactor(float percentage) {
        setScaleFactor(scaleFactor*(1 + percentage));
    }

    public void increaseScaleFactor(float percentage, Locator zoomPoint) {
        setScaleFactor(scaleFactor*(1 + percentage), zoomPoint);
    }

    public void setScaleFactor(float scaleFactor, Locator zoomPoint) {
        Locator zoomPointInWorldBefore = getScreenProjection().inverseTransform(zoomPoint);

        setScaleFactor(scaleFactor);

        Locator zoomPointInWorldAfter = getScreenProjection().inverseTransform(zoomPoint);
        moveBack(Vector.from(zoomPointInWorldBefore).to(zoomPointInWorldAfter));
    }

    @Override
    public Locator getLocation() {
        return location;
    }

    @Override
    public Projection getScreenProjection() {
        AffineTransform w2vTransform = w2vProjection.getTransform();
        w2vTransform.setToScale(scaleFactor, scaleFactor);
        w2vTransform.translate(-location.getX(), -location.getY());

        w2sProjection.setTo(view.getViewToScreenProjection());
        w2sProjection.add(w2vProjection);

        return w2sProjection;
    }

    @Override
    public String toString() {
        return String.format("Camera{location=%s, scaleFactor=%s, view=%s}", location, scaleFactor, view);
    }
}
