package mova.game.view;

public interface Camera extends Positionable {

    Projection getScreenProjection();

}
