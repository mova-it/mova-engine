package mova.game.view;

import mova.game.core.FatalError;
import mova.game.geom.Locator;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.util.Objects;

public class Projection {

    private final AffineTransform transform;

    public Projection() {
        this(new AffineTransform());
    }

    public Projection(AffineTransform transform) {
        this.transform = transform;
    }

    public void setTo(Projection projection) {
        transform.setTransform(projection.transform);
    }

    public void add(Projection projection) {
        transform.concatenate(projection.transform);
    }

    AffineTransform getTransform() {
        return transform;
    }

    public Shape transform(Shape s) {
        return transform.createTransformedShape(s);
    }

    public Locator transform(Locator p) {
        return (Locator) transform.transform(p, new Locator());
    }

    public Locator transform(int x, int y) {
        return transform(new Locator(x, y));
    }

    public Locator inverseTransform(Locator p) {
        try {
            return (Locator) transform.inverseTransform(p, new Locator());
        } catch (NoninvertibleTransformException e) {
            throw new FatalError("Impossible d'obtenir la transformation inverse de ce point: " + p, e);
        }
    }

    public Locator inverseTransform(int x, int y) {
        return inverseTransform(new Locator(x, y));
    }

    public Projection createInversedProjection() {
        try {
            return new Projection(transform.createInverse());
        } catch (NoninvertibleTransformException e) {
            throw new FatalError("Impossible d'obtenir la transformation inverse de ce transformation: " + transform, e);
        }
    }

    public float scaleDistance(float d) {
        return (float) (d*transform.getScaleX());
    }

    public float unscaleDistance(float d) {
        return (float) (d/transform.getScaleX());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Projection that = (Projection) o;
        return Objects.equals(transform, that.transform);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transform);
    }

    @Override
    public String toString() {
        return "Projection{" +
                "transform=" + transform +
                '}';
    }
}
