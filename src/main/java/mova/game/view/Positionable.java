package mova.game.view;

import mova.game.geom.Locator;
import mova.game.geom.Vector;

public interface Positionable {

    Locator getLocation();

    default float getX() {
        return (float) getLocation().getX();
    }

    default void setX(float x) {
        Locator location = getLocation();
        location.setLocation(x, location.getY());
    }

    default void addX(float x) {
        setX((float) getLocation().getX() + x);
    }

    default void subX(float x) {
        setX((float) getLocation().getX() - x);
    }

    default float getY() {
        return (float) getLocation().getY();
    }

    default void setY(float y) {
        Locator location = getLocation();
        location.setLocation(location.getX(), y);
    }

    default void addY(float y) {
        setY((float) getLocation().getY() + y);
    }

    default void subY(float y) {
        setY((float) getLocation().getY() - y);
    }

    default void moveTo(float x, float y) {
        setX(x);
        setY(y);
    }

    default void moveTo(Locator p) {
        moveTo((float) p.getX(), (float) p.getY());
    }

    default void moveOf(Vector v) {
        moveOf(v.getX(), v.getY());
    }

    default void moveOf(float x, float y) {
        moveTo(getX() + x, getY() + y);
    }

    default void moveBack(float x, float y) {
        moveTo(getX() - x, getY() - y);
    }

    default void moveBack(Vector v) {
        moveBack(v.getX(), v.getY());
    }
}
