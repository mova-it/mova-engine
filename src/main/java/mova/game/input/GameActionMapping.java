package mova.game.input;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class GameActionMapping {

    private static final int NUM_KEYS_CODES = 600;

    private final Logger logger = LoggerFactory.getLogger(GameActionMapping.class);

    private final GameAction[] keysActions = new GameAction[NUM_KEYS_CODES];
    private final GameAction[] mouseActions = new GameAction[MouseUserAction.values().length];

    public GameActionMapping() {}

    public GameActionMapping(GameActionMapping gameActionMapping) {
        System.arraycopy(gameActionMapping.keysActions, 0, keysActions, 0, NUM_KEYS_CODES);
        System.arraycopy(gameActionMapping.mouseActions, 0, mouseActions, 0, MouseUserAction.values().length);
    }

    public static String getKeyName(int keyCode) {
        return KeyEvent.getKeyText(keyCode);
    }

    public static String getMouseName(int mouseCode) {
        return MouseUserAction.values()[mouseCode].actionName;
    }

    public static int getMouseButtonCode(MouseEvent mouseEvent) {
        MouseUserAction mouseUserAction = getMouseButtonUserAction(mouseEvent);
        return getMouseButtonCode(mouseUserAction);
    }

    public static int getMouseButtonCode(MouseUserAction mouseUserAction) {
        return mouseUserAction != null ? mouseUserAction.ordinal() : -1;
    }

    public static MouseUserAction getMouseButtonUserAction(MouseEvent mouseEvent) {
        switch (mouseEvent.getButton()) {
            case MouseEvent.BUTTON1: return MouseUserAction.LEFT_BUTTON;
            case MouseEvent.BUTTON2: return MouseUserAction.WHEEL_BUTTON;
            case MouseEvent.BUTTON3: return MouseUserAction.RIGHT_BUTTON;
            case 4: return MouseUserAction.REAR_BUTTON;
            case 5: return MouseUserAction.FRONT_BUTTON;
            default: return null;
        }
    }

    public void mapToKey(GameAction gameAction, int keyCode) {
        if (logger.isDebugEnabled() && keysActions[keyCode] != null) {
            logger.warn("{} est déjà lié à une action: {}", keyCode, keysActions[keyCode].getName());
        }

        keysActions[keyCode] = gameAction;
    }

    public GameAction mapToKey(String name, int keyCode) {
        return mapToKey(name, keyCode, GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
    }

    public GameAction mapToKey(String name, int keyCode, GameAction.Behavior behavior) {
        mapToKey(new GameAction(name, behavior), keyCode);
        return keysActions[keyCode];
    }

    public void mapToMouse(GameAction gameAction, MouseUserAction mouseUserAction) {
        if (logger.isDebugEnabled() && mouseActions[mouseUserAction.ordinal()] != null) {
            logger.warn("{} est déjà lié à une action: {}", mouseUserAction, mouseActions[mouseUserAction.ordinal()].getName());
        }

        mouseActions[mouseUserAction.ordinal()] = gameAction;
    }

    public GameAction mapToMouse(String name, MouseUserAction mouseUserAction) {
        return mapToMouse(name, mouseUserAction, GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
    }

    public GameAction mapToMouse(String name, MouseUserAction mouseUserAction, GameAction.Behavior behavior) {
        mapToMouse(new GameAction(name, behavior), mouseUserAction);
        return mouseActions[mouseUserAction.ordinal()];
    }

    public GameAction getGameAction(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        return keyCode < keysActions.length ? keysActions[keyCode] : null;
    }

    public GameAction getGameAction(MouseEvent mouseEvent) {
        int mouseCode = getMouseButtonCode(mouseEvent);
        return mouseCode != -1 ? mouseActions[mouseCode] : null;
    }

    public GameAction getGameAction(MouseUserAction mouseUserAction) {
        int mouseCode = getMouseButtonCode(mouseUserAction);
        return mouseCode != -1 ? mouseActions[mouseCode] : null;
    }

    public GameAction getGameAction(int delta, MouseUserAction positiveAction, MouseUserAction negativeAction) {
        MouseUserAction mouseUserAction = MouseUserAction.getMouseUserAction(delta, positiveAction, negativeAction);
        return mouseActions[mouseUserAction.ordinal()];
    }

    public void clearGameActionMappings(GameAction gameAction) {
        for (int i = 0; i < keysActions.length; i++) {
            if (keysActions[i] == gameAction) clearGameAction(keysActions, i);
        }
        for (int i = 0; i < mouseActions.length; i++) {
            if (mouseActions[i] == gameAction) clearGameAction(mouseActions, i);
        }
    }

    public List<String> getGameActionMappings(GameAction gameAction) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < keysActions.length; i++) {
            if (keysActions[i] == gameAction) list.add(getKeyName(i));
        }
        for (int i = 0; i < mouseActions.length; i++) {
            if (mouseActions[i] == gameAction) list.add(getMouseName(i));
            if (mouseActions[i] == gameAction) list.add(MouseUserAction.values()[i].actionName);
        }

        return list;
    }

    public void clearAllGameActionMappings() {
        for (int i = 0; i < keysActions.length; i++) {
            if (keysActions[i] != null) clearGameAction(keysActions, i);
        }

        for (int i = 0; i < mouseActions.length; i++) {
            if (mouseActions[i] != null) clearGameAction(mouseActions, i);
        }
    }

    private void clearGameAction(GameAction[] gameActions, int index) {
        gameActions[index].reset();
        gameActions[index] = null;
    }

    public void resetAllGameActions() {
        for (GameAction keysAction : keysActions) {
            if (keysAction != null) keysAction.reset();
        }
        for (GameAction mouseAction : mouseActions) {
            if (mouseAction != null) mouseAction.reset();
        }
    }

}
