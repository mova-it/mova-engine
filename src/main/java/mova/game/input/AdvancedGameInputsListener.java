package mova.game.input;

import mova.game.geom.Locator;
import mova.game.graphics.component.GameWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public class AdvancedGameInputsListener extends GameInputsListener {

    private final GameInputsListener delegate;

    private Point centerLocation;
    private Robot robot;
    private boolean isRecentering;

    public AdvancedGameInputsListener(GameInputsListener gameInputsListener) {
        this.delegate = gameInputsListener;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        delegate.actionPerformed(actionEvent);
    }

    @Override
    public void setGameActionMapping(GameActionMapping gameActionMapping) {
        delegate.setGameActionMapping(gameActionMapping);
    }

    @Override
    public void listenTo(GameWindow gameWindow) {
        centerLocation = gameWindow.getCenterLocation();

        delegate.listenTo(gameWindow);
    }

    @Override
    public void detach() {
        delegate.detach();

        centerLocation = null;
    }

    @Override
    public Locator getMouseLocation() {
        return delegate.getMouseLocation();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        delegate.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        delegate.keyReleased(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        delegate.keyTyped(e);
    }

    @Override
    public boolean getMouseState(MouseUserAction mouseUserAction) {
        return delegate.getMouseState(mouseUserAction);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        delegate.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        delegate.mouseReleased(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        delegate.mouseClicked(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        delegate.mouseEntered(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        delegate.mouseExited(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        delegate.mouseDragged(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point newLocation = e.getPoint();

        if (isRecentering) isRecentering = false;
        else {
            Locator oldLocation = getMouseLocation();
            int dx = (int) (newLocation.x - oldLocation.getX());
            int dy = (int) (newLocation.y - oldLocation.getY());

            mouseMovement(dx, MouseUserAction.MOVE_RIGHT, MouseUserAction.MOVE_LEFT);
            mouseMovement(dy, MouseUserAction.MOVE_DOWN, MouseUserAction.MOVE_UP);

            if (isRelativeMouseMode()) recenterMouse();
        }

        delegate.mouseMoved(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        delegate.mouseWheelMoved(e);
    }

    public void setRelativeMouseMode(boolean enable) {
        if (isRelativeMouseMode() != enable) {
            if (enable) {
                try {
                    robot = new Robot();
                    recenterMouse();
                } catch (AWTException awte) {
                    throw new AWTError(awte.getLocalizedMessage());
                }
            } else {
                robot = null;
            }
        }
    }

    public boolean isRelativeMouseMode() {
        return robot != null;
    }

    private synchronized void recenterMouse() {
        if (isRelativeMouseMode() && delegate.getGameWindow().isShowing()) {
            delegate.getGameWindow().getCenterLocation(centerLocation);
            isRecentering = true;
            robot.mouseMove(centerLocation.x, centerLocation.y);
        }
    }
}
