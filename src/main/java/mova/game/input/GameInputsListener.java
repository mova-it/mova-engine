package mova.game.input;

import mova.game.geom.Locator;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.event.GameActionListener;

import java.awt.*;
import java.awt.event.*;
import java.util.EnumMap;

public class GameInputsListener implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener, GameActionListener {


    private final EnumMap<MouseUserAction, Boolean> mouseStates = new EnumMap<>(MouseUserAction.class);
    private Locator mouseLocation = new Locator();
    private GameActionMapping gameActionMapping;
    private GameWindow gameWindow;

    public GameInputsListener() {
        for (MouseUserAction value : MouseUserAction.values()) {
            mouseStates.put(value, Boolean.FALSE);
        }
    }

    public GameWindow getGameWindow() {
        return gameWindow;
    }

    public void setGameActionMapping(GameActionMapping gameActionMapping) {
        if (gameActionMapping == null) throw new IllegalArgumentException("Game Action Mapping can't be null");

        this.gameActionMapping = gameActionMapping;
    }

    public void listenTo(GameWindow gameWindow) {
        this.gameWindow = gameWindow;

        mouseLocation = gameWindow.getMousePosition();

        gameWindow.addListeners(this);
    }

    public void detach() {
        if (gameWindow != null) {
            gameWindow.removeListeners(this);
            gameWindow = null;
        }

        if (gameActionMapping != null) {
            gameActionMapping.clearAllGameActionMappings();
            gameActionMapping = null;
        }

        mouseLocation = new Locator();
    }

    public Locator getMouseLocation() {
        return new Locator(mouseLocation);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        GameAction gameAction = gameActionMapping.getGameAction(e);
        if (gameAction != null) gameAction.press();
        e.consume();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        GameAction gameAction = gameActionMapping.getGameAction(e);
        if (gameAction != null) gameAction.release();
        e.consume();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }

    public boolean getMouseState(MouseUserAction mouseUserAction) {
        return mouseStates.get(mouseUserAction);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        MouseUserAction mouseUserAction = GameActionMapping.getMouseButtonUserAction(e);
        mouseStates.replace(mouseUserAction, Boolean.FALSE, Boolean.TRUE);

        GameAction gameAction = gameActionMapping.getGameAction(mouseUserAction);
        if (gameAction != null) gameAction.press();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        MouseUserAction mouseUserAction = GameActionMapping.getMouseButtonUserAction(e);
        mouseStates.replace(mouseUserAction, Boolean.TRUE, Boolean.FALSE);

        GameAction gameAction = gameActionMapping.getGameAction(mouseUserAction);
        if (gameAction != null) gameAction.release();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseMoved(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Point newLocation = e.getPoint();

        Locator oldLocation = getMouseLocation();
        int dx = (int) (newLocation.x - oldLocation.getX());
        int dy = (int) (newLocation.y - oldLocation.getY());

        mouseMovement(dx, MouseUserAction.DRAG_RIGHT, MouseUserAction.DRAG_LEFT);
        mouseMovement(dy, MouseUserAction.DRAG_DOWN, MouseUserAction.DRAG_UP);

        mouseLocation.setLocation(newLocation);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point newLocation = e.getPoint();

        mouseLocation.setLocation(newLocation);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        mouseMovement(e.getWheelRotation(), MouseUserAction.WHEEL_DOWN, MouseUserAction.WHEEL_UP);
    }

    protected void mouseMovement(int delta, MouseUserAction positiveAction, MouseUserAction negativeAction) {
        GameAction gameAction = gameActionMapping.getGameAction(delta, positiveAction, negativeAction);

        if (gameAction != null) {
            gameAction.press(Math.abs(delta));
            gameAction.release();
        }
    }
}
