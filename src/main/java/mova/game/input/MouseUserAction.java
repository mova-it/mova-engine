package mova.game.input;

public enum MouseUserAction {
    MOVE_LEFT("Mouse Move Left"),
    MOVE_RIGHT("Mouse Move Right"),
    MOVE_UP("Mouse Move Up"),
    MOVE_DOWN("Mouse Move Down"),
    DRAG_LEFT("Mouse Drag Left"),
    DRAG_RIGHT("Mouse Drag Right"),
    DRAG_UP("Mouse Drag Up"),
    DRAG_DOWN("Mouse Drag Down"),
    WHEEL_UP("Mouse Wheel Up"),
    WHEEL_DOWN("Mouse Wheel Down"),
    LEFT_BUTTON("Left Mouse Button"),
    WHEEL_BUTTON("Wheel Mouse Button"),
    RIGHT_BUTTON("Right Mouse Button"),
    REAR_BUTTON("Rear Mouse Button"),
    FRONT_BUTTON("Front Mouse Button");

    public final String actionName;

    MouseUserAction(String actionName) {
        this.actionName = actionName;
    }

    static MouseUserAction getMouseUserAction(int delta, MouseUserAction positiveAction, MouseUserAction negativeAction) {
        return delta < 0 ? negativeAction : positiveAction;
    }
}
