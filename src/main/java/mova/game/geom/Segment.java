package mova.game.geom;

import mova.lang.MathUtils;

import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

// TODO y'a un truc qui va pas avec mes machin de Locator, Move Segment etc. Il faut introduire la notion de Vecteur quelque part ; quitte ensuite à deleguer au vecteur pour la class Move par exemple
public class Segment extends Line2D {

    private float x1;

    private float y1;

    private float x2;

    private float y2;

    public Segment() {}

    public Segment(float x1, float y1, float x2, float y2) {
        setLine(x1, y1, x2, y2);
    }

    public Segment(double x1, double y1, double x2, double y2) {
        setLine(x1, y1, x2, y2);
    }

    public Segment(Locator p1, Locator p2) {
        setLine(p1, p2);
    }

    @Override
    public double getX1() {
        return x1;
    }

    @Override
    public double getY1() {
        return y1;
    }

    @Override
    public Locator getP1() {
        return new Locator(x1, y1);
    }

    @Override
    public double getX2() {
        return x2;
    }

    @Override
    public double getY2() {
        return y2;
    }

    @Override
    public Locator getP2() {
        return new Locator(x2, y2);
    }

    @Override
    public void setLine(double x1, double y1, double x2, double y2) {
        this.x1 = (float) x1;
        this.y1 = (float) y1;
        this.x2 = (float) x2;
        this.y2 = (float) y2;
    }

    public boolean liesOn(Locator l) {
        if (!MathUtils.isBetween(l.getX(), getX1(), getX2())) return false;

        return MathUtils.isBetween(l.getY(), getY1(), getY2());
    }

    @Override
    public Rectangle2D getBounds2D() {
        float x;
        float y;
        float w;
        float h;

        if (x1 < x2) {
            x = x1;
            w = x2 - x1;
        } else {
            x = x2;
            w = x1 - x2;
        }

        if (y1 < y2) {
            y = y1;
            h = y2 - y1;
        } else {
            y = y2;
            h = y1 - y2;
        }

        return new Rectangle2D.Float(x, y, w, h);
    }

    public Locator[] toLocators() {
        return new Locator[]{
                getP1(),
                getP2()
        };
    }

    @Override
    public String toString() {
        return "Segment{" +
                "p1=" + getP1() +
                ", p2=" + getP2() +
                '}';
    }
}
