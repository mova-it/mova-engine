package mova.game.geom;

import java.awt.geom.Point2D;

public class Locator extends Point2D {

    // TODO dans un tableau encore mieux non?
    private float x;

    private float y;

    public Locator() {}

    public Locator(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Locator(double x, double y) {
        this((float) x, (float) y);
    }

    public Locator(Point2D p) {
        this(p.getX(), p.getY());
    }

    @Override
    public double getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void addX(float dx) {
        this.x += dx;
    }

    @Override
    public double getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void addY(float dy) {
        this.y += dy;
    }

    @Override
    public void setLocation(double x, double y) {
        this.x = (float) x;
        this.y = (float) y;
    }

    public void setLocation(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void move(float dx, float dy) {
        setLocation(x + dx, y + dy);
    }

    public void move(Vector move) {
        move.apply(this);
    }

    public Vector to(Locator l) {
        return new Vector(this, l);
    }

    public Vector from(Locator l) {
        return new Vector(l, this);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format("[x=%s, y=%s]", x, y);
    }
}
