package mova.game.geom;

import mova.trigo.Angle;

// TODO pour info, les opérations sur un Vecteur 2D ou 3D obtiennent le même résultat avec 0 pour z. Et la normal puor le vecteur 2D est égale au produit en croix avec le vecteur 0, 0, -1
//  Il y a donc peut être moyen d'avoir une seule classe qui gère les 2 cas ou tout du moins il y a du code qui devrait pouvoir être partagé
public class Vector {

    private static final Locator O = new Locator();

    private float x;
    private float y;

    public Vector() {}

    public Vector(double x, double y) {
        this((float) x, (float) y);
    }

    public Vector(Vector v) {
        this(v.x, v.y);
    }

    public Vector(float x, float y) {
        setTo(x, y);
    }

    public Vector(Locator from, Locator to) {
        this(to.getX() - from.getX(), to.getY() - from.getY());
    }

    public Vector(Locator to) {
        this(to.getX(), to.getY());
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setTo(Vector v) {
        setTo(v.x, v.y);
    }

    public void setTo(double x, double y) {
        setTo((float) x, (float) y);
    }

    public void setTo(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void add(Vector v) {
        add(v.x, v.y);
    }

    public void add(double x, double y) {
        add((float) x, (float) y);
    }

    public void add(float x, float y) {
        this.x += x;
        this.y += y;
    }

    public void addX(double x) {
        addX((float) x);
    }

    public void addX(float x) {
        this.x += x;
    }

    public void addY(double y) {
        addY((float) y);
    }

    public void addY(float y) {
        this.y += y;
    }

    public void sub(Vector v) {
        sub(v.x, v.y);
    }

    public void sub(double x, double y) {
        sub((float) x, (float) y);
    }

    public void sub(float x, float y) {
        add(-x, -y);
    }

    public void subX(double x) {
        subX((float) x);
    }

    public void subX(float x) {
        addX(-x);
    }

    public void subY(double y) {
        subY((float) y);
    }

    public void subY(float y) {
        addY(-y);
    }

    public void mul(double s) {
        mul((float) s);
    }

    public void mul(float s) {
        mul(s, s);
    }

    public void mul(double sx, double sy) {
        mul((float) sx, (float) sy);
    }

    public void mul(float sx, float sy) {
        this.x *= sx;
        this.y *= sy;
    }

    public void mulX(double s) {
        mulX((float) s);
    }

    public void mulX(float s) {
        this.x *= s;
    }

    public void mulY(double s) {
        mulY((float) s);
    }

    public void mulY(float s) {
        this.y *= s;
    }

    public void div(double d) {
        div((float) d);
    }

    public void div(float d) {
        div(d, d);
    }

    public void div(double dx, double dy) {
        div((float) dx, (float) dy);
    }

    public void div(float dx, float dy) {
        this.x /= dx;
        this.y /= dy;
    }

    public void divX(double d) {
        divX((float) d);
    }

    public void divX(float d) {
        this.x /= d;
    }

    public void divY(double d) {
        divY((float) d);
    }

    public void divY(float d) {
        this.y /= d;
    }

    public void rotate(Angle angle) {
        rotate(angle.cos(), angle.sin());
    }

    public void subRotate(Angle angle) {
        rotate(angle.cos(), -angle.sin());
    }

    public void rotate(double cosAngle, double sinAngle) {
        rotate((float) cosAngle, (float) sinAngle);
    }

    public void subRotate(double cosAngle, double sinAngle) {
        rotate(cosAngle, -sinAngle);
    }

    public void subRotate(float cosAngle, float sinAngle) {
        rotate(cosAngle, -sinAngle);
    }

    public void rotate(float cosAngle, float sinAngle) {
        float newX = x*cosAngle - y*sinAngle;
        float newY = x*sinAngle + y*cosAngle;

        x = newX;
        y = newY;
    }

    public void addFrom0To(Locator l) {
        add(l.getX(), l.getY());
    }

    public void addTo0From(Locator l) {
        add(-l.getX(), -l.getY());
    }

    public float length() {
        return (float) Math.hypot(x, y);
    }

    public void normalize() {
        div(length());
    }

    /**
     * Permet d'analyser l'angle entre 2 vecteurs. Notamment:
     * <p>
     * - si U.V < 0 alors teta > 90°
     * - si U.V = 0 alors teta = 90°
     * - si U.V > 0 alors teta < 90°
     */
    public float dotProduct(Vector v) {
        return x*v.x + y*v.y;
    }

    public Vector normal() {
        //noinspection SuspiciousNameCombination
        return new Vector(-y, x);
    }

    public Vector opposit() {
        return new Vector(-x, -y);
    }

    public void apply(Locator l) {
        l.move(getX(), getY());
    }

    public boolean isNull() {
        return getX() == 0 && getY() == 0;
    }

    public Locator locatorFromO() {
        return locatorFrom(O);
    }

    public Locator locatorFrom(Locator l) {
        Locator locator = new Locator(l);
        locator.move(this);

        return locator;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + "]";
    }

    public static FromVectorBuilder from(Locator from) {
        return new FromVectorBuilder(from);
    }

    public static ToVectorBuilder to(Locator to) {
        return new ToVectorBuilder(to);
    }

    abstract static class MoveBuilder {

        protected final Locator first;

        protected MoveBuilder(Locator first) {
            this.first = first;
        }

        protected Vector build(Locator from, Locator to) {
            return new Vector(from, to);
        }
    }

    public static class FromVectorBuilder extends MoveBuilder {

        protected FromVectorBuilder(Locator from) {
            super(from);
        }

        public Vector to(Locator to) {
            return build(first, to);
        }
    }

    public static class ToVectorBuilder extends MoveBuilder {

        protected ToVectorBuilder(Locator to) {
            super(to);
        }

        public Vector from(Locator from) {
            return build(from, first);
        }
    }

}
