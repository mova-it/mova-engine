package mova.game.sound;

public interface SoundFilter {

    default void reset() {

    }

    default int getRemainingSize() {
        return 0;
    }

    default void filter(byte[] samples) {
        filter(samples, 0, samples.length);
    }

    void filter(byte[] samples, int offset, int length);

    static short getSample(byte[] buffer, int position) {
        return (short) ((buffer[position + 1] & 0xff) << 8 | buffer[position] & 0xff);
    }

    static void setSample(byte[] buffer, int position, short sample) {
        buffer[position] = (byte) (sample & 0xff);
        buffer[position + 1] = (byte) ((sample >> 8) & 0xff);
    }
}
