package mova.game.sound;

public class Sound {

    private final byte[] samples;

    public Sound(byte[] samples) {
        this.samples = samples;
    }

    public byte[] getSamples() {
        return samples;
    }
}
