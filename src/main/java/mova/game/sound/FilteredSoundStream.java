package mova.game.sound;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class FilteredSoundStream extends FilterInputStream {

    private static final int REMAINING_SIZE_UNKNOWN = -1;

    private int remainingSize;
    private final SoundFilter soundFilter;

    public FilteredSoundStream(InputStream inputStream, SoundFilter soundFilter) {
        super(inputStream);

        this.remainingSize = REMAINING_SIZE_UNKNOWN;
        this.soundFilter = soundFilter;
    }

    @Override
    public int read(byte[] samples, int offset, int length) throws IOException {
        int bytesRead = super.read(samples, offset, length);
        if (bytesRead > 0) {
            soundFilter.filter(samples, offset, length);
            return bytesRead;
        } else {

            if (remainingSize == REMAINING_SIZE_UNKNOWN) {
                remainingSize = soundFilter.getRemainingSize();
                remainingSize = remainingSize / 4 * 4;
            }

            if (remainingSize > 0) {
                length = Math.min(length, remainingSize);

                Arrays.fill(samples, (byte) 0);

                soundFilter.filter(samples, offset, length);
                remainingSize -= length;

                return length;
            } else {
                return -1;
            }
        }
    }
}
