package mova.game.sound;

import com.sun.javafx.application.PlatformImpl;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.util.HashMap;
import java.util.Map;

public class SoundSystem {

    private static final Map<String, MediaPlayer> players = new HashMap<>();

    private static boolean soundSystemEnabled = false;
    private static MediaProvider mediaProvider = Media::new;

    public static void setMediaProvider(MediaProvider mediaProvider) {
        SoundSystem.mediaProvider = mediaProvider;
    }

    public static Media play(String s) {
        if (!soundSystemEnabled) return null;

        Media media = mediaProvider.provide(s);
        return play0(media);
    }

    public static Media play(Media media) {
        if (!soundSystemEnabled) return null;

        return play0(media);
    }

    private static Media play0(Media media) {
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        MediaPlayer oldPlayer = players.put(media.getSource(), mediaPlayer);
        if (oldPlayer != null) {
            oldPlayer.stop();
            oldPlayer.dispose();
        }
        mediaPlayer.play();

        return media;
    }

    public static void stop(String s) {
        if (soundSystemEnabled) {
            Media media = mediaProvider.provide(s);
            stop0(media);
        }
    }

    public static void stop(Media media) {
        if (soundSystemEnabled) stop0(media);
    }

    private static void stop0(Media media) {
        MediaPlayer player = players.get(media.getSource());
        if (player != null) player.stop();
    }

    public static void start() {
        if (!soundSystemEnabled) {
            PlatformImpl.startup(() -> {});

            soundSystemEnabled = true;
        }
    }

    public static void shutdown() {
        if (soundSystemEnabled) {
            soundSystemEnabled = false;

            PlatformImpl.exit();
        }
    }

    public interface MediaProvider {
        Media provide(String s);
    }
}
