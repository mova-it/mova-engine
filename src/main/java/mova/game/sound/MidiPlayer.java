package mova.game.sound;

import javax.sound.midi.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MidiPlayer implements MetaEventListener {

    public static final int END_OF_TRACK_MESSAGE = 47;

    private Sequencer sequencer;
    private boolean loop;
    private boolean paused;

    public MidiPlayer() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequencer.addMetaEventListener(this);
        } catch (MidiUnavailableException mue) {
            sequencer = null;
            mue.printStackTrace();
        }
    }

    public Sequence getSequence(String filename) {
        return getSequence(getClass().getResourceAsStream(filename));
    }

    public Sequence getSequence(InputStream inputStream) {
        if (inputStream == null) return null;

        try (InputStream bufferedStream = inputStream.markSupported() ? inputStream : new BufferedInputStream(inputStream)) {
            return MidiSystem.getSequence(bufferedStream);
        } catch (InvalidMidiDataException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void play(Sequence sequence, boolean loop) {
        if (sequencer != null && sequence != null && sequencer.isOpen()) {
            try {
                sequencer.setSequence(sequence);
                sequencer.start();
                this.loop = loop;
            } catch (InvalidMidiDataException ime) {
                ime.printStackTrace();
            }
        }
    }

    @Override
    public void meta(MetaMessage meta) {
        // TODO en vue d'optimiser le temps passer dans ce if, ne vaut-il pas mieux mettre le loop au début du test?
        if (meta.getType() == END_OF_TRACK_MESSAGE && sequencer != null && sequencer.isOpen() && loop) {
            sequencer.setTickPosition(0);
            sequencer.start();
        }
    }

    public void stop() {
        if (sequencer != null && sequencer.isOpen()) {
            sequencer.stop();
            sequencer.setMicrosecondPosition(0);
        }
    }

    public void close() {
        if (sequencer != null && sequencer.isOpen()) sequencer.close();
    }

    public Sequencer getSequencer() {
        return sequencer;
    }

    public void setPaused(boolean paused) {
        if (this.paused != paused && sequencer != null && sequencer.isOpen()) {
            this.paused = paused;

            if (paused) sequencer.stop();
            else sequencer.start();
        }
    }

    public boolean isPaused() {
        return paused;
    }
}
