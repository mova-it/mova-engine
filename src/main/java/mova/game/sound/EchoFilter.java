package mova.game.sound;

import java.nio.ShortBuffer;

public class EchoFilter implements SoundFilter {

    private static final float FINAL_DECAY = .01f;
    private static final double FINAL_DECAY_LOG = Math.log(FINAL_DECAY);

    private final float decay;
    private final ShortBuffer shortBuffer;

    public EchoFilter(int numDelaySamples, float decay) {
        shortBuffer = ShortBuffer.allocate(numDelaySamples);
        this.decay = decay;
    }

    @Override
    public int getRemainingSize() {
        int numRemainingBuffers = (int) Math.ceil(FINAL_DECAY_LOG/Math.log(decay));
        int bufferSize = shortBuffer.capacity()*2;

        return bufferSize*numRemainingBuffers;
    }

    @Override
    public void reset() {
        shortBuffer.clear();
    }

    @Override
    public void filter(byte[] samples, int offset, int length) {
        for (int i = offset; i < offset + length; i+=2) {
            short oldSample = SoundFilter.getSample(samples, i);

            short newSample = (short) (oldSample + decay*shortBuffer.get(shortBuffer.position()));

            SoundFilter.setSample(samples, i, newSample);

            shortBuffer.put(newSample);
            if (!shortBuffer.hasRemaining()) shortBuffer.position(0);
        }
    }
}
