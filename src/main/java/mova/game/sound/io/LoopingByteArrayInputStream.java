package mova.game.sound.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class LoopingByteArrayInputStream extends ByteArrayInputStream {

    private boolean closed = false;

    public LoopingByteArrayInputStream(byte[] buf) {
        super(buf);
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) {
        if (closed) return -1;

        int totalBytesRead = 0;
        while (totalBytesRead < len) {
            int numBytesRead = super.read(b, off + totalBytesRead, len - totalBytesRead);

            if (numBytesRead > 0) totalBytesRead += numBytesRead;
            else reset();
        }

        return totalBytesRead;
    }

    @Override
    public void close() throws IOException {
        super.close();
        closed = true;
    }
}
