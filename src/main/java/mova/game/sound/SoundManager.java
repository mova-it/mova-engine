package mova.game.sound;

import mova.game.core.FatalError;
import mova.game.sound.io.LoopingByteArrayInputStream;
import mova.lang.SteppedThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SoundManager {

    private final Logger logger = LoggerFactory.getLogger(SoundManager.class);
    private final ExecutorService executorService;
    private final AudioFormat playbackFormat;
    private final ThreadLocal<SourceDataLine> localLine;
    private final ThreadLocal<byte[]> localBuffer;
    private final Object pausedLock;
    private boolean paused;

    public SoundManager(AudioFormat playbackFormat) {
        this(playbackFormat, getMaxSimultaneousSounds(playbackFormat));
    }

    public SoundManager(AudioFormat playbackFormat, int maxSimultaneousSounds) {
        executorService = Executors.newFixedThreadPool(maxSimultaneousSounds, SoundPlayerThread::new);

        this.playbackFormat = playbackFormat;
        localLine = new ThreadLocal<>();
        localBuffer = new ThreadLocal<>();
        pausedLock = new Object();

        warmup(maxSimultaneousSounds);
    }

    private void warmup(int maxSimultaneousSounds) {
        try {
            executorService.invokeAll(Stream.generate(() -> (Callable<Object>) () -> null).limit(maxSimultaneousSounds).collect(Collectors.toList()));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new FatalError("Interruption du warmup du " + SoundManager.class.getName());
        }
    }

    protected void cleanUp() {
        setPaused(false);

        Mixer mixer = AudioSystem.getMixer(null);
        if (mixer.isOpen()) mixer.close();

        localLine.remove();
        localBuffer.remove();
    }

    public void close() {
        cleanUp();
        executorService.shutdownNow();
        try {
            boolean terminated = executorService.awaitTermination(10, TimeUnit.SECONDS);
            if (!terminated) logger.warn("Le {} n'a pas pu se terminer dans le temps imparti", SoundManager.class);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new FatalError("Interruption lors de la fermeture du " + SoundManager.class);
        }
    }

    public void setPaused(boolean paused) {
        if (this.paused != paused) {
            synchronized (pausedLock) {
                this.paused = paused;
                if (!paused) pausedLock.notifyAll();
            }
        }
    }

    public boolean isPaused() {
        return paused;
    }

    public Sound getSound(String filename) {
        return getSound(getAudioInputStream(filename));
    }

    public Sound getSound(AudioInputStream audioInputStream) {
        if (audioInputStream == null) return null;

        int length = (int) (audioInputStream.getFrameLength() * audioInputStream.getFormat().getFrameSize());
        byte[] samples = new byte[length];
        DataInputStream dataInputStream = new DataInputStream(audioInputStream);
        try {
            dataInputStream.readFully(samples);
        } catch (IOException ioe) {
            throw new FatalError("Impossible de lire le son", ioe);
        }

        return new Sound(samples);
    }

    public AudioInputStream getAudioInputStream(String filename) {
        try {
            URL url = getClass().getResource(filename);
            AudioInputStream source = AudioSystem.getAudioInputStream(url);

            return AudioSystem.getAudioInputStream(playbackFormat, source);
        } catch (UnsupportedAudioFileException | IOException | IllegalArgumentException e) {
            throw new FatalError("Impossible de récupérer le son désigné par " + filename, e);
        }
    }

    public InputStream play(Sound sound) {
        return play(sound, null, false);
    }

    public InputStream play(Sound sound, SoundFilter filter, boolean loop) {
        if (sound != null) {
            InputStream inputStream = loop
                    ? new LoopingByteArrayInputStream(sound.getSamples())
                    : new ByteArrayInputStream(sound.getSamples());

            return play(inputStream, filter);
        }

        return null;
    }

    public InputStream play(InputStream inputStream) {
        return play(inputStream, null);
    }

    public InputStream play(InputStream inputStream, SoundFilter filter) {
        if (inputStream != null) {
            if (filter != null) inputStream = new FilteredSoundStream(inputStream, filter);
            executorService.execute(new SoundPlayer(inputStream));
        }

        return inputStream;
    }

    private class SoundPlayerThread extends SteppedThread {

        public SoundPlayerThread(Runnable target) {
            super(target);

            setDaemon(true);
        }

        @Override
        public void init() {
            int bufferSize = playbackFormat.getFrameSize() * Math.round(playbackFormat.getSampleRate() / 10);

            SourceDataLine line;
            try {
                line = AudioSystem.getSourceDataLine(playbackFormat);
                line.open(playbackFormat, bufferSize);
            } catch (LineUnavailableException lue) {
                Thread.currentThread().interrupt();
                throw new FatalError("Impossible de récupérer la ligne lié à " + Thread.currentThread(), lue);
            }

            line.start();

            // TODO Je sais pas pourquoi pour que le premier son passe, il faut le warmup + le write?!!
            line.write(new byte[]{1, 1}, 0, 2);

            byte[] buffer = new byte[bufferSize];
            localLine.set(line);
            localBuffer.set(buffer);
        }

        @Override
        public void release() {
            SourceDataLine line = localLine.get();
            if (line != null) {
                line.drain();
                line.close();
            }
        }
    }

    protected class SoundPlayer implements Runnable {

        private final InputStream source;

        public SoundPlayer(InputStream source) {
            this.source = source;
        }

        @Override
        public void run() {
            SourceDataLine line = localLine.get();
            byte[] buffer = localBuffer.get();
            if (line == null || buffer == null) return;

            try {
                int numBytesRead = 0;
                while (numBytesRead != -1) {
                    synchronized (pausedLock) {
                        if (paused) pausedLock.wait();
                    }

                    numBytesRead = source.read(buffer, 0, buffer.length);
                    if (numBytesRead != -1) {
                        line.write(buffer, 0, numBytesRead);
                    }
                }
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
                logger.warn("{} a été interrompu", SoundPlayer.class);
            } catch (IOException ioe) {
                logger.error("{} a été interrompu", SoundPlayer.class, ioe);
            }
        }
    }

    public static int getMaxSimultaneousSounds(AudioFormat playbackFormat) {
        DataLine.Info lineInfo = new DataLine.Info(SourceDataLine.class, playbackFormat);
        Mixer mixer = AudioSystem.getMixer(null);
        int maxLines = mixer.getMaxLines(lineInfo);
        return maxLines == AudioSystem.NOT_SPECIFIED ? 32 : maxLines;
    }
}
