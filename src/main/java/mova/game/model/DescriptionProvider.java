package mova.game.model;

import mova.game.core.ecs.AbstractGameComponent;
import mova.game.core.ecs.GameObject;
import mova.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public class DescriptionProvider extends AbstractGameComponent {

    private final Map<String, Function<GameObject, String>> descriptionCollectors = new LinkedHashMap<>();

    public boolean createCollector(String key, Function<GameObject, String> collector) {
        if (key == null) throw new IllegalArgumentException("Un collecteur de description ne peut être lié à une clé null");
        if (collector == null) throw new IllegalArgumentException("Un collecteur de description ne peut être null");

        Function<GameObject, String> previousCollector = descriptionCollectors.put(key, collector);
        if (previousCollector != null) {
            logger.warn("Un collecteur de description existait déjà avec la clé \"{}\"", key);
            return true;
        }

        return false;
    }

    public Map<String, String> getDescriptions() {
        GameObject gameObject = getGameObject();

        Map<String, String> descriptions = new LinkedHashMap<>();
        for (Map.Entry<String, Function<GameObject, String>> entry : descriptionCollectors.entrySet()) {
            descriptions.put(entry.getKey(), entry.getValue().apply(gameObject));
        }

        return descriptions;
    }

    public String getDescription(String key) {
        Function<GameObject, String> descriptionCollector = descriptionCollectors.get(key);
        if (descriptionCollector == null) {
            logger.warn("Pas de collecteur de description pour la clé \"{}\": {}", key, descriptionCollectors.keySet());
            return StringUtils.EMPTY;
        }

        return descriptionCollector.apply(getGameObject());
    }

    public Set<String> getCollectorKeys() {
        return descriptionCollectors.keySet();
    }

}
