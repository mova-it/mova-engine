package mova.game.model;

import mova.game.core.Copyable;
import mova.game.core.ecs.AbstractGameComponent;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ModelContainer extends AbstractGameComponent implements Copyable<ModelContainer> {

    private final Map<Class<?>, Object> models = new HashMap<>();

    public ModelContainer(Object firstModel, Object... models) {
        this(Stream.concat(Stream.of(firstModel), Arrays.stream(models)).collect(Collectors.toList()));
    }

    public ModelContainer(Collection<?> models) {
        if (models == null || models.isEmpty()) throw new IllegalArgumentException("Un container de modèles ne peut être vide");

        for (Object model : models) {
            this.models.put(model.getClass(), model);
        }
    }

    private ModelContainer(Map<Class<?>, Object> models) {
        this.models.putAll(models);
    }

    public <T> T getModel(Class<T> type) {
        Object model = models.get(type);
        return model != null ? type.cast(model) : null;
    }

    public boolean hasModel(Class<?> modelClass) {
        return models.containsKey(modelClass);
    }

    @Override
    public ModelContainer copy() {
        return new ModelContainer(models);
    }
}
