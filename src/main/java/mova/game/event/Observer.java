package mova.game.event;

public interface Observer {

    void onNotify(Event event);
}
