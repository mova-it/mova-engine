package mova.game.event;

public enum EventType {
    PSM_START, PSM_STOP, GAME_START, GAME_END
}
