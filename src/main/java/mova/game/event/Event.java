package mova.game.event;

public class Event {

    EventType type;
    Object detail;

    public Event(EventType type) {
        this.type = type;
    }

    public Event(EventType type, Object detail) {
        this.type = type;
        this.detail = detail;
    }

    public EventType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Event{" +
                "type=" + type +
                (detail != null ? ", " + "cause=" + detail : "") +
                '}';
    }
}
