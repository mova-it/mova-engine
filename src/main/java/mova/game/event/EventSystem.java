package mova.game.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class EventSystem {

    private static final Logger logger = LoggerFactory.getLogger(EventSystem.class);

    private EventSystem() {}

    private static final Set<Observer> OBSERVERS = new HashSet<>();

    public static void addObserver(Observer observer) {
        OBSERVERS.add(observer);
    }

    public static void addObservers(Collection<Observer> observers) {
        OBSERVERS.addAll(observers);
    }

    public static void notify(EventType type) {
        notify(new Event(type));
    }

    public static void notify(Event event) {
        for (Observer observer : OBSERVERS) {
            observer.onNotify(event);
            if (logger.isDebugEnabled()) {
                logger.debug(observer.getClass() + " is notified of " + event, (Throwable) (event.detail instanceof Throwable ? event.detail : null));
            }
        }
    }
}
