package mova.game.graphics;

import mova.game.core.Scene;
import mova.game.core.ecs.GameObject;
import mova.game.graphics.geom.ShapeUtils;
import mova.game.view.Projection;
import mova.game.view.View;

import java.awt.*;
import java.awt.geom.GeneralPath;

public abstract class ShapeRenderer extends Renderer {

    private final Shape shape;

    private boolean forceViewability = false;

    private final Transform lastTransform = new Transform(); // dirty flag
    private final Projection lastProjection = new Projection(); // dirty flag
    private final GeneralPath lastShape = new GeneralPath(); // dirty flag
    private Shape transformedShape;

    protected ShapeRenderer(Shape shape) {
        this.shape = shape;
        transformedShape = shape;
    }

    public Shape getShape() {
        return shape;
    }

    protected Shape transformAndProject() {
        Transform transform = getTransform();
        Projection projection = getScreenProjection();

        if (isDirty(projection, transform)) {
            Shape computedShape = getShape();

            if (isMutable()) {
                lastShape.reset();
                lastShape.append(computedShape, false);
            }

            if (transform != null) {
                computedShape = transform.apply(computedShape);
                lastTransform.setTo(transform);
            }

            if (projection != null) {
                computedShape = projection.transform(computedShape);
                lastProjection.setTo(projection);
            }

            transformedShape = computedShape;
        }

        return transformedShape;
    }

    private Transform getTransform() {
        GameObject gameObject = getGameObject();
        return gameObject != null ? gameObject.getComponent(Transform.class) : null;
    }

    private Projection getScreenProjection() {
        if (!isProjected()) return null;

        GameObject gameObject = getGameObject();
        return gameObject != null && gameObject.getScene() != null && gameObject.getScene().getCamera() != null ? gameObject.getScene().getCamera().getScreenProjection() : null;
    }

    private boolean isDirty(Projection projection, Transform transform) {
        return projection != null && !lastProjection.equals(projection)
                || transform != null && !lastTransform.equals(transform)
                || isMutable() && !ShapeUtils.equals(shape, lastShape);
    }

    @Override
    protected void render(MovaGraphics movaGraphics) {
        Shape projectedShape = transformAndProject();

        if (isViewable()) renderShape(movaGraphics, projectedShape);
    }

    protected abstract void renderShape(MovaGraphics movaGraphics, Shape shape);

    // TODO pour l'instant ca va mais je pense que je passe à coté de quelque chose
    public void forceViewability(boolean forceViewability) {
        this.forceViewability = forceViewability;
    }

    public boolean isViewable() {
        if (forceViewability) return true;

        GameObject parent = getGameObject();
        if (parent != null) {
            Scene scene = parent.getScene();
            if (scene != null) {
                View view = scene.getView();
                return transformedShape.intersects(view.getBounds());
            }
        }

        return false;
    }
}
