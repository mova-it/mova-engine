package mova.game.graphics;

import mova.game.core.Copyable;
import mova.game.core.FatalError;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.geom.Vector;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.util.Objects;

// TODO la classe Projection et Transform se ressemble beaucoup: Il faut revoir Projection, Transform et Vector et Locator comme un tout
public class Transform extends AbstractGameComponent implements Copyable<Transform> {

    private final AffineTransform affineTransform = new AffineTransform();
    private int zIndex = 0;

    public Transform() {}

    public Transform(AffineTransform affineTransform, int zIndex) {
        this.affineTransform.setTransform(affineTransform);
        this.zIndex = zIndex;
    }

    public void reset() {
        affineTransform.setToIdentity();
    }

    public int getZIndex() {
        return zIndex;
    }

    public void setZIndex(int zIndex) {
        this.zIndex = zIndex;
    }

    public void setTo(Transform transform) {
        setTo(transform.affineTransform);
    }

    public void setTo(AffineTransform affineTransform) {
        this.affineTransform.setTransform(affineTransform);
    }

    public void setToLocation(Locator location) {
        setToTranslation(location.getX(), location.getY());
    }

    public void setToTranslation(double tx, double ty) {
        affineTransform.setToTranslation(tx, ty);
    }

    public void add(Transform transform) {
        add(transform.affineTransform);
    }

    public void add(AffineTransform affineTransform) {
        this.affineTransform.preConcatenate(affineTransform);
    }

    public void addTranslation(double tx, double ty) {
        affineTransform.translate(tx, ty);
    }

    public void addTranslation(Vector v) {
        addTranslation(v.getX(), v.getY());
    }

    public Shape apply(Shape shape) {
        return affineTransform.createTransformedShape(shape);
    }

    public Locator apply(Locator p) {
        return apply(p, new Locator());
    }

    public Locator apply(Locator pSrc, Locator pDst) {
        return (Locator) affineTransform.transform(pSrc, pDst);
    }

    public Segment apply(Segment segment) {
        return apply(segment, new Segment());
    }

    public Segment apply(Segment segmentSrc, Segment segmentDst) {
        Locator p1 = apply(segmentSrc.getP1());
        Locator p2 = apply(segmentSrc.getP2());

        segmentDst.setLine(p1, p2);

        return segmentDst;
    }

    public Locator inverseApply(Locator p) {
        return inverseApply(p, new Locator());
    }

    public Locator inverseApply(Locator pSrc, Locator pDst) {
        try {
            return (Locator) affineTransform.inverseTransform(pSrc, pDst);
        } catch (NoninvertibleTransformException nite) {
            throw new FatalError("Impossible d'inverser la transformation de ce point: " + pSrc, nite);
        }
    }

    public Locator getLocation() {
        return getLocation(new Locator());
    }

    public Locator getLocation(Locator pDst) {
        return apply(pDst);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Transform)) return false;

        Transform transform = (Transform) o;
        return affineTransform.equals(transform.affineTransform) && zIndex == transform.zIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(affineTransform, zIndex);
    }

    @Override
    public String toString() {
        return affineTransform + "; zIndex = " + zIndex;
    }

    @Override
    public Transform copy() {
        Transform copy = new Transform();
        copy.add(affineTransform);
        return copy;
    }
}
