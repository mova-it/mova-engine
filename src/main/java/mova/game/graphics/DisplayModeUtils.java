package mova.game.graphics;

import java.awt.*;

public class DisplayModeUtils {

    private DisplayModeUtils() {}

    public static String toString(DisplayMode displayMode) {
        return displayMode.getWidth() + "x" + displayMode.getHeight() + ", " + displayMode.getBitDepth() + " bits color, " + displayMode.getRefreshRate() + "Hz";
    }

    public static boolean match(DisplayMode displayMode1, DisplayMode displayMode2) {
        if (displayMode1.getHeight() != displayMode2.getHeight()) return false;
        if (displayMode1.getWidth() != displayMode2.getWidth()) return false;
        if (displayMode1.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI
                && displayMode2.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI
                && displayMode1.getBitDepth() != displayMode2.getBitDepth()) {
            return false;
        }
        return displayMode1.getRefreshRate() == DisplayMode.REFRESH_RATE_UNKNOWN
                || displayMode2.getRefreshRate() == DisplayMode.REFRESH_RATE_UNKNOWN
                || displayMode1.getRefreshRate() == displayMode2.getRefreshRate();
    }
}
