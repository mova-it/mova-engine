package mova.game.graphics;

import javax.swing.*;

public class NullRepaintManager extends RepaintManager {

    public static void install() {
        NullRepaintManager nullRepaintManager = new NullRepaintManager();
        nullRepaintManager.setDoubleBufferingEnabled(false);

        RepaintManager.setCurrentManager(nullRepaintManager);
    }

    @Override
    public synchronized void addInvalidComponent(JComponent invalidComponent) {
        // Do nothing
    }

    @Override
    public void addDirtyRegion(JComponent c, int x, int y, int w, int h) {
        // Do nothing
    }

    @Override
    public void markCompletelyDirty(JComponent aComponent) {
        // Do nothing
    }

    @Override
    public void paintDirtyRegions() {
        // Do nothing
    }
}
