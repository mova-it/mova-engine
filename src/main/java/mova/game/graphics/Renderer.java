package mova.game.graphics;

import mova.game.core.Copyable;
import mova.game.core.ecs.AbstractGameComponent;

public abstract class Renderer extends AbstractGameComponent implements Copyable<Renderer> {

    private boolean visible = true;
    private boolean mutable = false;
    private boolean projected = true;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isMutable() {
        return mutable;
    }

    public void setMutable(boolean mutable) {
        this.mutable = mutable;
    }

    public boolean isProjected() {
        return projected;
    }

    public void setProjected(boolean projected) {
        this.projected = projected;
    }

    protected abstract void render(MovaGraphics movaGraphics);

    @Override
    public final void draw(MovaGraphics movaGraphics) {
        if (isVisible()) render(movaGraphics);
    }
}
