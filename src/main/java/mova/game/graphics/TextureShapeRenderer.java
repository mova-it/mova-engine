package mova.game.graphics;

import mova.game.core.Scene;
import mova.game.core.ecs.GameObject;
import mova.game.graphics.component.GameWindow;
import mova.game.view.Camera;
import mova.game.view.Projection;
import mova.game.view.View;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class TextureShapeRenderer extends ShapeRenderer {

    private final BufferedImage texture;
    private final int textureSize;
    private final GameWindow gameWindow;

    private final Projection lastProjection = new Projection(); // dirty flag
    private Image scaledTexture = null;

    public TextureShapeRenderer(Shape shape, BufferedImage texture, GameWindow gameWindow) {
        this(shape, texture, gameWindow, true);
    }

    public TextureShapeRenderer(Shape shape, BufferedImage texture, GameWindow gameWindow, boolean visible) {
        super(shape);

        this.texture = texture;
        if (texture.getWidth() != texture.getHeight()) logger.warn("Width and height of texture is different that is not supported yet");
        this.textureSize = texture.getWidth();

        this.gameWindow = gameWindow;
        setVisible(visible);
    }

    @Override
    protected void renderShape(MovaGraphics movaGraphics, Shape shape) {
        Image computedScaleTexture = getScaleImage();

        Rectangle2D shapeBounds = shape.getBounds2D();

        GameObject gameObject = getGameObject();
        Scene scene = gameObject != null ? gameObject.getScene() : null;
        View view = scene != null ? scene.getView() : null;

        Rectangle2D viewBounds = view != null ? view.getBounds() : shapeBounds;

        double scaledTextureSize = lastProjection.scaleDistance(textureSize);
        double startX = getStart(shapeBounds.getMinX(), viewBounds.getMinX(), scaledTextureSize);
        double startY = getStart(shapeBounds.getMinY(), viewBounds.getMinY(), scaledTextureSize);
        double maxX = Math.min(shapeBounds.getMaxX(), viewBounds.getMaxX());
        double maxY = Math.min(shapeBounds.getMaxY(), viewBounds.getMaxY());

        movaGraphics.setClip(shape);

        for (double y = startY; y <= maxY; y += scaledTextureSize) {
            for (double x = startX; x <= maxX; x += scaledTextureSize) {
                movaGraphics.drawImage(computedScaleTexture, x, y);
            }
        }

        movaGraphics.resetClip();
    }

    private Image getScaleImage() {
        GameObject gameObject = getGameObject();
        if (gameObject == null) return texture;

        Scene scene = gameObject.getScene();
        if (scene == null) return texture;

        Camera camera = scene.getCamera();
        if (camera == null) return texture;

        Projection sceneProjection = camera.getScreenProjection();
        if (sceneProjection == null) return texture;

        if (!lastProjection.equals(sceneProjection)) {
            lastProjection.setTo(sceneProjection);

            // TODO ici la texture est dessiné sur un pixel de plus pour palier le problème de floating point précision (des lignes blanches intercallaire)
            scaledTexture = ImageUtils.scale(gameWindow, texture, Math.round(lastProjection.scaleDistance(textureSize)) + 1);
        }

        return scaledTexture;
    }

    private double getStart(double shapeMin, double viewMin, double scaledTextureSize) {
        return Math.max(shapeMin, shapeMin + Math.floor((viewMin - shapeMin)/scaledTextureSize)*scaledTextureSize);
    }

    @Override
    public ShapeRenderer copy() {
        return new TextureShapeRenderer(getShape(), texture, gameWindow);
    }
}
