package mova.game.graphics;

import mova.game.graphics.component.GameWindow;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.Graphics2D;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class ImageUtils {

    private static final String RESOURCE_NOT_FOUND_MSG = "La resource n'a pas été trouvée: ";

    private ImageUtils() {}

    public static ImageIcon loadImageIcon(String filename) {
        return loadImageIcon(ImageUtils.class, filename);
    }

    public static ImageIcon loadImageIcon(Class<?> clazz, String filename) {
        URL url = clazz.getResource(filename);

        if (url == null) {
            throw new IllegalArgumentException(RESOURCE_NOT_FOUND_MSG + filename);
        }

        return new ImageIcon(url);
    }

    public static BufferedImage loadImage(String filename) throws IOException {
        URL url = ClassLoader.getSystemResource(filename);

        if (url == null) {
            throw new IllegalArgumentException(RESOURCE_NOT_FOUND_MSG + filename);
        }

        return loadImage(url);
    }

    public static BufferedImage loadImage(Class<?> clazz, String filename) throws IOException {
        URL url = clazz.getResource(filename);

        if (url == null) {
            throw new IllegalArgumentException(RESOURCE_NOT_FOUND_MSG + filename);
        }

        return loadImage(url);
    }

    public static BufferedImage loadImage(URL url) throws IOException {
        return ImageIO.read(url);
    }

    public static BufferedImage scale(GameWindow gameWindow, BufferedImage img, int scaledSize) {
        return scale(gameWindow, img, scaledSize, scaledSize, Transparency.OPAQUE);
    }

    public static BufferedImage scale(GameWindow gameWindow, BufferedImage img, int scaledWidth, int scaledHeight) {
        return scale(gameWindow, img, scaledWidth, scaledHeight, Transparency.OPAQUE);
    }

    public static BufferedImage scale(GameWindow gameWindow, BufferedImage img, int scaledWidth, int scaledHeight, int transparency) {
        return scale(gameWindow.getGraphicsConfiguration(), img, scaledWidth, scaledHeight, transparency);
    }

    public static BufferedImage scale(GraphicsConfiguration graphicsConfiguration, BufferedImage img, int scaledWidth, int scaledHeight, int transparency) {
        BufferedImage dstImg = createCompatibleImage(graphicsConfiguration, scaledWidth, scaledHeight, transparency);

        Graphics2D graphics2D = dstImg.createGraphics();
        graphics2D.drawImage(img, 0, 0, dstImg.getWidth(), dstImg.getHeight(), null);
        graphics2D.dispose();
        return dstImg;
    }

    public static BufferedImage toBufferedImage(GameWindow gameWindow, Image img, int transparency) {
        return toBufferedImage(gameWindow.getGraphicsConfiguration(), img, transparency);
    }

    public static BufferedImage toBufferedImage(GraphicsConfiguration graphicsConfiguration, Image img, int transparency) {
        if (img instanceof BufferedImage) return (BufferedImage) img;

        BufferedImage dstImg = createCompatibleImage(graphicsConfiguration, img.getWidth(null), img.getHeight(null), transparency);
        return copyToBufferedImage(img, dstImg);
    }

    public static BufferedImage toBufferedImage(Image img, int type) {
        if (img instanceof BufferedImage) return (BufferedImage) img;

        // Create a buffered image with transparency
        BufferedImage dstImg = new BufferedImage(img.getWidth(null), img.getHeight(null), type);
        return copyToBufferedImage(img, dstImg);
    }

    public static <T extends Image> T copyToBufferedImage(Image srcImg, T dstImg) {
        Graphics graphics = dstImg.getGraphics();
        graphics.drawImage(srcImg, 0, 0, null);
        graphics.dispose();

        return dstImg;
    }

    public static BufferedImage createCompatibleImage(GameWindow gameWindow, int w, int h, int transparency) {
        return createCompatibleImage(gameWindow.getGraphicsConfiguration(), w, h, transparency);
    }

    public static BufferedImage createCompatibleImage(GraphicsConfiguration graphicsConfiguration, int w, int h, int transparency) {
        return graphicsConfiguration.createCompatibleImage(w, h, transparency);
    }

    public static BufferedImage createTranslucent(GameWindow gameWindow, Image image, float alpha) {
        return createTranslucent(gameWindow.getGraphicsConfiguration(), image, alpha);
    }

    public static BufferedImage createTranslucent(GraphicsConfiguration graphicsConfiguration, Image image, float alpha) {
        BufferedImage translucentImage = createCompatibleImage(graphicsConfiguration, image.getWidth(null), image.getHeight(null), Transparency.TRANSLUCENT);
        MovaGraphics g = new MovaGraphics((Graphics2D) translucentImage.getGraphics());
        Composite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
        g.setComposite(alphaComposite);
        g.drawImage(image);
        g.dispose();

        return translucentImage;
    }

    public static BufferedImage convertTo(BufferedImage image, int imageType) {
        if (image.getType() != imageType) {
            BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), imageType);
            Graphics2D g = newImage.createGraphics();
            g.drawImage(image, 0, 0, null);
            g.dispose();
            return newImage;
        }

        return image;
    }

    public static void fillWithTile(int[] tileBuffer, int tileSize, int firstTileX, int firstTileY, int[] dstBuffer, int w, int h) {
        int offsetX = tileSize + firstTileX;
        int offsetY = tileSize + firstTileY;


        for (int y = 0; y < h; y++) {
            int currentTileRow = ((y - offsetY + tileSize)*tileSize)%tileBuffer.length;
            int destPos = y*w;

            System.arraycopy(tileBuffer, currentTileRow - offsetX + tileSize, dstBuffer, destPos, offsetX);

            for (int x = offsetX; x < w; x += tileSize) {
                int length = Math.min(tileSize, w - x);
                System.arraycopy(tileBuffer, currentTileRow, dstBuffer, x + destPos, length);
            }
        }
    }
}
