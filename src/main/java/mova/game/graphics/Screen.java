package mova.game.graphics;

import mova.game.graphics.component.GameWindow;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.logging.Logger;

public class Screen {

    private static final String DISPLAY_MODE_WARNING = "Impossible de configurer le plein écran pour cette resolution: %s\n%s";

    public static final Comparator<DisplayMode> DISPLAY_MODE_MAXIMIZER = Comparator.comparingInt(DisplayMode::getHeight).thenComparing(DisplayMode::getWidth).thenComparing(DisplayMode::getBitDepth).thenComparing(DisplayMode::getRefreshRate);

    protected final Logger logger = Logger.getLogger(getClass().getName());

    private final GraphicsDevice graphicsDevice;

    public Screen() {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
    }

    public void setFullScreenWindow(GameWindow gameWindow) {
        DisplayMode displayMode = getHighestDisplayMode();
        setFullScreenWindow(gameWindow, displayMode);
    }

    public void setFullScreenWindow(GameWindow gameWindow, DisplayMode displayMode) {
        gameWindow.fullWindowOn(graphicsDevice);

        if (displayMode != null && graphicsDevice.isDisplayChangeSupported()) {
            try {
                graphicsDevice.setDisplayMode(displayMode);
            } catch (IllegalArgumentException iae) {
                logger.warning(String.format(DISPLAY_MODE_WARNING, DisplayModeUtils.toString(displayMode), ExceptionUtils.getStackTrace(iae)));
            }
        }
    }

    public void dispose() {
        Window window = graphicsDevice.getFullScreenWindow();
        if (window != null) window.dispose();
        graphicsDevice.setFullScreenWindow(null);
    }

    public DisplayMode[] getCompatibleDisplayModes() {
        return Arrays.stream(graphicsDevice.getDisplayModes()).distinct().toArray(DisplayMode[]::new);
    }

    public DisplayMode getHighestDisplayMode() {
        return Arrays.stream(graphicsDevice.getDisplayModes())
                .distinct()
                .max(DISPLAY_MODE_MAXIMIZER)
                .orElse(null);
    }

    public DisplayMode findFirstCompatibleDisplayMode(DisplayMode[] displayModes) {
        DisplayMode[] goodModes = getCompatibleDisplayModes();
        for (DisplayMode displayMode : displayModes) {
            for (DisplayMode goodMode : goodModes) {
                if (DisplayModeUtils.match(displayMode, goodMode)) {
                    return displayMode;
                }
            }
        }

        return null;
    }

    public DisplayMode getCurrentDisplayMode() {
        return graphicsDevice.getDisplayMode();
    }
}
