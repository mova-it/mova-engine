package mova.game.graphics;

import sun.awt.image.SurfaceManager;
import sun.java2d.SurfaceData;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

public final class UpdatableGif extends Image {

    private long frameTime = 0;
    private int currentFrame = 0;
    private float speed = 1;

    private final GifDecoder.GifImage gif;

    public UpdatableGif(GifDecoder.GifImage gif) {
        this.gif = gif;

        SurfaceManager.setManager(this, new SurfaceManager() {
            @Override
            public SurfaceData getPrimarySurfaceData() {
                return SurfaceData.getPrimarySurfaceData(getCurrentFrame());
            }

            @Override
            public SurfaceData restoreContents() {
                return SurfaceData.restoreContents(getCurrentFrame());
            }
        });
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void update(long dt) {
        frameTime += dt*speed;

        if (frameTime >= getCurrentDelay()) {
            nextFrame();
            frameTime -= getCurrentDelay();
        }
    }

    public int getWidth() {
        return getWidth(null);
    }

    public int getHeight() {
        return getHeight(null);
    }

    @Override
    public int getWidth(ImageObserver observer) {
        return getCurrentFrame().getWidth(observer);
    }

    @Override
    public int getHeight(ImageObserver observer) {
        return getCurrentFrame().getHeight(observer);
    }

    @Override
    public ImageProducer getSource() {
        return getCurrentFrame().getSource();
    }

    @Override
    public Graphics getGraphics() {
        return getCurrentFrame().getGraphics();
    }

    @Override
    public Object getProperty(String name, ImageObserver observer) {
        return getCurrentFrame().getProperty(name, observer);
    }

    public int getFrameIndex() {
        return currentFrame;
    }

    public Image getCurrentFrame() {
        return gif.getFrame(currentFrame);
    }

    private long getCurrentDelay() {
        return gif.getDelay(currentFrame)*10L;
    }

    private void nextFrame() {
        currentFrame = (currentFrame + 1)%gif.getFrameCount();
    }
}
