package mova.game.graphics;

import mova.game.geom.Locator;

import java.awt.*;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.text.AttributedString;

public class MovaGraphics extends DelegateGraphics2D {

    public MovaGraphics(java.awt.Graphics2D graphics2D) {
        this(graphics2D, false);
    }

    public MovaGraphics(java.awt.Graphics2D graphics2D, boolean antialiasingEnable) {
        super(graphics2D);

        // Note: pour les hints, allez voir: https://docs.oracle.com/javase/tutorial/2d/advanced/quality.html
//        if (antialiasingEnable) {
//            setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//            setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        }
    }

    public void drawImage(Image image, Point2D coordinate) {
        drawImage(image, (int) Math.round(coordinate.getX()), (int) Math.round(coordinate.getY()), null);
    }

    public void drawImage(Image image, Dimension dimension) {
        drawImage(image, 0, 0, dimension.width, dimension.height, null);
    }

    public void drawImage(Image image, Point2D coordinate, Dimension dimension) {
        drawImage(image, (int) Math.round(coordinate.getX()), (int) Math.round(coordinate.getY()), dimension.width, dimension.height, null);
    }

    public void drawImage(Image image) {
        drawImage(image, 0, 0);
    }

    public void drawImage(Image image, int x, int y) {
        drawImage(image, x, y, null);
    }

    public void drawImage(Image image, long x, long y) {
        drawImage(image, (int) x, (int) y);
    }

    public void drawImage(Image image, float x, float y) {
        drawImage(image, Math.round(x), Math.round(y));
    }

    public void drawImage(Image image, double x, double y) {
        drawImage(image, Math.round(x), Math.round(y));
    }

    public void drawLine(Point2D from, Point2D to) {
        drawLine((int) Math.round(from.getX()), (int) Math.round(from.getY()), (int) Math.round(to.getX()), (int) Math.round(to.getY()));
    }

    public void drawPoint(Point2D p) {
        int x = (int) Math.round(p.getX());
        int y = (int) Math.round(p.getY());
        drawLine(x, y, x, y);
    }

    public void fillRect(Color color, int width, int height) {
        setColor(color);
        fillRect(0, 0, width, height);
    }

    public void fillRect(Color color, Dimension dimension) {
        setColor(color);
        fillRect(0, 0, dimension.width, dimension.height);
    }

    public void resetClip() {
        setClip(null);
    }

    public void drawStringWithinBounds(String text, Rectangle bounds) {
        // Note: je ne sais pas ce qui serait possible de réaliser grâce aux java.awt.font.TextAttribute
        AttributedString attributedString = new AttributedString(text);
        LineBreakMeasurer measurer = new LineBreakMeasurer(attributedString.getIterator(), getFontRenderContext());

        Locator pen = new Locator(bounds.getLocation());
        while (measurer.getPosition() < text.length()) {
            int limit = getLimitCharactersForWidth(measurer, text, bounds.width);

            TextLayout layout = measurer.nextLayout(bounds.width, limit, false);

            pen.addY(layout.getAscent());
            layout.draw(this, (float) pen.getX(), (float) pen.getY());
            pen.addY(layout.getDescent() + layout.getLeading());
        }
    }

    private int getLimitCharactersForWidth(LineBreakMeasurer measurer, String text, int width) {
        int next = measurer.nextOffset(width);
        if (next <= text.length()) {
            for (int i = measurer.getPosition(); i < next; ++i) {
                char c = text.charAt(i);
                if (c == '\n') return i + 1;
            }
        }

        return next;
    }
}
