package mova.game.graphics.geom;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class WindowUtils {

    private WindowUtils() {}

    public static AtomicReference<JFrame> paint(Consumer<Graphics2D> paintConsumer) {
        AtomicReference<JFrame> f = new AtomicReference<>();
        SwingUtilities.invokeLater(() -> {
            // TODO ajouter la possibilité de fermer la fenêtre en faisant echap?
            f.set(new JFrame());
            f.get().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.get().getContentPane().add(new UtilsPanel(paintConsumer));
            f.get().setSize(1100, 800);
            f.get().setLocationRelativeTo(null);
            f.get().setVisible(true);
        });

        return f;
    }

    private static class UtilsPanel extends JPanel {

        private final transient Consumer<Graphics2D> paintConsumer;

        private UtilsPanel(Consumer<Graphics2D> paintConsumer) {
            setOpaque(false);
            this.paintConsumer = paintConsumer;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            paintConsumer.accept((Graphics2D) g);
            g.dispose();
        }
    }
}
