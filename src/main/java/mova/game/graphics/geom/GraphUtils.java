package mova.game.graphics.geom;

import mova.game.geom.Locator;
import mova.game.geom.Vector;

public class GraphUtils {

    private GraphUtils() {}

    public static Locator getIntersection(Locator p1, Locator p2, Vector v, Locator p0) {
        double s1 = getSlope(p1, p2);
        double i1 = getIntercept(s1, p1);

        double s2 = getSlope(v);
        double i2 = getIntercept(s2, p0);

        if (Double.isInfinite(s2)) return new Locator(p0.getX(), solve(p0.getX(), s1, i1));
        if (Double.isInfinite(s1)) return new Locator(p1.getX(), solve(p1.getX(), s2, i2));

        return getIntersection(s1, i1, s2, i2);
    }

    public static Locator getIntersection(Locator p1, Locator p2, Locator p3, Locator p4) {
        double s1 = GraphUtils.getSlope(p1, p2);
        double i1 = GraphUtils.getIntercept(s1, p1);

        double s2 = GraphUtils.getSlope(p3, p4);
        double i2 = GraphUtils.getIntercept(s2, p3);

        if (Double.isInfinite(s2)) return new Locator(p3.getX(), GraphUtils.solve(p3.getX(), s1, i1));
        if (Double.isInfinite(s1)) return new Locator(p1.getX(), GraphUtils.solve(p1.getX(), s2, i2));

        return GraphUtils.getIntersection(s1, i1, s2, i2);
    }

    public static double getSlope(Locator p1, Locator p2) {
        return getSlope(p2.getX() - p1.getX(),p2.getY() - p1.getY());
    }

    public static double getSlope(Vector v) {
        return getSlope(v.getX(), v.getY());
    }

    public static double getSlope(double dx, double dy) {
        return dy/dx;
    }

    public static double getIntercept(double slope, Locator p) {
        return p.getY() - slope*p.getX();
    }

    public static double solve(double x, double slope, double intercept) {
        return slope*x + intercept;
    }

    public static Locator getIntersection(double slope1, double intercept1, double slope2, double intercept2) {
        if (slope1 == slope2) return null;

        if (slope1 == 0) return new Locator((intercept1 - intercept2)/slope2, intercept1);
        if (slope2 == 0) return new Locator((intercept2 - intercept1)/slope1, intercept2);

        double x = getSlope(slope2 - slope1, -(intercept2 - intercept1));
        double y = solve(x, slope1, intercept1);
        return new Locator(x, y);
    }

}
