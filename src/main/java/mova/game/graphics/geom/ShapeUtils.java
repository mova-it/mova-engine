package mova.game.graphics.geom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.geom.*;
import java.util.List;
import java.util.*;
import java.util.function.ObjIntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ShapeUtils {

    public static boolean equals(Shape shape1, Shape shape2) {
        if (shape1 == shape2) return true;

        if (shape1 == null || shape2 == null) return false;

        PathIterator pathIterator1 = shape1.getPathIterator(null);
        PathIterator pathIterator2 = shape2.getPathIterator(null);

        if (pathIterator1.getWindingRule() != pathIterator2.getWindingRule()) return false;

        float[] segment1 = new float[6];
        float[] segment2 = new float[6];
        while (!pathIterator1.isDone()) {
            if (pathIterator2.isDone()) return false;

            int type1 = pathIterator1.currentSegment(segment1);
            int type2 = pathIterator2.currentSegment(segment2);

            if (type1 != type2) return false;
            if (!Arrays.equals(segment1, segment2)) return false;

            pathIterator1.next();
            pathIterator2.next();
        }

        return pathIterator2.isDone();
    }

    public static String toString(Shape shape) {
        PathIterator pathIterator = shape.getPathIterator(null);

        PathSegment[] pathSegments = PathSegments.toArray(pathIterator);
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(pathSegments);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String toString(PathSegment pathSegment) {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(pathSegment);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    public static boolean intersect(Shape s1, Shape s2) {
        return intersect(new Area(s1), new Area(s2));
    }

    public static boolean intersect(Area a1, Area a2) {
        Area area = new Area(a1);
        area.intersect(a2);
        return !area.isEmpty();
    }

    public enum SegmentType {
        SEG_MOVETO('M', PathIterator.SEG_MOVETO),
        SEG_LINETO('L', PathIterator.SEG_LINETO),
        SEG_QUADTO('Q', PathIterator.SEG_QUADTO),
        SEG_CUBICTO('C', PathIterator.SEG_CUBICTO),
        SEG_CLOSE('X', PathIterator.SEG_CLOSE);

        public final char symbol;
        public final int type;

        SegmentType(char symbol, int type) {
            this.symbol = symbol;
            this.type = type;
        }

        static SegmentType valueOf(char symbol) {
            switch (symbol) {
                case 'M': return SEG_MOVETO;
                case 'L': return SEG_LINETO;
                case 'Q': return SEG_QUADTO;
                case 'C': return SEG_CUBICTO;
                case 'X': return SEG_CLOSE;
                default: throw new IllegalArgumentException("Segment symbol unknown : " + symbol);
            }
        }

        static SegmentType valueOf(int type) {
            return SegmentType.values()[type];
        }
    }

    // TODO cette méthode à beaucoup de limite lorsqu'il s'agit d'offset négatif. Continuer à chercher une lib qui gère ca correctement
    //  A défaut potentiellement utiliser la lib Clipper2 en trouvant un moyen de passer de Shape à Paths et vice et versa
    public static Shape offset(Shape shape, float offset) {
        if (offset < 0) {
            Rectangle2D bounds = shape.getBounds2D();
            double rx = bounds.getCenterX() - bounds.getMinX();
            double ry = bounds.getCenterY() - bounds.getMinY();
            double minR = Math.min(rx, ry);

            if (offset <= -minR) throw new IllegalArgumentException("Offset trop petit: offset = " + offset + "; r = " + minR);
        }

        // On trace la strokeShape centrer sur la ligne de la shape
        Stroke stroke = new BasicStroke((float) Math.abs(2.0 * offset), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 10f);
        Shape strokeShape = stroke.createStrokedShape(shape);
        Area strokeArea = new Area(strokeShape);

        // Si l'offset est positif on ajoute l'aire de la strokeShape à l'aire de la shape
        Area offsetShape = new Area(shape);
        if (offset > 0) offsetShape.add(strokeArea);
        // Sinon on la soustrait
        else offsetShape.subtract(strokeArea);

        // ShapeUtils.list fournit la liste des paths composant la shape. Il semblerait que le path englobant ce situe toujours en dernier
        // On renvoie le path englobant (fonctionne très bien sur l'ajout d'offset mais ne suffit pas pour gérer tous les cas d'offset négatif)
        return ((LinkedList<Path2D>) decompose(offsetShape)).getLast();
    }

    public static List<Path2D> decompose(Shape shape) {
        LinkedList<Path2D> paths = new LinkedList<>();
        consume(shape.getPathIterator(null), (segment, type) -> {
            switch (type) {
                case PathIterator.SEG_MOVETO: {
                    Path2D path = new Path2D.Float();
                    path.moveTo(segment[0], segment[1]);
                    paths.add(path);
                    break;
                }
                case PathIterator.SEG_LINETO: {
                    paths.getLast().lineTo(segment[0], segment[1]);
                    break;
                }
                case PathIterator.SEG_QUADTO: {
                    paths.getLast().quadTo(segment[0], segment[1], segment[2], segment[3]);
                    break;
                }
                case PathIterator.SEG_CUBICTO: {
                    paths.getLast().curveTo(segment[0], segment[1], segment[2], segment[3], segment[4], segment[5]);
                    break;
                }
                case PathIterator.SEG_CLOSE: {
                    paths.getLast().closePath();
                    break;
                }
                default: throw new IllegalStateException(type + " is unknown as PathIterator segment type");
            }
        });

        return paths;
    }

    public static void consume(PathIterator iterator, ObjIntConsumer<float[]> consumer) {
        while (!iterator.isDone()) {
            float[] segment = new float[6];
            int type = iterator.currentSegment(segment);

            consumer.accept(segment, type);

            iterator.next();
        }
    }

    public static Path2D construcPath(PathSegment[] pathSegments) {
        Path2D.Double path = new Path2D.Double();
        for (PathSegment pathSegment : pathSegments) appendSegment(path, pathSegment);

        return path;
    }

    private static void appendSegment(Path2D path, PathSegment pathSegment) {
        float[] coordinates = pathSegment.getCoordinates();
        switch (ShapeUtils.SegmentType.valueOf(pathSegment.getType())) {
            case SEG_MOVETO:
                path.moveTo(coordinates[0], coordinates[1]);
                break;
            case SEG_LINETO:
                path.lineTo(coordinates[0], coordinates[1]);
                break;
            case SEG_QUADTO:
                path.quadTo(coordinates[0], coordinates[1], coordinates[2], coordinates[3]);
                break;
            case SEG_CUBICTO:
                path.curveTo(coordinates[0], coordinates[1], coordinates[2], coordinates[3], coordinates[4], coordinates[5]);
                break;
            case SEG_CLOSE:
                path.closePath();
        }
    }

    public static String toJson(String commandsString) {
        String[] commands = commandsString.split("(?=[A-Z])");

        ObjectMapper mapper = new ObjectMapper();
        PathSegment[] pathSegments = Arrays.stream(commands).map(ShapeUtils::convert).toArray(PathSegment[]::new);

        Logger logger = LoggerFactory.getLogger(ShapeUtils.class);
        if (logger.isInfoEnabled()) {
            logger.info(Arrays.stream(pathSegments).map(pathSegment -> {
                String coordinates = pathSegment.getCoordinates() != null
                        ? IntStream.range(0, pathSegment.getCoordinates().length).mapToObj(i -> pathSegment.getCoordinates()[i]).map(Object::toString).collect(Collectors.joining(" "))
                        : "";
                return pathSegment.getType() + " " + coordinates;
            }).collect(Collectors.joining(" ")));
        }

        return Arrays.stream(pathSegments).map(pathSegment -> {
            try {
                return mapper.writeValueAsString(pathSegment);
            } catch (JsonProcessingException e) {
                throw new IllegalStateException(e);
            }
        }).collect(Collectors.joining(",\n", "[\n", "\n]"));
    }

    public static PathSegment convert(String command) {
        String[] commandParts = command.split(" ");
        char type = commandParts[0].charAt(0);

        if (type == 'Z') return new PathSegment(type, null);

        float[] coordinates = new float[commandParts.length - 1];
        for (int i = 0; i < coordinates.length; i++) {
            coordinates[i] = Math.round(Float.parseFloat(commandParts[i + 1]));
        }

        return new PathSegment(type, coordinates);
    }

}
