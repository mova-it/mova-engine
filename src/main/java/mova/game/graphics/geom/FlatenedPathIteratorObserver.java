package mova.game.graphics.geom;

abstract class FlatenedPathIteratorObserver<T> extends PathIteratorObserver<T> {
    @Override
    protected final void onQuadTo(PathSegment pathSegment) {
        throw new IllegalStateException("Flatness of the Shape should not allowed other Path elements than MOVE_TO, LINE_TO or CLOSE");
    }

    @Override
    protected final void onCubicTo(PathSegment pathSegment) {
        throw new IllegalStateException("Flatness of the Shape should not allowed other Path elements than MOVE_TO, LINE_TO or CLOSE");
    }
}
