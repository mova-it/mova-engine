package mova.game.graphics.geom;

import java.util.stream.Stream;

abstract class PathIteratorObserver<T> {

    protected final float[] pos = new float[2];
    protected final float[] lastMovePos = new float[2];

    protected void onMoveTo(PathSegment pathSegment) {
    }

    protected void onLineTo(PathSegment pathSegment) {
    }

    protected void onQuadTo(PathSegment pathSegment) {
    }

    protected void onCubicTo(PathSegment pathSegment) {
    }

    protected void onClose(PathSegment pathSegment) {
    }

    protected void afterAll(PathSegment pathSegment) {
    }

    protected abstract T getResult();

    public T observe(Stream<PathSegment> pathSegmentStream) {
        pathSegmentStream.forEach(pathSegment -> {
            switch (ShapeUtils.SegmentType.valueOf(pathSegment.getType())) {
                case SEG_MOVETO:
                    onMoveTo(pathSegment);
                    break;
                case SEG_LINETO:
                    onLineTo(pathSegment);
                    break;
                case SEG_QUADTO:
                    onQuadTo(pathSegment);
                    break;
                case SEG_CUBICTO:
                    onCubicTo(pathSegment);
                    break;
                case SEG_CLOSE:
                    onClose(pathSegment);
                    break;
            }

            afterAll(pathSegment);
        });

        return getResult();
    }
}
