package mova.game.graphics.geom;

import mova.game.geom.Segment;

import java.util.Arrays;

abstract class SegmentObserver<T> extends FlatenedPathIteratorObserver<T> {
    @Override
    protected final void onMoveTo(PathSegment pathSegment) {
        System.arraycopy(pathSegment.getCoordinates(), 0, lastMovePos, 0, 2);
        System.arraycopy(pathSegment.getCoordinates(), 0, pos, 0, 2);
    }

    @Override
    protected final void onLineTo(PathSegment pathSegment) {
        float[] coords = pathSegment.getCoordinates();
        computeSegment(new Segment(pos[0], pos[1], coords[0], coords[1]));
        System.arraycopy(pathSegment.getCoordinates(), 0, pos, 0, 2);
    }

    @Override
    protected final void onClose(PathSegment pathSegment) {
        if (!Arrays.equals(pos, lastMovePos)) {
            computeSegment(new Segment(pos[0], pos[1], lastMovePos[0], lastMovePos[1]));
            System.arraycopy(pathSegment.getCoordinates(), 0, pos, 0, 2);
        }
    }

    protected abstract void computeSegment(Segment segment);
}
