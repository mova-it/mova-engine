package mova.game.graphics.geom;

import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.geom.Vector;

import java.awt.*;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.*;
import java.util.stream.Stream;

public class GeometryUtils {

    private static final float DEFAULT_FLATNESS = 1f;

    private GeometryUtils() {}

    public static <T extends Locator> T closest(List<T> points, T from) {
        return points.stream().reduce((l1, l2) -> l1.distanceSq(from) > l2.distanceSq(from) ? l2 : l1).orElse(null);
    }

    public static Locator middle(Locator p1, Locator p2) {
        return new Locator((p1.getX() + p2.getX())/2, (p1.getY() + p2.getY())/2);
    }

    public static Locator[] getIntersections(Shape shape, Segment segment) {
        return getIntersections(shape, segment, DEFAULT_FLATNESS);
    }

    public static Locator[] getIntersections(Shape shape, Segment segment, float flatness) {
        return observe(shape, flatness, new IntersectionsObserver(segment));
    }

    public static Optional<Locator> getIntersection(Segment seg1, Segment seg2) {
        Locator locator = GraphUtils.getIntersection(seg1.getP1(), seg1.getP2(), seg2.getP1(), seg2.getP2());
        if (locator == null) return Optional.empty();

        return Optional.ofNullable(seg1.liesOn(locator) && seg2.liesOn(locator) ? locator : null);
    }

    /**
     * Retourne le rectangle englobant de la forme passée en paramètre.
     * <p/>
     * Au contraire de la méthode getBounds2D de Shape, celle-ci renvoie un rectangle plus précis,
     * ne prenant pas en compte les points de controle de la forme, mais bien la forme elle même.
     *
     * @param s la forme
     * @return le rectangle englobant de la forme
     */
    public static Rectangle2D getBounds2D(Shape s) {
        return getBounds2D(s, DEFAULT_FLATNESS);
    }

    public static Rectangle2D getBounds2D(Shape s, float flatness) {
        return observe(s, flatness, new BoundsMeasurerObserver());
    }

    public static Locator getClosestPointFrom(Shape s, Locator from) {
        return getClosestPointFrom(s, from, DEFAULT_FLATNESS);
    }

    public static Locator getClosestPointFrom(Shape s, Locator from, float flatness) {
        return observe(s, flatness, new ClosestPointObserver(from));
    }

    private static <T> T observe(Shape shape, float flatness, PathIteratorObserver<T> observer) {
        PathIterator pi = shape.getPathIterator(null, flatness);
        return observer.observe(PathSegments.stream(pi));
    }

    private static class IntersectionsObserver extends SegmentObserver<Locator[]> {
        private final Segment segment;
        private final Set<Locator> intersections = new HashSet<>();

        public IntersectionsObserver(Segment segment) {
            this.segment = segment;
        }

        @Override
        protected void computeSegment(Segment currentSegment) {
            getIntersection(segment, currentSegment).ifPresent(intersections::add);
        }

        @Override
        protected Locator[] getResult() {
            return intersections.toArray(new Locator[0]);
        }
    }

    private static class BoundsMeasurerObserver extends FlatenedPathIteratorObserver<Rectangle2D> {

        private float minX = Float.MAX_VALUE;
        private float minY = Float.MAX_VALUE;
        private float maxX = Float.MIN_VALUE;
        private float maxY = Float.MIN_VALUE;
        @Override
        protected void onMoveTo(PathSegment pathSegment) {
            onLineTo(pathSegment);
        }

        @Override
        protected void onLineTo(PathSegment pathSegment) {
            float[] coords = pathSegment.getCoordinates();
            minX = Math.min(minX, coords[0]);
            maxX = Math.max(maxX, coords[0]);
            minY = Math.min(minY, coords[1]);
            maxY = Math.max(maxY, coords[1]);
        }

        @Override
        protected Rectangle2D getResult() {
            return new Rectangle2D.Double(minX, minY, maxX-minX, maxY-minY);
        }
    }

    private static class ClosestPointObserver extends SegmentObserver<Locator> {

        private static class ClosestPointInfo {
            private final Locator locator;
            private final float distanceSqr;

            private ClosestPointInfo(Locator locator, float distanceSqr) {
                this.locator = locator;
                this.distanceSqr = distanceSqr;
            }
        }

        private final Stream.Builder<ClosestPointInfo> locatorBuilder = Stream.builder();
        private final Locator from;

        public ClosestPointObserver(Locator from) {
            this.from = from;
        }

        @Override
        protected void computeSegment(Segment currentSegment) {
            Vector directeur = new Vector(currentSegment.getP1(), currentSegment.getP2());
            Vector normal = directeur.normal();

            Locator l = GraphUtils.getIntersection(currentSegment.getP1(), currentSegment.getP2(), normal, from);

            if (currentSegment.liesOn(l)) {
                float distanceSqr = (float) from.distanceSq(l);
                locatorBuilder.accept(new ClosestPointInfo(l, distanceSqr));
            } else {
                float distanceSqr1 = (float) from.distanceSq(currentSegment.getP1());
                float distanceSqr2 = (float) from.distanceSq(currentSegment.getP2());
                if (distanceSqr1 < distanceSqr2) locatorBuilder.accept(new ClosestPointInfo(currentSegment.getP1(), distanceSqr1));
                else locatorBuilder.accept(new ClosestPointInfo(currentSegment.getP2(), distanceSqr2));
            }
        }

        @Override
        protected Locator getResult() {
            ClosestPointInfo closestPointInfo = locatorBuilder.build().min(Comparator.comparingDouble(value -> value.distanceSqr)).orElseThrow(IllegalStateException::new);
            return closestPointInfo.locator;
        }
    }

}
