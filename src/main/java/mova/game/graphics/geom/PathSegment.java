package mova.game.graphics.geom;

import java.util.Arrays;

public class PathSegment {
    private char type;
    private float[] coordinates;

    public PathSegment() {
    }

    public PathSegment(char type, float[] coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public float[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(float[] coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "PathSegment{" +
                "type=" + type +
                ", coordinates=" + Arrays.toString(coordinates) +
                '}';
    }
}
