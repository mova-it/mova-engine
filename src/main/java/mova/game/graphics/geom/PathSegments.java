package mova.game.graphics.geom;

import java.awt.*;
import java.awt.geom.PathIterator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PathSegments {

    private PathSegments() {}

    public static Iterator<PathSegment> iterator(PathIterator pathIterator) {
        return new Iterator<PathSegment>() {
            @Override
            public boolean hasNext() {
                return !pathIterator.isDone();
            }

            @Override
            public PathSegment next() {
                if (!hasNext()) throw new NoSuchElementException();

                float[] coords = new float[6];
                int type = pathIterator.currentSegment(coords);
                pathIterator.next();

                return new PathSegment(ShapeUtils.SegmentType.valueOf(type).symbol, coords);
            }
        };
    }

    public static Stream<PathSegment> stream(PathIterator pathIterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(PathSegments.iterator(pathIterator), Spliterator.NONNULL | Spliterator.IMMUTABLE), false);
    }

    public static PathSegment[] toArray(PathIterator pathIterator) {
        return stream(pathIterator).toArray(PathSegment[]::new);
    }
}
