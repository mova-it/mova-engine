package mova.game.graphics;

import mova.game.core.ecs.GameObject;
import mova.util.StringUtils;

import java.util.*;

public class RenderableGroup extends Renderer {


    // Note: la linkedHashMap c'est pas forcément la plus performante quand il s'agit d'itérer pour dessiner
    private final Map<String, Renderer> shapeRenderers = new LinkedHashMap<>();
    private boolean visible;

    public RenderableGroup() {
        this(true);
    }

    public RenderableGroup(boolean visible) {
        this.visible = visible;
    }

    public <T extends Renderer> T addShapeRenderer(String name, T renderer) {
        if (StringUtils.isBlank(name)) throw new IllegalArgumentException("Le nom d'un renderer dans un groupe ne peut être null ou vide: " + name);
        if (renderer == null) throw new IllegalArgumentException("Un renderer dans un groupe ne peut être null");

        renderer.setGameObject(getGameObject());

        shapeRenderers.put(name, renderer);

        return renderer;
    }

    public Renderer removeShapeRenderer(String name) {
        Renderer renderer = shapeRenderers.remove(name);
        if (renderer != null) renderer.setGameObject(null);
        return renderer;
    }

    public Renderer getShapeRenderer(String name) {
        return shapeRenderers.get(name);
    }

    public <T extends Renderer> T getShapeRenderer(String name, Class<T> renderableClass) {
        return renderableClass.cast(getShapeRenderer(name));
    }

    public Collection<Renderer> getShapeRenderers() {
        return Collections.unmodifiableCollection(shapeRenderers.values());
    }

    public Set<String> getShapeRendererNames() {
        return Collections.unmodifiableSet(shapeRenderers.keySet());
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    // TODO le problème ici c'est qu'on ne sait pas si ni comment on doit gérer le fait de setter les champs visible, projected à travers le groupe:
    //      ex: On peut avoir un groupe pour lequel projected est false composé d'éléments pour lesquels projected est true ou false
    //      doit on envisager la présence d'un élément parent dans les Renderer? laisser tel quel? rendre impossible de setter les éléments du groupe individuellement?
    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    protected void render(MovaGraphics movaGraphics) {
        shapeRenderers.values().forEach(shapeRenderer -> shapeRenderer.draw(movaGraphics));
    }

    @Override
    public void setGameObject(GameObject gameObject) {
        super.setGameObject(gameObject);

        shapeRenderers.values().forEach(shapeRenderer -> shapeRenderer.setGameObject(gameObject));
    }

    @Override
    public RenderableGroup copy() {
        RenderableGroup group = new RenderableGroup();
        group.setVisible(isVisible());

        for (Map.Entry<String, Renderer> entry : shapeRenderers.entrySet()) {
            group.addShapeRenderer(entry.getKey(), entry.getValue().copy());
        }

        return group;
    }
}
