package mova.game.graphics.component;

import mova.Settings;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

// TODO idée supplémentaire:
//  - ajouter une méthode clear UI qui va supprimer tous les boutons en cache (et voir s'il faut pas nettoyer de la même manière Scene, GameActionMapping, etc.)
public class UserInterface {

    private final Map<String, GameButton> gameButtons = new HashMap<>();

    public void add(GameButton gameButton) {
        gameButtons.put(gameButton.getName(), gameButton);
    }

    public GameButton get(String name) {
        return gameButtons.get(name);
    }

    public void clear() {
        gameButtons.clear();
    }

    public static GameButton createTextualButton(String label) {
        return createTextualButton(label, Settings.DIALOG_FONT);
    }

    public static GameButton createTextualButton(String label, Font font) {
        return createTextualButton(label, font != null ? font: Settings.DIALOG_FONT, null);
    }

    public static GameButton createTextualButton(String label, Font defaultFont, Font hoverFont) {
        GameButton button = new GameButton(label);

        button.setText(label);
        button.setFont(defaultFont);

        if (hoverFont != null) {
            onHoverFontBehavior(defaultFont, hoverFont, button);
        }

        return button;
    }

    private static void onHoverFontBehavior(Font defaultFont, Font hoverFont, GameButton button) {
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                GameButton gameButton = (GameButton) e.getComponent();
                gameButton.setFont(hoverFont);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                GameButton gameButton = (GameButton) e.getComponent();
                gameButton.setFont(defaultFont);
            }
        });
    }
}
