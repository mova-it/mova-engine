package mova.game.graphics.component.event;

import mova.game.graphics.component.GameButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// TODO c'est pas forcément très bien conçu parce que ca devrait être automatiquement relayé. Est-ce qu'il faut mettre le ActionListener au niveau du gameWindow ou du UserInterface pour qu'il soit
//  automatiquement inclu quand on ajoute un GameButton?
public interface GameActionListener extends ActionListener {

    @Override
    default void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof GameButton) {
            GameButton source = (GameButton) actionEvent.getSource();
            source.tap();
        }
    }
}
