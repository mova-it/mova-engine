package mova.game.graphics.component;

import javax.swing.*;
import javax.swing.border.AbstractBorder;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;

public class GameImage extends JLabel {

    private Path2D clip;
    private Path2D border;

    public GameImage(Image image) {
        super(new ImageIcon(image));

        // TODO il fraudrait gérer le clip et le border du coup parce que là c'est pas très généralisable
        clip = new Path2D.Float();
        clip.moveTo(0, 0);
        clip.lineTo(200, 0);
        clip.lineTo(200, 100);
        clip.lineTo(100, 200);
        clip.lineTo(0, 200);
        clip.closePath();

        border = new Path2D.Float();
        border.moveTo(200, 0);
        border.lineTo(200, 100);
        border.lineTo(100, 200);
        border.lineTo(0, 200);

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                ImageIcon icon = (ImageIcon) getIcon();
                BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
                Graphics graphics = image.getGraphics();
                graphics.drawImage(icon.getImage(), e.getComponent().getX(), e.getComponent().getY(), e.getComponent().getWidth(), e.getComponent().getHeight(), null);
                graphics.dispose();
                setIcon(new ImageIcon(image));
            }
        });

        setBorder(new AbstractBorder() {
            @Override
            public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
                Color oldColor = g.getColor();

                g.translate(x, y);


                g.setColor(new Color(0x6E2917));
                ((Graphics2D) g).setStroke(new BasicStroke(10));
                ((Graphics2D) g).draw(border);

                g.setColor(Color.BLACK);
                ((Graphics2D) g).setStroke(new BasicStroke(5));
                ((Graphics2D) g).draw(border);


                g.translate(-x, -y);
                g.setColor(oldColor);
            }

            @Override
            public Insets getBorderInsets(Component c) {
                return new Insets(5, 5, 5, 5);
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setClip(clip);

        super.paintComponent(g);
    }

    public void setImage(Image image) {
        setIcon(new ImageIcon(image));

        ComponentEvent e = new ComponentEvent(this, ComponentEvent.COMPONENT_RESIZED);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(e);
    }
}
