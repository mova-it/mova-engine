package mova.game.graphics.component;

import javax.swing.*;

public class IconSet {

    private final ImageIcon iconDefault;
    private final ImageIcon iconRollover;
    private final ImageIcon iconPressed;

    public IconSet(ImageIcon iconDefault, ImageIcon iconRollover, ImageIcon iconPressed) {
        this.iconDefault = iconDefault;
        this.iconRollover = iconRollover;
        this.iconPressed = iconPressed;
    }

    public ImageIcon getIconDefault() {
        return iconDefault;
    }

    public ImageIcon getIconRollover() {
        return iconRollover;
    }

    public ImageIcon getIconPressed() {
        return iconPressed;
    }
}
