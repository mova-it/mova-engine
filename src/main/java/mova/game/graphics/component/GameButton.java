package mova.game.graphics.component;

import mova.Settings;
import mova.game.input.GameAction;

import javax.swing.*;
import java.awt.*;

public class GameButton extends JButton {

    //TODO y'a certainement des chose à faire dans les construtor. Implémenter soit même un LookAndFeel Swing par exemple ou utiliser des méthode factory ou autres
    private final GameAction action;

    public GameButton(String name) {
        setName(name);

        setIgnoreRepaint(true);
        setFocusable(false);
        setBorder(null);
        setContentAreaFilled(false);

        setForeground(Color.WHITE);
        setFont(Settings.DIALOG_FONT);

        Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
        setCursor(cursor);

        action = new GameAction(name, GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
    }

    public GameAction getGameAction() {
        return action;
    }

    public boolean isPressed() {
        return action.isPressed();
    }

    public void tap() {
        action.tap();
    }

    public void setIconSet(IconSet iconSet, String tooltip) {
        setToolTipText(tooltip);
        setIcon(iconSet.getIconDefault());
        setRolloverIcon(iconSet.getIconRollover());
        setPressedIcon(iconSet.getIconPressed());
    }

}
