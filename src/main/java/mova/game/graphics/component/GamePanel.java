package mova.game.graphics.component;

import javax.swing.*;
import java.awt.*;

public class GamePanel extends JPanel {

    public GamePanel() {
        setOpaque(false);
    }

    public GamePanel(LayoutManager layoutManager) {
        super(layoutManager);

        setOpaque(false);
    }
}
