package mova.game.graphics.component;

import mova.Settings;
import mova.game.geom.Locator;
import mova.game.graphics.MovaGraphics;
import mova.game.input.GameInputsListener;
import mova.util.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.logging.Logger;

public class GameWindow {

    static final Cursor INVISIBLE_CURSOR;
    static {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        INVISIBLE_CURSOR = toolkit.createCustomCursor(toolkit.getImage(StringUtils.EMPTY), new Point(), "Invisible");
    }

    protected final Logger logger = Logger.getLogger(getClass().getName());
    private final JFrame window;
    private final UserInterface ui;

    public GameWindow() {
        window = new JFrame();
        window.setFont(Settings.DIALOG_FONT);
        window.setBackground(Color.WHITE);
        window.setForeground(Color.BLACK);
        window.setFocusTraversalKeysEnabled(false);
        window.setUndecorated(true);
        window.setResizable(false);

        Container container = window.getContentPane();
        container.setLayout(new BorderLayout());
        if (container instanceof JComponent) ((JComponent) container).setOpaque(false);

        ui = new UserInterface();
    }

    public int getWidth() {
        return window.getWidth();
    }

    public int getHeight() {
        return window.getHeight();
    }

    public Dimension getSize() {
        return window.getSize();
    }

    public void setSize(int w, int h) {
        if (window.getGraphicsConfiguration().getDevice().getFullScreenWindow() == this.window) {
            throw new IllegalStateException("On ne peut pas régler la taille d'une fenêtre en plein écran");
        }

        window.setSize(w, h);
    }

    public void setVisible(boolean visible) {
        window.setVisible(visible);
    }

    public void center() {
        window.setLocationRelativeTo(null);
    }

    public Point getCenterLocation() {
        return getCenterLocation(new Point());
    }

    public Point getCenterLocation(Point centerLocation) {
        Dimension dimension = getSize();
        centerLocation.x = dimension.width / 2;
        centerLocation.y = dimension.height / 2;
        SwingUtilities.convertPointToScreen(centerLocation, window);

        return centerLocation;
    }

    public Locator getMousePosition() {
        return new Locator(window.getMousePosition());
    }

    public void fullWindowOn(GraphicsDevice graphicsDevice) {
        graphicsDevice.setFullScreenWindow(window);
    }

    public MovaGraphics getGraphics() {
        return new MovaGraphics((Graphics2D) getGraphics0());
    }

    public MovaGraphics getGraphics(boolean antialiasingEnable) {
        return new MovaGraphics((Graphics2D) getGraphics0(), antialiasingEnable);
    }

    private Graphics getGraphics0() {
        BufferStrategy bufferStrategy = window.getBufferStrategy();
        return bufferStrategy != null
                ? bufferStrategy.getDrawGraphics()
                : window.getGraphics();
    }

    public void enableBufferStrategy() {
        window.setIgnoreRepaint(true);
        window.createBufferStrategy(2);
    }

    public boolean hasBufferStrategy() {
        return window.getBufferStrategy() != null;
    }

    public void updateBufferStrategy() {
        BufferStrategy bufferStrategy = window.getBufferStrategy();
        if (bufferStrategy != null && !bufferStrategy.contentsLost()) {
            bufferStrategy.show();
        }

        Toolkit.getDefaultToolkit().sync();
    }

    public GraphicsConfiguration getGraphicsConfiguration() {
        return window.getGraphicsConfiguration();
    }

    public void addContents(Component... components) {
        addContents(null, components);
    }

    public void addContents(Object constraints, Component... components) {
        for (Component component : components) {
            window.getContentPane().add(component, constraints);

            addComponentsToUi(component);
        }

        window.validate();
    }

    public void paintContent(Graphics2D graphics2D) {
        window.getContentPane().paintComponents(graphics2D);
    }

    public void clearContent() {
        // TODO Il faut donc tout supprimer de l'UI alors?
        window.getContentPane().removeAll();
        window.getContentPane().revalidate();
    }

    public void addListeners(GameInputsListener gameInputsListener) {
        window.addKeyListener(gameInputsListener);
        window.addMouseListener(gameInputsListener);
        window.addMouseMotionListener(gameInputsListener);
        window.addMouseWheelListener(gameInputsListener);
    }

    public void removeListeners(GameInputsListener gameInputsListener) {
        window.removeKeyListener(gameInputsListener);
        window.removeMouseListener(gameInputsListener);
        window.removeMouseMotionListener(gameInputsListener);
        window.removeMouseWheelListener(gameInputsListener);
    }

    public boolean isShowing() {
        return window.isShowing();
    }

    public void hideCursor() {
        window.setCursor(INVISIBLE_CURSOR);
    }

    public UserInterface getUserInterface() {
        return ui;
    }

    private void addComponentsToUi(Component... components) {
        for (Component component : components) {
            if (component instanceof GameButton) {
                ui.add((GameButton) component);
            } else if (component instanceof Container) {
                Container container = (Container) component;
                addComponentsToUi(container.getComponents());
            }
        }
    }

}
