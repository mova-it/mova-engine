package mova.game.graphics;

import java.awt.*;

public class SolidShape extends ShapeRenderer {

    private Color fillColor;
    private Stroke stroke;
    private Color drawColor;

    public SolidShape(Shape shape, Color fillColor) {
        this(shape, fillColor, null, null);
    }

    public SolidShape(Shape shape, Stroke stroke, Color drawColor) {
        this(shape, null, stroke, drawColor);
    }

    public SolidShape(Shape shape, Stroke stroke, Color drawColor, boolean visible) {
        this(shape, null, stroke, drawColor, visible);
    }

    public SolidShape(Shape shape, Color fillColor, Stroke stroke, Color drawColor) {
        this(shape, fillColor, stroke, drawColor, true);
    }

    public SolidShape(Shape shape, Color fillColor, Stroke stroke, Color drawColor, boolean visible) {
        super(shape);

        this.fillColor = fillColor;
        this.stroke = stroke;
        this.drawColor = drawColor;
        setVisible(visible);
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public Stroke getStroke() {
        return stroke;
    }

    public void setStroke(Stroke stroke) {
        this.stroke = stroke;
    }

    public Color getDrawColor() {
        return drawColor;
    }

    public void setDrawColor(Color drawColor) {
        this.drawColor = drawColor;
    }

    @Override
    protected void renderShape(MovaGraphics movaGraphics, Shape shape) {
        if (getFillColor() != null) {
            movaGraphics.setColor(getFillColor());
            movaGraphics.fill(shape);
        }

        if (getStroke() != null) {
            movaGraphics.setStroke(getStroke());
            movaGraphics.setColor(getDrawColor());
            movaGraphics.draw(shape);
        }
    }

    @Override
    public SolidShape copy() {
        return new SolidShape(getShape(), fillColor, stroke, drawColor, isVisible());
    }
}
