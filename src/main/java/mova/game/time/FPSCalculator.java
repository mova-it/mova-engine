package mova.game.time;

import java.util.EnumMap;
import java.util.concurrent.TimeUnit;

public class FPSCalculator {

    private static final EnumMap<TimeUnit, Long> COMPUTATION_TIMES = new EnumMap<>(TimeUnit.class);
    private static final EnumMap<TimeUnit, Long> FRAME_CONVERSION_FACTORS = new EnumMap<>(TimeUnit.class);
    static {
        for (TimeUnit unit : TimeUnit.values()) {
            COMPUTATION_TIMES.put(unit, unit.convert(500, TimeUnit.MILLISECONDS));
            FRAME_CONVERSION_FACTORS.put(unit, unit.convert(1, TimeUnit.SECONDS));
        }
    }

    private static int numFrames = 0;
    private static float frameRate = 0;

    private static long elapsedTime = 0;

    private FPSCalculator() {}

    public static void clear() {
        numFrames = 0;
        frameRate = 0;
        elapsedTime = 0;
    }

    public static float tic(long dt) {
        return tic(dt, TimeUnit.MILLISECONDS);
    }

    public static float tic(long dt, TimeUnit unit) {
        numFrames++;

        elapsedTime += dt;
        long computationTime = COMPUTATION_TIMES.get(unit);
        if (elapsedTime > computationTime) {
            frameRate = (float) numFrames*FRAME_CONVERSION_FACTORS.get(unit)/elapsedTime;
            elapsedTime -= computationTime;
            numFrames = 0;
        }

        return frameRate;
    }

}
