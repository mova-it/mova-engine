package mova.game.time;


import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Timer {

    private static final long NOT_STARTED = 0;

    private static final int DEFAULT_CAPACITY = 10;

    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    private static long[] timers = new long[DEFAULT_CAPACITY];
    static {
        Arrays.fill(timers, NOT_STARTED);
    }

    private Timer() {}

    public static long start() {
        return start(0);
    }

    public static long reset() {
        return reset(0);
    }

    public static synchronized long start(int timerIndex) {
        rangeCheck(timerIndex);

        timers[timerIndex] = System.nanoTime();
        return timers[timerIndex];
    }

    public static synchronized long start(int timerIndex, TimeUnit unit) {
        return unit.convert(start(timerIndex), TimeUnit.NANOSECONDS);
    }

    public static synchronized long reset(int timerIndex) {
        rangeCheck(timerIndex);

        if (timers[timerIndex] == NOT_STARTED) throw new TimerNotStartedException();

        timers[timerIndex] = System.nanoTime();
        return timers[timerIndex];
    }

    public static synchronized long reset(int timerIndex, TimeUnit unit) {
        return unit.convert(reset(timerIndex), TimeUnit.NANOSECONDS);
    }

    /**
     * @deprecated use {@link #getElapsedTime()} instead
     */
    @Deprecated
    public static long getElapsedNanos() {
        return getElapsedTime(0);
    }

    /**
     * @deprecated use {@link #getElapsedTime(TimeUnit unit)} instead with TimeUnit.MILLISECONDS
     */
    @Deprecated
    public static long getElapsedMillis() {
        return getElapsedMillis(0);
    }

    /**
     * @deprecated use {@link #getElapsedTime(int timeIndex)} instead with TimeUnit.MILLISECONDS
     */
    @Deprecated
    public static synchronized long getElapsedNanos(int timerIndex) {
        return getElapsedTime(timerIndex);
    }

    /**
     * @deprecated use {@link #getElapsedTime(int timeIndex, TimeUnit unit)} instead with TimeUnit.MILLISECONDS
     */
    @Deprecated
    public static synchronized long getElapsedMillis(int timerIndex) {
        return getElapsedTime(timerIndex, TimeUnit.MILLISECONDS);
    }

    public static synchronized long getElapsedTime() {
        return getElapsedTime(0);
    }

    public static synchronized long getElapsedTime(int timerIndex) {
        rangeCheck(timerIndex);

        if (timers[timerIndex] == NOT_STARTED) throw new TimerNotStartedException();

        long elapsedNanos = System.nanoTime() - timers[timerIndex];
        timers[timerIndex] += elapsedNanos;

        return elapsedNanos;
    }

    public static synchronized long getElapsedTime(int timerIndex, TimeUnit unit) {
        return unit.convert(getElapsedTime(timerIndex), TimeUnit.NANOSECONDS);
    }

    public static synchronized long getElapsedTime(TimeUnit unit) {
        return getElapsedTime(0, unit);
    }

    private static void rangeCheck(int index) {
        if (index - timers.length >= 0) grow(index + 1);
    }

    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0) throw new OutOfMemoryError();

        return (minCapacity > MAX_ARRAY_SIZE) ? Integer.MAX_VALUE : MAX_ARRAY_SIZE;
    }

    private static void grow(int minCapacity) {
        int oldCapacity = timers.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0) newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0) newCapacity = hugeCapacity(minCapacity);

        timers = Arrays.copyOf(timers, newCapacity);
    }

}
