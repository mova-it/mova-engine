package mova.debug;

import mova.Settings;
import mova.game.geom.Locator;
import mova.game.graphics.MovaGraphics;

import java.awt.*;

public class DebugUtils {

    private static final int DEBUG_LINE_X_OFFSET = 5;

    private static final int DEBUG_LINE_HEIGHT = 25;

    private static int currentY;

    private DebugUtils() {}

    public static void drawDebugLine(MovaGraphics movaGraphics, String label) {
        drawDebugLine(movaGraphics, label, Color.BLACK);
    }

    public static void drawDebugLine(MovaGraphics movaGraphics, String label, Color color) {
        movaGraphics.setColor(color);
        movaGraphics.setFont(Settings.DIALOG_FONT);

        currentY += DEBUG_LINE_HEIGHT;

        movaGraphics.drawString(label, DEBUG_LINE_X_OFFSET, currentY);
    }

    public static void drawDebugPoint(MovaGraphics movaGraphics, String label, Locator p) {
        movaGraphics.setColor(Color.WHITE);
        movaGraphics.setFont(Settings.DIALOG_FONT);

        movaGraphics.drawRect((int) (p.getX() - 1), (int) (p.getY() - 1), 3, 3);
        movaGraphics.drawString(label, (int) (p.getX() + 10), (int) (p.getY() + 10));
    }

    public static void resetDebugSession() {
        currentY = 0;
    }
}
