package mova.debug;

import mova.game.core.ecs.GameComponent;
import mova.game.core.ecs.GameObject;
import mova.game.graphics.RenderableGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameObjectUtils {

    private GameObjectUtils() {}

    public static void printState(GameObject gameObject) {
        Logger logger = LoggerFactory.getLogger(GameObjectUtils.class);

        StringBuilder builder = new StringBuilder();
        builder.append("\t- scene = ").append(gameObject.getScene()).append("\n");
        builder.append("\t- visible = ").append(gameObject.isVisible()).append("\n");
        builder.append("\t- active = ").append(gameObject.isActive()).append("\n");

        builder.append("\t- components:").append("\n");
        for (GameComponent component : gameObject.getComponents()) {
            builder.append("\t\t- ").append(component.getClass()).append("\n");
        }

        logger.info("{}: {}\n{}", gameObject.getClass(), gameObject.getName(), builder);
    }

    public static void controlOwner(GameObject gameObject) {
        Logger logger = LoggerFactory.getLogger(GameObjectUtils.class);

        StringBuilder builder = new StringBuilder();
        gameObject.getComponents().forEach(component -> builder.append("\t- ").append(component.getClass()).append(" is owned by ").append(component.getGameObject()).append("\n"));
        gameObject.getComponent(RenderableGroup.class).getShapeRenderers().forEach(shapeRenderer -> builder.append("\t- ").append(shapeRenderer.getClass()).append(" is owned by ").append(shapeRenderer.getGameObject()).append("\n"));

        logger.info("GameObject.ref: {}\n{}", gameObject, builder);
    }
}
