package mova.debug;

import java.util.*;
import java.util.function.BiFunction;
import java.util.logging.Level;
import java.util.logging.Logger;

// TODO penser à retirer les abérations comme dans Benchmark
public class BenchDataObserver {

    private static final Map<String, List<Long>> data = new LinkedHashMap<>();
    private static final Map<String, Deque<Long>> stacks = new HashMap<>();

    private BenchDataObserver() {}

    public static final Comparator<Map.Entry<String, List<Long>>> STATS_COMPARATOR = Comparator.comparingLong(entry -> entry.getValue().stream().mapToLong(Long::valueOf).sum());

    public static String tic() {
        return tic(getEntryName());
    }

    public static String tic(String entryName) {
        Deque<Long> startTimes = stacks.computeIfAbsent(entryName, s -> new ArrayDeque<>());
        startTimes.addLast(System.nanoTime());
        return entryName;
    }

    public static void tac(String entryName) {
        tac(entryName, -1);
    }

    public static void tac(String entryName, int option) {
        try {
            String dataEntryName = option >= 0 ? entryName + option : entryName;

            List<Long> times = data.computeIfAbsent(dataEntryName, s -> new ArrayList<>());
            times.add(System.nanoTime() - stacks.get(entryName).removeLast());
        } catch (Exception t) {
            Logger logger = Logger.getLogger(BenchDataObserver.class.getName());
            logger.severe(() -> "Une erreur s'est produite. Peut être manque-t-il un tac à votre tic?");
            logger.severe(() -> "tic = " + stacks.keySet());
            logger.severe(() -> "Cette entré ne correspond à aucun tic: " + entryName);
            throw t;
        }
    }

    private static final String BENCH_LOG_FORMAT = "%-65s: avg.time = %.2fµs; (count = %d; |-> %.2fµs)%n";

    public static void resume() {
        Logger logger = Logger.getLogger(BenchDataObserver.class.getName());

        if (data.isEmpty()) {
            logger.info("BenchDataObserver: Call to resume without data");
            return;
        }

        // TODO peut être qu'au lieu de trier on peut noter les chose dans l'ordre dans lequel elles sont entrées avec un TreeMap?
        //  enutilisant le STATS_COMPARATOR
        data.forEach((key, value) -> {
            LongSummaryStatistics stats = value.stream().mapToLong(Long::valueOf).summaryStatistics();
            logger.log(Level.INFO, () -> String.format(BENCH_LOG_FORMAT, key, stats.getAverage() / 1000f, stats.getCount(), (stats.getMax() - stats.getMin()) / 1000f));
        });

        if (stacks.values().stream().mapToLong(Collection::size).sum() > 0) {
            logger.warning(() -> "Il reste des éléments non comptés");
        }
    }

    private static String getEntryName() {
        StackTraceElement element = Thread.currentThread().getStackTrace()[3];
        return element.getClassName() + "." + element.getMethodName();
    }
}
